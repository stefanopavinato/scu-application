/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/LockId.h"

namespace ResourceSharing {
class ILock {
 public:
  virtual ~ILock() = default;

  [[nodiscard]] virtual Types::LockId GetId() const = 0;

  virtual void Acquire() = 0;

  virtual void Release() = 0;

  [[nodiscard]] virtual bool Update() = 0;
};
}  // namespace ResourceSharing
