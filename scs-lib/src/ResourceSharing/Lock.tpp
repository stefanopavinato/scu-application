/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Logger/ILogger.h"
#include "Messages/Addresses/AddressLock.h"
#include "Messages/Addresses/AddressClient.h"
#include "Messages/Addresses/AddressVisitor.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/Data/DataLock.h"
#include "Messages/Data/DataVisitor.h"
#include "Messages/MessageData.h"
#include "LockSite.h"

namespace ResourceSharing {

template <class T, class P> const char Lock<T, P>::kId[] = "id";
template <class T, class P> const char Lock<T, P>::kAcquireLock[] = "acquire";
template <class T, class P> const char Lock<T, P>::kTimeStamp[] = "timestamp";
template <class T, class P> const char Lock<T, P>::kSites[] = "sites";

template <class T, class P>
Lock<T, P>::Lock(
  Logger::ILogger *logger,
  const Types::LockId::Enum &id)
  : Distributor::IDistributorSubscriber(
    std::make_unique<Messages::Addresses::AddressLock>(id))
  , logger_(logger)
  , id_(id)
  , acquireLock_(0)
  , timeStamp_(TimeStamp<T, P>(0, 0)) {
}

template <class T, class P>
void Lock<T, P>::AddSite(const std::string & ip) {
  if (sites_.find(ip) == sites_.end()) {
    logger_->Info(std::string("Added site with ip: ").append(ip));
    sites_.insert(std::make_pair(ip, std::make_unique<LockSite>(ip)));
  }
}

template <class T, class P>
void Lock<T, P>::RemoveSite(const std::string & ip) {
  auto site = sites_.find(ip);
  if (site != sites_.end()) {
    logger_->Info(std::string("Removed site with ip: ").append(ip));
    sites_.erase(site);
  }
}

template <class T, class P>
Types::LockId Lock<T, P>::GetId() const {
  return id_;
}

template <class T, class P>
void Lock<T, P>::Acquire() {
  std::unique_lock lock(mutex_);
  // Lease timestamp if lock is newly acquired
  if (acquireLock_ == 0) {
    timeStamp_ = ResourceSharing::TimeStamp(
      this->GetClock()->GetTime(),
      this->GetPriority());
    acquireLock_++;
  }
}

template <class T, class P>
bool Lock<T, P>::Update() {
  // Enter the critical section only after receiving all reply messages
  bool retVal = true;
  // Sends a lock request to all sites
  for (auto & [key, site] : sites_) {
    switch (site->GetLocalLockStatus()) {
      case LockStatus::Enum::ACCEPT: {
//        logger_->Info(std::string("Acquire [time = ").append(std::to_string(timeStamp_.GetTime()))
//                      .append(", priority = ").append(std::to_string(timeStamp_.GetPriority())).append("]"));
        break;
      }
      case LockStatus::Enum::REQUEST: {
        if (timeout_ < std::chrono::steady_clock::now()) {
          site->SetLocalLockStatus(LockStatus::Enum::NONE);
        }
        retVal = false;
        break;
      }
      case LockStatus::Enum::NONE: {
        timeout_ = std::chrono::steady_clock::now() + std::chrono::milliseconds(500);
        auto message = Messages::MessageData();
        message.SetSender(GetAddress()->Clone());
        message.SetReceiver(site->GetAddress()->Clone());
        message.SetData(std::make_unique<Messages::Data::DataLock>(
          timeStamp_,
          LockStatus(LockStatus::Enum::REQUEST)));
        if (Send(&message)) {
          // Wait for accept of other site
          site->SetLocalLockStatus(LockStatus::Enum::REQUEST);
          retVal = false;
        } else {
          // Set lock status accept, since other site is not connected
          site->SetLocalLockStatus(LockStatus::Enum::ACCEPT);
        }
        break;
      }
    }
  }
  return retVal;
}

template <class T, class P>
void Lock<T, P>::Release() {
  std::unique_lock lock(mutex_);
//  logger_->Info(std::string("Release [time = ").append(std::to_string(timeStamp_.GetTime()))
//                .append(", priority = ").append(std::to_string(timeStamp_.GetPriority())).append("]"));
  // Clear acquire lock flag
  acquireLock_--;
  if (acquireLock_ == 0) {
    // Send a timestamped reply message to all sites requesting the lock
    for (auto&[key, site] : sites_) {
      // Reset local lock status
      site->SetLocalLockStatus(LockStatus::Enum::NONE);
      // Handle remote lock status
      if (site->GetRemoteLockStatus() == LockStatus::Enum::REQUEST) {
        auto message = Messages::MessageData();
        message.SetSender(GetAddress()->Clone());
        message.SetReceiver(site->GetAddress()->Clone());
        message.SetData(std::make_unique<Messages::Data::DataLock>(
          ResourceSharing::TimeStamp(
            this->GetClock()->GetTime(),
            this->GetPriority()),
          LockStatus(LockStatus::Enum::ACCEPT)));
        if (Send(&message)) {
          site->SetRemoteLockStatus(LockStatus::Enum::NONE);
        } else {
          site->SetRemoteLockStatus(LockStatus::Enum::NONE);
        }
      }
    }
  }
}

template <class T, class P>
bool Lock<T, P>::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

template <class T, class P>
bool Lock<T, P>::Visit(Messages::MessageData *message) {
  // Lock
  std::unique_lock lock(mutex_);
  // Update logical clock
  auto data = Messages::Data::DataVisitor<Messages::Data::DataLock>();
  if (!message->GetData()->Accept(&data)) {
    logger_->Error("Lock data invalid");
    return true;
  }

  this->GetClock()->SetTime(data.GetData()->GetTimeStamp().GetTime());

  switch (data.GetData()->GetLockStatus().GetValue()) {
    case LockStatus::Enum::ACCEPT: {
//      logger_->Info(std::string("Accept local lock [time = ")
//      .append(std::to_string(message->GetTimeStamp().GetTime())).append("]"));
      // Set accept status for local site
      auto address = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressClient>();
      if (!message->GetSender()->Accept(&address)) {
        logger_->Error("Client address invalid");
        break;
      }
      auto site = sites_.find(address.GetAddress()->GetIp());
      if (site == sites_.end()) {
        logger_->Info("Remote lock not found");
      }
      site->second->SetLocalLockStatus(LockStatus::Enum::ACCEPT);
      break;
    }
    case LockStatus::Enum::REQUEST: {
//      logger_->Info(std::string("Request remote lock [time = ")
//        .append(std::to_string(message->GetTimeStamp().GetTime())).append("]"));
      // Immediately send a timestamped reply message if
      //  * the receiving process is not currently interested in the critical section OR
      //  * the receiving process has a lower priority
      if (acquireLock_ == 0 || timeStamp_ > data.GetData()->GetTimeStamp()) {
        auto messageReply = Messages::MessageData();
        messageReply.SetSender(message->GetReceiver()->Clone());
        messageReply.SetReceiver(message->GetSender()->Clone());
        messageReply.SetData(std::make_unique<Messages::Data::DataLock>(
          ResourceSharing::TimeStamp(
            this->GetClock()->GetTime(),
            this->GetPriority()),
          LockStatus(LockStatus::Enum::ACCEPT)));
        if (!Send(&messageReply)) {
          logger_->Warning("Failed to send message");
        }
        break;
      } else {
        // Otherwise defer the reply message
        auto address = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressClient>();
        if (!message->GetSender()->Accept(&address)) {
          logger_->Error("Client address invalid");
          break;
        }
        auto site = sites_.find(address.GetAddress()->GetIp());
        if (site == sites_.end()) {
          logger_->Info("Site not found");
          break;
        }
        site->second->SetRemoteLockStatus(LockStatus::Enum::REQUEST);
      }
      break;
    }
    case LockStatus::Enum::NONE: {
      logger_->Info("None");
      break;
    }
  }
  return true;
}

template <class T, class P>
bool Lock<T, P>::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  (*val)[kId] = id_.ToString();
  (*val)[kAcquireLock] = static_cast<uint32_t>(acquireLock_);
  if (timeStamp_.Visit(data)) {
    (*val)[kTimeStamp] = *data->Pop();
  }
  uint32_t i = 0;
  for (const auto & site : sites_) {
    if (site.second->Visit(data)) {
      (*val)[kSites][i++] = *data->Pop();
    }
  }
  data->Push(std::move(val));
  return true;
}

}  // namespace ResourceSharing
