/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <shared_mutex>
#include <unordered_map>
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriber.h"
#include "TimeStamp.h"
#include "ILockHandlerSubscriber.h"
#include "ILock.h"

//  RicartAgrawala algorithm
//  https://en.wikipedia.org/wiki/Ricart%E2%80%93Agrawala_algorithm

namespace Logger {
class ILogger;
}  // namespace Logger
namespace ResourceSharing {
class LockSite;
template <class T, class P> class LockHandler;
template <class T, class P>
class Lock :
  public ILock,
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriber,
  public ILockHandlerSubscriber<T, P> {
 public:
  Lock(
    Logger::ILogger *logger,
    const Types::LockId::Enum &id);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageData * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] Types::LockId GetId() const override;

  void Acquire() override;

  void Release() override;

  [[nodiscard]] bool Update() override;

  void AddSite(const std::string &ip);

  void RemoveSite(const std::string &ip);

 private:
  Logger::ILogger *logger_;

  const Types::LockId id_;

  std::atomic_uint32_t acquireLock_;

  mutable std::shared_mutex mutex_;

  // Timestamp of the local lock request
  TimeStamp<T, P> timeStamp_;

  std::chrono::steady_clock::time_point timeout_;

  // Status of the other lock sites (lock granted)
  std::unordered_map<std::string, std::unique_ptr<LockSite>> sites_;

  static const char kId[];
  static const char kAcquireLock[];
  static const char kTimeStamp[];
  static const char kSites[];
};
}  // namespace ResourceSharing

#include "Lock.tpp"
