/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Addresses/AddressClient.h"
#include "Messages/Data/ResponseOverview.h"
#include "LockSite.h"

namespace ResourceSharing {


const char LockSite::kAddress[] = "address";
const char LockSite::kLocalLockStatus[] = "status_local";
const char LockSite::kRemoteLockStatus[] = "status_remote";

LockSite::LockSite(
  const std::string & ip)
  : address_(std::make_unique<Messages::Addresses::AddressClient>(
    ip,
    Communication::Types::ProtocolType(Communication::Types::ProtocolType::Enum::ARBITRATION)))
  , localLockStatus_(LockStatus::Enum::NONE)
  , remoteLockStatus_(LockStatus::Enum::NONE) {
}

bool LockSite::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  if (address_->Visit(data)) {
    (*val)[kAddress] = *data->Pop();
  }
  (*val)[kLocalLockStatus] = localLockStatus_.ToString();
  (*val)[kRemoteLockStatus] = remoteLockStatus_.ToString();
  data->Push(std::move(val));
  return false;
}

Messages::Addresses::AddressClient * LockSite::GetAddress() {
  return address_.get();
}

LockStatus::Enum LockSite::GetLocalLockStatus() const {
  return localLockStatus_.GetValue();
}

void LockSite::SetLocalLockStatus(const LockStatus::Enum & lockStatus) {
  localLockStatus_.SetValue(lockStatus);
}

LockStatus::Enum LockSite::GetRemoteLockStatus() const {
  return remoteLockStatus_.GetValue();
}

void LockSite::SetRemoteLockStatus(const LockStatus::Enum & lockStatus) {
  remoteLockStatus_.SetValue(lockStatus);
}

}  // namespace ResourceSharing
