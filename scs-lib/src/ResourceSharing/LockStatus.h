/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace ResourceSharing {
class LockStatus {
 public:
  enum class Enum {
    DISCONNECTED = 0,
    CONNECTED = 1,
    REQUEST = 2,
    ACCEPT = 3,
    NONE
  };

  LockStatus();

  explicit LockStatus(const Enum &value);

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum & value);

  [[nodiscard]] std::string ToString() const;

  static Enum ToEnum(const uint32_t &value);

 private:
  Enum value_;

  static const char kDisconnected[];
  static const char kConnected[];
  static const char kRequest[];
  static const char kAccept[];
  static const char kNone[];
};
}  // namespace ResourceSharing
