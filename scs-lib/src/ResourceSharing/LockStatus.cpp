/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LockStatus.h"

namespace ResourceSharing {

const char LockStatus::kDisconnected[] = "disconnected";
const char LockStatus::kConnected[] = "connected";
const char LockStatus::kRequest[] = "request";
const char LockStatus::kAccept[] = "accept";
const char LockStatus::kNone[] = "none";

LockStatus::LockStatus()
  : value_(Enum::NONE) {
}

LockStatus::LockStatus(const Enum &value)
  : value_(value) {
}

LockStatus::Enum LockStatus::GetValue() const {
  return value_;
}


void LockStatus::SetValue(const Enum & value) {
  value_ = value;
}

std::string LockStatus::ToString() const {
  switch (value_) {
    case LockStatus::Enum::DISCONNECTED: {
      return kDisconnected;
    }
    case LockStatus::Enum::CONNECTED: {
      return kConnected;
    }
    case LockStatus::Enum::REQUEST: {
      return kRequest;
    }
    case LockStatus::Enum::ACCEPT: {
      return kAccept;
    }
    case LockStatus::Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

LockStatus::Enum LockStatus::ToEnum(const uint32_t &value) {
  if (value == 0) {
    return LockStatus::Enum::DISCONNECTED;
  } else if (value == 1) {
    return LockStatus::Enum::CONNECTED;
  } else if (value == 2) {
    return LockStatus::Enum::REQUEST;
  } else if (value == 3) {
    return LockStatus::Enum::ACCEPT;
  } else {
    return LockStatus::Enum::NONE;
  }
}

}  // namespace ResourceSharing
