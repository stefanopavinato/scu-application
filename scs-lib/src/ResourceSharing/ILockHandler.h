/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>

namespace ResourceSharing {
template <class T> class LogicalClock;
template <class T, class P> class Lock;
template <class T, class P>
class ILockHandler {
 public:
  virtual ~ILockHandler() = default;

  [[nodiscard]] virtual bool AddLock(std::unique_ptr<Lock<T, P>> lockNew) = 0;

  [[nodiscard]] virtual LogicalClock<T> * GetClock() = 0;

  [[nodiscard]] virtual P GetPriority() const = 0;
};
}  // namespace ResourceSharing
