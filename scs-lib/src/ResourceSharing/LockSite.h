/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <memory>
#include "LockStatus.h"
#include "Messages/Data/DataVisitorBase.h"

namespace Messages::Addresses {
class AddressClient;
}  // namespace Messages::Addresses
namespace ResourceSharing {
class LockSite :
  public Messages::Data::DataVisitorBase {
 public:
  explicit LockSite(
    const std::string & ip);

  bool Visit(Messages::Data::ResponseOverview * data) override;

  Messages::Addresses::AddressClient * GetAddress();

  [[nodiscard]] LockStatus::Enum GetLocalLockStatus() const;

  void SetLocalLockStatus(const LockStatus::Enum & lockStatus);

  [[nodiscard]] LockStatus::Enum GetRemoteLockStatus() const;

  void SetRemoteLockStatus(const LockStatus::Enum & lockStatus);

 private:
  std::unique_ptr<Messages::Addresses::AddressClient> address_;

  LockStatus localLockStatus_;

  LockStatus remoteLockStatus_;

  static const char kAddress[];
  static const char kLocalLockStatus[];
  static const char kRemoteLockStatus[];
};

}  // namespace ResourceSharing
