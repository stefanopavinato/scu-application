/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace ResourceSharing {
template<class T> class LogicalClock;
template<class T, class P> class ILockHandler;
template <class T, class P> class LockHandler;
template <class T, class P>
class ILockHandlerSubscriber {
  friend class LockHandler<T, P>;
 public:
  ILockHandlerSubscriber();

  [[nodiscard]] LogicalClock<T> * GetClock();

  [[nodiscard]] P GetPriority() const;

 private:
  [[nodiscard]] bool SetLockHandler(ILockHandler<T, P> * lockHandler);

  ILockHandler<T, P> * lockHandler_;
};
}  // namespace ResourceSharing

#include "ILockHandlerSubscriber.tpp"
