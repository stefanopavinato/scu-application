/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <list>
#include <memory>
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriberManager.h"
#include "LogicalClock.h"
#include "ILockHandler.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Messages {
class MessageBase;
}  // namespace Messages
namespace ResourceSharing {
template <class T, class P>
class LockHandler :
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriberManager,
  public ILockHandler<T, P> {
 public:
  LockHandler(
    Logger::ILogger *logger,
    const P & priority);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageData * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] bool AddLock(std::unique_ptr<Lock<T, P>> lock) override;

  [[nodiscard]] LogicalClock<T> * GetClock() override;

  [[nodiscard]] P GetPriority() const override;

 private:
  Logger::ILogger *const logger_;

  LogicalClock<T> clock_;

  const P priority_;

  std::list<std::unique_ptr<Lock<T, P>>> locks_;

  static const char kName[];
  static const char kNameKey[];
  static const char kClockKey[];
  static const char kLocksKey[];
};
}  // namespace ResourceSharing

#include "LockHandler.tpp"
