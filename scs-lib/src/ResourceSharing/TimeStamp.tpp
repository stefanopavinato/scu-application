/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"

namespace ResourceSharing {

template <class T, class P> const char TimeStamp<T, P>::kTime[] = "time";

template <class T, class P> const char TimeStamp<T, P>::kPriority[] = "priority";

template <class T, class P>
TimeStamp<T, P>::TimeStamp(
  const T & time,
  const P & priority)
  : time_(time)
  , priority_(priority) {
}

template <class T, class P>
bool TimeStamp<T, P>::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<Json::Value>();
  switch (data->GetDetails().GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      (*val)[kPriority] = priority_;
    }
    default: {
      (*val)[kTime] = time_;
    }
  }
  data->Push(std::move(val));
  return true;
}

template <class T, class P>
T TimeStamp<T, P>::GetTime() const {
  return time_;
}

template <class T, class P>
P TimeStamp<T, P>::GetPriority() const {
  return priority_;
}

template <class T, class P>
TimeStamp<T, P>& TimeStamp<T, P>::operator=(const TimeStamp & other) {
  // Guard self assignment
  if (this == &other) {
    return *this;
  }
  // Set properties
  time_ = other.time_;
  priority_ = other.priority_;
}

template <class T, class P>
bool TimeStamp<T, P>::operator==(const TimeStamp<T, P>& other) const {
  return time_ == other.time_ &&
         priority_ == other.priority_;
}

template <class T, class P>
bool TimeStamp<T, P>::operator!=(const TimeStamp<T, P> &other) const {
  return time_ != other.time_ ||
         priority_ != other.priority_;
}

template <class T, class P>
bool TimeStamp<T, P>::operator<(const TimeStamp<T, P>& other) const {
  if (time_ < other.time_) {
    return true;
  }
  if (other.time_ < time_) {
    return false;
  }
  return priority_ < other.priority_;
}

template <class T, class P>
bool TimeStamp<T, P>::operator>(const TimeStamp<T, P> &other) const {
  if (time_ > other.time_) {
    return true;
  }
  if (other.time_ > time_) {
    return false;
  }
  return priority_ > other.priority_;
}

}  // namespace ResourceSharing
