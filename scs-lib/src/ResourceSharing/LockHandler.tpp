/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/MessageData.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/ResponseOverview.h"
#include "Lock.h"

namespace ResourceSharing {

template <class T, class P> const char LockHandler<T, P>::kName[] = "LockHandler";

template <class T, class P> const char LockHandler<T, P>::kNameKey[] = "name";
template <class T, class P> const char LockHandler<T, P>::kClockKey[] = "clock";
template <class T, class P> const char LockHandler<T, P>::kLocksKey[] = "locks";

template <class T, class P>
LockHandler<T, P>::LockHandler(
  Logger::ILogger *logger,
  const P & priority)
  : Distributor::IDistributorSubscriberManager(
    std::make_unique<Messages::Addresses::AddressModule>(::Types::SoftwareModule::Enum::LOCK_HANDLER))
  , logger_(logger)
  , clock_(LogicalClock<T>())
  , priority_(priority) {
  logger_->Info("Created");
}

template <class T, class P>
LogicalClock<T> * LockHandler<T, P>::GetClock() {
  return &clock_;
}

template <class T, class P>
P LockHandler<T, P>::GetPriority() const {
  return priority_;
}

template <class T, class P>
bool LockHandler<T, P>::AddLock(std::unique_ptr<Lock<T, P>> lockNew) {
  if (!lockNew) {
    logger_->Error("Lock is nullptr");
    return false;
  }
  if (!lockNew->SetLockHandler(this)) {
    logger_->Error("Failed to set lock handler");
    return false;
  }
  for (auto &lock : locks_) {
    if (*lockNew->GetAddress() == *lock->GetAddress()) {
      logger_->Error("Lock already added");
      return false;
    }
  }
  if (!AddSubscriber(lockNew.get())) {
    logger_->Error("Failed to add lock to distributor");
    return false;
  }
  locks_.push_back(std::move(lockNew));
  return true;
}

template <class T, class P>
bool LockHandler<T, P>::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

template <class T, class P>
bool LockHandler<T, P>::Visit(Messages::MessageData * message) {
  // Update message direction
  message->SwapAddresses();
  // Create response
  auto response = Messages::Data::ResponseOverview::Create(message->GetData());
  if (!response) {
    logger_->Error("Failed to create response");
    return false;
  }
  // Process response
  if (!response.value()->Accept(this)) {
    logger_->Error("Failed to process response");
    return false;
  }
  // Send message
  message->SetData(std::move(response.value()));
  return Send(message);
}

template <class T, class P>
bool LockHandler<T, P>::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (data->GetDetails().GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      if (clock_.Visit(data)) {
        (*jsonValue)[kClockKey] =  *data->Pop();
      }
    }
    default: {
      (*jsonValue)[kNameKey] = kName;
      if (data->GetLevel() > 0) {
        uint32_t i = 0;
        for (const auto & lock : locks_) {
          if (lock->Visit(data)) {
            (*jsonValue)[kLocksKey][i++] = *data->Pop();
          }
        }
      }
    }
    data->Push(std::move(jsonValue));
  }
  return true;
}


}  // namespace ResourceSharing
