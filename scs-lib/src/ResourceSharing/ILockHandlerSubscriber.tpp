/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ILockHandler.h"

namespace ResourceSharing {

template <class T, class P>
ILockHandlerSubscriber<T, P>::ILockHandlerSubscriber()
  : lockHandler_(nullptr) {
}

template <class T, class P>
LogicalClock<T> * ILockHandlerSubscriber<T, P>::GetClock() {
  return lockHandler_->GetClock();
}

template <class T, class P>
P ILockHandlerSubscriber<T, P>::GetPriority() const {
  return lockHandler_->GetPriority();
}

template <class T, class P>
bool ILockHandlerSubscriber<T, P>::SetLockHandler(ILockHandler<T, P> *lockHandler) {
  if (lockHandler == nullptr) {
    return false;
  }
  lockHandler_ = lockHandler;
  return true;
}

}  // namespace ResourceSharing
