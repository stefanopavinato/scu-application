/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace ResourceSharing {

template <class T> const char LogicalClock<T>::kTime[] = "time";

template <typename T>
LogicalClock<T>::LogicalClock()
  : time_(0) {
}

template <typename T>
bool LogicalClock<T>::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  (*val)[kTime] = time_;
  data->Push(std::move(val));
  return true;
}

template <typename T>
T LogicalClock<T>::GetTime() {
  std::shared_lock lock(mutex_);
  time_++;
  return time_;
}

template <typename T>
void LogicalClock<T>::SetTime(const T & time) {
  std::shared_lock lock(mutex_);
  time_ = std::max(time, time_);
}

}  // namespace ResourceSharing
