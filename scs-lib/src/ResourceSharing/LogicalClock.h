/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <shared_mutex>
#include "Messages/Data/DataVisitorBase.h"

namespace ResourceSharing {
template <typename T>
class LogicalClock :
  public Messages::Data::DataVisitorBase {
 public:
  LogicalClock();

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] T GetTime();

  void SetTime(const T & time);

 private:
  T time_;

  mutable std::shared_mutex mutex_;

  static const char kTime[];
};

}  // namespace ResourceSharing

#include "LogicalClock.tpp"
