/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Data/DataVisitorBase.h"

namespace ResourceSharing {
template <class T, class P>
class TimeStamp :
  public Messages::Data::DataVisitorBase {
 public:
  TimeStamp(
    const T & time,
    const P & priority);

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] T GetTime() const;

  [[nodiscard]] P GetPriority() const;

  TimeStamp& operator=(const TimeStamp & other);

  bool operator==(const TimeStamp& other) const;

  bool operator!=(const TimeStamp& other) const;

  bool operator<(const TimeStamp& other) const;

  bool operator>(const TimeStamp& other) const;

 private:
  T time_;

  P priority_;

  static const char kTime[];
  static const char kPriority[];
};
}  // namespace ResourceSharing

#include "TimeStamp.tpp"
