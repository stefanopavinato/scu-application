/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "VisitorProperty.h"

namespace Messages::Visitor {

VisitorProperty::VisitorProperty(
  const std::string & path,
  const Types::AttributeType::Enum &attribute)
  : VisitorBase(path)
  , attribute_(attribute) {
}

VisitorProperty::VisitorProperty(
  const std::string & path,
  const Types::AttributeType::Enum &attribute,
  ::Types::VariantValue value)
  : VisitorBase(path)
  , attribute_(attribute)
  , value_(std::move(value)) {
}

Types::AttributeType VisitorProperty::GetAttribute() const {
  return attribute_;
}

::Types::VariantValue VisitorProperty::GetValue() {
  return value_;
}

void VisitorProperty::SetValue(const ::Types::VariantValue & value) {
  value_ = value;
}

}  // namespace Messages::Visitor
