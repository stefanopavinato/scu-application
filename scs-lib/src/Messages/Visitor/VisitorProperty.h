/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <list>
#include "Types/VariantValue.h"
#include "Types/AttributeType.h"
#include "VisitorBase.h"

namespace Messages::Visitor {
class VisitorProperty :
  public VisitorBase {
 public:
  explicit VisitorProperty(
    const std::string & path,
    const Types::AttributeType::Enum &attribute);

  VisitorProperty(
    const std::string &path,
    const Types::AttributeType::Enum &attribute,
    ::Types::VariantValue value);

  ~VisitorProperty() override = default;

  [[nodiscard]] Types::AttributeType GetAttribute() const;

  [[nodiscard]] ::Types::VariantValue GetValue();

  void SetValue(const ::Types::VariantValue & value);

 private:
  Types::AttributeType attribute_;

  ::Types::VariantValue value_;
};

}  // namespace Messages::Visitor
