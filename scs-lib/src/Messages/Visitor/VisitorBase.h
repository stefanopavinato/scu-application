/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <stack>
#include <queue>
#include <cstdint>
#include <string>
#include "Messages/Data/DataBase.h"

namespace Types {
class ComponentStatus;
class MonitoringFunctionStatus;
}  // namespace Types
namespace SystemModules::Framework {
namespace Components {
class ItemInBase;

class ItemOutBase;

class ItemInOutBase;

class ParameterItemInBase;

class ParameterItemOutBase;

class Container;

class ModuleBase;

class ModuleHandler;
}  // namespace Components
namespace MonitoringFunctions {
class MFBase;
}  // namespace MonitoringFunctions
namespace Actions {
class ActionBase;
}  // namespace Actions
namespace Accessors {
class AccessorBase;
}  // namespace Accessors
}  // namespace SystemModules::Framework
namespace Messages::Visitor {
class VisitorBase :
  public Messages::Data::DataBase {
 public:
  explicit VisitorBase(
    std::string path);

  ~VisitorBase() override = default;

  std::queue<std::string> *GetPath();

  std::string GetPathString();

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ModuleBase *module) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::Container *container) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ItemInBase *itemIn) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ParameterItemInBase *itemIn) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Components::ParameterItemOutBase *itemOut) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Actions::ActionBase *action) = 0;

  [[nodiscard]] virtual bool Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) = 0;

  [[nodiscard]] virtual bool Visit(::Types::ComponentStatus *status) = 0;

  [[nodiscard]] virtual bool Visit(::Types::MonitoringFunctionStatus *status) = 0;

 protected:
  static const char kName[];

  static const char kDescription[];

  static const char kStatus[];

  static const char kState[];

  static const char kUpdateInterval[];

  static const char kGroupAlertSeverities[];

  static const char kIsValueOutForced[];

  static const char kIsValueInForced[];

  static const char kModules[];

  static const char kComponents[];

  static const char kValue[];

  static const char kValueInRead[];

  static const char kValueIn[];

  static const char kValueOut[];

  static const char kValueOutWrite[];

  static const char kComponentType[];

  static const char kValueType[];

  static const char kGroup[];

  static const char kMonitoringFunctions[];

  static const char kAlertSeverity[];

  static const char kId[];

  static const char kNote[];

  static const char kIsValid[];

  static const char kIsActive[];

  static const char kCompareValues[];

  static const char kAccessor[];

  static const char kAction[];

  static const char kIsInitialized[];

  static const char kOnDelay[];

  static const char kOffDelay[];

  static const char kEvent[];

  static const char kOnEvent[];

  static const char kOffEvent[];

  static const char kIsPaused[];

 private:
  std::queue<std::string> path_;

  std::string pathString_;

  static const char kDelimiter;
};

}  // namespace Messages::Visitor
