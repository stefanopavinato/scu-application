/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include "Types/AttributeType.h"
#include "VisitorProperty.h"

namespace Messages::Visitor {
class VisitorPropertySet :
  public VisitorProperty {
 public:
  VisitorPropertySet(
    const std::string & path,
    const Types::AttributeType::Enum &attribute,
    const ::Types::VariantValue &value);

  ~VisitorPropertySet() override = default;

  [[nodiscard]] bool Accept(Data::DataVisitorBase * visitor) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::Container *container) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  [[nodiscard]] bool Visit(::Types::ComponentStatus *status) override;

  [[nodiscard]] bool Visit(::Types::MonitoringFunctionStatus *status) override;
};

}  // namespace Messages::Visitor
