/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages::Visitor {
class VisitorBase;

class IVisitor {
 public:
  virtual ~IVisitor() = default;

  [[nodiscard]] virtual bool Accept(VisitorBase *visitor) = 0;
};
}  // namespace Messages::Visitor
