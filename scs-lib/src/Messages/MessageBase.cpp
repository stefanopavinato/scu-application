/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageBase.h"

namespace Messages {

MessageBase::MessageBase()
  : receiver_(nullptr)
  , sender_(nullptr) {
}

MessageBase::MessageBase(
  std::unique_ptr<Addresses::AddressBase> sender,
  std::unique_ptr<Addresses::AddressBase> receiver)
  : sender_(std::move(sender))
  , receiver_(std::move(receiver)) {
}

Addresses::AddressBase * MessageBase::GetSender() const {
  return sender_.get();
}

void MessageBase::SetSender(std::unique_ptr<Addresses::AddressBase> sender) {
  sender_ = std::move(sender);
}

Addresses::AddressBase * MessageBase::GetReceiver() const {
  return receiver_.get();
}

void MessageBase::SetReceiver(std::unique_ptr<Addresses::AddressBase> receiver) {
  receiver_ = std::move(receiver);
}

void MessageBase::SwapAddresses() {
  auto tmp = std::move(sender_);
  sender_ = std::move(receiver_);
  receiver_ = std::move(tmp);
}

}  // namespace Messages
