/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unordered_map>
#include "Communication/Protocols/FbisNetwork/Types/RequestId.h"
#include "Messages/Types/MessageStatus.h"
#include "Messages/Types/DataStatus.h"
#include "MessageBase.h"

namespace Messages::Visitor {
class VisitorProperty;
}  // namespace Messages::Visitor
namespace Messages {
class Message :
  public MessageBase {
 public:
  Message();

  ~Message() override = default;

  [[nodiscard]] bool Accept(MessageVisitorBase * visitor) override;

  [[nodiscard]] Communication::Protocols::FbisNetwork::Types::RequestId GetRequestId() const;

  void SetRequestId(const Communication::Protocols::FbisNetwork::Types::RequestId::Enum &requestId);

  [[nodiscard]] Messages::Types::MessageStatus *MessageStatus();

  [[nodiscard]] Messages::Types::DataStatus *DataStatus();

  void AddData(const std::string & name, std::unique_ptr<Visitor::VisitorProperty> data);

  [[nodiscard]] std::optional<Visitor::VisitorProperty *> GetData(const std::string &name) const;

  using data_t = std::unordered_map<std::string, std::unique_ptr<Visitor::VisitorProperty>>;

  data_t::iterator begin() { return data_.begin(); }
  data_t::iterator end() { return data_.end(); }
  data_t::const_iterator begin() const { return data_.begin(); }
  data_t::const_iterator end() const { return data_.end(); }
  data_t::const_iterator cbegin() const { return data_.begin(); }
  data_t::const_iterator cend() const { return data_.end(); }

 private:
  Communication::Protocols::FbisNetwork::Types::RequestId requestId_;

  Messages::Types::MessageStatus messageStatus_;

  Messages::Types::DataStatus dataStatus_;

  data_t data_;
};


}  // namespace Messages

