/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages {
class MessageBase;
class Message;
class MessageData;
class MessageDataArray;
class MessageVisitorBase {
 public:
  virtual ~MessageVisitorBase() = default;

  virtual bool OnMessageReceived(MessageBase * message) = 0;

  virtual bool Visit(Message * message);

  virtual bool Visit(MessageData * message);

  virtual bool Visit(MessageDataArray * message);
};
}  // namespace Messages
