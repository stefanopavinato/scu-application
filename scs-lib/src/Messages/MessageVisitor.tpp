/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Messages {

template <class T>
bool MessageVisitor<T>::OnMessageReceived(MessageBase * message) {
  message->Accept(this);
}

template <class T>
bool MessageVisitor<T>::Visit(T * message) {
  if (!message) {
    return false;
  }
  message_ = message;
  return true;
}

template <class T>
T * MessageVisitor<T>::GetMessage() {
  return message_;
}

}  // namespace Messages
