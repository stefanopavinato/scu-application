/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MessageVisitorBase.h"

namespace Messages {
template <class T>
class MessageVisitor :
  public MessageVisitorBase {
 public:
  ~MessageVisitor() override = default;

  bool OnMessageReceived(MessageBase * message) override;

  bool Visit(T * message);

  [[nodiscard]] T * GetMessage();

 private:
  T * message_;
};
}  // namespace Messages

#include "MessageVisitor.tpp"
