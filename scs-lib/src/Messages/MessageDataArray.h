/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unordered_map>
#include "Messages/Data/DataBase.h"
#include "MessageBase.h"

namespace Messages {
class MessageDataArray :
  public MessageBase {
 public:
  MessageDataArray() = default;

  ~MessageDataArray() override = default;

  [[nodiscard]] bool Accept(MessageVisitorBase * visitor) override;

  [[nodiscard]] bool AddData(const std::string & name, std::unique_ptr<Data::DataBase> data);

  [[nodiscard]] std::optional<Data::DataBase *> GetData(const std::string &name) const;

  using data_t = std::unordered_map<std::string, std::unique_ptr<Data::DataBase>>;

  data_t::iterator begin() { return data_.begin(); }
  data_t::iterator end() { return data_.end(); }
  data_t::const_iterator begin() const { return data_.begin(); }
  data_t::const_iterator end() const { return data_.end(); }
  data_t::const_iterator cbegin() const { return data_.begin(); }
  data_t::const_iterator cend() const { return data_.end(); }

 private:
  data_t data_;
};
}  // namespace Messages
