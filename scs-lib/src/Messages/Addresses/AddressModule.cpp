/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"
#include "AddressVisitorBase.h"
#include "AddressModule.h"

namespace Messages::Addresses {

const char AddressModule::kModule[] = "module";

AddressModule::AddressModule(const Types::SoftwareModule::Enum &module)
  : AddressBase()
  , module_(module) {
}

bool AddressModule::Accept(AddressVisitorBase *visitor) {
  return visitor->Visit(this);
}

bool AddressModule::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kModule] = module_.ToString();
  data->Push(std::move(jsonValue));
  return true;
}

std::unique_ptr<AddressBase> AddressModule::Clone() const {
  return std::make_unique<AddressModule>(module_.GetEnum());
}

Types::SoftwareModule AddressModule::GetModule() const {
  return module_;
}

std::string AddressModule::ToString() const {
  return module_.ToString();
}

bool AddressModule::operator==(const AddressBase &other) const {
  auto otherAddress = dynamic_cast<const AddressModule *>(&other);
  return otherAddress && *this == *otherAddress;
}

bool AddressModule::operator==(const AddressModule& other) const {
  return module_ == other.module_;
}

}  // namespace Messages::Addresses

size_t std::hash<Messages::Addresses::AddressModule>::operator()(
  const Messages::Addresses::AddressModule & address) const {
  return std::hash<Types::SoftwareModule>()(address.module_);
}
