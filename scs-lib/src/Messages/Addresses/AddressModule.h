/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "Types/SoftwareModule.h"
#include "AddressBase.h"

namespace Messages::Addresses {
class AddressModule;
}  // namespace Messages::Addresses

template<>
class std::hash<Messages::Addresses::AddressModule> {
 public:
  size_t operator()(const Messages::Addresses::AddressModule & address) const;
};

namespace Messages::Addresses {
class AddressModule : public AddressBase {
  friend size_t std::hash<AddressModule>::operator()(const AddressModule & address) const;

 public:
  explicit AddressModule(
    const ::Types::SoftwareModule::Enum &module);

  AddressModule(const AddressModule &address) = default;

  ~AddressModule() override = default;

  bool Accept(AddressVisitorBase * visitor) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] std::unique_ptr<AddressBase> Clone() const override;

  [[nodiscard]] ::Types::SoftwareModule GetModule() const;

  [[nodiscard]] std::string ToString() const override;

  bool operator==(const AddressBase& other) const override;

  bool operator==(const AddressModule& other) const;

 private:
  ::Types::SoftwareModule module_;

  static const char kModule[];
};

}  // namespace Messages::Addresses
