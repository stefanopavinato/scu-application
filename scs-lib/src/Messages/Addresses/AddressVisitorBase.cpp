/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "AddressClient.h"
#include "AddressListener.h"
#include "AddressLock.h"
#include "AddressModule.h"
#include "AddressVisitorBase.h"

namespace Messages::Addresses {

bool AddressVisitorBase::Visit(AddressClient * address) {
  return false;
}

bool AddressVisitorBase::Visit(AddressListener * address) {
  return false;
}

bool AddressVisitorBase::Visit(AddressLock * address) {
  return false;
}

bool AddressVisitorBase::Visit(AddressModule * address) {
  return false;
}

}  // namespace Messages::Addresses

