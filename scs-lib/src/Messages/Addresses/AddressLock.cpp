/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"
#include "AddressVisitorBase.h"
#include "AddressLock.h"

namespace Messages::Addresses {

const char AddressLock::kId[] = "id";

AddressLock::AddressLock(const Types::LockId::Enum &id)
  : AddressBase()
  , id_(id) {
}

bool AddressLock::Accept(AddressVisitorBase *visitor) {
  return visitor->Visit(this);
}

bool AddressLock::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kId] = id_.ToString();
  data->Push(std::move(jsonValue));
  return true;
}

std::unique_ptr<AddressBase> AddressLock::Clone() const {
  return std::make_unique<AddressLock>(id_.GetValue());
}

Types::LockId AddressLock::GetId() const {
  return id_;
}

std::string AddressLock::ToString() const {
  return id_.ToString();
}

bool AddressLock::operator==(const AddressBase &other) const {
  auto otherAddress = dynamic_cast<const AddressLock *>(&other);
  return otherAddress && *this == *otherAddress;
}

bool AddressLock::operator==(const AddressLock& other) const {
  return id_ == other.id_;
}

}  // namespace Messages::Addresses

size_t std::hash<Messages::Addresses::AddressLock>::operator()(
  const Messages::Addresses::AddressLock & address) const {
  return std::hash<Types::LockId>()(address.id_);
}
