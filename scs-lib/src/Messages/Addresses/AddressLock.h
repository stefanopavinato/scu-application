/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "Types/LockId.h"
#include "AddressBase.h"

namespace Messages::Addresses {
class AddressLock;
}  // namespace Messages::Addresses

template<>
class std::hash<Messages::Addresses::AddressLock> {
 public:
  size_t operator()(const Messages::Addresses::AddressLock & address) const;
};

namespace Messages::Addresses {
class AddressLock :
  public AddressBase {
  friend size_t std::hash<AddressLock>::operator()(const AddressLock & address) const;

 public:
  explicit AddressLock(
    const ::Types::LockId::Enum &id);

  AddressLock(const AddressLock &address) = default;

  ~AddressLock() override = default;

  bool Accept(AddressVisitorBase * visitor) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] std::unique_ptr<AddressBase> Clone() const override;

  [[nodiscard]] ::Types::LockId GetId() const;

  [[nodiscard]] std::string ToString() const override;

  bool operator==(const AddressBase& other) const override;

  bool operator==(const AddressLock& other) const;

 private:
  ::Types::LockId id_;

  static const char kId[];
};

}  // namespace Messages::Addresses
