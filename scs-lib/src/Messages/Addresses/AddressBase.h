/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <string>
#include "Messages/Data/DataVisitorBase.h"

namespace Messages::Addresses {
class AddressVisitorBase;
class AddressBase :
  public Data::DataVisitorBase {
 public:
  AddressBase() = default;

  ~AddressBase() override = default;

  [[nodiscard]] virtual std::unique_ptr<AddressBase> Clone() const = 0;

  // Returns false if the address does not accept the visitor
  virtual bool Accept(AddressVisitorBase * visitor) = 0;

  [[nodiscard]] virtual std::string ToString() const = 0;

  virtual bool operator== (const AddressBase &other) const = 0;
};
}  // namespace Messages::Addresses

