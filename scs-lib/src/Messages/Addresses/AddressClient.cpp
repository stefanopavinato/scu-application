/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include <arpa/inet.h>
#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"
#include "AddressVisitorBase.h"
#include "AddressClient.h"

namespace Messages::Addresses {

const char AddressClient::kIp[] = "ip";
const char AddressClient::kProtocol[] = "protocol";

AddressClient::AddressClient(
  std::string ip,
  const Communication::Types::ProtocolType &protocol)
  : AddressBase()
  , ip_(std::move(ip))
  , protocol_(protocol) {
}

bool AddressClient::Accept(AddressVisitorBase *visitor) {
  return visitor->Visit(this);
}

std::unique_ptr<AddressBase> AddressClient::Clone() const {
  return std::make_unique<AddressClient>(ip_, protocol_);
}

bool AddressClient::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kIp] = ip_;
  (*jsonValue)[kProtocol] = protocol_.ToString();
  data->Push(std::move(jsonValue));
  return true;
}

std::string AddressClient::GetIp() const {
  return ip_;
}

Communication::Types::ProtocolType AddressClient::GetProtocol() const {
  return protocol_;
}

uint32_t AddressClient::ToUInt32() const {
  auto i = inet_addr(ip_.c_str());
  return i;
}

std::string AddressClient::ToString() const {
  std::stringstream out;
  out << ip_ << ":" << protocol_;
  return out.str();
}

AddressClient &AddressClient::operator=(const AddressClient &other) {
  if (this != &other) {
    ip_ = other.ip_;
  }
  return *this;
}

bool AddressClient::operator==(const AddressBase& other) const {
  auto otherAddress = dynamic_cast<const AddressClient *>(&other);
  return otherAddress && *this == *otherAddress;
}

bool AddressClient::operator==(const AddressClient& other) const {
  return ip_ == other.ip_ && protocol_ == other.protocol_;
}

bool AddressClient::operator!=(const AddressClient& other) const {
  return ip_ != other.ip_;
}

bool AddressClient::operator>(const AddressClient& other) const {
  return ToUInt32() > other.ToUInt32();
}

bool AddressClient::operator<(const AddressClient& other) const {
  return ToUInt32() < other.ToUInt32();
}

}  // namespace Messages::Addresses

size_t std::hash<Messages::Addresses::AddressClient>::operator()(
  const Messages::Addresses::AddressClient & address) const {
  return std::hash<Communication::Types::ProtocolType>()(address.protocol_) ^ std::hash<string>()(address.ip_);
}

std::ostream& operator<<(std::ostream& out, const Messages::Addresses::AddressClient& address) {
  return out << address.GetIp() << ":" << address.GetProtocol();
}
