/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "Communication/Types/ProtocolType.h"
#include "AddressBase.h"

namespace Messages::Addresses {
class AddressClient;
}  // namespace Messages::Addresses

template<>
class std::hash<Messages::Addresses::AddressClient> {
 public:
  size_t operator()(const Messages::Addresses::AddressClient & address) const;
};

namespace Messages::Addresses {
class AddressClient :
  public AddressBase {
  friend size_t std::hash<AddressClient>::operator()(const AddressClient & address) const;

 public:
  AddressClient(
    std::string ip,
    const Communication::Types::ProtocolType &protocol);

  AddressClient(const AddressClient &address) = default;

  ~AddressClient() override = default;

  bool Accept(AddressVisitorBase * visitor) override;

  [[nodiscard]] std::unique_ptr<AddressBase> Clone() const override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] std::string GetIp() const;

  [[nodiscard]] Communication::Types::ProtocolType GetProtocol() const;

  [[nodiscard]] uint32_t ToUInt32() const;

  [[nodiscard]] std::string ToString() const override;

  AddressClient &operator=(const AddressClient &other);

  bool operator==(const AddressBase& other) const override;

  bool operator==(const AddressClient& other) const;

  bool operator!=(const AddressClient& other) const;

  bool operator>(const AddressClient& other) const;

  bool operator<(const AddressClient& other) const;

 private:
  std::string ip_;

  Communication::Types::ProtocolType protocol_;

  static const char kIp[];
  static const char kProtocol[];
};

}  // namespace Messages::Addresses

