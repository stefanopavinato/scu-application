/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages::Addresses {
class AddressClient;
class AddressListener;
class AddressLock;
class AddressModule;
class AddressVisitorBase {
 public:
  virtual ~AddressVisitorBase() = default;

  virtual bool Visit(AddressClient * address);

  virtual bool Visit(AddressListener * address);

  virtual bool Visit(AddressLock * address);

  virtual bool Visit(AddressModule * address);
};
}  // namespace Messages::Addresses
