/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Messages::Addresses {

template <class T>
bool AddressVisitor<T>::Visit(T * address) {
  if (!address) {
    return false;
  }
  address_ = address;
  return true;
}

template <class T>
T * AddressVisitor<T>::GetAddress() const {
  return address_;
}

}  // namespace Messages::Addresses
