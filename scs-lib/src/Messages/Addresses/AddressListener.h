/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <string>
#include "Communication/Types/ProtocolType.h"
#include "AddressBase.h"

namespace Messages::Addresses {
class AddressListener;
}  // namespace Messages::Addresses

template<>
class std::hash<Messages::Addresses::AddressListener> {
 public:
  size_t operator()(const Messages::Addresses::AddressListener & address) const;
};

namespace Messages::Addresses {
class AddressListener : public AddressBase {
  friend size_t std::hash<AddressListener>::operator()(const AddressListener &address) const;

 public:
  explicit AddressListener(
    const uint16_t & port);

  AddressListener(const AddressListener &address) = default;

  ~AddressListener() override = default;

  bool Accept(AddressVisitorBase * visitor) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] std::unique_ptr<AddressBase> Clone() const override;

  [[nodiscard]] uint16_t GetPort() const;

  [[nodiscard]] std::string ToString() const override;


  bool operator==(const AddressBase& other) const override;

  bool operator==(const AddressListener& other) const;

 private:
  uint16_t port_;

  static const char kPort[];
};
}  // namespace Messages::Addresses
