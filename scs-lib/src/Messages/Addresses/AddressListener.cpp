/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"
#include "AddressVisitorBase.h"
#include "AddressListener.h"

namespace Messages::Addresses {

const char AddressListener::kPort[] = "port";

AddressListener::AddressListener(
  const uint16_t &port)
  : port_(port) {
}

bool AddressListener::Accept(AddressVisitorBase *visitor) {
  return visitor->Visit(this);
}

bool AddressListener::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kPort] = port_;
  data->Push(std::move(jsonValue));
  return true;
}

std::unique_ptr<AddressBase> AddressListener::Clone() const {
  return std::make_unique<AddressListener>(port_);
}

uint16_t AddressListener::GetPort() const {
  return port_;
}

std::string AddressListener::ToString() const {
  return std::to_string(port_);
}

bool AddressListener::operator==(const AddressBase& other) const {
  auto otherAddress = dynamic_cast<const AddressListener *>(&other);
  return otherAddress && *this == *otherAddress;
}

bool AddressListener::operator==(const AddressListener& other) const {
  return port_ == other.port_;
}

}  // namespace Messages::Addresses

size_t std::hash<Messages::Addresses::AddressListener>::operator()(
  const Messages::Addresses::AddressListener & address) const {
  return std::hash<uint16_t>()(address.port_);
}

std::ostream& operator<<(std::ostream& out, const Messages::Addresses::AddressListener& address) {
  return out << std::to_string(address.GetPort());
}
