/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Communication/Types/ProtocolType.h"
#include "AddressVisitorBase.h"

namespace Messages::Addresses {
template <class T>
class AddressVisitor :
  public AddressVisitorBase {
 public:
  bool Visit(T * address) override;

  [[nodiscard]] T * GetAddress() const;

 private:
  T * address_;
};
}  // namespace Messages::Addresses

#include "AddressVisitor.tpp"
