/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Visitor/VisitorProperty.h"
#include "MessageVisitorBase.h"
#include "Message.h"

namespace Messages {

Message::Message()
  : requestId_(Communication::Protocols::FbisNetwork::Types::RequestId::Enum::NONE)
  , messageStatus_(Messages::Types::MessageStatus::Enum::OK)
  , dataStatus_(Messages::Types::DataStatus::Enum::OK) {
}

bool Message::Accept(MessageVisitorBase *visitor) {
  return visitor->Visit(this);
}

Communication::Protocols::FbisNetwork::Types::RequestId Message::GetRequestId() const {
  return requestId_;
}

void Message::SetRequestId(const Communication::Protocols::FbisNetwork::Types::RequestId::Enum &requestId) {
  requestId_.SetValue(requestId);
}

Messages::Types::MessageStatus *Message::MessageStatus() {
  return &messageStatus_;
}

Messages::Types::DataStatus *Message::DataStatus() {
  return &dataStatus_;
}

void Message::AddData(const std::string & name, std::unique_ptr<Visitor::VisitorProperty> data) {
  data_.insert(std::make_pair(name, std::move(data)));
}

std::optional<Visitor::VisitorProperty *> Message::GetData(const std::string &name) const {
  auto data = data_.find(name);
  if (data == data_.end()) {
    return std::nullopt;
  }
  return data->second.get();
}

}  // namespace Messages
