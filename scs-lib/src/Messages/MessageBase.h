/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <cstdint>
#include "Messages/Addresses/AddressBase.h"

namespace Messages {
class MessageVisitorBase;
class MessageBase {
 public:
  MessageBase();

  MessageBase(
    std::unique_ptr<Addresses::AddressBase> sender,
    std::unique_ptr<Addresses::AddressBase> receiver);

  virtual ~MessageBase() = default;

  // Returns false if the message does not accept the visitor
  virtual bool Accept(MessageVisitorBase * visitor) = 0;

  [[nodiscard]] Addresses::AddressBase * GetSender() const;

  void SetSender(std::unique_ptr<Addresses::AddressBase> sender);

  [[nodiscard]] Addresses::AddressBase * GetReceiver() const;

  void SetReceiver(std::unique_ptr<Addresses::AddressBase> receiver);

  void SwapAddresses();

 private:
  std::unique_ptr<Addresses::AddressBase> sender_;

  std::unique_ptr<Addresses::AddressBase> receiver_;
};
}  // namespace Messages
