/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MessageVisitorBase.h"
#include "MessageDataArray.h"

namespace Messages {

bool MessageDataArray::Accept(MessageVisitorBase * visitor) {
  return visitor->Visit(this);
}

bool MessageDataArray::AddData(const std::string & name, std::unique_ptr<Data::DataBase> data) {
  data_.insert(std::make_pair(name, std::move(data)));
}

std::optional<Data::DataBase *> MessageDataArray::GetData(const std::string &name) const {
  auto data = data_.find(name);
  if (data == data_.end()) {
    return std::nullopt;
  }
  return data->second.get();
}

}  // namespace Messages
