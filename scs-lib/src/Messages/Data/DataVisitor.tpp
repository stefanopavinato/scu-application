/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Messages::Data {

template <class T>
bool DataVisitor<T>::Visit(T * data) {
  if (!data) {
    return false;
  }
  data_ = data;
  return true;
}

template <class T>
T * DataVisitor<T>::GetData() {
  return data_;
}

}  // namespace Messages::Data
