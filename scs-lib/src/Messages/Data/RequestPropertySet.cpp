/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "RequestPropertySet.h"

namespace Messages::Data {

RequestPropertySet::RequestPropertySet(
  std::string path,
  const Messages::Visitor::Types::AttributeType &attribute,
  ::Types::VariantValue value)
  : path_(std::move(path))
  , attribute_(attribute)
  , value_(std::move(value)) {
}

bool RequestPropertySet::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

std::string RequestPropertySet::GetPath() const {
  return path_;
}

Messages::Visitor::Types::AttributeType RequestPropertySet::GetAttribute() const {
  return attribute_;
}

::Types::VariantValue RequestPropertySet::GetValue() {
  return value_;
}

}  // namespace Messages::Data
