/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages::Data {
class DataVisitorBase;
class DataBase {
 public:
  DataBase() = default;

  virtual ~DataBase() = default;

  [[nodiscard]] virtual bool Accept(DataVisitorBase * visitor) = 0;
};
}  // namespace Messages::Data
