/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "DataVisitorBase.h"

namespace Messages::Data {
template <class T>
class DataVisitor :
  public DataVisitorBase {
 public:
  ~DataVisitor() override = default;

  bool Visit(T * data);

  [[nodiscard]] T * GetData();

 private:
  T * data_;
};
}  // namespace Messages::Data

#include "DataVisitor.tpp"
