/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitor.h"
#include "RequestFirmwareRegisterWrite.h"
#include "ResponseFirmwareRegisterWrite.h"

namespace Messages::Data {

ResponseFirmwareRegisterWrite::ResponseFirmwareRegisterWrite(
  const uint32_t & address,
  const uint32_t & value)
  : address_(address)
  , value_(value) {
}

bool ResponseFirmwareRegisterWrite::Accept(DataVisitorBase *visitor) {
  return visitor->Visit(this);
}

uint32_t ResponseFirmwareRegisterWrite::GetAddress() const {
  return address_;
}

uint32_t ResponseFirmwareRegisterWrite::GetValue() const {
  return value_;
}

std::optional<std::unique_ptr<ResponseFirmwareRegisterWrite>> ResponseFirmwareRegisterWrite::Create(
  DataBase * request) {
  auto visitor = DataVisitor<RequestFirmwareRegisterWrite>();
  if (!request->Accept(&visitor)) {
    return std::nullopt;
  }
  return std::make_unique<ResponseFirmwareRegisterWrite>(
    visitor.GetData()->GetAddress(),
    visitor.GetData()->GetValue());
}


}  // namespace Messages::Data
