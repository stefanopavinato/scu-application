/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitor.h"
#include "RequestFirmwareRegisterRead.h"
#include "ResponseFirmwareRegisterRead.h"

namespace Messages::Data {

ResponseFirmwareRegisterRead::ResponseFirmwareRegisterRead(
  const uint32_t & address)
  : address_(address)
  , value_(0) {
}

bool ResponseFirmwareRegisterRead::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

uint32_t ResponseFirmwareRegisterRead::GetAddress() const {
  return address_;
}

uint32_t ResponseFirmwareRegisterRead::GetValue() const {
  return value_;
}

void ResponseFirmwareRegisterRead::SetValue(const uint32_t & value) {
  value_ = value;
}

std::optional<std::unique_ptr<ResponseFirmwareRegisterRead>> ResponseFirmwareRegisterRead::Create(
  DataBase * request) {
  auto visitor = DataVisitor<RequestFirmwareRegisterRead>();
  if (!request->Accept(&visitor)) {
    return std::nullopt;
  }
  return std::make_unique<ResponseFirmwareRegisterRead>(
    visitor.GetData()->GetAddress());
}

}  // namespace Messages::Data
