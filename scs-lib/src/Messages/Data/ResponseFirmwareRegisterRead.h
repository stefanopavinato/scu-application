/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <optional>
#include "DataBase.h"

namespace Messages::Data {
class ResponseFirmwareRegisterRead :
  public DataBase {
 public:
  explicit ResponseFirmwareRegisterRead(
    const uint32_t & address);

  bool Accept(DataVisitorBase *visitor) override;

  [[nodiscard]] uint32_t GetAddress() const;

  [[nodiscard]] uint32_t GetValue() const;

  void SetValue(const uint32_t & value);

  static std::optional<std::unique_ptr<ResponseFirmwareRegisterRead>> Create(DataBase * request);

 private:
  const uint32_t address_;

  uint32_t value_;
};
}  // namespace Messages::Data
