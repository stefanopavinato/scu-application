/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Visitor/Types/AttributeType.h"
#include "DataBase.h"

namespace Messages::Data {
class RequestPropertyGet
    : public DataBase {
 public:
  RequestPropertyGet(
    std::string path,
    const Messages::Visitor::Types::AttributeType &attribute);

  ~RequestPropertyGet() override = default;

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] std::string GetPath() const;

  [[nodiscard]] Messages::Visitor::Types::AttributeType GetAttribute() const;

 private:
  std::string path_;

  const Messages::Visitor::Types::AttributeType attribute_;
};
}  // namespace Messages::Data
