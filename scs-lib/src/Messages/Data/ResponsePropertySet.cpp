/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/MonitoringFunctions/MFBase.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "DataVisitor.h"
#include "RequestPropertySet.h"
#include "ResponsePropertySet.h"

namespace Messages::Data {

ResponsePropertySet::ResponsePropertySet(
  const std::string & path,
  const Visitor::Types::AttributeType::Enum &attribute,
  const ::Types::VariantValue &value)
  : VisitorProperty(
  path,
  attribute,
  value) {
}

bool ResponsePropertySet::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

std::optional<std::unique_ptr<ResponsePropertySet>> ResponsePropertySet::Create(DataBase * data) {
  auto visitor = DataVisitor<RequestPropertySet>();
  if (!data->Accept(&visitor)) {
    return std::nullopt;
  }
  return std::make_unique<ResponsePropertySet>(
    visitor.GetData()->GetPath(),
    visitor.GetData()->GetAttribute().GetValue(),
    visitor.GetData()->GetValue());
}


bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  (void) moduleHandler;
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ModuleBase *module) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      module->ResetAlertSeverity();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::Container *container) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      container->ResetAlertSeverity();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ItemInBase *itemIn) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      return itemIn->SetUpdateInterval(GetValue());
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE: {
      return itemIn->ForceValue(
        GetValue(),
        ::Types::ForceStatus::Enum::PERMANENT);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      return itemIn->ForceValue(
        GetValue(),
        ::Types::ForceStatus::Enum::ONCE);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      return itemIn->ReleaseValueForced();
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemIn->ResetAlertSeverity();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ItemOutBase *itemOut) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      return itemOut->SetUpdateInterval(GetValue());
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      return itemOut->ForceValue(
        GetValue(),
        ::Types::ForceStatus::Enum::PERMANENT);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      return itemOut->ForceValue(
        GetValue(),
        ::Types::ForceStatus::Enum::ONCE);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      return itemOut->ReleaseValueForced();
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemOut->ResetAlertSeverity();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      return itemInOut->SetUpdateInterval(GetValue());
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE: {
      return itemInOut->ForceValueIn(
        GetValue(),
        ::Types::ForceStatus::Enum::PERMANENT);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE_ONCE: {
      return itemInOut->ForceValueIn(
        GetValue(),
        ::Types::ForceStatus::Enum::ONCE);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_FORCE_RELEASE: {
      return itemInOut->ReleaseValueInForced();
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE: {
      return itemInOut->ForceValueOut(
        GetValue(),
        ::Types::ForceStatus::Enum::PERMANENT);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE: {
      return itemInOut->ForceValueOut(
        GetValue(),
        ::Types::ForceStatus::Enum::ONCE);
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE: {
      return itemInOut->ReleaseValueOutForced();
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      itemInOut->ResetAlertSeverity();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  (void) parameterItemIn;
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  (void) parameterItemOut;
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::STATUS_RESET: {
      monitoringFunction->RequestReset();
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Actions::ActionBase *action) {
  (void) action;
  return false;
}

bool ResponsePropertySet::Visit(
  SystemModules::Framework::Accessors::AccessorBase *accessor) {
  (void) accessor;
  return false;
}

bool ResponsePropertySet::Visit(::Types::ComponentStatus *status) {
  (void) status;
  return false;
}

bool ResponsePropertySet::Visit(::Types::MonitoringFunctionStatus *status) {
  (void) status;
  return false;
}

}  // namespace Messages::Data
