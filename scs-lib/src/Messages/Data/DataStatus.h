/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Messages/Types/DataStatus.h"
#include "Messages/Data/DataBase.h"

namespace Messages::Data {
class DataStatus :
  public Messages::Data::DataBase {
 public:
  DataStatus(
    const Types::DataStatus::Enum & status,
    std::string description);

  bool Accept(DataVisitorBase *visitor) override;

  [[nodiscard]] Types::DataStatus GetStatus() const;

  [[nodiscard]] std::string GetDescription() const;

 private:
  Types::DataStatus status_;

  std::string description_;
};
}  // namespace Messages::Data
