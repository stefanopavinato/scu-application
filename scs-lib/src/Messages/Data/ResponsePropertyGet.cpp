/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "SystemModules/Framework/Components/ParameterItemInBase.h"
#include "SystemModules/Framework/Components/ParameterItemOutBase.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "DataVisitor.h"
#include "RequestPropertyGet.h"
#include "ResponsePropertyGet.h"

namespace Messages::Data {

ResponsePropertyGet::ResponsePropertyGet(
  const std::string & path,
  const Visitor::Types::AttributeType::Enum &attribute)
  : Visitor::VisitorProperty(
  path,
  attribute) {
}

bool ResponsePropertyGet::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

std::optional<std::unique_ptr<ResponsePropertyGet>> ResponsePropertyGet::Create(DataBase * data) {
  auto visitor = DataVisitor<RequestPropertyGet>();
  if (!data->Accept(&visitor)) {
    return std::nullopt;
  }
  return std::make_unique<ResponsePropertyGet>(
    visitor.GetData()->GetPath(),
    visitor.GetData()->GetAttribute().GetValue());
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  (void) moduleHandler;
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ModuleBase *module) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::MODULE_STATE: {
      SetValue(module->GetStateId());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::Container *container) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(container->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(container->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(container->GetDescription());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ItemInBase *itemIn) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemIn->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(itemIn->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemIn->GetDescription());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemIn->GetAlertSeverity()));
      return true;
    }
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemIn->GetUpdateInterval());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_READ: {
      auto inRead = itemIn->GetVariantValueRead();
      if (!inRead) {
        return false;
      }
      SetValue(inRead.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN: {
      auto in = itemIn->GetVariantValue();
      if (!in) {
        return false;
      }
      SetValue(in.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_IS_FORCED: {
      SetValue(itemIn->IsValueInForced());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemOut->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(itemOut->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemOut->GetDescription());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemOut->GetAlertSeverity()));
      return true;
    }
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemOut->GetUpdateInterval());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE: {
      auto outWrite = itemOut->GetVariantValueWrite();
      if (!outWrite) {
        return false;
      }
      SetValue(outWrite.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT: {
      auto out = itemOut->GetVariantValue();
      if (!out) {
        return false;
      }
      SetValue(out.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_IS_FORCED: {
      SetValue(itemOut->IsValueOutForced());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(itemInOut->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(itemInOut->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(itemInOut->GetDescription());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(itemInOut->GetAlertSeverity()));
      return true;
    }
    case Visitor::Types::AttributeType::Enum::UPDATE_INTERVAL: {
      SetValue(itemInOut->GetUpdateInterval());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_READ: {
      auto inRead = itemInOut->GetVariantValueInRead();
      if (!inRead) {
        return false;
      }
      SetValue(inRead.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN: {
      auto in = itemInOut->GetVariantValueIn();
      if (!in) {
        return false;
      }
      SetValue(in.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN_IS_FORCED: {
      SetValue(itemInOut->IsValueInForced());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE: {
      auto outWrite = itemInOut->GetVariantValueOutWrite();
      if (!outWrite) {
        return false;
      }
      SetValue(outWrite.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT: {
      auto out = itemInOut->GetVariantValueOut();
      if (!out) {
        return false;
      }
      SetValue(out.value());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT_IS_FORCED: {
      SetValue(itemInOut->IsValueOutForced());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(parameterItemIn->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(parameterItemIn->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(parameterItemIn->GetDescription());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(parameterItemIn->GetAlertSeverity()));
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_IN: {
      auto in = parameterItemIn->GetVariantValue();
      if (!in) {
        return false;
      }
      SetValue(in.value());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  switch (GetAttribute().GetValue()) {
    case Visitor::Types::AttributeType::Enum::COMPONENT_TYPE: {
      SetValue(parameterItemOut->GetComponentType());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::NAME: {
      SetValue(parameterItemOut->GetName());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::DESCRIPTION: {
      SetValue(parameterItemOut->GetDescription());
      return true;
    }
    case Visitor::Types::AttributeType::Enum::ALERT_SEVERITY: {
      SetValue(::Types::AlertSeverity(parameterItemOut->GetAlertSeverity()));
      return true;
    }
    case Visitor::Types::AttributeType::Enum::VALUE_OUT: {
      auto out = parameterItemOut->GetVariantValue();
      if (!out) {
        return false;
      }
      SetValue(out.value());
      return true;
    }
    default: {
    }
  }
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  (void) monitoringFunction;
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Actions::ActionBase *action) {
  (void) action;
  return false;
}

bool ResponsePropertyGet::Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) {
  (void) accessor;
  return false;
}

bool ResponsePropertyGet::Visit(::Types::ComponentStatus *status) {
  (void) status;
  return false;
}

bool ResponsePropertyGet::Visit(::Types::MonitoringFunctionStatus *status) {
  (void) status;
  return false;
}

}  // namespace Messages::Data
