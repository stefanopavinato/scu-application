/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "DataBase.h"

namespace Messages::Data {
class RequestFirmwareRegisterWrite :
  public DataBase {
 public:
  RequestFirmwareRegisterWrite(
    const uint32_t & address,
    const uint32_t & value);

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] uint32_t GetAddress() const;

  [[nodiscard]] uint32_t GetValue() const;

 private:
  const uint32_t address_;

  const uint32_t value_;
};
}  // namespace Messages::Data
