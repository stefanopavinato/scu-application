/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include "Utils/TimeFormatter.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/MonitoringFunctions/MFBase.h"
#include "SystemModules/Framework/Actions/ActionBase.h"
#include "SystemModules/Framework/Components/ItemInBase.h"
#include "SystemModules/Framework/Components/ItemOutBase.h"
#include "SystemModules/Framework/Components/ItemInOutBase.h"
#include "SystemModules/Framework/Components/ParameterItemInBase.h"
#include "SystemModules/Framework/Components/ParameterItemOutBase.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ModuleBase.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "DataVisitor.h"
#include "RequestOverview.h"
#include "ResponseOverview.h"

namespace Messages::Data {

ResponseOverview::ResponseOverview(
  std::string path,
  const uint32_t &level,
  const ::Types::JsonDetails::Enum &details)
  : VisitorBase(std::move(path))
  , details_(details)
  , level_(level) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)["name"] = "response overview";
  stack_.push(std::move(jsonValue));
}

bool ResponseOverview::Accept(DataVisitorBase * visitor) {
  level_--;
  auto retVal = visitor->Visit(this);
  level_++;
  return retVal;
}

::Json::Value ResponseOverview::GetJson() const {
  return *stack_.top();
}

::Types::JsonDetails ResponseOverview::GetDetails() const {
  return details_;
}

uint32_t ResponseOverview::GetLevel() const {
  return level_;
}

void ResponseOverview::Push(std::unique_ptr<::Json::Value> value) {
  stack_.push(std::move(value));
}

std::unique_ptr<::Json::Value> ResponseOverview::Pop() {
  auto retVal = std::move(stack_.top());
  stack_.pop();
  return retVal;
}


std::optional<std::unique_ptr<ResponseOverview>> ResponseOverview::Create(
  DataBase * data) {
  auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestOverview>();
  if (!data->Accept(&visitor)) {
    return std::nullopt;
  }
  return std::make_unique<ResponseOverview>(
    visitor.GetData()->GetPath(),
    visitor.GetData()->GetLevel(),
    visitor.GetData()->GetDetails().GetEnum());
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = moduleHandler->GetDescription();
      // Group alerts
      {
        uint32_t i = 0;
        for (const auto &group : *moduleHandler->GetGroupAlertSeverities()) {
          (*jsonValue)[kGroupAlertSeverities][i][kGroup] = Types::ValueGroup(group.first).ToString();
          (*jsonValue)[kGroupAlertSeverities][i++][kAlertSeverity] = group.second.ToString();
        }
      }
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = moduleHandler->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = moduleHandler->GetComponentType();
      // Set components
      if (level_ > 0) {
        level_--;
        uint32_t i = 0;
        for (const auto &component : *moduleHandler->GetModules()) {
          component.second->Accept(this);
          (*jsonValue)[kModules][i++] = *stack_.top();
          stack_.pop();
        }
        level_++;
      }
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ModuleBase *module) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set state
      (*jsonValue)[kState][kId] = module->GetStateId().ToString();
      (*jsonValue)[kState][kNote] = module->GetStateNote();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(module->GetAlertSeverity()).ToString();
      // Set description
      (*jsonValue)[kDescription] = module->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = module->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = module->GetComponentType();
      // Set components
      if (level_ > 0) {
        level_--;
        uint32_t i = 0;
        for (const auto &component : *module->GetComponents()) {
          component.second->Accept(this);
          (*jsonValue)[kComponents][i++] = *stack_.top();
          stack_.pop();
        }
        level_++;
      }
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::Container *container) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(container->GetAlertSeverity()).ToString();
      // Set description
      (*jsonValue)[kDescription] = container->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL:
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = container->GetName();
      // Set type
      (*jsonValue)[kComponentType] = container->GetComponentType();
      // Set components
      if (level_ > 0) {
        level_--;
        uint32_t i = 0;
        for (const auto &component : *container->GetComponents()) {
          component.second->Accept(this);
          (*jsonValue)[kComponents][i++] = *stack_.top();
          stack_.pop();
        }
        level_++;
      }
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ItemInBase *itemIn) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set monitoring functions
      uint32_t i = 0;
      for (const auto &monitoringFunction : itemIn->GetMonitoringFunctions()) {
        monitoringFunction->Accept(this);
        (*jsonValue)[kMonitoringFunctions][i++] = *stack_.top();
        stack_.pop();
      }
      // Set accessor
      auto accessor = itemIn->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemIn->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemIn->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemIn->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemIn->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueInForced] = itemIn->IsValueInForced();
      // Set value in read
      (*jsonValue)[kValueInRead] = Types::VariantValueUtils::ToString(itemIn->GetVariantValueRead().value());
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemIn->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kIsValid] = itemIn->IsValid();
      (*jsonValue)[kValueIn] = Types::VariantValueUtils::ToString(itemIn->GetVariantValue().value());
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemIn->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemIn->GetComponentType();
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set action
      auto action = itemOut->GetAction();
      if (action != nullptr && action->Accept(this)) {
        (*jsonValue)[kAction] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAction] = "not set";
      }
      // Set accessor
      auto accessor = itemOut->GetAccessor();
      if (accessor != nullptr && accessor->Accept(this)) {
        (*jsonValue)[kAccessor] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemOut->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemOut->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemOut->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueOutForced] = itemOut->IsValueOutForced();
      // Set value out
      (*jsonValue)[kValueOut] = Types::VariantValueUtils::ToString(itemOut->GetVariantValue().value());
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemOut->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value out write
      (*jsonValue)[kIsValid] = itemOut->IsValid();
      (*jsonValue)[kValueOutWrite] = Types::VariantValueUtils::ToString(itemOut->GetVariantValueWrite().value());
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemOut->GetComponentType();
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set monitoring functions
      uint32_t i = 0;
      for (const auto &monitoringFunction : itemInOut->GetMonitoringFunctions()) {
        monitoringFunction->Accept(this);
        (*jsonValue)[kMonitoringFunctions][i++] = *stack_.top();
        stack_.pop();
      }
      // Set action
      auto action = itemInOut->GetAction();
      if (action != nullptr && action->Accept(this)) {
        (*jsonValue)[kAction] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAction] = "not set";
      }
      // Set accessor
      auto accessor = itemInOut->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set update interval
      (*jsonValue)[kUpdateInterval] = itemInOut->GetUpdateInterval().count();
      // Set value type
      (*jsonValue)[kValueType] = itemInOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = itemInOut->GetDescription();
      // Set group
      (*jsonValue)[kGroup] = itemInOut->GetGroup().ToString();
      // Set value forced indicator
      (*jsonValue)[kIsValueInForced] = itemInOut->IsValueInForced();
      (*jsonValue)[kIsValueOutForced] = itemInOut->IsValueOutForced();
      // Set value in read
      (*jsonValue)[kValueInRead] = Types::VariantValueUtils::ToString(itemInOut->GetVariantValueInRead().value());
      (*jsonValue)[kValueOut] = Types::VariantValueUtils::ToString(itemInOut->GetVariantValueOut().value());
      // Set alert severity
      (*jsonValue)[kAlertSeverity] = Types::AlertSeverity(itemInOut->GetAlertSeverity()).ToString();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kIsValid] = itemInOut->IsValid();
      (*jsonValue)[kValueIn] = Types::VariantValueUtils::ToString(itemInOut->GetVariantValueIn().value());
      (*jsonValue)[kValueOutWrite] = Types::VariantValueUtils::ToString(itemInOut->GetVariantValueOutWrite().value());
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = itemInOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = itemInOut->GetComponentType();
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set accessor
      auto accessor = parameterItemIn->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set value type
//      (*jsonValue)[kValueType] = parameterItemIn->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = parameterItemIn->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kValueIn] = Types::VariantValueUtils::ToString(parameterItemIn->GetVariantValue().value());
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = parameterItemIn->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = parameterItemIn->GetComponentType();
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (details_.GetEnum()) {
    case Types::JsonDetails::Enum::VERBOSE: {
      // Set accessor
      auto accessor = parameterItemOut->GetAccessor();
      if (accessor != nullptr) {
        accessor->Accept(this);
        (*jsonValue)[kAccessor] = *stack_.top();
        stack_.pop();
      } else {
        (*jsonValue)[kAccessor] = "not set";
      }
      // Set value type
//      (*jsonValue)[kValueType] = parameterItemOut->GetValueType().ToString();
    }
    case Types::JsonDetails::Enum::COMPACT: {
      // Set description
      (*jsonValue)[kDescription] = parameterItemOut->GetDescription();
    }
    case Types::JsonDetails::Enum::MINIMAL: {
      // Set value in
      (*jsonValue)[kValueOut] = Types::VariantValueUtils::ToString(parameterItemOut->GetVariantValue().value());
    }
    case Types::JsonDetails::Enum::CONFIG: {
      // Set name
      (*jsonValue)[kName] = parameterItemOut->GetName();
      // Set component type
      (*jsonValue)[kComponentType] = parameterItemOut->GetComponentType();
    }
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kComponentType] = monitoringFunction->GetComponentType();
  (*jsonValue)[kOnDelay] = monitoringFunction->GetOnDelay().count();
  (*jsonValue)[kOffDelay] = monitoringFunction->GetOffDelay().count();
  (*jsonValue)[kOnEvent] = Utils::TimeFormatter::ToString(monitoringFunction->GetOnEventTime());
  (*jsonValue)[kOffEvent] = Utils::TimeFormatter::ToString(monitoringFunction->GetOffEventTime());
  (*jsonValue)[kIsActive] = monitoringFunction->IsActive();
  (*jsonValue)[kCompareValues] = monitoringFunction->GetCompareValues();
  // Set status
  if (monitoringFunction->Status().Accept(this)) {
    (*jsonValue)[kStatus] = *stack_.top();
    stack_.pop();
  }
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Actions::ActionBase *action) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kName] = action->GetDescription();
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kComponentType] = accessor->GetTypeName();
  (*jsonValue)[kDescription] = accessor->GetDescription();
  (*jsonValue)[kIsInitialized] = accessor->IsInitialized();
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(::Types::ComponentStatus *status) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kId] = static_cast<uint32_t>(status->GetId().GetEnum());
  (*jsonValue)[kIsValid] = status->IsValid();
  (*jsonValue)[kAlertSeverity] = status->AlertSeverity().ToString();
  (*jsonValue)[kDescription] = status->GetId().ToString();
  (*jsonValue)[kEvent] = Utils::TimeFormatter::ToString(status->GetTimeStamp());
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

bool ResponseOverview::Visit(::Types::MonitoringFunctionStatus *status) {
  auto jsonValue = std::make_unique<Json::Value>();
  (*jsonValue)[kId] = status->Id().ToString();
  (*jsonValue)[kGroup] = status->Group().ToString();
  (*jsonValue)[kAlertSeverity] = status->AlertSeverity().ToString();
  (*jsonValue)[kDescription] = status->Description();
  // Push json value
  stack_.push(std::move(jsonValue));
  return true;
}

}  // namespace Messages::Data
