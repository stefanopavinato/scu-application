/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "DataVisitorBase.h"
#include "ResponsePropertyGet.h"
#include "Messages/Visitor/VisitorProperty.h"

namespace Messages::Data {
class ResponsePropertyGet :
  public Visitor::VisitorProperty {
 public:
  ResponsePropertyGet(
    const std::string & path,
    const Visitor::Types::AttributeType::Enum &attribute);

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  static std::optional<std::unique_ptr<ResponsePropertyGet>> Create(DataBase * data);

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::Container *container) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  [[nodiscard]] bool Visit(::Types::ComponentStatus *status) override;

  [[nodiscard]] bool Visit(::Types::MonitoringFunctionStatus *status) override;
};
}  // namespace Messages::Data
