/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "RequestPropertyGet.h"

namespace Messages::Data {

RequestPropertyGet::RequestPropertyGet(
  std::string path,
  const Messages::Visitor::Types::AttributeType &attribute)
  : path_(std::move(path))
  , attribute_(attribute) {
}

bool RequestPropertyGet::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

std::string RequestPropertyGet::GetPath() const {
  return path_;
}

Messages::Visitor::Types::AttributeType RequestPropertyGet::GetAttribute() const {
  return attribute_;
}

}  // namespace Messages::Data
