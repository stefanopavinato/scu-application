/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "RequestOverview.h"

namespace Messages::Data {

RequestOverview::RequestOverview(
  std::string path,
  const uint32_t &level,
  const ::Types::JsonDetails &details)
  : DataBase()
  , path_(std::move(path))
  , level_(level)
  , details_(details) {
}

bool RequestOverview::Accept(DataVisitorBase *visitor) {
  return visitor->Visit(this);
}

std::string RequestOverview::GetPath() const {
  return path_;
}

uint32_t RequestOverview::GetLevel() const {
  return level_;
}

::Types::JsonDetails RequestOverview::GetDetails() const {
  return details_;
}

}  // namespace Messages::Data
