/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Visitor/Types/AttributeType.h"
#include "Types/ValueType.h"
#include "Types/VariantValue.h"
#include "DataBase.h"

namespace Messages::Data {
class RequestPropertySet :
  public DataBase {
 public:
  RequestPropertySet(
    std::string path,
    const Messages::Visitor::Types::AttributeType &attribute,
    ::Types::VariantValue value);

  ~RequestPropertySet() override = default;

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] std::string GetPath() const;

  [[nodiscard]] Messages::Visitor::Types::AttributeType GetAttribute() const;

  [[nodiscard]] ::Types::VariantValue GetValue();

 private:
  std::string path_;

  const Messages::Visitor::Types::AttributeType attribute_;

  ::Types::VariantValue value_;
};

}  // namespace Messages::Data
