/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "DataStatus.h"

namespace Messages::Data {

DataStatus::DataStatus(
  const Types::DataStatus::Enum & status,
  std::string description)
  : status_(status)
  , description_(std::move(description)) {
}

bool DataStatus::Accept(DataVisitorBase *visitor) {
  return visitor->Visit(this);
}

Types::DataStatus DataStatus::GetStatus() const {
  return status_;
}

std::string DataStatus::GetDescription() const {
  return description_;
}

}  // namespace Messages::Data
