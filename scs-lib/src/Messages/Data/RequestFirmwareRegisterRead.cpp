/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "RequestFirmwareRegisterRead.h"

namespace Messages::Data {
RequestFirmwareRegisterRead::RequestFirmwareRegisterRead(
  const uint32_t & address)
  : address_(address) {
}

bool RequestFirmwareRegisterRead::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

uint32_t RequestFirmwareRegisterRead::GetAddress() const {
  return address_;
}

}  // namespace Messages::Data
