/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/JsonDetails.h"
#include "DataBase.h"

namespace Messages::Data {
class RequestOverview :
  public DataBase {
 public:
  RequestOverview(
    std::string path,
    const uint32_t &level,
    const ::Types::JsonDetails &details);

  ~RequestOverview() override = default;

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] std::string GetPath() const;

  [[nodiscard]] uint32_t GetLevel() const;

  [[nodiscard]] ::Types::JsonDetails GetDetails() const;

 private:
  const std::string path_;

  const uint32_t level_;

  const ::Types::JsonDetails details_;
};

}  // namespace Messages::Data
