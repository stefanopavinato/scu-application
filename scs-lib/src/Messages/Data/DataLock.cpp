/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "DataLock.h"

namespace Messages::Data {

DataLock::DataLock(
  const ResourceSharing::TimeStamp<uint32_t, uint32_t> &timeStamp,
  const ResourceSharing::LockStatus &lockStatus)
  : DataBase()
  , timeStamp_(timeStamp)
  , lockStatus_(lockStatus) {
}

bool DataLock::Accept(DataVisitorBase *visitor) {
  return visitor->Visit(this);
}

ResourceSharing::LockStatus DataLock::GetLockStatus() const {
  return lockStatus_;
}

ResourceSharing::TimeStamp<uint32_t, uint32_t> DataLock::GetTimeStamp() const {
  return timeStamp_;
}


}  // namespace Messages::Data
