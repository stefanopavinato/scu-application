/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"
#include "RequestFirmwareRegisterWrite.h"
#include "RequestFirmwareRegisterWrite.h"

namespace Messages::Data {
RequestFirmwareRegisterWrite::RequestFirmwareRegisterWrite(
  const uint32_t & address,
  const uint32_t & value)
  : address_(address)
  , value_(value) {
}

bool RequestFirmwareRegisterWrite::Accept(DataVisitorBase * visitor) {
  return visitor->Visit(this);
}

uint32_t RequestFirmwareRegisterWrite::GetAddress() const {
  return address_;
}

uint32_t RequestFirmwareRegisterWrite::GetValue() const {
  return value_;
}

}  // namespace Messages::Data
