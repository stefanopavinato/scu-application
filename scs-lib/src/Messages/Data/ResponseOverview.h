/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <queue>
#include <optional>
#include <json/json.h>
#include "Types/JsonDetails.h"
#include "Messages/Visitor/VisitorBase.h"

namespace Messages::Data {
class DataBase;
class ResponseOverview :
  public Messages::Visitor::VisitorBase {
 public:
  ResponseOverview(
    std::string path,
    const uint32_t &level,
    const ::Types::JsonDetails::Enum &details);

  ~ResponseOverview() override = default;

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] ::Json::Value GetJson() const;

  [[nodiscard]] ::Types::JsonDetails GetDetails() const;

  [[nodiscard]] uint32_t GetLevel() const;

  void Push(std::unique_ptr<::Json::Value> value);

  std::unique_ptr<::Json::Value> Pop();

  static std::optional<std::unique_ptr<ResponseOverview>> Create(DataBase * request);

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleHandler *moduleHandler) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ModuleBase *module) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::Container *container) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInBase *itemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemOutBase *itemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ItemInOutBase *itemInOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemInBase *parameterItemIn) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Components::ParameterItemOutBase *parameterItemOut) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::MonitoringFunctions::MFBase *monitoringFunction) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Actions::ActionBase *action) override;

  [[nodiscard]] bool Visit(SystemModules::Framework::Accessors::AccessorBase *accessor) override;

  [[nodiscard]] bool Visit(::Types::ComponentStatus *status) override;

  [[nodiscard]] bool Visit(::Types::MonitoringFunctionStatus *status) override;

 private:
  uint32_t level_;

  const ::Types::JsonDetails details_;

  std::stack<std::unique_ptr<Json::Value>> stack_;
};

}  // namespace Messages::Data
