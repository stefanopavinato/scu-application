/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "DataVisitorBase.h"

namespace Messages::Data {

bool DataVisitorBase::Visit(DataStatus * data) {
  return false;
}

bool DataVisitorBase::Visit(DataLock * data) {
  return false;
}

bool DataVisitorBase::Visit(RequestOverview * data) {
  return false;
}

bool DataVisitorBase::Visit(RequestPropertyGet * data) {
  return false;
}

bool DataVisitorBase::Visit(RequestPropertySet * data) {
  return false;
}

bool DataVisitorBase::Visit(RequestFirmwareRegisterRead * data) {
  return false;
}

bool DataVisitorBase::Visit(RequestFirmwareRegisterWrite * data) {
  return false;
}

bool DataVisitorBase::Visit(ResponseOverview * data) {
  return false;
}

bool DataVisitorBase::Visit(ResponsePropertyGet * data) {
  return false;
}

bool DataVisitorBase::Visit(ResponsePropertySet * data) {
  return false;
}

bool DataVisitorBase::Visit(ResponseFirmwareRegisterRead * data) {
  return false;
}

bool DataVisitorBase::Visit(ResponseFirmwareRegisterWrite * data) {
  return false;
}

bool DataVisitorBase::Visit(Visitor::VisitorPropertyGet * data) {
  return false;
}

bool DataVisitorBase::Visit(Visitor::VisitorPropertySet * data) {
  return false;
}

}  // namespace Messages::Data
