/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ResourceSharing/TimeStamp.h"
#include "ResourceSharing/LockStatus.h"
#include "DataBase.h"

namespace Messages::Data {
class DataLock :
  public DataBase {
 public:
  DataLock(
    const ResourceSharing::TimeStamp<uint32_t, uint32_t> &timeStamp,
    const ResourceSharing::LockStatus & lockStatus);

  ~DataLock() override = default;

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] ResourceSharing::LockStatus GetLockStatus() const;

  [[nodiscard]] ResourceSharing::TimeStamp<uint32_t, uint32_t> GetTimeStamp() const;

 private:
  const ResourceSharing::LockStatus lockStatus_;

  const ResourceSharing::TimeStamp<uint32_t, uint32_t> timeStamp_;
};

}  // namespace Messages::Data
