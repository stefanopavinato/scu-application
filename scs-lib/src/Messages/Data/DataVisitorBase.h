/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Messages::Visitor {
class VisitorPropertyGet;
class VisitorPropertySet;
}  // namespace Messages::Visitor
namespace Messages::Data {
class DataStatus;
class DataLock;
class RequestOverview;
class RequestPropertyGet;
class RequestPropertySet;
class RequestFirmwareRegisterRead;
class RequestFirmwareRegisterWrite;
class ResponseOverview;
class ResponsePropertyGet;
class ResponsePropertySet;
class ResponseFirmwareRegisterRead;
class ResponseFirmwareRegisterWrite;
class DataVisitorBase {
 public:
  virtual ~DataVisitorBase() = default;

  virtual bool Visit(DataStatus * data);

  virtual bool Visit(DataLock * data);

  virtual bool Visit(RequestOverview * data);

  virtual bool Visit(RequestPropertyGet * data);

  virtual bool Visit(RequestPropertySet * data);

  virtual bool Visit(RequestFirmwareRegisterRead * data);

  virtual bool Visit(RequestFirmwareRegisterWrite * data);

  virtual bool Visit(ResponseOverview * data);

  virtual bool Visit(ResponsePropertyGet * data);

  virtual bool Visit(ResponsePropertySet * data);

  virtual bool Visit(ResponseFirmwareRegisterRead * data);

  virtual bool Visit(ResponseFirmwareRegisterWrite * data);

  virtual bool Visit(Visitor::VisitorPropertyGet * data);

  virtual bool Visit(Visitor::VisitorPropertySet * data);
};
}  // namespace Messages::Data
