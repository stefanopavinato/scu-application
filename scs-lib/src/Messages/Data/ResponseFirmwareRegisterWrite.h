/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <optional>
#include "DataBase.h"

namespace Messages::Data {
class ResponseFirmwareRegisterWrite :
  public DataBase {
 public:
  ResponseFirmwareRegisterWrite(
    const uint32_t & address,
    const uint32_t & value);

  bool Accept(DataVisitorBase *visitor) override;

  [[nodiscard]] uint32_t GetAddress() const;

  [[nodiscard]] uint32_t GetValue() const;

  static std::optional<std::unique_ptr<ResponseFirmwareRegisterWrite>> Create(DataBase * request);

 private:
  const uint32_t address_;

  const uint32_t value_;
};
}  // namespace Messages::Data
