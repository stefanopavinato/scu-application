/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "DataBase.h"

namespace Messages::Data {
class RequestFirmwareRegisterRead :
  public DataBase {
 public:
  explicit RequestFirmwareRegisterRead(
    const uint32_t & address);

  [[nodiscard]] bool Accept(DataVisitorBase * visitor) override;

  [[nodiscard]] uint32_t GetAddress() const;

 private:
  const uint32_t address_;
};
}  // namespace Messages::Data
