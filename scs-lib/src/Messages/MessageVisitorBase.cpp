/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/MessageData.h"
#include "Messages/MessageDataArray.h"
#include "MessageVisitorBase.h"

namespace Messages {

bool MessageVisitorBase::Visit(Message *message) {
  (void) message;
  return false;
}

bool MessageVisitorBase::Visit(MessageData * message) {
  (void) message;
  return false;
}

bool MessageVisitorBase::Visit(MessageDataArray * message) {
  (void) message;
  return false;
}

}  // namespace Messages
