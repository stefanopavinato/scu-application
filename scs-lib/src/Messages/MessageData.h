/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MessageBase.h"

namespace Messages {
namespace Data {
class DataBase;
}  // namespace Data
class MessageData :
  public MessageBase {
 public:
  MessageData();

  ~MessageData() override = default;

  bool Accept(MessageVisitorBase * visitor) override;

  [[nodiscard]] Data::DataBase * GetData() const;

  void SetData(std::unique_ptr<Data::DataBase> data);

 private:
  std::unique_ptr<Data::DataBase> data_;
};
}  // namespace Messages
