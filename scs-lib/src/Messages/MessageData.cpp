/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Data/DataBase.h"
#include "MessageVisitorBase.h"
#include "MessageData.h"

namespace Messages {

MessageData::MessageData()
  : MessageBase() {
}

bool MessageData::Accept(MessageVisitorBase * visitor) {
  return visitor->Visit(this);
}

Data::DataBase * MessageData::GetData() const {
  return data_.get();
}

void MessageData::SetData(std::unique_ptr<Data::DataBase> data) {
  data_ = std::move(data);
}

}  // namespace Messages
