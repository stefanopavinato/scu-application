/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************


#include "TypeScaler.h"

namespace Utils {

template<typename VALUE, typename SCALE>
bool TypeScaler::InnerTypeScaler<VALUE, SCALE>::ScaleWrite(
  const VALUE &in,
  const SCALE &multiply,
  const SCALE &divide,
  const SCALE &offset,
  SCALE *out) {
  auto tempVal = (in - offset) * divide;
  *out = (static_cast<SCALE>((tempVal / multiply)));
  return true;
}

template<typename VALUE, typename SCALE>
bool TypeScaler::InnerTypeScaler<VALUE, SCALE>::ScaleRead(
  const SCALE &in,
  const SCALE &multiply,
  const SCALE &divide,
  const SCALE &offset,
  VALUE *out) {
  auto tempVal = in * multiply;
  *out = static_cast<VALUE>((tempVal / divide) + offset);
  return true;
}

template<>
bool TypeScaler::InnerTypeScaler<Types::Value<int32_t>, int32_t>::ScaleWrite(
  const Types::Value<int32_t> &in,
  const int32_t &multiply,
  const int32_t &divide,
  const int32_t &offset,
  int32_t *out) {
  auto tempVal = (in.GetValue() - offset) * divide;
  *out = (static_cast<int32_t>((tempVal / multiply)));
  return true;
}

template<>
bool TypeScaler::InnerTypeScaler<Types::Value<double>, double>::ScaleWrite(
  const Types::Value<double> &in,
  const double &multiply,
  const double &divide,
  const double &offset,
  double *out) {
  auto tempVal = (in.GetValue() - offset) * divide;
  *out = (static_cast<double>((tempVal / multiply)));
  return true;
}

template<>
bool TypeScaler::InnerTypeScaler<Types::Value<int32_t>, int32_t>::ScaleRead(
  const int32_t &in,
  const int32_t &multiply,
  const int32_t &divide,
  const int32_t &offset,
  Types::Value<int32_t> *out) {
  auto tempVal = in * multiply;
  out->SetValue(static_cast<int32_t>((tempVal / divide) + offset));
  return true;
}

template<>
bool TypeScaler::InnerTypeScaler<Types::Value<double>, double>::ScaleRead(
  const double &in,
  const double &multiply,
  const double &divide,
  const double &offset,
  Types::Value<double> *out) {
  auto tempVal = in * multiply;
  out->SetValue(static_cast<double>((tempVal / divide) + offset));
  return true;
}

}  // namespace Utils
