//// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ThreadHandler.h"

namespace Utils {

ThreadHandler::ThreadHandler(
  Logger::ILogger * logger)
  : logger_(logger) {
}

bool ThreadHandler::AddThread(std::unique_ptr<ThreadBase> thread) {
  if (!thread->Start()) {
    return false;
  }
  threads_.push_back(std::move(thread));
  return true;
}

bool ThreadHandler::AllThreadsRunning() const {
  for (const auto & thread : threads_) {
    if (!thread->IsRunning()) {
      logger_->Warning(std::string("Thread not running: ").append(thread->Name()));
      return false;
    }
  }
  return true;
}

bool ThreadHandler::Stop() {
  for (auto &thread : threads_) {
    thread->Stop();
  }
  return true;
}

}  // namespace Utils
