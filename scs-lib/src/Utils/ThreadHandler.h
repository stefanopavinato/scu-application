//// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "ThreadBase.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Utils {
class ThreadHandler {
 public:
  explicit ThreadHandler(
    Logger::ILogger * logger);

  bool AddThread(std::unique_ptr<ThreadBase> thread);

  [[nodiscard]] bool AllThreadsRunning() const;

  [[nodiscard]] bool Stop();

 private:
  Logger::ILogger * logger_;

  std::list<std::unique_ptr<ThreadBase>> threads_;
};
}  // namespace Utils
