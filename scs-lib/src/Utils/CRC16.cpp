/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "CRC16.h"

namespace Utils {

uint16_t CRC16::Calculate(const std::vector<char> &data) {
  uint16_t crc = 0xFFFF;
  for (char pos : data) {
    crc = static_cast<uint16_t>(crc ^ (static_cast<uint16_t>(pos) << 8));
    for (uint32_t i = 0; i < 8; i++) {
      if (crc & 0x8000) {
        crc = static_cast<uint16_t>(crc << 1) ^ 0x1021;
      } else {
        crc = static_cast<uint16_t>(crc << 1);
      }
    }
  }
  return crc & 0xFFFF;
}

}  // namespace Utils
