/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <vector>

namespace Utils {
class CRC16 {
 public:
  // CRC-16/CCITT-FALSE from https://gist.github.com/tijnkooijmans/10981093
  // CRC-Calc: https://crccalc.com/
  static uint16_t Calculate(const std::vector<char> &data);
};
}  // namespace Utils



