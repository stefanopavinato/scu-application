/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************


namespace Utils::Filter {

template <class T>
Filter<T>::Filter(
  const uint32_t & length)
  : length_(length)
  , sum_(0)
  , mean_(0) {
}

template <class T>
void Filter<T>::Push(const T & value) {
  if (values_.size() < length_) {
    sum_ += value;
    values_.push(value);
  } else {
    sum_ += value - values_.front();
    values_.pop();
    values_.push(value);
  }
  mean_ = sum_ / length_;
}

template <class T>
T Filter<T>::Update() {
  return mean_;
}


}  // namespace Utils::Filter
