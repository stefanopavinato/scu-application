/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ParserNodeUnary.h"

namespace Utils::Parser {
class ParserNodeNot : public ParserNodeUnary {
 public:
  explicit ParserNodeNot(
    std::unique_ptr<ParserNode> expression);

  bool Evaluate() override;
};
}  // namespace Utils::Parser
