/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeNot.h"

namespace Utils::Parser {
ParserNodeNot::ParserNodeNot(
  std::unique_ptr<ParserNode> expression)
  : ParserNodeUnary(
  std::move(expression)) {
}

bool ParserNodeNot::Evaluate() {
  return !Expression()->Evaluate();
}

}  // namespace Utils::Parser
