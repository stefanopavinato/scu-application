/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <memory>
#include <list>

namespace Utils::Parser {
class Token {
 public:
  enum class Type {
    Unknown,
    Variable,
    Operator,
    LeftParen,
    RightParen
  };

  enum class Operator {
    Or,
    And,
    Xor,
    Not,
    None
  };

  Token(
    const Type &type,
    const std::string &str,
    const Operator &op = Operator::None,
    const bool &isLeftAssociative = true,
    const int &precedence = -1);

  [[nodiscard]] Type GetType() const;

  [[nodiscard]] Operator GetOperator() const;

  [[nodiscard]] std::string GetString() const;

  [[nodiscard]] int GetPrecedence() const;

  [[nodiscard]] bool IsLeftAssociative() const;

 private:
  Type type_;

  Operator operator_;

  std::string str_;

  int precedence_;

  bool isLeftAssociative_;
};
}  // namespace Utils::Parser
