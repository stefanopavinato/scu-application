/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeBinary.h"

namespace Utils::Parser {

ParserNodeBinary::ParserNodeBinary(
  std::unique_ptr<ParserNode> expressionLeft,
  std::unique_ptr<ParserNode> expresionRight)
  : expressionLeft_(std::move(expressionLeft)), expressionRight_(std::move(expresionRight)) {
}

ParserNode *ParserNodeBinary::ExpressionLeft() const {
  return expressionLeft_.get();
}

ParserNode *ParserNodeBinary::ExpressionRight() const {
  return expressionRight_.get();
}

}  // namespace Utils::Parser
