/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "ParserNode.h"

namespace Utils::Parser {
class ParserNodeUnary : public ParserNode {
 public:
  ParserNode *Expression() const;

 protected:
  explicit ParserNodeUnary(
    std::unique_ptr<ParserNode> expression);

 private:
  std::unique_ptr<ParserNode> expression_;
};
}  // namespace Utils::Parser
