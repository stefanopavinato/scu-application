/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "ParserNode.h"

namespace Utils::Parser {
class ParserNodeBinary : public ParserNode {
 public:
  ParserNode *ExpressionLeft() const;

  ParserNode *ExpressionRight() const;

 protected:
  ParserNodeBinary(
    std::unique_ptr<ParserNode> expressionLeft,
    std::unique_ptr<ParserNode> expressionRight);

 private:
  std::unique_ptr<ParserNode> expressionLeft_;
  std::unique_ptr<ParserNode> expressionRight_;
};
}  // namespace Utils::Parser
