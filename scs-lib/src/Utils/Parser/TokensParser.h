/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <string>
#include <list>
#include "Token.h"

namespace Utils::Parser {
class TokensParser {
 public:
  static std::unique_ptr<std::list<Token>> Parse(std::string const &expression);
};
}  // namespace Utils::Parser
