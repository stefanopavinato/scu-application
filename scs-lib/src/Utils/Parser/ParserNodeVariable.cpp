/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeVariable.h"

namespace Utils::Parser {
ParserNodeVariable::ParserNodeVariable(
  bool const *variable)
  : variable_(variable) {
}

bool ParserNodeVariable::Evaluate() {
  return *variable_;
}

}  // namespace Utils::Parser
