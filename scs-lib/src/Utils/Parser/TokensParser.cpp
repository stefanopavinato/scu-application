/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "TokensParser.h"

namespace Utils::Parser {
std::unique_ptr<std::list<Token>> TokensParser::Parse(std::string const &expression) {
  auto tokens = std::make_unique<std::list<Token>>();
  std::string token;
  for (const auto *p = expression.c_str(); *p; ++p) {
    if (isalpha(*p)) {
      const auto *b = p;
      for (; isalpha(*p); ++p) {}
      const auto s = std::string(b, p);
      tokens->push_back(Token(Token::Type::Variable, s));
      --p;
    } else {
      switch (*p) {
        case '(': {
          tokens->push_back(
            Token(
              Token::Type::LeftParen,
              std::string(1, *p)));
          break;
        }
        case ')': {
          tokens->push_back(
            Token(
              Token::Type::RightParen,
              std::string(1, *p)));
          break;
        }
        case '!': {
          tokens->push_back(
            Token(
              Token::Type::Operator,
              std::string(1, *p),
              Token::Operator::Not,
              false,
              4));
          break;
        }
        case '^': {
          tokens->push_back(
            Token(
              Token::Type::Operator,
              std::string(1, *p),
              Token::Operator::Xor,
              true,
              2));
          break;
        }
        case '&': {
          ++p;
          if ((*p) == '&') {
            tokens->push_back(
              Token(
                Token::Type::Operator,
                std::string(2, *p),
                Token::Operator::And,
                true,
                3));
          } else {
            tokens->push_back((
                                Token(
                                  Token::Type::Unknown,
                                  std::string(2, *p))));
          }
          break;
        }
        case '|': {
          ++p;
          if ((*p) == '|') {
            tokens->push_back(
              Token(
                Token::Type::Operator,
                std::string(2, *p),
                Token::Operator::Or,
                true,
                1));
          } else {
            tokens->push_back(
              (Token(
                Token::Type::Unknown,
                std::string(2, *p))));
          }
          break;
        }
        default: {
          break;
        }
      }
    }
  }
  return tokens;
}

}  // namespace Utils::Parser
