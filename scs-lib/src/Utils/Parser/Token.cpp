/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Token.h"

namespace Utils::Parser {

Token::Token(
  const Type &type,
  const std::string &str,
  const Operator &op,
  const bool &isLeftAssociative,
  const int &precedence)
  : type_(type), operator_(op), str_(str), precedence_(precedence), isLeftAssociative_(isLeftAssociative) {
}

Token::Type Token::GetType() const {
  return type_;
}

Token::Operator Token::GetOperator() const {
  return operator_;
}

std::string Token::GetString() const {
  return str_;
}

int Token::GetPrecedence() const {
  return precedence_;
}

bool Token::IsLeftAssociative() const {
  return isLeftAssociative_;
}
}  // namespace Utils::Parser
