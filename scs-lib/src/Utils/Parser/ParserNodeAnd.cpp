/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParserNodeAnd.h"


namespace Utils::Parser {
ParserNodeAnd::ParserNodeAnd(
  std::unique_ptr<ParserNode> expressionLeft,
  std::unique_ptr<ParserNode> expressionRight)
  : ParserNodeBinary(
  std::move(expressionLeft),
  std::move(expressionRight)) {
}

bool ParserNodeAnd::Evaluate() {
  return ExpressionLeft()->Evaluate() && ExpressionRight()->Evaluate();
}

}  // namespace Utils::Parser
