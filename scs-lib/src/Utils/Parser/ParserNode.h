/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Utils::Parser {
class ParserNode {
 public:
  virtual bool Evaluate() = 0;
};
}  // namespace Utils::Parser
