/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include "TimeFormatter.h"

namespace Utils {

std::string TimeFormatter::ToString(const std::chrono::system_clock::time_point &timePoint) {
  // Get date and time
  auto dateTime = std::chrono::system_clock::to_time_t(timePoint);
  auto stringDateTime = std::string(20, '\0');
  std::strftime(
    &stringDateTime[0],
    stringDateTime.size(),
    "%Y-%m-%d %H:%M:%S",
    std::localtime(&dateTime));
  stringDateTime.pop_back();
  // Get milli seconds
  auto seconds = std::chrono::time_point_cast<std::chrono::seconds>(timePoint);
  auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(timePoint - seconds);
  std::string stringMilliseconds = "";
  if (milliseconds.count() >= 100) {
    stringMilliseconds.append(std::to_string(milliseconds.count()));
  } else if (milliseconds.count() >= 10) {
    stringMilliseconds.append("0").append(std::to_string(milliseconds.count()));
  } else {
    stringMilliseconds.append("00").append(std::to_string(milliseconds.count()));
  }
  // Return formatted string
  return stringDateTime.append(".").append(stringMilliseconds);
}

}  // namespace Utils
