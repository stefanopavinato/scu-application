/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include <queue>

namespace Utils::Filter {
template <class T>
class Filter{
 public:
  explicit Filter(
    const uint32_t & length);

  void Push(const T & value);

  [[nodiscard]] T Update();

 private:
  std::queue<T> values_;

  const uint32_t length_;

  T sum_;

  T mean_;
};
}  // namespace Utils::Filter

#include "Filter.tpp"
