/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Types/OplStatus.h"
#include "Types/ScuStatus.h"

namespace Utils {
class TypeConverter {
 public:
  template<typename TYPE_IN, typename TYPE_OUT = TYPE_IN>
  static bool Convert(
    const TYPE_IN &in,
    TYPE_OUT *out);

 private:
  template<typename TYPE_IN, typename TYPE_OUT = TYPE_IN>
  class InnerTypeConverter {
   public:
    static bool Convert(
      const TYPE_IN &in,
      TYPE_OUT *out);
  };
};

template<typename TYPE_IN, typename TYPE_OUT>
bool TypeConverter::Convert(
  const TYPE_IN &in,
  TYPE_OUT *out) {
  return InnerTypeConverter<TYPE_IN, TYPE_OUT>::Convert(
    in,
    out);
}

// Converter used if no conversion is needed
template
class TypeConverter::InnerTypeConverter<int32_t>;

template
class TypeConverter::InnerTypeConverter<int16_t>;

template
class TypeConverter::InnerTypeConverter<int8_t>;

template
class TypeConverter::InnerTypeConverter<uint32_t>;

template
class TypeConverter::InnerTypeConverter<uint16_t>;

template
class TypeConverter::InnerTypeConverter<uint8_t>;

template
class TypeConverter::InnerTypeConverter<double>;

template
class TypeConverter::InnerTypeConverter<float>;

template
class TypeConverter::InnerTypeConverter<bool>;

template
class TypeConverter::InnerTypeConverter<std::string>;

// Converter used in scu driver
template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint32_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint16_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<uint8_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int32_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int16_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<int8_t>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<double>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::Value<float>>;

template
class TypeConverter::InnerTypeConverter<uint64_t, uint32_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, uint16_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, uint8_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, int32_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, int16_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, int8_t>;

template
class TypeConverter::InnerTypeConverter<uint64_t, double>;

template
class TypeConverter::InnerTypeConverter<uint64_t, float>;

template
class TypeConverter::InnerTypeConverter<uint64_t, bool>;

template
class TypeConverter::InnerTypeConverter<uint64_t, std::string>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::ModuleType>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::OplStatus>;

template
class TypeConverter::InnerTypeConverter<uint64_t, Types::ScuStatus>;

template
class TypeConverter::InnerTypeConverter<uint64_t, std::chrono::minutes>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint32_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint16_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint8_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int32_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int16_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int8_t>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<double>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<float>, uint64_t>;

template
class TypeConverter::InnerTypeConverter<uint32_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<uint16_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<uint8_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<int32_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<int16_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<int8_t, uint64_t>;

template
class TypeConverter::InnerTypeConverter<double, uint64_t>;

template
class TypeConverter::InnerTypeConverter<float, uint64_t>;

template
class TypeConverter::InnerTypeConverter<bool, uint64_t>;

template
class TypeConverter::InnerTypeConverter<std::string, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::ModuleType, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::ScuStatus, uint64_t>;

template
class TypeConverter::InnerTypeConverter<Types::OplStatus, uint64_t>;

template
class TypeConverter::InnerTypeConverter<std::chrono::minutes, uint64_t>;

// Converter used in eeprom
template
class TypeConverter::InnerTypeConverter<Types::ModuleType, std::string>;

template
class TypeConverter::InnerTypeConverter<std::string, Types::ModuleType>;

template
class TypeConverter::InnerTypeConverter<Types::ModuleType, uint32_t>;

template
class TypeConverter::InnerTypeConverter<uint32_t, Types::ModuleType>;

template
class TypeConverter::InnerTypeConverter<std::chrono::minutes, uint32_t>;

template
class TypeConverter::InnerTypeConverter<int32_t, std::string>;

template
class TypeConverter::InnerTypeConverter<uint32_t, std::string>;

template
class TypeConverter::InnerTypeConverter<uint8_t, std::string>;

template
class TypeConverter::InnerTypeConverter<double, std::string>;

template
class TypeConverter::InnerTypeConverter<std::string, int32_t>;

template
class TypeConverter::InnerTypeConverter<std::string, uint32_t>;

template
class TypeConverter::InnerTypeConverter<std::string, uint8_t>;

template
class TypeConverter::InnerTypeConverter<std::string, double>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int32_t>, int32_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int16_t>, int16_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<int8_t>, int8_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint32_t>, uint32_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint16_t>, uint16_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<uint8_t>, uint8_t>;

template
class TypeConverter::InnerTypeConverter<Types::Value<double>, double>;

template
class TypeConverter::InnerTypeConverter<Types::Value<float>, float>;

template
class TypeConverter::InnerTypeConverter<int32_t, Types::Value<int32_t>>;

template
class TypeConverter::InnerTypeConverter<int16_t, Types::Value<int16_t>>;

template
class TypeConverter::InnerTypeConverter<int8_t, Types::Value<int8_t>>;

template
class TypeConverter::InnerTypeConverter<uint32_t, Types::Value<uint32_t>>;

template
class TypeConverter::InnerTypeConverter<uint16_t, Types::Value<uint16_t>>;

template
class TypeConverter::InnerTypeConverter<uint8_t, Types::Value<uint8_t>>;

template
class TypeConverter::InnerTypeConverter<double, Types::Value<double>>;

template
class TypeConverter::InnerTypeConverter<float, Types::Value<float>>;
}  // namespace Utils
