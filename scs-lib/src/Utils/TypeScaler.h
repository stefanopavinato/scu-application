/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"

namespace Utils {
class TypeScaler {
 public:
  template<typename VALUE, typename SCALE = VALUE>
  static bool ScaleWrite(
    const VALUE &in,
    const SCALE &multiply,
    const SCALE &divide,
    const SCALE &offset,
    SCALE *out);

  template<typename VALUE, typename SCALE = VALUE>
  static bool ScaleRead(
    const SCALE &in,
    const SCALE &multiply,
    const SCALE &divide,
    const SCALE &offset,
    VALUE *out);

 private:
  template<typename VALUE, typename SCALE = VALUE>
  class InnerTypeScaler {
   public:
    static bool ScaleWrite(
      const VALUE &in,
      const SCALE &multiply,
      const SCALE &divide,
      const SCALE &offset,
      SCALE *out);

    static bool ScaleRead(
      const SCALE &in,
      const SCALE &multiply,
      const SCALE &divide,
      const SCALE &offset,
      VALUE *out);
  };
};

template<typename VALUE, typename SCALE>
bool TypeScaler::ScaleWrite(
  const VALUE &in,
  const SCALE &multiply,
  const SCALE &divide,
  const SCALE &offset,
  SCALE *out) {
  return InnerTypeScaler<VALUE, SCALE>::ScaleWrite(
    in,
    multiply,
    divide,
    offset,
    out);
}

template<typename VALUE, typename SCALE>
bool TypeScaler::ScaleRead(
  const SCALE &in,
  const SCALE &multiply,
  const SCALE &divide,
  const SCALE &offset,
  VALUE *out) {
  return InnerTypeScaler<VALUE, SCALE>::ScaleRead(
    in,
    multiply,
    divide,
    offset,
    out);
}

template
class TypeScaler::InnerTypeScaler<int32_t>;

template
class TypeScaler::InnerTypeScaler<uint32_t>;

template
class TypeScaler::InnerTypeScaler<uint8_t>;

template
class TypeScaler::InnerTypeScaler<double>;

template
class TypeScaler::InnerTypeScaler<Types::Value<int32_t>, int32_t>;

template
class TypeScaler::InnerTypeScaler<Types::Value<double>, double>;

}  // namespace Utils
