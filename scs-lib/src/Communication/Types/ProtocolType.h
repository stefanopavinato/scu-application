/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Communication::Types {
class ProtocolType;
}  // namespace Communication::Types

template<>
class std::hash<Communication::Types::ProtocolType> {
 public:
  size_t operator()(const Communication::Types::ProtocolType & protocol) const;
};

namespace Communication::Types {
class ProtocolType {
  friend size_t std::hash<ProtocolType>::operator()(const ProtocolType & protocol) const;

 public:
  enum class Enum {
    FBIS_NETWORK,
    JSON,
    ARBITRATION,
    NONE
  };

  ProtocolType();

  ProtocolType(const ProtocolType &other) = default;

  explicit ProtocolType(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  bool operator==(const ProtocolType &other) const;

  friend std::ostream &operator<<(std::ostream &out, const ProtocolType &protocol) {
    return out << protocol.ToString();
  }

  static std::string ToString(Enum value_);

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kFbisNetwork[];
  static const char kJson[];
  static const char kArbitration[];
  static const char kNone[];
};
}  // namespace Communication::Types
