/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ProtocolType.h"

namespace Communication::Types {

const char ProtocolType::kTypeName[] = "ProtocolType";

const char ProtocolType::kFbisNetwork[] = "FBIS-Network";
const char ProtocolType::kJson[] = "Json";
const char ProtocolType::kArbitration[] = "Arbitration";
const char ProtocolType::kNone[] = "None";

ProtocolType::ProtocolType()
  : value_(Enum::NONE) {
}

ProtocolType::ProtocolType(const Enum &value)
  : value_(value) {
}

std::string ProtocolType::GetTypeName() {
  return kTypeName;
}

ProtocolType::Enum ProtocolType::GetValue() const {
  return value_;
}

void ProtocolType::SetValue(const Enum &value) {
  value_ = value;
}

bool ProtocolType::operator==(const ProtocolType &other) const {
  return value_ == other.GetValue();
}


std::string ProtocolType::ToString() const {
  switch (value_) {
    case Enum::FBIS_NETWORK: {
      return kFbisNetwork;
    }
    case Enum::JSON: {
      return kJson;
    }
    case Enum::ARBITRATION: {
      return kArbitration;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void ProtocolType::FromString(const std::string &str) {
  if (str == kFbisNetwork) {
    value_ = Enum::FBIS_NETWORK;
  } else if (str == kJson) {
    value_ = Enum::JSON;
  } else if (str == kArbitration) {
    value_ = Enum::ARBITRATION;
  } else {
    value_ = Enum::NONE;
  }
}

std::string ProtocolType::ToString(Enum value_) {
  return ProtocolType(value_).ToString();
}

}  // namespace Communication::Types

size_t std::hash<Communication::Types::ProtocolType>::operator()(
  const Communication::Types::ProtocolType & protocol) const {
  return std::hash<Communication::Types::ProtocolType::Enum>()(protocol.value_);
}
