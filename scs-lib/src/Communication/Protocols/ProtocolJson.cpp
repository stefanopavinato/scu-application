/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <optional>
#include <json/json.h>
#include <sstream>
#include <ostream>
#include "Logger/ILogger.h"
#include "Messages/MessageData.h"
#include "Messages/Data/DataBase.h"
#include "Communication/Protocols/Json/JsonAddress.h"
#include "Communication/Protocols/Json/JsonData.h"
#include "ProtocolJson.h"

namespace Communication::Protocols {

const char ProtocolJson::kAddress[] = "address";
const char ProtocolJson::kData[] = "data";

ProtocolJson::ProtocolJson(Logger::ILogger *logger)
  : ProtocolBase(
  logger,
  Types::ProtocolType::Enum::JSON) {
}

bool ProtocolJson::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ProtocolJson::Visit(Messages::MessageData *message) {
  auto json = ::Json::Value();
  json[kAddress] = Json::JsonAddress::Serialize(message->GetSender());
  json[kData] = Json::JsonData::Serialize(message->GetData());
  // Set string format
  ::Json::StreamWriterBuilder builder;
  builder["commentStyle"] = "None";
  builder["indentation"] = "  ";
  builder["dropNullPlaceholders"] = true;
  // Build string
  std::unique_ptr<::Json::StreamWriter> writer(builder.newStreamWriter());
  auto outputStream = std::stringstream();
  writer->write(json, &outputStream);
  auto str = json.toStyledString();
  // Set output vector
  out_ = std::make_unique<std::vector<char>>();
  out_->insert(
    std::begin(*out_),
    std::istreambuf_iterator<char>{outputStream},
    std::istreambuf_iterator<char>{});
  return true;
}

std::optional<std::unique_ptr<Messages::MessageBase>> ProtocolJson::Deserialize(
  std::unique_ptr<std::vector<char>> input) const {
  // Parse input to json
  ::Json::Value value;
  std::string errors;
  ::Json::CharReaderBuilder builder;
  auto inputStream = std::stringstream();
  inputStream << input->data();
  if (!::Json::parseFromStream(builder, inputStream, &value, &errors)) {
    Logger()->Error("Failed to parse json");
    return std::nullopt;
  }
  // Create message
  auto message = std::make_unique<Messages::MessageData>();
  // Parse address
  if (!value.isMember(kAddress) || !value[kAddress].isObject()) {
    Logger()->Error("Address object not found");
    return std::nullopt;
  }
  auto address = Json::JsonAddress::DeSerialize(value[kAddress]);
  if (!address) {
    Logger()->Error("Failed to parse address");
    return std::nullopt;
  }
  message->SetReceiver(std::move(address.value()));
  // Parse data
  if (!value.isMember(kData) || !value[kData].isObject()) {
    Logger()->Error("Data object not found");
    return std::nullopt;
  }
  auto data = Json::JsonData::DeSerialize(value[kData]);
  if (!data) {
    Logger()->Error("Failed to parse data");
    return std::nullopt;
  }
  message->SetData(std::move(data.value()));
  // Return message
  return std::move(message);
}

}  // namespace Communication::Protocols
