/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <optional>
#include "Communication/Types/ProtocolType.h"

namespace Communication::Protocols {
class ProtocolBase;

class ProtocolFactory {
 public:
  static std::optional<std::unique_ptr<ProtocolBase>> Create(
    Logger::ILogger * logger,
    const Types::ProtocolType::Enum &protocolType);
};

}  // namespace Communication::Protocols
