/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Logger/ILogger.h"
#include "Messages/Addresses/AddressLock.h"
#include "Messages/Addresses/AddressVisitor.h"
#include "Messages/MessageData.h"
#include "Messages/Data/DataLock.h"
#include "Messages/Data/DataVisitor.h"
#include "ProtocolArbitration.h"

namespace Communication::Protocols {

ProtocolArbitration::ProtocolArbitration(Logger::ILogger * logger)
  : ProtocolBase(
  logger,
  Types::ProtocolType::Enum::ARBITRATION) {
}

bool ProtocolArbitration::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ProtocolArbitration::Visit(Messages::MessageData * message) {
  // Clear out vector
  out_ = std::make_unique<std::vector<char>>();
  out_->resize(13);
  uint32_t i = 0;
  // Serialize lock id
  auto address = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressLock>();
  if (!message->GetSender()->Accept(&address)) {
    Logger()->Error("Lock address invalid");
    return false;
  }
  auto data = Messages::Data::DataVisitor<Messages::Data::DataLock>();
  if (!message->GetData()->Accept(&data)) {
    Logger()->Error("Lock data invalid");
    return false;
  }
  auto lockId = static_cast<uint32_t>(address.GetAddress()->GetId().GetValue());
  (*out_)[i++] = static_cast<char>((lockId >> 24) & 0xFF);
  (*out_)[i++] = static_cast<char>((lockId >> 16) & 0xFF);
  (*out_)[i++] = static_cast<char>((lockId >> 8) & 0xFF);
  (*out_)[i++] = static_cast<char>((lockId >> 0) & 0xFF);
  // Serialize status
  auto status = data.GetData()->GetLockStatus();
  (*out_)[i++] = static_cast<char>(status.GetValue());
  // Serialize timestamp time
  auto time = data.GetData()->GetTimeStamp().GetTime();
  (*out_)[i++] = static_cast<char>((time >> 24) & 0xFF);
  (*out_)[i++] = static_cast<char>((time >> 16) & 0xFF);
  (*out_)[i++] = static_cast<char>((time >> 8) & 0xFF);
  (*out_)[i++] = static_cast<char>((time >> 0) & 0xFF);
  // Serialize timestamp priority
  auto priority = data.GetData()->GetTimeStamp().GetPriority();
  (*out_)[i++] = static_cast<char>((priority >> 24) & 0xFF);
  (*out_)[i++] = static_cast<char>((priority >> 16) & 0xFF);
  (*out_)[i++] = static_cast<char>((priority >> 8) & 0xFF);
  (*out_)[i++] = static_cast<char>((priority >> 0) & 0xFF);
  return true;
}

std::optional<std::unique_ptr<Messages::MessageBase>> ProtocolArbitration::Deserialize(
  std::unique_ptr<std::vector<char>> input) const {
  // Deserializer lock id
  uint32_t lockId = input->at(0) << 24;
  lockId |= input->at(1) << 16;
  lockId |= input->at(2) << 8;
  lockId |= input->at(3) << 0;
  auto lockIdEnum = ::Types::LockId::ToEnum(lockId);
  // Deserializer status
  auto lockStatusEnum = ResourceSharing::LockStatus::ToEnum(input->at(4));
  // Deserialize timestamp time
  uint32_t time = input->at(5) << 24;
  time |= input->at(6) << 16;
  time |= input->at(7) << 8;
  time |= input->at(8) << 0;
  // Deserialize timestamp priority
  uint32_t priority = input->at(9) << 24;
  priority |= input->at(10) << 16;
  priority |= input->at(11) << 8;
  priority |= input->at(12) << 0;
  // Create message
  auto message = std::make_unique<Messages::MessageData>();
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressLock>(lockIdEnum));
  message->SetData(std::make_unique<Messages::Data::DataLock>(
    ResourceSharing::TimeStamp(time, priority),
    ResourceSharing::LockStatus(lockStatusEnum)));
  return std::move(message);
}

}  // namespace Communication::Protocols
