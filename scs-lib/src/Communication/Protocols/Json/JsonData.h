/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include <json/json.h>

namespace Messages::Data {
class DataBase;
class RequestOverview;
}  // namespace Messages::Data
namespace Communication::Protocols::Json {
class JsonData {
 public:
  static ::Json::Value Serialize(Messages::Data::DataBase * data);

  static std::optional<std::unique_ptr<Messages::Data::DataBase>> DeSerialize(
    const ::Json::Value &input);

 private:
  static const char kStatus[];
  static const char kDescription[];

  static const char kPath[];

  static const char kLevel[];
  static const char kDetail[];

  static const char kAttribute[];
  static const char kType[];
  static const char kValue[];

  static const char kAddress[];
  static const char kDirection[];
};
}  // namespace Communication::Protocols::Json
