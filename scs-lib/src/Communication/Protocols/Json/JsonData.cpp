/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Direction.h"
#include "Messages/Data/DataVisitor.h"
#include "Messages/Data/RequestOverview.h"
#include "Messages/Data/RequestPropertyGet.h"
#include "Messages/Data/RequestPropertySet.h"
#include "Messages/Data/RequestFirmwareRegisterRead.h"
#include "Messages/Data/RequestFirmwareRegisterWrite.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/Data/ResponsePropertyGet.h"
#include "Messages/Data/ResponsePropertySet.h"
#include "Messages/Data/ResponseFirmwareRegisterRead.h"
#include "Messages/Data/ResponseFirmwareRegisterWrite.h"
#include "Messages/Data/DataStatus.h"
#include "JsonData.h"

namespace Communication::Protocols::Json {

const char JsonData::kStatus[] = "status";
const char JsonData::kDescription[] = "description";

const char JsonData::kPath[] = "path";

const char JsonData::kLevel[] = "level";
const char JsonData::kDetail[] = "detail";

const char JsonData::kAttribute[] = "attribute";
const char JsonData::kType[] = "type";
const char JsonData::kValue[] = "value";

const char JsonData::kAddress[] = "address";
const char JsonData::kDirection[] = "direction";



::Json::Value JsonData::Serialize(Messages::Data::DataBase *data) {
  auto json = ::Json::Value();
  // Serializer request overview
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestOverview>();
    if (data->Accept(&visitor)) {
      json[kPath] = visitor.GetData()->GetPath();
      json[kLevel] = visitor.GetData()->GetLevel();
      json[kDetail] = visitor.GetData()->GetDetails().ToString();
      return json;
    }
  }
  // Serialize response overview
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::ResponseOverview>();
    if (data->Accept(&visitor)) {
      return visitor.GetData()->GetJson();
    }
  }
  // Serialize request property get
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestPropertyGet>();
    if (data->Accept(&visitor)) {
      json[kPath] = visitor.GetData()->GetPath();
      json[kAttribute] = visitor.GetData()->GetAttribute().ToString();
      return json;
    }
  }
  // Serialize response property get
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::ResponsePropertyGet>();
    if (data->Accept(&visitor)) {
      return json;
    }
  }
  // Serialize request property set
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestPropertySet>();
    if (data->Accept(&visitor)) {
      json[kPath] = visitor.GetData()->GetPath();
      json[kAttribute] = visitor.GetData()->GetAttribute().ToString();
      json[kType] = Types::VariantValueUtils::GetType(visitor.GetData()->GetValue()).ToString();
      json[kValue] = Types::VariantValueUtils::ToString(visitor.GetData()->GetValue());
      return json;
    }
  }
  // Serialize response property set
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::ResponsePropertySet>();
    if (data->Accept(&visitor)) {
      json[kStatus] = visitor.GetData();
      return json;
    }
  }
  // Serialize request firmware register read
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestFirmwareRegisterRead>();
    if (data->Accept(&visitor)) {
      json[kAddress] = visitor.GetData()->GetAddress();
      return json;
    }
  }
  // Serialize response firmware register read
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::ResponseFirmwareRegisterRead>();
    if (data->Accept(&visitor)) {
      json[kAddress] = visitor.GetData()->GetAddress();
      json[kValue] = visitor.GetData()->GetValue();
      json[kStatus] = "ok";
      return json;
    }
  }
  // Serialize request firmware register write
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::RequestFirmwareRegisterWrite>();
    if (data->Accept(&visitor)) {
      json[kAddress] = visitor.GetData()->GetAddress();
      json[kValue] = visitor.GetData()->GetValue();
      return json;
    }
  }
  // Serialize response firmware register write
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::ResponseFirmwareRegisterWrite>();
    if (data->Accept(&visitor)) {
      json[kAddress] = visitor.GetData()->GetAddress();
      json[kStatus] = "ok";
      return json;
    }
  }
  // Serialize data status
  {
    auto visitor = Messages::Data::DataVisitor<Messages::Data::DataStatus>();
    if (data->Accept(&visitor)) {
      json[kStatus] = visitor.GetData()->GetStatus().ToString();
      json[kDescription] = visitor.GetData()->GetDescription();
      return json;
    }
  }

  return json;
}
std::optional<std::unique_ptr<Messages::Data::DataBase>> JsonData::DeSerialize(
  const ::Json::Value &input) {
  // Parse request overview
  {
    if (input.isMember(kPath) && input[kPath].isString() &&
        input.isMember(kDetail) && input[kDetail].isString() &&
        input.isMember(kLevel) && input[kLevel].isInt()) {
      auto details = ::Types::JsonDetails();
      details.FromString(input[kDetail].asString());
      return std::make_unique<Messages::Data::RequestOverview>(
        input[kPath].asString(),
        input[kLevel].asInt(),
        details);
    }
  }
  // Parse request property set
  {
    if (input.isMember(kPath) && input[kPath].isString() &&
        input.isMember(kAttribute) && input[kAttribute].isString() &&
        input.isMember(kType) && input[kType].isString() &&
        input.isMember(kValue)) {
      auto attribute = Messages::Visitor::Types::AttributeType(input[kAttribute].asString());
      auto type = ::Types::ValueType(input[kType].asString());
      auto value = Types::VariantValueUtils::FromString(type, input[kValue].asString());
      if (!value) {
        return std::nullopt;
      }
      return std::make_unique<Messages::Data::RequestPropertySet>(
        input[kPath].asString(),
        attribute,
        value.value());
    }
  }
  // Parse request property get
  {
    if (input.isMember(kPath) && input[kPath].isString() &&
        input.isMember(kAttribute) && input[kAttribute].isString()) {
      auto attribute = Messages::Visitor::Types::AttributeType(input[kAttribute].asString());
      return std::make_unique<Messages::Data::RequestPropertyGet>(
        input[kPath].asString(),
        attribute);
    }
  }
  // Parse request firmware register write
  {
    if (input.isMember(kAddress) && input[kAddress].isInt() &&
        input.isMember(kValue) && input[kValue].isInt()) {
      return std::make_unique<Messages::Data::RequestFirmwareRegisterWrite>(
        input[kAddress].asInt(),
        input[kValue].asInt());
    }
  }
  // Parse request firmware register read
  {
    if (input.isMember(kAddress) && input[kAddress].isInt()) {
      return std::make_unique<Messages::Data::RequestFirmwareRegisterRead>(
        input[kAddress].asInt());
    }
  }
  // Parse data status
  {
    if (input.isMember(kStatus) && input[kStatus].isString() &&
        input.isMember(kDescription) && input[kDescription].isString()) {
      return std::make_unique<Messages::Data::DataStatus>(
        Messages::Types::DataStatus::FromString(input[kStatus].asString()),
        input[kDescription].asString());
    }
  }
  return std::nullopt;
}
}  // namespace Communication::Protocols::Json
