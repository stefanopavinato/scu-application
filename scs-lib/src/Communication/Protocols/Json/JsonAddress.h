/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include <json/json.h>

namespace Messages::Addresses {
class AddressBase;
}  // namespace Messages::Addresses
namespace Communication::Protocols::Json {
class JsonAddress {
 public:
  static ::Json::Value Serialize(Messages::Addresses::AddressBase * address);

  static std::optional<std::unique_ptr<Messages::Addresses::AddressBase>> DeSerialize(
    const ::Json::Value &input);

 private:
  static const char kModule[];
  static const char kIp[];
  static const char kPort[];
  static const char kProtocol[];
  static const char kId[];
};
}  // namespace Communication::Protocols::Json
