/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Addresses/AddressVisitor.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Addresses/AddressClient.h"
#include "Messages/Addresses/AddressLock.h"
#include "JsonAddress.h"

namespace Communication::Protocols::Json {

const char JsonAddress::kModule[] = "module";
const char JsonAddress::kIp[] = "ip";
const char JsonAddress::kPort[] = "port";
const char JsonAddress::kProtocol[] = "protocol";
const char JsonAddress::kId[] = "id";

::Json::Value JsonAddress::Serialize(Messages::Addresses::AddressBase *address) {
  auto json = ::Json::Value();
  // Serialize module address
  {
    auto visitor = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressModule>();
    if (address->Accept(&visitor)) {
      json[kModule] = visitor.GetAddress()->GetModule().ToString();
      return json;
    }
  }
  // Serialize client address
  {
    auto visitor = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressClient>();
    if (address->Accept(&visitor)) {
      json[kIp] = visitor.GetAddress()->GetIp();
      json[kProtocol] = visitor.GetAddress()->GetProtocol().ToString();
      return json;
    }
  }
  // Serialize lock address
  {
    auto visitor = Messages::Addresses::AddressVisitor<Messages::Addresses::AddressLock>();
    if (address->Accept(&visitor)) {
      json[kId] = visitor.GetAddress()->GetId().ToString();
      return json;
    }
  }
  return json;
}

std::optional<std::unique_ptr<Messages::Addresses::AddressBase>> JsonAddress::DeSerialize(
  const ::Json::Value &input) {
  // Parse module address
  {
    if (input.isMember(kModule) && input[kModule].isString()) {
      auto module = ::Types::SoftwareModule();
      module.FromString(input[kModule].asString());
      return std::make_unique<Messages::Addresses::AddressModule>(
        module.GetEnum());
    }
  }
  // Parse client address
  {
    if (input.isMember(kIp) && input[kIp].isString() &&
        input.isMember(kProtocol) && input[kProtocol].isString()) {
      auto protocol = Types::ProtocolType();
      protocol.FromString(input[kProtocol].asString());
      return std::make_unique<Messages::Addresses::AddressClient>(
        input[kIp].asString(),
        protocol);
    }
  }
  // Parse lock address
  {
    if (input.isMember(kId) && input[kId].isString()) {
      auto id = ::Types::LockId();
      id.FromString(input[kId].asString());
      return std::make_unique<Messages::Addresses::AddressLock>(
        id.GetValue());
    }
  }

  return std::nullopt;
}

}  // namespace Communication::Protocols::Json
