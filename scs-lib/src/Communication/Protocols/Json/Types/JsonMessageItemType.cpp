/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "JsonMessageItemType.h"

namespace Communication::Protocols::Json::Types {

const char JsonMessageItemType::kTypeName[] = "JsonMessageItemType";

const char JsonMessageItemType::kSetRequest[] = "Set";
const char JsonMessageItemType::kGetRequest[] = "Get";
const char JsonMessageItemType::kNone[] = "NONE";

JsonMessageItemType::JsonMessageItemType()
  : value_(Enum::NONE) {
}

JsonMessageItemType::JsonMessageItemType(const Enum &value)
  : value_(value) {
}

JsonMessageItemType::JsonMessageItemType(const std::string &name) {
  FromString(name);
}

std::string JsonMessageItemType::GetTypeName() {
  return kTypeName;
}

JsonMessageItemType::Enum JsonMessageItemType::GetValue() const {
  return value_;
}

void JsonMessageItemType::SetValue(const JsonMessageItemType::Enum &value) {
  value_ = value;
}

bool JsonMessageItemType::operator==(const JsonMessageItemType &other) const {
  return value_ == other.GetValue();
}

std::string JsonMessageItemType::ToString() const {
  switch (value_) {
    case Enum::SET_REQUEST: {
      return kSetRequest;
    }
    case Enum::GET_REQUEST: {
      return kGetRequest;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void JsonMessageItemType::FromString(const std::string &str) {
  if (str == kSetRequest) {
    value_ = Enum::SET_REQUEST;
  } else if (str == kGetRequest) {
    value_ = Enum::GET_REQUEST;
  } else {
    value_ = Enum::NONE;
  }
}

void JsonMessageItemType::FromValue(const uint32_t &value) {
  auto tempVal = static_cast<Enum>(value);
  if (tempVal == Enum::SET_REQUEST) {
    value_ = Enum::SET_REQUEST;
  } else if (tempVal == Enum::GET_REQUEST) {
    value_ = Enum::GET_REQUEST;
  } else {
    value_ = Enum::NONE;
  }
}
}  // namespace Communication::Protocols::Json::Types
