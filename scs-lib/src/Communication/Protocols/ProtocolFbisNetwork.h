/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Communication/Protocols/FbisNetwork/FbisMessageFactory.h"
#include "ProtocolBase.h"

namespace Communication::Protocols {
class ProtocolFbisNetwork :
  public ProtocolBase {
 public:
  explicit ProtocolFbisNetwork(Logger::ILogger *logger);

  ~ProtocolFbisNetwork() override = default;

  [[nodiscard]] std::optional<std::unique_ptr<Messages::MessageBase>> Deserialize(
    std::unique_ptr<std::vector<char>> input) const override;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message *message) override;

  bool Visit(Messages::MessageData *message) override;

 private:
  FbisNetwork::FbisMessageFactory factory_;
};
}  // namespace Communication::Protocols
