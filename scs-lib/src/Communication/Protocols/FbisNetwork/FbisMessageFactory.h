/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include <vector>
#include "Messages/Message.h"
#include "Messages/MessageVisitorBase.h"
#include "Communication/Protocols/FbisNetwork/Types/RequestId.h"

namespace Communication::Protocols::FbisNetwork {
class FbisMessageFactory : public Messages::MessageVisitorBase {
 public:
  FbisMessageFactory() = default;

  static std::optional<std::unique_ptr<Messages::MessageBase>> DeSerialize(
    const Communication::Protocols::FbisNetwork::Types::RequestId &requestId,
    std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message * message) override;

  bool Visit(Messages::MessageData * message) override;

  std::unique_ptr<std::vector<char>> GetData();

 private:
  std::unique_ptr<std::vector<char>> data_;
};
}  // namespace Communication::Protocols::FbisNetwork
