/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/Message.h"
#include "SWBeamSwitchOffRequestWrite.h"

namespace Communication::Protocols::FbisNetwork {

const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexPosition = 3;
const int32_t SWBeamSwitchOffRequestWrite::kSignalValuePosition = 4;
const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexMin = 1;
const int32_t SWBeamSwitchOffRequestWrite::kSignalIndexMax = 3;

std::optional<std::unique_ptr<Messages::Message>>
SWBeamSwitchOffRequestWrite::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_WRITE);
  // Check message length
  auto length = (*input).size();
  if (length != 7) {  // outer frame 5 chars, inner frame 2 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode index of signal to be overridden
  auto signalIndex = static_cast<uint8_t>((*input)[kSignalIndexPosition]);
  if (signalIndex < kSignalIndexMin || signalIndex > kSignalIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
    return std::move(message);
  }
  // Decode value of signal to be overridden and check if valid
  auto signalValueTest = static_cast<uint8_t>((*input)[kSignalValuePosition]);
  if (signalValueTest > 1) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_VALUE);
    return std::move(message);
  }

  auto signalValue = static_cast<bool>((*input)[kSignalValuePosition]);
  std::string signalName;
  switch (signalIndex) {
    case 1: {
      signalName = "SWBIRequest";
      break;
    }
    case 2: {
      signalName = "SWRBIRequest";
      break;
    }
    case 3: {
      signalName = "SWEBIRequest";
      break;
    }
    default: {
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
      return std::move(message);
    }
  }
  if (signalValue) {
    // Cause switch-off request
    message->AddData(
      "default-value",
      std::make_unique<Messages::Visitor::VisitorPropertySet>(
        std::string("Serializer").append("/Serializer/").append(signalName),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE,
        static_cast<uint8_t>(0)));
    // we need to write any value different to 0x9 to cause a SW BI/RBI/EBI switch-off request
  } else {
    // Clear switch-off request
    message->AddData(
      "default-value",
      std::make_unique<Messages::Visitor::VisitorPropertySet>(
        std::string("Serializer").append("/Serializer/").append(signalName),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE,
        static_cast<uint8_t>(9)));
  }
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SWBeamSwitchOffRequestWrite::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
