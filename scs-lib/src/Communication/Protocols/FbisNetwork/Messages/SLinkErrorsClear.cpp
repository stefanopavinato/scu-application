/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/Message.h"
#include "SLinkErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const int32_t SLinkErrorsClear::kSLinkPosition = 3;
const int32_t SLinkErrorsClear::kSLinkMin = 1;
const int32_t SLinkErrorsClear::kSLinkMax = 3;

const char SLinkErrorsClear::kSLinkErrorsClearName[] = "SLinkErrorsClear";

std::optional<std::unique_ptr<Messages::Message>>
SLinkErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SCU_SLINK_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode index of signal to be overridden
  auto slink = static_cast<uint8_t>((*input)[kSLinkPosition]);
  if (slink < kSLinkMin || slink > kSLinkMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
    return std::move(message);
  }
  // Cause switch-off request
  message->AddData(
    "default-value",
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("Serializer").append("/Serializer/SLinkInterface")
        .append(std::to_string(slink)).append("/").append(kSLinkErrorsClearName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SLinkErrorsClear::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
