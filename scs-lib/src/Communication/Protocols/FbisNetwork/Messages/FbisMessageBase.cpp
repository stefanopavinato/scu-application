/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include "FbisMessageBase.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t FbisMessageBase::kFirstParam = 3;
const uint32_t FbisMessageBase::kSlotMin = 1;
const uint32_t FbisMessageBase::kSlotMax = 12;

bool FbisMessageBase::Serialize(std::vector<char> *out, const int32_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 24) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 16) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 8) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 0) & 0x000000FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const int16_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 8) & 0x00FF));
  out->push_back(static_cast<char>((value >> 0) & 0x00FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const int8_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>(value));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint32_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 24) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 16) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 8) & 0x000000FF));
  out->push_back(static_cast<char>((value >> 0) & 0x000000FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint16_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>((value >> 8) & 0x00FF));
  out->push_back(static_cast<char>((value >> 0) & 0x00FF));
  return true;
}

bool FbisMessageBase::Serialize(std::vector<char> *out, const uint8_t &value) {
  if (!out) {
    return false;
  }
  out->push_back(static_cast<char>(value));
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, int8_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position) {
    std::cout << "size to small" << std::endl;
    return false;
  }
  *value |= in.at(position);
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, int16_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position + 1) {
    std::cout << "size to small" << std::endl;
    return false;
  }

  *value |= in.at(position+0) << 8;
  *value |= in.at(position+1);
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, int32_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position + 3) {
    std::cout << "size to small" << std::endl;
    return false;
  }

  *value = in.at(position) << 24;
  *value |= in.at(position+1) << 16;
  *value |= in.at(position+2) << 8;
  *value |= in.at(position+3);
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, uint8_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position) {
    std::cout << "size to small" << std::endl;
    return false;
  }
  *value |= in.at(position);
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, uint16_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position + 1) {
    std::cout << "size to small" << std::endl;
    return false;
  }
  *value |= in.at(position+0) << 8;
  *value |= in.at(position+1);
  return true;
}

bool FbisMessageBase::DeSerializer(const std::vector<char> &in, const uint32_t &position, uint32_t *value) {
  if (!value) {
    std::cout << "value not valid" << std::endl;
    return false;
  }
  if (in.size() < position + 3) {
    std::cout << "size to small" << std::endl;
    return false;
  }

  *value = in.at(position) << 24;
  *value |= in.at(position+1) << 16;
  *value |= in.at(position+2) << 8;
  *value |= in.at(position+3);
  return true;
}

}  // namespace Communication::Protocols::FbisNetwork
