/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class PSUStatusRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const uint32_t kPsuIndexMin;
  static const uint32_t kPsuIndexMax;

  static const char kPresent[];
  static const char kPWR_FAILName[];
  static const char kInhibit[];
  static const char kTemp_Warning[];
  static const char kS200_3[];
  static const char kS200_4[];

  static const char kMainSupplyCurrent[];
  static const char kMainSupplyVoltage[];
  static const char kStandbySupplyCurrent[];
  static const char kStandbySupplyVoltage[];

  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;
};
}  // namespace Communication::Protocols::FbisNetwork
