/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "PSUStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t PSUStatusRead::kPsuIndexMin = 1;
const uint32_t PSUStatusRead::kPsuIndexMax = 2;

const char PSUStatusRead::kPresent[] = "Present";
const char PSUStatusRead::kPWR_FAILName[] = "Power_Fail";
const char PSUStatusRead::kInhibit[] = "Inhibit";
const char PSUStatusRead::kTemp_Warning[] = "Temp_Warning";
const char PSUStatusRead::kS200_3[] = "S200_3";
const char PSUStatusRead::kS200_4[] = "S200_4";

const char PSUStatusRead::kMainSupplyCurrent[] = "MainSupplyCurrent";
const char PSUStatusRead::kMainSupplyVoltage[] = "MainSupplyVoltage";
const char PSUStatusRead::kStandbySupplyCurrent[] = "StandbySupplyCurrent";
const char PSUStatusRead::kStandbySupplyVoltage[] = "StandbySupplyVoltage";

const int32_t PSUStatusRead::kCurrentScale = 1;
const int32_t PSUStatusRead::kVoltageScale = 10;

std::optional<std::unique_ptr<Messages::Message>>
PSUStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_PSU_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode PsuIndex
  auto psuIndex = static_cast<uint8_t>((*input)[kFirstParam]);
  if (psuIndex < kPsuIndexMin || psuIndex > kPsuIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add MC Driver status signals
  message->AddData(
    kPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kPresent),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kPWR_FAILName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kPWR_FAILName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kInhibit,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kInhibit),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE));
  message->AddData(
    kTemp_Warning,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kTemp_Warning),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kS200_3,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kS200_3),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kS200_4,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/").append(kS200_4),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kMainSupplyCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/MainSupply/MainSupplyCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal main voltage
  message->AddData(
    kMainSupplyVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/MainSupply/MainSupplyVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStandbySupplyCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/StandbySupply/StandbySupplyCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal Standby voltage
  message->AddData(
    kStandbySupplyVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("PowerSupplyUnits/PSUnit/PSUModule").append(std::to_string(psuIndex))
        .append("/StandbySupply/StandbySupplyVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> PSUStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    uint8_t psuStatus = 0;
    if (const auto &item = message->GetData(kPresent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto txFault = static_cast<bool>(retVal.value());
        if (txFault) {
          psuStatus |= 0x01;
        }
      }
    }
    if (const auto &item = message->GetData(kPWR_FAILName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto rxLos = static_cast<bool>(retVal.value());
        if (rxLos) {
          psuStatus |= 0x02;
        }
      }
    }
    if (const auto &item = message->GetData(kInhibit)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto rxLos = static_cast<bool>(retVal.value());
        if (rxLos) {
          psuStatus |= 0x04;
        }
      }
    }
    if (const auto &item = message->GetData(kTemp_Warning)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto txDisable = static_cast<bool>(retVal.value());
        if (txDisable) {
          psuStatus |= 0x08;
        }
      }
    }
    if (const auto &item = message->GetData(kS200_3)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto present = static_cast<bool>(retVal.value());
        if (present) {
          psuStatus |= 0x10;
        }
      }
    }
    if (const auto &item = message->GetData(kS200_4)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto present = static_cast<bool>(retVal.value());
        if (present) {
          psuStatus |= 0x20;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), psuStatus);
  }
  // ======== Serialize "MAIN Supply Current" ========
  if (const auto &item = message->GetData(kMainSupplyCurrent)) {
    if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
      item.value()->GetValue())) {
      auto current = static_cast<int16_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
      FbisMessageBase::Serialize(out.get(), current);
    }
  }
  // ======== Serialize "MAIN Supply Voltage" ========
  if (const auto &item = message->GetData(kMainSupplyVoltage)) {
    if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
      item.value()->GetValue())) {
      auto voltage = static_cast<int16_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
      FbisMessageBase::Serialize(out.get(), voltage);
    }
  }
  // ======== Serialize "Standby Supply Current" ========
  if (const auto &item = message->GetData(kStandbySupplyCurrent)) {
    if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
      item.value()->GetValue())) {
      auto current = static_cast<int16_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
      FbisMessageBase::Serialize(out.get(), current);
    }
  }
  // ======== Serialize "Standby Supply Voltage" ========
  if (const auto &item = message->GetData(kStandbySupplyVoltage)) {
    if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
      item.value()->GetValue())) {
      auto voltage = static_cast<int16_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
      FbisMessageBase::Serialize(out.get(), voltage);
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
