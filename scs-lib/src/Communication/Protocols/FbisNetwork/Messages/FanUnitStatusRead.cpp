/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "FanUnitStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char FanUnitStatusRead::kFanUnitOperatingHours[] = "OperatingHours";
const char FanUnitStatusRead::kFanASpeed[] = "FanA/Speed";
// const char FanUnitStatusRead::kFanMSpeed[] = "FanM/Speed";
const char FanUnitStatusRead::kFanBSpeed[] = "FanB/Speed";
const char FanUnitStatusRead::kFanAOperatingHours[] = "FanAOperatingHours";
// const char FanUnitStatusRead::kFanMOperatingHours[] = "FanMOperatingHours";
const char FanUnitStatusRead::kFanBOperatingHours[] = "FanBOperatingHours";

std::optional<std::unique_ptr<Messages::Message>>
FanUnitStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_FAN_UNIT_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add item operating hours
  message->AddData(
    kFanUnitOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/EEProm/").append(kFanUnitOperatingHours),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add FanASpeed
  message->AddData(
    kFanASpeed,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/").append(kFanASpeed),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
//  // Add FanMSpeed
//  message->AddMessageItem(
//          kFanMSpeed,
//          std::string("FanUnit/FanUnit/").append(kFanMSpeed),
//          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add FanBSpeed
  message->AddData(
    kFanBSpeed,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/").append(kFanBSpeed),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add FanA operating hours
  message->AddData(
    kFanAOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/EEProm/").append(kFanAOperatingHours),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
//  // Add FanM operating hours
//  message->AddMessageItem(
//          kFanMOperatingHours,
//          std::string("FanUnit/FanUnit/EEProm/").append(kFanMOperatingHours),
//          std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//              Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add FanB operating hours
  message->AddData(
    kFanBOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/EEProm/").append(kFanBOperatingHours),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> FanUnitStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "FanUnit Operating Hours" ========
    if (const auto &item = message->GetData(kFanUnitOperatingHours)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
        item.value()->GetValue())) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "FanASpeed" ========
    if (const auto &item = message->GetData(kFanASpeed)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<double>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "FanMSpeed" ========
    FbisMessageBase::Serialize(out.get(), static_cast<int16_t>(0x0000));
//    if (const auto &item = message.GetMessageItem(kFanMSpeed)) {
//      if (const auto &retVal = ::Types::TestVariantValue::GetValue<::Types::Value<double>>(
//      item.value()->GetVariantValue())) {
//        auto current = static_cast<int16_t>(retVal.value().GetValue());
//        FbisMessageBase::Serialize(out.get(), current);
//      }
//    }
    // ======== Serialize "FanBSpeed" ========
    if (const auto &item = message->GetData(kFanBSpeed)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<double>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "FanA Operating Hours" ========
    if (const auto &item = message->GetData(kFanAOperatingHours)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
        item.value()->GetValue())) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "FanM Operating Hours" ========
    FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0x00000000));
//    if (const auto &item = message.GetMessageItem(kFanMOperatingHours)) {
//      if (const auto &retVal = ::Types::TestVariantValue::GetValue<std::chrono::minutes>(
//      item.value()->GetVariantValue())) {
//        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
//        FbisMessageBase::Serialize(out.get(), operationTime);
//      }
//    }
    // ======== Serialize "FanB Operating Hours" ========
    if (const auto &item = message->GetData(kFanBOperatingHours)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
        item.value()->GetValue())) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min!
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
