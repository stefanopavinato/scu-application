/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "SERStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SERStatusRead::kPathBase[] = "Serializer/Serializer/";
const char SERStatusRead::kSfpModule[] = "SfpModule";
const char SERStatusRead::kHumiditySensor[] = "HumiditySensor";
const char SERStatusRead::kHotSwapController[] = "HotSwapController";

const char SERStatusRead::kOperatingHours[] = "OperatingHours";

const char SERStatusRead::kPresent[] = "Present";
const char SERStatusRead::kTxFault[] = "TxFault";
const char SERStatusRead::kRxLos[] = "RxLos";
const char SERStatusRead::kTxDisable[] = "TxDisable";
const char SERStatusRead::kRateSelect0[] = "RateSelect0";
const char SERStatusRead::kRateSelect1[] = "RateSelect1";
const char SERStatusRead::kLinkState[] = "LinkState";
const char SERStatusRead::kPgmClkStatusLockDetect[] = "PgmClkStatusLockDetect";
const char SERStatusRead::kPgmClkStatusHoldover[] = "PgmClkStatusHoldover";
const char SERStatusRead::kPgmClkStatusClkIn0[] = "PgmClkStatusClkIn0";
const char SERStatusRead::kPgmClkStatusClkIn1[] = "PgmClkStatusClkIn1";
const char SERStatusRead::kPgmClkSync[] = "PgmClkSync";

const char SERStatusRead::kVccInt0V85Fail[] = "VCCINT_0V85_FAIL";
const char SERStatusRead::kMgtraVcc0V85Fail[] = "MGTRAVCC_0V85_FAIL";
const char SERStatusRead::kMgtraVcc0V90Fail[] = "MGTRAVCC_0V90_FAIL";
const char SERStatusRead::kMainPowerSupplyPowerGood[] = "MainPowerSupplyPowerGood";
const char SERStatusRead::kMainPowerSupplyNotAlert[] = "MainPowerSupplyNotAlert";
const char SERStatusRead::kTemperatureSensorAlert[] = "TemperatureSensorAlert";
const char SERStatusRead::kHdcDrdyInterrupt[] = "HdcDrdyInterrupt";
const char SERStatusRead::kVccInternal0V85NotAlert[] = "VccInternal0V85NotAlert";
const char SERStatusRead::kVaa1V2fail[] = "VAA_1V2_FAIL";
const char SERStatusRead::kVcc1V2fail[] = "VCC_1V2_FAIL";
const char SERStatusRead::kVdd1V2fail[] = "VDD_1V2_FAIL";
const char SERStatusRead::kVee1V2Fail[] = "VEE_1V2_FAIL";
const char SERStatusRead::kVddDdr1V2Fail[] = "VDD_DDR_1V2_FAIL";
const char SERStatusRead::kDdrVttFail[] = "DDR_VTT_FAIL";
const char SERStatusRead::kDdrVrefFail[] = "DDR_VREF_FAIL";
const char SERStatusRead::kVii1V5Fail[] = "VII_1V5_FAIL";
const char SERStatusRead::kVii1V6Fail[] = "VII_1V6_FAIL";
const char SERStatusRead::kVjj1V6Fail[] = "VJJ_1V6_FAIL";
const char SERStatusRead::kVcc1V8Fail[] = "VCC_1V8_FAIL";
const char SERStatusRead::kSfpPowerFail[] = "SFP_POWER_FAIL";
const char SERStatusRead::kVaaMgt1V8Fail[] = "VAA_MGT_1V8_FAIL";
const char SERStatusRead::kVii2V1Fail[] = "VII_2V1_FAIL";
const char SERStatusRead::kVcc2V5Fail[] = "VCC_2V5_FAIL";
const char SERStatusRead::kVii2V8Fail[] = "VII_2V8_FAIL";
const char SERStatusRead::kVcc3V3Fail[] = "VCC_3V3_FAIL";
const char SERStatusRead::kVcc5V0Fail[] = "VCC_5V0_FAIL";

const char SERStatusRead::kHumidity[] = "Humidity";
const char SERStatusRead::kTemperatureAtHumidity[] = "TemperatureAtHumidity";
const char SERStatusRead::kMainCurrent[] = "MainCurrent";
const char SERStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char SERStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char SERStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char SERStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t SERStatusRead::kCurrentScale = 50;
const int32_t SERStatusRead::kVoltageScale = 200;

std::optional<std::unique_ptr<Messages::Message>>
SERStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SERIALIZER_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add item operating hours
  message->AddData(
    kOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(kPathBase).append("EEProm/OperatingHours"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add sfp modules
  for (uint8_t transceiverIndex = 1; transceiverIndex < 4; transceiverIndex++) {
    message->AddData(
      std::string(kTxDisable).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule)
          .append(std::to_string(transceiverIndex)).append("/TxDisable"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
    message->AddData(
      std::string(kRateSelect0).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule)
          .append(std::to_string(transceiverIndex)).append("/RateSelect0"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
    message->AddData(
      std::string(kRateSelect1).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule)
          .append(std::to_string(transceiverIndex)).append("/RateSelect1"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
    message->AddData(
      std::string(kLinkState).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule)
          .append(std::to_string(transceiverIndex)).append("/LinkState"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
    message->AddData(
      std::string(kPresent).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule)
          .append(std::to_string(transceiverIndex)).append("/Present"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      std::string(kTxFault).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule).append(std::to_string(transceiverIndex)).append("/TxFault"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      std::string(kRxLos).append(std::to_string(transceiverIndex)),
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpModule).append(std::to_string(transceiverIndex)).append("/RxLos"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  }
  // Add pgm clock status
  {
    message->AddData(
      kPgmClkStatusLockDetect,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kPgmClkStatusLockDetect),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kPgmClkStatusHoldover,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kPgmClkStatusHoldover),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kPgmClkStatusClkIn0,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kPgmClkStatusClkIn0),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kPgmClkStatusClkIn1,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kPgmClkStatusClkIn1),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kPgmClkSync,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kPgmClkSync),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  }
  // Add power status
  {
    message->AddData(
      kVccInt0V85Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVccInt0V85Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kMgtraVcc0V85Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kMgtraVcc0V85Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kMgtraVcc0V90Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kMgtraVcc0V90Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kMainPowerSupplyPowerGood,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kMainPowerSupplyPowerGood),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kMainPowerSupplyNotAlert,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kMainPowerSupplyNotAlert),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kTemperatureSensorAlert,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kTemperatureSensorAlert),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kHdcDrdyInterrupt,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHdcDrdyInterrupt),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVccInternal0V85NotAlert,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVccInternal0V85NotAlert),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVaa1V2fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVaa1V2fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVcc1V2fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVcc1V2fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVdd1V2fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVdd1V2fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVee1V2Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVee1V2Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVddDdr1V2Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVddDdr1V2Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kDdrVttFail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kDdrVttFail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kDdrVrefFail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kDdrVrefFail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVii1V5Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVii1V5Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVii1V6Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVii1V6Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVjj1V6Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVjj1V6Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVcc1V8Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVcc1V8Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kSfpPowerFail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kSfpPowerFail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVaaMgt1V8Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVaaMgt1V8Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVii2V1Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVii2V1Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVcc2V5Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVcc2V5Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVii2V8Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVii2V8Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVcc3V3Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVcc3V3Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    message->AddData(
      kVcc5V0Fail,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kVcc5V0Fail),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  }
  // Add humidity
  {
    message->AddData(
      kHumidity,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHumiditySensor).append("/Humidity"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    // Add item internal main voltage
    message->AddData(
      kTemperatureAtHumidity,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHumiditySensor).append("/Temperature"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  }
  // Add item main current
  {
    message->AddData(
      kMainCurrent,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHotSwapController).append("/MainCurrent"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    // Add item internal main voltage
    message->AddData(
      kInternalMainVoltage,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHotSwapController).append("/MainVoltage"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
    // Add item internal standby voltage
    message->AddData(
      kInternalStandbyVoltage,
      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
        std::string(kPathBase).append(kHotSwapController).append("/StandbyVoltage"),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  }
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SERStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "MC Operating Hours" ========
    {
      if (const auto &item = message->GetData(kOperatingHours)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
          item.value()->GetValue())) {
          auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min! (add const)
          FbisMessageBase::Serialize(out.get(), operationTime);
        }
      }
    }
    // ======== Serialize "SFP Control/Status" ========
    {
      for (uint8_t transceiverIndex = 1; transceiverIndex < 4; transceiverIndex++) {
        uint8_t sfpStatus = 0;
        if (const auto &item = message->GetData(std::string(kTxDisable)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto txDisable = static_cast<bool>(retVal.value());
            if (txDisable) {
              sfpStatus |= 0x01;
            }
          }
        }
        if (const auto &item = message->GetData(std::string(kRateSelect0)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto rateSelect = static_cast<bool>(retVal.value());
            if (rateSelect) {
              sfpStatus |= 0x02;
            }
          }
        }
        if (const auto &item1 = message->GetData(std::string(kRateSelect1)
                                                   .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item1.value()->GetValue())) {
            auto rateSelect = static_cast<bool>(retVal.value());
            if (rateSelect) {
              sfpStatus |= 0x04;
            }
          }
        }
        if (const auto &item = message->GetData(std::string(kLinkState)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto txFault = static_cast<bool>(retVal.value());
            if (txFault) {
              sfpStatus |= 0x08;
            }
          }
        }
        if (const auto &item = message->GetData(std::string(kPresent)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto notpresent = !static_cast<bool>(retVal.value());
            if (notpresent) {
              sfpStatus |= 0x10;
            }
          }
        }
        if (const auto &item = message->GetData(std::string(kTxFault)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto txFault = static_cast<bool>(retVal.value());
            if (txFault) {
              sfpStatus |= 0x20;
            }
          }
        }
        if (const auto &item = message->GetData(std::string(kRxLos)
                                                  .append(std::to_string(transceiverIndex)))) {
          if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
            auto rxLos = static_cast<bool>(retVal.value());
            if (rxLos) {
              sfpStatus |= 0x40;
            }
          }
        }
        FbisMessageBase::Serialize(out.get(), sfpStatus);
      }
    }
    // ======== Serialize "PGM CLK Status" ========
    {
      uint8_t pgmClkStatus = 0;
      if (const auto &item = message->GetData(kPgmClkStatusLockDetect)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            pgmClkStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message->GetData(kPgmClkStatusHoldover)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            pgmClkStatus |= 0x02;
          }
        }
      }
      if (const auto &item = message->GetData(kPgmClkStatusClkIn0)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            pgmClkStatus |= 0x04;
          }
        }
      }
      if (const auto &item = message->GetData(kPgmClkStatusClkIn1)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            pgmClkStatus |= 0x08;
          }
        }
      }
      if (const auto &item = message->GetData(kPgmClkSync)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            pgmClkStatus |= 0x10;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), pgmClkStatus);
    }
    // ======== Serialize "Power Status" ========
    {
      uint32_t powerStatus = 0;
      if (const auto &item = message->GetData(kVccInt0V85Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 0);
          }
        }
      }
      if (const auto &item = message->GetData(kMgtraVcc0V85Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 1);
          }
        }
      }
      if (const auto &item = message->GetData(kMgtraVcc0V90Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 2);
          }
        }
      }
      if (const auto &item = message->GetData(kMainPowerSupplyPowerGood)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 3);
          }
        }
      }
      if (const auto &item = message->GetData(kMainPowerSupplyNotAlert)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 4);
          }
        }
      }
      // bit 5 is reserved
      // bit 6 is reserved

      if (const auto &item = message->GetData(kVccInternal0V85NotAlert)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 7);
          }
        }
      }
      if (const auto &item = message->GetData(kVaa1V2fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 8);
          }
        }
      }
      if (const auto &item = message->GetData(kVcc1V2fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 9);
          }
        }
      }
      if (const auto &item = message->GetData(kVdd1V2fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 10);
          }
        }
      }
      if (const auto &item = message->GetData(kVee1V2Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 11);
          }
        }
      }
      if (const auto &item = message->GetData(kVddDdr1V2Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 12);
          }
        }
      }
      if (const auto &item = message->GetData(kDdrVttFail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 13);
          }
        }
      }
      if (const auto &item = message->GetData(kDdrVrefFail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 14);
          }
        }
      }
      if (const auto &item = message->GetData(kVii1V5Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 15);
          }
        }
      }
      if (const auto &item = message->GetData(kVii1V6Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 16);
          }
        }
      }
      if (const auto &item = message->GetData(kVjj1V6Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 17);
          }
        }
      }
      if (const auto &item = message->GetData(kVcc1V8Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 18);
          }
        }
      }
      if (const auto &item = message->GetData(kSfpPowerFail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 19);
          }
        }
      }
      if (const auto &item = message->GetData(kVaaMgt1V8Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 20);
          }
        }
      }
      if (const auto &item = message->GetData(kVii2V1Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 21);
          }
        }
      }
      if (const auto &item = message->GetData(kVcc2V5Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 22);
          }
        }
      }
      if (const auto &item = message->GetData(kVii2V8Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 23);
          }
        }
      }
      if (const auto &item = message->GetData(kVcc3V3Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 24);
          }
        }
      }
      if (const auto &item = message->GetData(kVcc5V0Fail)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            powerStatus |= (1 << 25);
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), powerStatus);
    }
    // ======== Serialize "Temperature Status" ========
    {
      uint8_t temperatureStatus = 0;
      if (const auto &item = message->GetData(kTemperatureSensorAlert)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            temperatureStatus |= 0x01;
          }
        }
      }
      if (const auto &item = message->GetData(kHdcDrdyInterrupt)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
          if (retVal.value()) {
            temperatureStatus |= 0x02;
          }
        }
      }
      FbisMessageBase::Serialize(out.get(), temperatureStatus);
    }
    // ======== Serialize "Humidity" ========
    {
      if (const auto &item = message->GetData(kHumidity)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
          item.value()->GetValue())) {
          auto current = static_cast<int16_t>(retVal.value().GetValue());
          FbisMessageBase::Serialize(out.get(), current);
        }
      }
    }
    // ======== Serialize "Temperature at Humidity Sensor" ========
    {
      if (const auto &item = message->GetData(kTemperatureAtHumidity)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
          item.value()->GetValue())) {
          auto voltage = static_cast<int32_t>(retVal.value().GetValue());
          FbisMessageBase::Serialize(out.get(), voltage);
        }
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    {
      if (const auto &item = message->GetData(kMainCurrent)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
          item.value()->GetValue())) {
          auto current = static_cast<int8_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
          FbisMessageBase::Serialize(out.get(), current);
        }
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    {
      if (const auto &item = message->GetData(kInternalMainVoltage)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
          item.value()->GetValue())) {
          auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
          FbisMessageBase::Serialize(out.get(), voltage);
        }
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    {
      if (const auto &item = message->GetData(kInternalStandbyVoltage)) {
        if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
          item.value()->GetValue())) {
          auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
          FbisMessageBase::Serialize(out.get(), voltage);
        }
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
