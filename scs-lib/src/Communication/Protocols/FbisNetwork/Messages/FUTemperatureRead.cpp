/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "FbisMessageBase.h"
#include "FUTemperatureRead.h"

namespace Communication::Protocols::FbisNetwork {

const char FUTemperatureRead::kFanUnitControllerATemperature[] = "FanA/Temperature";
// const char FUTemperatureRead::kFanUnitControllerMTemperature[] = "FanM/Temperature";
const char FUTemperatureRead::kFanUnitControllerBTemperature[] = "FanB/Temperature";

std::optional<std::unique_ptr<Messages::Message>>
FUTemperatureRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_FAN_UNIT_TEMPERATURE_READ);
  // Add Fan Unit Controller A temperature item
  message->AddData(
    kFanUnitControllerATemperature,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/").append(kFanUnitControllerATemperature),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
//  // Add Fan Unit Controller M temperature item
//  message->AddMessageItem(
//      kFanUnitControllerMTemperature,
//      std::string("FanUnit/FanUnit/").append(kFanUnitControllerMTemperature),
//      std::make_unique<Messages::Visitor::VisitorPropertyGet>(
//        Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add Fan Unit Controller B temperature item
  message->AddData(
    kFanUnitControllerBTemperature,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("FanUnit/FanUnit/").append(kFanUnitControllerATemperature),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  (void) input;
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> FUTemperatureRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "SCU Fan Unit Temperatures" ========
    // Serialize Fan Unit Controller A temperature  item
    if (const auto &item = message->GetData(kFanUnitControllerATemperature)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
    // Serialize Fan Unit Controller M temperature  item
    FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0x00000000));
//    if (const auto &item = message.GetMessageItem(kFanUnitControllerMTemperature)) {
//      if (const auto &retVal = ::Types::TestVariantValue::GetValue<::Types::Value<int32_t>>(
//      item.value()->GetVariantValue())) {
//        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
//        FbisMessageBase::Serialize(out.get(), temperature);
//      }
//    }
    // Serialize Fan Unit Controller B temperature  item
    if (const auto &item = message->GetData(kFanUnitControllerBTemperature)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
