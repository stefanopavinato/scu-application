/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class LVDSMCDatalinkErrorsRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kDatalinkPort1TimeExpectationTimeoutsName[];
  static const char kDatalinkPort1LifeSignErrorsName[];
  static const char kDatalinkPort1DecoderErrorsName[];
  static const char kDatalinkPort2TimeExpectationTimeoutsName[];
  static const char kDatalinkPort2LifeSignErrorsName[];
  static const char kDatalinkPort2DecoderErrorsName[];
  static const char kDatalinkPort3TimeExpectationTimeoutsName[];
  static const char kDatalinkPort3LifeSignErrorsName[];
  static const char kDatalinkPort3DecoderErrorsName[];
};
}  // namespace Communication::Protocols::FbisNetwork
