/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/Message.h"
#include "LVDSMCDatalinkErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const char LVDSMCDatalinkErrorsClear::kBoardName[] = "BoardName";
const char LVDSMCDatalinkErrorsClear::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1TimeExpectationTimeoutsResetName[] =
  "MCDriverPort1/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1LifeSignErrorsResetName[] =
  "MCDriverPort1/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort1DecoderErrorsResetName[] =
  "MCDriverPort1/Status/DatalinkDecoderErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2TimeExpectationTimeoutsResetName[] =
  "MCDriverPort2/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2LifeSignErrorsResetName[] =
  "MCDriverPort2/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort2DecoderErrorsResetName[] =
  "MCDriverPort2/Status/DatalinkDecoderErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3TimeExpectationTimeoutsResetName[] =
  "MCDriverPort3/Status/DatalinkTimeExpectationTimeoutCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3LifeSignErrorsResetName[] =
  "MCDriverPort3/Status/DatalinkLifeSignErrorCounterReset";
const char LVDSMCDatalinkErrorsClear::kDatalinkPort3DecoderErrorsResetName[] =
  "MCDriverPort3/Status/DatalinkDecoderErrorCounterReset";

std::optional<std::unique_ptr<Messages::Message>>
LVDSMCDatalinkErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Clear error counters
  message->AddData(
    kDatalinkPort1TimeExpectationTimeoutsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1TimeExpectationTimeoutsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort1LifeSignErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1LifeSignErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort1DecoderErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1DecoderErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort2TimeExpectationTimeoutsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2TimeExpectationTimeoutsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort2LifeSignErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2LifeSignErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort2DecoderErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2DecoderErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort3TimeExpectationTimeoutsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3TimeExpectationTimeoutsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort3LifeSignErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3LifeSignErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  message->AddData(
    kDatalinkPort3DecoderErrorsResetName,
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3DecoderErrorsResetName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> LVDSMCDatalinkErrorsClear::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleType>(
        item.value()->GetValue())) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_LVDSMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
