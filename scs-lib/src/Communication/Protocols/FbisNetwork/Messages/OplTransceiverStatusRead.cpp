/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "OplTransceiverStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t OplTransceiverStatusRead::kTransceiverIndexMin = 1;
const uint32_t OplTransceiverStatusRead::kTransceiverIndexMax = 2;

const char OplTransceiverStatusRead::kStatus[] = "Status";
const char OplTransceiverStatusRead::kSoftErrorCounter[] = "SoftErrorCounter";
const char OplTransceiverStatusRead::kHardErrorCounter[] = "HardErrorCounter";
const char OplTransceiverStatusRead::kChannelDownCounter[] = "ChannelDownCounter";
const char OplTransceiverStatusRead::kLaneDownCounter[] = "LaneDownCounter";

std::optional<std::unique_ptr<Messages::Message>>
OplTransceiverStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SCU_OPL_TRANSCEIVER_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode transceiverIndex
  auto transceiverIndex = static_cast<uint8_t>((*input)[kFirstParam]);
  if (transceiverIndex < kTransceiverIndexMin || transceiverIndex > kTransceiverIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }

  auto transceiverName = std::string("Transceiver");
  switch (transceiverIndex) {
    case 1:
      transceiverName.append("A");
      break;
    case 2:
      transceiverName.append("B");
      break;
    default:
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
  }
  // Add status signals
  message->AddData(
    kStatus,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/")
        .append(transceiverName).append("/").append(kStatus),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kSoftErrorCounter,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/")
        .append(transceiverName).append("/").append(kSoftErrorCounter),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kHardErrorCounter,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/")
        .append(transceiverName).append("/").append(kHardErrorCounter),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kChannelDownCounter,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/")
        .append(transceiverName).append("/").append(kChannelDownCounter),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kLaneDownCounter,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/")
        .append(transceiverName).append("/").append(kLaneDownCounter),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> OplTransceiverStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize Transceiver status
    if (const auto &item = message->GetData(kStatus)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(retVal.value()));
      }
    }
    // Serialize Soft Error Counter
    if (const auto &item = message->GetData(kSoftErrorCounter)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Hard Error Counter
    if (const auto &item = message->GetData(kHardErrorCounter)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Channel Down Counter
    if (const auto &item = message->GetData(kChannelDownCounter)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Lane Down Counter
    if (const auto &item = message->GetData(kLaneDownCounter)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
