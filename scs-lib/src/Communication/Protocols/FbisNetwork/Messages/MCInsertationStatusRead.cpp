/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "MCInsertionStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char MCInsertionStatusRead::kMCInsertionStatus[] = "MCInsertionStatus";

std::optional<std::unique_ptr<Messages::Message>>
MCInsertionStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_MC_INSERTION_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add MC Insertion Status item
  message->AddData(
    kMCInsertionStatus,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)),  // Prüfen of Pfad korrekt
      Messages::Visitor::Types::AttributeType::Enum::MODULE_STATE));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> MCInsertionStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kMCInsertionStatus)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleState>(
        item.value()->GetValue())) {
        auto moduleStatus = static_cast<int8_t>(retVal.value().GetEnum());
        out->push_back(static_cast<char>(dataStatus));
        FbisMessageBase::Serialize(out.get(), moduleStatus);
      } else {
        out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
      }
    } else {
      out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
    }
  } else {
    out->push_back(static_cast<char>(Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE));
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
