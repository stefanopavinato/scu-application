/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "BUMCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char BUMCStatusRead::kBoardName[] = "BoardName";
const char BUMCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char BUMCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char BUMCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char BUMCStatusRead::kDriverEnable[] = "DriverEnable";
const char BUMCStatusRead::kDriverReset[] = "DriverReset";
const char BUMCStatusRead::kOperatingHours[] = "OperatingHours";
const char BUMCStatusRead::kRwReg1[] = "RwReg1";
const char BUMCStatusRead::kRwReg2[] = "RwReg2";
const char BUMCStatusRead::kRwReg3[] = "RwReg3";
const char BUMCStatusRead::kStatReg1[] = "StatReg1";
const char BUMCStatusRead::kStatReg2[] = "StatReg2";
const char BUMCStatusRead::kStatReg3[] = "StatReg3";
const char BUMCStatusRead::kStatReg4[] = "StatReg4";
const char BUMCStatusRead::kStatReg5[] = "StatReg5";
const char BUMCStatusRead::kStatReg6[] = "StatReg6";

std::optional<std::unique_ptr<Messages::Message>>
BUMCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_BUMC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/BoardName"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card power enable item
  message->AddData(
    kMezzanineCardEnable,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardEnable"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card reset item
  message->AddData(
    kMezzanineCardReset,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/MezzanineCardReset"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add driver enable item
  message->AddData(
    kDriverEnable,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverEnable"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE));
  // Add driver reset item
  message->AddData(
    kDriverReset,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/CardDetectionItems/DriverReset"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE));
  // Add item operating hours
  message->AddData(
    kOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Management/EEProm/OperatingHours"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kRwReg1,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kRwReg1),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kRwReg2,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kRwReg2),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kRwReg3,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kRwReg3),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add MC Driver input signal override collection
  message->AddData(
    kStatReg1,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg1),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStatReg2,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg2),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStatReg3,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg3),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStatReg4,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg4),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStatReg5,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg5),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kStatReg6,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append(
        "/Application/MCDriver/").append(kStatReg6),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> BUMCStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleType>(
        item.value()->GetValue())) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_BUMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message->GetData(kMezzanineCardEnable)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message->GetData(kMezzanineCardReset)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message->GetData(kDriverEnable)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message->GetData(kDriverReset)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (!retVal.value()) {
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message->GetData(kOperatingHours)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
        item.value()->GetValue())) {
        auto hours = static_cast<uint32_t>(retVal.value().count() / 15);
        FbisMessageBase::Serialize(out.get(), hours);
      }
    }
    // ======== Serialize "Overriding of BUMC Input Signals" ========
    if (const auto &item = message->GetData(kRwReg1)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kRwReg2)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kRwReg3)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg1)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg2)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg3)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg4)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg5)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
    if (const auto &item = message->GetData(kStatReg6)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto reg = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), reg);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
