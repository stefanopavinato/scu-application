/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/Message.h"
#include "OplErrorsClear.h"

namespace Communication::Protocols::FbisNetwork {

const char OplErrorsClear::kOplErrorsClearName[] = "OplErrorsClear";

std::optional<std::unique_ptr<Messages::Message>>
OplErrorsClear::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SCU_OPL_ERRORS_CLEAR);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Cause switch-off request
  message->AddData(
    "default-value",
    std::make_unique<Messages::Visitor::VisitorPropertySet>(
      std::string("Serializer").append("/Serializer/OplInterface/").append(kOplErrorsClearName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_ONCE,
      true));
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> OplErrorsClear::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
