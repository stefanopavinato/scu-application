/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "MCUserDataRead.h"
#include "SystemModules/Modules/MezzanineCards/Common/ScuArray.h"

namespace Communication::Protocols::FbisNetwork {

const char MCUserDataRead::kUserDataRead[] = "UserDataRead";

std::optional <std::unique_ptr<Messages::Message>>
MCUserDataRead::Deserialize(std::unique_ptr <std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_MC_USER_DATA_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add fast frame signals read
  message->AddData(
    kUserDataRead,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/ScuArray/UserData"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional <std::unique_ptr<std::vector < char>>>

MCUserDataRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique < std::vector < char >> ();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "fast frame signals" ========
    if (const auto &item = message->GetData(kUserDataRead)) {
      if (const auto &fastFrameSignalVector =
        ::Types::VariantValueUtils::GetValue<std::vector<uint32_t>>(item.value()->GetValue())) {
        for (const auto fFSignal : fastFrameSignalVector.value()) {
          FbisMessageBase::Serialize(out.get(), static_cast<int8_t>(fFSignal));
        }
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
