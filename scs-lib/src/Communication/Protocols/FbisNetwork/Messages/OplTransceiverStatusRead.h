/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class OplTransceiverStatusRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const uint32_t kTransceiverIndexMin;
  static const uint32_t kTransceiverIndexMax;

  static const char kStatus[];
  static const char kSoftErrorCounter[];
  static const char kHardErrorCounter[];
  static const char kChannelDownCounter[];
  static const char kLaneDownCounter[];
};
}  // namespace Communication::Protocols::FbisNetwork
