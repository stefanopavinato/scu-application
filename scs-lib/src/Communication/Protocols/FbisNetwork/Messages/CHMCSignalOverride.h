/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class CHMCSignalOverride : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const int32_t kSignalIndexPosition;
  static const int32_t kSignalValuePosition;
  static const int32_t kSignalIndexMin;
  static const int32_t kSignalIndexMax;
};
}  // namespace Communication::Protocols::FbisNetwork
