/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class FanUnitStatusRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kFanUnitOperatingHours[];
  static const char kFanASpeed[];
  static const char kFanMSpeed[];
  static const char kFanBSpeed[];
  static const char kFanAOperatingHours[];
  static const char kFanMOperatingHours[];
  static const char kFanBOperatingHours[];
};
}  // namespace Communication::Protocols::FbisNetwork
