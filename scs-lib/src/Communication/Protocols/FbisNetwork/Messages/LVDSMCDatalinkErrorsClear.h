/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class LVDSMCDatalinkErrorsClear : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kDatalinkPort1TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort1LifeSignErrorsResetName[];
  static const char kDatalinkPort1DecoderErrorsResetName[];
  static const char kDatalinkPort2TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort2LifeSignErrorsResetName[];
  static const char kDatalinkPort2DecoderErrorsResetName[];
  static const char kDatalinkPort3TimeExpectationTimeoutsResetName[];
  static const char kDatalinkPort3LifeSignErrorsResetName[];
  static const char kDatalinkPort3DecoderErrorsResetName[];
};
}  // namespace Communication::Protocols::FbisNetwork
