/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages::DirectAccess {
class ScuRegister_Write;
}  // namespace Messages::DirectAccess
namespace Communication::Protocols::FbisNetwork {
class FirmwareRegisterValueWrite : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::MessageBase>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::unique_ptr<std::vector<char>> Serialize(Messages::Data::ResponseFirmwareRegisterWrite * data);

 private:
  static const uint32_t kMessageLength;
  static const uint32_t kAddressPosition;
  static const uint32_t kValuePosition;
};
}  // namespace Communication::Protocols::FbisNetwork
