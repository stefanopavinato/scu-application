/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class SERVersionRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kPSVersion[];
  static const char kPSBranch[];
  static const char kPSBuildTimeStamp[];
  static const char kPSIsDirty[];
  static const char kPTVersion[];
  static const char kPTBranch[];
  static const char kPTBuildTimeStamp[];
  static const char kPTIsDirty[];
  static const char kPLBuildVersion[];
  static const char kPLLocalId[];
  static const char kPLBuildTimestamp[];
};
}  // namespace Communication::Protocols::FbisNetwork
