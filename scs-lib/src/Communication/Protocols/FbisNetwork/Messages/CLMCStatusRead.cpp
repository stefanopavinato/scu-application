/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "CLMCStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const char CLMCStatusRead::kBoardName[] = "BoardName";
const char CLMCStatusRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char CLMCStatusRead::kMezzanineCardEnable[] = "MezzanineCardEnable";
const char CLMCStatusRead::kMezzanineCardReset[] = "MezzanineCardReset";
const char CLMCStatusRead::kDriverEnable[] = "DriverEnable";
const char CLMCStatusRead::kDriverReset[] = "DriverReset";
const char CLMCStatusRead::kOperatingHours[] = "OperatingHours";
const char CLMCStatusRead::kMCDriverOverride[] = "MCDriverOverride";
const char CLMCStatusRead::kMCDriverStatus[] = "MCDriverStatus";
const char CLMCStatusRead::kLiveSignCounter[] = "LiveSignCounter";
const char CLMCStatusRead::kPBDA[] = "PBDA";
const char CLMCStatusRead::kPBDB[] = "PBDB";
const char CLMCStatusRead::kPBMA[] = "PBMA";
const char CLMCStatusRead::kPBMB[] = "PBMB";
const char CLMCStatusRead::kSenderId[] = "SenderId";
const char CLMCStatusRead::kDatalinkDebugInfo[] = "DatalinkDebugInfo";
const char CLMCStatusRead::kBeamPermitCurrent[] = "BeamPermitCurrent";
const char CLMCStatusRead::kReadyCurrent[] = "ReadyCurrent";
const char CLMCStatusRead::kBeamPermitAndReadyVoltage[] = "BeamPermitAndReadyVoltage";
const char CLMCStatusRead::kRedundantBeamPermitCurrent[] = "RedundantBeamPermitCurrent";
const char CLMCStatusRead::kRedundantReadyCurrent[] = "RedundantReadyCurrent";
const char CLMCStatusRead::kRedundantBeamPermitAndReadyVoltage[] = "RedundantBeamPermitAndReadyVoltage";
const char CLMCStatusRead::kMainCurrent[] = "MainCurrent";
const char CLMCStatusRead::kInternalMainVoltage[] = "InternalMainVoltage";
const char CLMCStatusRead::kInternalStandbyVoltage[] = "InternalStandbyVoltage";
const char CLMCStatusRead::kInternal3V3Voltage[] = "Internal3V3Voltage";
const char CLMCStatusRead::kInternal1V8Voltage[] = "Internal1V8Voltage";

const int32_t CLMCStatusRead::kCurrentScale = 50;
const int32_t CLMCStatusRead::kVoltageScale = 200;

std::optional<std::unique_ptr<Messages::Message>>
CLMCStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_CLMC_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card power enable item
  message->AddData(
    kMezzanineCardEnable,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardEnable"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card reset item
  message->AddData(
    kMezzanineCardReset,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardReset"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add driver enable item
  message->AddData(
    kDriverEnable,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverEnable"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE));
  // Add driver reset item
  message->AddData(
    kDriverReset,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/DriverReset"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_WRITE));
  // Add item operating hours
  message->AddData(
    kOperatingHours,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/OperatingHours"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add MC Driver override signal collection
  message->AddData(
    kMCDriverOverride,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Override/Collection"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add MC Driver status signal collection
  message->AddData(
    kMCDriverStatus,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/Collection"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add live sign counter
  message->AddData(
    kLiveSignCounter,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/LiveSignCounter"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add status PBD A
  message->AddData(
    kPBDA,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbdA"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add status PBD B
  message->AddData(
    kPBDB,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbdB"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add status PBM A
  message->AddData(
    kPBMA,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbmA"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add status PBM B
  message->AddData(
    kPBMB,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/PbmB"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item datalink sender id
  message->AddData(
    kSenderId,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/SenderId"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item datalink debug info
  message->AddData(
    kDatalinkDebugInfo,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver/Status/DatalinkDebugInfo"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item beam permit current
  message->AddData(
    kBeamPermitCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn1/BeamPermitCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item ready current
  message->AddData(
    kReadyCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn2/ReadyCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item beam permit and ready voltage
  message->AddData(
    kBeamPermitAndReadyVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn1/BeamPermitAndReadyVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item redundant beam permit current
  message->AddData(
    kRedundantBeamPermitCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn3/RedundantBeamPermitCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item redundant ready current
  message->AddData(
    kRedundantReadyCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/AnalogIn4/RedundantReadyCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item redundant beam permit and ready voltage
  message->AddData(
    kRedundantBeamPermitAndReadyVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/AnalogIn3/RedundantBeamPermitAndReadyVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item main current
  message->AddData(
    kMainCurrent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainCurrent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal main voltage
  message->AddData(
    kInternalMainVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/MainVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal standby voltage
  message->AddData(
    kInternalStandbyVoltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/HotSwapController/StandbyVoltage"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal 3V3 voltage
  message->AddData(
    kInternal3V3Voltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage3V3"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add item internal 1V8 voltage
  message->AddData(
    kInternal1V8Voltage,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Application/VoltageMonitor/Voltage1V8"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> CLMCStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleType>(
        item.value()->GetValue())) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_CLMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Platform Control Signal Status" ========
    // Parse platform control signal status
    uint8_t platformControlSignalStatus = 0x00;
    // Serialize mezzanine card present item
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x01;
        }
      }
    }
    // Serialize mezzanine card power enable item
    if (const auto &item = message->GetData(kMezzanineCardEnable)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x02;
        }
      }
    }
    // Serialize mezzanine card reset item
    if (const auto &item = message->GetData(kMezzanineCardReset)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          platformControlSignalStatus |= 0x04;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), platformControlSignalStatus);
    // ======== Serialize "MC_Driver Status" ========
    // Parse driver status
    uint8_t driverStatus = 0x00;
    // Serialize driver enable item
    if (const auto &item = message->GetData(kDriverEnable)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (retVal.value()) {
          driverStatus |= 0x01;
        }
      }
    }
    // Serialize driver reset item
    if (const auto &item = message->GetData(kDriverReset)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        if (!retVal.value()) {  // MCDriverReset is inverted in FBIS-Network-Protocol
          driverStatus |= 0x02;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), driverStatus);
    // ======== Serialize "MC Operating Hours" ========
    if (const auto &item = message->GetData(kOperatingHours)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::chrono::minutes>(
        item.value()->GetValue())) {
        auto operationTime = static_cast<uint32_t>(retVal.value().count() / 15);  // Cast to 15 min! (add const)
        FbisMessageBase::Serialize(out.get(), operationTime);
      }
    }
    // ======== Serialize "Overriding of CLMC Input Signals" ========
    if (const auto &item = message->GetData(kMCDriverOverride)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Datalink DebugInfos" ========
    if (const auto &item = message->GetData(kDatalinkDebugInfo)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Details" ========
    // Serialize item datalink sender id
    if (const auto &item = message->GetData(kSenderId)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBM B
    if (const auto &item = message->GetData(kPBMB)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBM A
    if (const auto &item = message->GetData(kPBMA)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBD B
    if (const auto &item = message->GetData(kPBDB)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize status PBD A
    if (const auto &item = message->GetData(kPBDA)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize live sign counter
    if (const auto &item = message->GetData(kLiveSignCounter)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // Serialize MC Driver status signals
    if (const auto &item = message->GetData(kMCDriverStatus)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), retVal.value());
      }
    }
    // ======== Serialize "Beam Permit Signal Current" ========
    if (const auto &item = message->GetData(kBeamPermitCurrent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Ready Signal Current" ========
    if (const auto &item = message->GetData(kReadyCurrent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Beam Permit and Ready Signals Supply Voltage" ========
    if (const auto &item = message->GetData(kBeamPermitAndReadyVoltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Redundant Beam Permit Signal Current" ========
    if (const auto &item = message->GetData(kRedundantBeamPermitCurrent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Redundant Ready Signal Current" ========
    if (const auto &item = message->GetData(kRedundantReadyCurrent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto current = static_cast<int16_t>(retVal.value().GetValue() / 10);  // change unit from uA to 10uA
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Redundant Beam Permit and Ready Signals Supply Voltage" ========
    if (const auto &item = message->GetData(kRedundantBeamPermitAndReadyVoltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int16_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "MAIN Supply Current" ========
    if (const auto &item = message->GetData(kMainCurrent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto current = static_cast<int8_t>((retVal.value().GetValue() + kCurrentScale / 2) / kCurrentScale);
        FbisMessageBase::Serialize(out.get(), current);
      }
    }
    // ======== Serialize "Internal MAIN Supply Voltage" ========
    if (const auto &item = message->GetData(kInternalMainVoltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal STANDBY Supply Voltage" ========
    if (const auto &item = message->GetData(kInternalStandbyVoltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 3V3 Supply Voltage" ========
    if (const auto &item = message->GetData(kInternal3V3Voltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
    // ======== Serialize "Internal 1V8 Supply Voltage" ========
    if (const auto &item = message->GetData(kInternal1V8Voltage)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto voltage = static_cast<int8_t>((retVal.value().GetValue() + kVoltageScale / 2) / kVoltageScale);
        FbisMessageBase::Serialize(out.get(), voltage);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
