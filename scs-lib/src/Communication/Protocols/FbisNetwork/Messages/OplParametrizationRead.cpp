/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "OplParametrizationRead.h"

namespace Communication::Protocols::FbisNetwork {

const char OplParametrizationRead::kOplModeRegister[] = "OplModeRegister";
const char OplParametrizationRead::kTransceiverRouteOplGtARegister[] = "TransceiverRouteOplGtARegister";
const char OplParametrizationRead::kIntermediateNode[] = "IntermediateNode";
const char OplParametrizationRead::kLeakyBucketMaxValueRegister[] = "LeakyBucketMaxValue";
const char OplParametrizationRead::kLeakyBucketThresholdRegister[] = "LeakyBucketThreshold";
const char OplParametrizationRead::kLeakyBucketIncrementValueRegister[] = "LeakyBucketIncrementValue";

std::optional<std::unique_ptr<Messages::Message>>
OplParametrizationRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SCU_OPL_PARAMETRIZATION_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  message->AddData(
    kOplModeRegister,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kOplModeRegister),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kIntermediateNode,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kIntermediateNode),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kTransceiverRouteOplGtARegister,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kTransceiverRouteOplGtARegister),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kLeakyBucketMaxValueRegister,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketMaxValueRegister),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kLeakyBucketThresholdRegister,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketThresholdRegister),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kLeakyBucketIncrementValueRegister,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kLeakyBucketIncrementValueRegister),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> OplParametrizationRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize OPL enabled
    if (const auto &item = message->GetData(kOplModeRegister)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>((retVal.value()) & 0x01));
      }
    }
    // Serialize OPL Mode (end-node, intermediate-node)
    if (const auto &item = message->GetData(kIntermediateNode)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>((retVal.value()) & 0x01));
      }
    }
    // Serialize gt_lane
    if (const auto &item = message->GetData(kTransceiverRouteOplGtARegister)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket MaxValue
    if (const auto &item = message->GetData(kLeakyBucketMaxValueRegister)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket IncrementValue
    if (const auto &item = message->GetData(kLeakyBucketIncrementValueRegister)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize LeakyBucket Threshold
    if (const auto &item = message->GetData(kLeakyBucketThresholdRegister)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
