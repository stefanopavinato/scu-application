/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class OplParametrizationRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const char kOplModeRegister[];
  static const char kTransceiverRouteOplGtARegister[];
  static const char kIntermediateNode[];
  static const char kLeakyBucketMaxValueRegister[];
  static const char kLeakyBucketThresholdRegister[];
  static const char kLeakyBucketIncrementValueRegister[];
};
}  // namespace Communication::Protocols::FbisNetwork
