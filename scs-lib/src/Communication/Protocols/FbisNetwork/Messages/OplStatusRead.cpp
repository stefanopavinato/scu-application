/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "OplStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t OplStatusRead::kTransceiverIndexMin = 1;
const uint32_t OplStatusRead::kTransceiverIndexMax = 2;

const char OplStatusRead::kOplStatusCollection[] = "OplStatusCollection";
const char OplStatusRead::kDSOplErrors[] = "DSOplErrors";
const char OplStatusRead::kUSOplErrors[] = "USOplErrors";
const char OplStatusRead::kDSOplEncodingSupervisionErrors[] = "DSOplEncodingSupervisionErrors";
const char OplStatusRead::kDSOplLifeSignErrors[] = "DSOplLifeSignErrors";
const char OplStatusRead::kDSOplRxFlagErrors[] = "DSOplRxFlagErrors";
const char OplStatusRead::kDSOplTxFrameDropped[] = "DSOplTxFrameDropped";
const char OplStatusRead::kDSOplRxFrameDropped[] = "DSOplRxFrameDropped";
const char OplStatusRead::kDSOplRxFrameErrors[] = "DSOplRxFrameErrors";
const char OplStatusRead::kDSOplNeighbourStateErrors[] = "DSOplNeighbourStateErrors";
const char OplStatusRead::kDSOplNeighbourAfterNextStateErrors[] = "DSOplNeighbourAfterNextStateErrors";
const char OplStatusRead::kUSOplEncodingSupervisionErrors[] = "USOplEncodingSupervisionErrors";
const char OplStatusRead::kUSOplLifeSignErrors[] = "USOplLifeSignErrors";
const char OplStatusRead::kUSOplRxFlagErrors[] = "USOplRxFlagErrors";
const char OplStatusRead::kUSOplTxFrameDropped[] = "USOplTxFrameDropped";
const char OplStatusRead::kUSOplRxFrameDropped[] = "USOplRxFrameDropped";
const char OplStatusRead::kUSOplRxFrameErrors[] = "USOplRxFrameErrors";
const char OplStatusRead::kUSOplNeighbourStateErrors[] = "USOplNeighbourStateErrors";
const char OplStatusRead::kUSOplNeighbourAfterNextStateErrors[] = "USOplNeighbourAfterNextStateErrors";
const char OplStatusRead::kDSOplErrorsMaxValue[] = "DSOplErrorsMaxValue";
const char OplStatusRead::kUSOplErrorsMaxValue[] = "USOplErrorsMaxValue";

std::optional<std::unique_ptr<Messages::Message>>
OplStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SCU_OPL_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add OplStatusCollection item
  message->AddData(
    kOplStatusCollection,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kOplStatusCollection),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplErrors item
  message->AddData(
    kDSOplErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplErrors item
  message->AddData(
    kUSOplErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplEncodingSupervisionErrors item
  message->AddData(
    kDSOplEncodingSupervisionErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplEncodingSupervisionErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplLifeSignErrors item
  message->AddData(
    kDSOplLifeSignErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplLifeSignErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplRxFlagErrors item
  message->AddData(
    kDSOplRxFlagErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFlagErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplTxFrameDropped item
  message->AddData(
    kDSOplTxFrameDropped,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplTxFrameDropped),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplRxFrameDropped item
  message->AddData(
    kDSOplRxFrameDropped,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFrameDropped),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplRxFrameErrors item
  message->AddData(
    kDSOplRxFrameErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplRxFrameErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplNeighbourStateErrors item
  message->AddData(
    kDSOplNeighbourStateErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplNeighbourStateErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplNeighbourAfterNextStateErrors item
  message->AddData(
    kDSOplNeighbourAfterNextStateErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplNeighbourAfterNextStateErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplEncodingSupervisionErrors item
  message->AddData(
    kUSOplEncodingSupervisionErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplEncodingSupervisionErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplLifeSignErrors item
  message->AddData(
    kUSOplLifeSignErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplLifeSignErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplRxFlagErrors item
  message->AddData(
    kUSOplRxFlagErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFlagErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplTxFrameDropped item
  message->AddData(
    kUSOplTxFrameDropped,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplTxFrameDropped),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplRxFrameDropped item
  message->AddData(
    kUSOplRxFrameDropped,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFrameDropped),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplRxFrameErrors item
  message->AddData(
    kUSOplRxFrameErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplRxFrameErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplNeighbourStateErrors item
  message->AddData(
    kUSOplNeighbourStateErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplNeighbourStateErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplNeighbourAfterNextStateErrors item
  message->AddData(
    kUSOplNeighbourAfterNextStateErrors,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplNeighbourAfterNextStateErrors),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add DSOplErrorsMaxValue item
  message->AddData(
    kDSOplErrorsMaxValue,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kDSOplErrorsMaxValue),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add USOplErrorsMaxValue item
  message->AddData(
    kUSOplErrorsMaxValue,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("/Serializer/Serializer/OplInterface/").append(kUSOplErrorsMaxValue),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> OplStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "kOplStatusCollection" ========
    if (const auto &item = message->GetData(kOplStatusCollection)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplErrors" ========
    if (const auto &item = message->GetData(kDSOplErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint16_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplErrors" ========
    if (const auto &item = message->GetData(kUSOplErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint16_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint16_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplEncodingSupervisionErrors" ========
    if (const auto &item = message->GetData(kDSOplEncodingSupervisionErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplLifeSignErrors" ========
    if (const auto &item = message->GetData(kDSOplLifeSignErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFlagErrors" ========
    if (const auto &item = message->GetData(kDSOplRxFlagErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplTxFrameDropped" ========
    if (const auto &item = message->GetData(kDSOplTxFrameDropped)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFrameDropped" ========
    if (const auto &item = message->GetData(kDSOplRxFrameDropped)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplRxFrameErrors" ========
    if (const auto &item = message->GetData(kDSOplRxFrameErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplNeighbourStateErrors" ========
    if (const auto &item = message->GetData(kDSOplNeighbourStateErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplNeighbourAfterNextStateErrors" ========
    if (const auto &item = message->GetData(kDSOplNeighbourAfterNextStateErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplEncodingSupervisionErrors" ========
    if (const auto &item = message->GetData(kUSOplEncodingSupervisionErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplLifeSignErrors" ========
    if (const auto &item = message->GetData(kUSOplLifeSignErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFlagErrors" ========
    if (const auto &item = message->GetData(kUSOplRxFlagErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplTxFrameDropped" ========
    if (const auto &item = message->GetData(kUSOplTxFrameDropped)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFrameDropped" ========
    if (const auto &item = message->GetData(kUSOplRxFrameDropped)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplRxFrameErrors" ========
    if (const auto &item = message->GetData(kUSOplRxFrameErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplNeighbourStateErrors" ========
    if (const auto &item = message->GetData(kUSOplNeighbourStateErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplNeighbourAfterNextStateErrors" ========
    if (const auto &item = message->GetData(kUSOplNeighbourAfterNextStateErrors)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kUSOplErrorsMaxValue" ========
    if (const auto &item = message->GetData(kUSOplErrorsMaxValue)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // ======== Serialize "kDSOplErrorsMaxValue" ========
    if (const auto &item = message->GetData(kDSOplErrorsMaxValue)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    return std::move(out);
  }
}
}  // namespace Communication::Protocols::FbisNetwork
