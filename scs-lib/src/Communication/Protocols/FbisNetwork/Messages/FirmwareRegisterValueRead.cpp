/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Message.h"
#include "Messages/MessageData.h"
#include "Messages/Visitor/VisitorProperty.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/RequestFirmwareRegisterRead.h"
#include "Messages/Data/ResponseFirmwareRegisterRead.h"
#include "FirmwareRegisterValueRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t FirmwareRegisterValueRead::kMessageLength = 9;  // outer frame 5 chars, inner frame 4 chars
const uint32_t FirmwareRegisterValueRead::kAddressPosition = 3;

std::optional<std::unique_ptr<Messages::MessageBase>>
FirmwareRegisterValueRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Check message length
  auto length = input->size();
  if (length != kMessageLength) {
    auto message = std::make_unique<Messages::Message>();
    message->SetRequestId(Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_READ);
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Get address
  uint32_t address;
  if (!DeSerializer(*input, kAddressPosition, &address)) {
    auto message = std::make_unique<Messages::Message>();
    message->SetRequestId(Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_READ);
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Create message
  auto message = std::make_unique<Messages::MessageData>();
  // Set data
  message->SetData(std::make_unique<Messages::Data::RequestFirmwareRegisterRead>(
    address));
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>(
    ::Types::SoftwareModule::Enum::DIRECT_ACCESS));
  return std::move(message);
}

std::unique_ptr<std::vector<char>> FirmwareRegisterValueRead::Serialize(
  Messages::Data::ResponseFirmwareRegisterRead * data) {
  auto out = std::make_unique<std::vector<char>>();
  // Add request id
  out->push_back(static_cast<char>(FbisNetwork::Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_READ));
  // Add message status
  out->push_back((static_cast<char>(Messages::Types::MessageStatus::Enum::OK)));
  // Add data length
  uint16_t dataLength = 5;
  FbisMessageBase::Serialize(out.get(), dataLength);
  // Add data status
  out->push_back((static_cast<char>(Messages::Types::DataStatus::Enum::OK)));
  // Add value
  FbisMessageBase::Serialize(out.get(), data->GetValue());
  // Return vector
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
