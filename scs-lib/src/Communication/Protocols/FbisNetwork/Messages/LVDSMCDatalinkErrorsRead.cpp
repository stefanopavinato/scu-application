/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "LVDSMCDatalinkErrorsRead.h"

namespace Communication::Protocols::FbisNetwork {

const char LVDSMCDatalinkErrorsRead::kBoardName[] = "BoardName";
const char LVDSMCDatalinkErrorsRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1TimeExpectationTimeoutsName[] =
  "MCDriverPort1/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1LifeSignErrorsName[] =
  "MCDriverPort1/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort1DecoderErrorsName[] =
  "MCDriverPort1/Status/DatalinkDecoderErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2TimeExpectationTimeoutsName[] =
  "MCDriverPort2/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2LifeSignErrorsName[] =
  "MCDriverPort2/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort2DecoderErrorsName[] =
  "MCDriverPort2/Status/DatalinkDecoderErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3TimeExpectationTimeoutsName[] =
  "MCDriverPort3/Status/DatalinkTimeExpectationTimeoutCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3LifeSignErrorsName[] =
  "MCDriverPort3/Status/DatalinkLifeSignErrorCounter";
const char LVDSMCDatalinkErrorsRead::kDatalinkPort3DecoderErrorsName[] =
  "MCDriverPort3/Status/DatalinkDecoderErrorCounter";


std::optional<std::unique_ptr<Messages::Message>>
LVDSMCDatalinkErrorsRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_LVDSMC_DATALINK_ERRORS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort1TimeExpectationTimeoutsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1TimeExpectationTimeoutsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort1LifeSignErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1LifeSignErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort1DecoderErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort1DecoderErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort2TimeExpectationTimeoutsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2TimeExpectationTimeoutsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort2LifeSignErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2LifeSignErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort2DecoderErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort2DecoderErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));

  message->AddData(
    kDatalinkPort3TimeExpectationTimeoutsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3TimeExpectationTimeoutsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort3LifeSignErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3LifeSignErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kDatalinkPort3DecoderErrorsName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot))
        .append("/Application/MCDriver/").append(kDatalinkPort3DecoderErrorsName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> LVDSMCDatalinkErrorsRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleType>(
        item.value()->GetValue())) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_LVDSMC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize Datalink Port1 TimeExpectation-timeout Error Counter
    if (const auto &item = message->GetData(kDatalinkPort1TimeExpectationTimeoutsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port1 LifeSign Error Counter
    if (const auto &item = message->GetData(kDatalinkPort1LifeSignErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port1 DecoderError Error Counter
    if (const auto &item = message->GetData(kDatalinkPort1DecoderErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 TimeExpectation-timeout Error Counter
    if (const auto &item = message->GetData(kDatalinkPort2TimeExpectationTimeoutsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 LifeSign Error Counter
    if (const auto &item = message->GetData(kDatalinkPort2LifeSignErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port2 DecoderError Error Counter
    if (const auto &item = message->GetData(kDatalinkPort2DecoderErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 TimeExpectation-timeout Error Counter
    if (const auto &item = message->GetData(kDatalinkPort3TimeExpectationTimeoutsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 LifeSign Error Counter
    if (const auto &item = message->GetData(kDatalinkPort3LifeSignErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
    // Serialize Datalink Port3 DecoderError Error Counter
    if (const auto &item = message->GetData(kDatalinkPort3DecoderErrorsName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(retVal.value()));
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
