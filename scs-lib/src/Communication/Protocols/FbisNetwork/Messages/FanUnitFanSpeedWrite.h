/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class FanUnitFanSpeedWrite : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const int32_t kFanNumberPosition;
  static const int32_t kFanSpeedPosition;
  static const int32_t kFanSpeedMin;
  static const int32_t kFanSpeedMax;
  static const int32_t kFanSpeedAuto;
  static const int32_t kFanSpeedPwmMin;
  static const int32_t kFanSpeedPwmMax;
};
}  // namespace Communication::Protocols::FbisNetwork
