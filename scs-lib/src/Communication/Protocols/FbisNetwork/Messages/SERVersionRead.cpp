/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "SERVersionRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SERVersionRead::kPSVersion[] = "PSVersion";
const char SERVersionRead::kPSBranch[] = "PSBranch";
const char SERVersionRead::kPSBuildTimeStamp[] = "PSBuildTimeStamp";
const char SERVersionRead::kPSIsDirty[] = "PSIsDirty";
const char SERVersionRead::kPTVersion[] = "PTVersion";
const char SERVersionRead::kPTBranch[] = "PTBranch";
const char SERVersionRead::kPTBuildTimeStamp[] = "PTBuildTimeStamp";
const char SERVersionRead::kPTIsDirty[] = "PTIsDirty";
const char SERVersionRead::kPLBuildVersion[] = "PLBuildVersion";
const char SERVersionRead::kPLLocalId[] = "PLLocalId";
const char SERVersionRead::kPLBuildTimestamp[] = "PLBuildTimestamp";

std::optional<std::unique_ptr<Messages::Message>>
SERVersionRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SER_VERSION_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add PS Version item
  message->AddData(
    kPSVersion,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Application/Version"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PSBranch item
  message->AddData(
    kPSBranch,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Application/Branch"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PSBuildTimeStamp item
  message->AddData(
    kPSBuildTimeStamp,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Application/Timestamp"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PSIsDirty item
  message->AddData(
    kPSIsDirty,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Application/IsDirty"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PTVersion item
  message->AddData(
    kPTVersion,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Platform/Version"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PTBranch item
  message->AddData(
    kPTBranch,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Platform/Branch"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PTBuildTimeStamp item
  message->AddData(
    kPTBuildTimeStamp,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Platform/Timestamp"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add kPTPTIsDirty item
  message->AddData(
    kPTIsDirty,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/Software/Platform/IsDirty"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PLBuildVersion item
  message->AddData(
    kPLBuildVersion,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/PLBuildVersion"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PLLocalId item
  message->AddData(
    kPLLocalId,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/PLLocalID"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add PLBuildTimestamp item
  message->AddData(
    kPLBuildTimestamp,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/PLBuildTimestamp"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SERVersionRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "PSVersion" ========
    if (const auto &item = message->GetData(kPSVersion)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto version = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), version);
      }
    }
    // Serialize kPSBranch
    if (const auto &item = message->GetData(kPSBranch)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // ======== Serialize "PSBuildTimeStamp" ========
    if (const auto &item = message->GetData(kPSBuildTimeStamp)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto buildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), buildTimestamp);
      }
    }
    // ======== Serialize "kPSIsDirty" ========
    if (const auto &item = message->GetData(kPSIsDirty)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto val = retVal.value();
        if (val) {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(1));
        } else {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(0));
        }
      }
    }
    // ======== Serialize "PTVersion" ========
    if (const auto &item = message->GetData(kPTVersion)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto version = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), version);
      }
    }
    // Serialize kPTBranch
    if (const auto &item = message->GetData(kPTBranch)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // ======== Serialize "PTBuildTimeStamp" ========
    if (const auto &item = message->GetData(kPTBuildTimeStamp)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto buildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), buildTimestamp);
      }
    }
    // ======== Serialize "kPTIsDirty" ========
    if (const auto &item = message->GetData(kPTIsDirty)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto val = retVal.value();
        if (val) {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(1));
        } else {
          FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(0));
        }
      }
    }

    // ======== Serialize "plBuildVersion" ========
    if (const auto &item = message->GetData(kPLBuildVersion)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto plBuildVersion = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildVersion);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }

    // ======== Serialize "PLBuildTimestamp" ========
    if (const auto &item = message->GetData(kPLLocalId)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto plBuildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildTimestamp);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }

    // ======== Serialize "PLBuildTimestamp" ========
    if (const auto &item = message->GetData(kPLBuildTimestamp)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto plBuildTimestamp = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), plBuildTimestamp);
      } else {
        FbisMessageBase::Serialize(out.get(), static_cast<uint32_t>(0xDEADBEEF));
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
