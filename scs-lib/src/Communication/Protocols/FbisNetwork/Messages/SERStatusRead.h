/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class SERStatusRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kPathBase[];
  static const char kSfpModule[];
  static const char kHumiditySensor[];
  static const char kHotSwapController[];

  static const char kOperatingHours[];

  static const char kPresent[];
  static const char kTxFault[];
  static const char kRxLos[];
  static const char kTxDisable[];
  static const char kRateSelect0[];
  static const char kRateSelect1[];
  static const char kLinkState[];
  static const char kPgmClkStatusLockDetect[];
  static const char kPgmClkStatusHoldover[];
  static const char kPgmClkStatusClkIn0[];
  static const char kPgmClkStatusClkIn1[];
  static const char kPgmClkSync[];
  static const char kMainPowerSupplyPowerGood[];
  static const char kMainPowerSupplyNotAlert[];
  static const char kVaa1V2fail[];
  static const char kVcc1V2fail[];
  static const char kVdd1V2fail[];
  static const char kVccInternal0V85NotAlert[];
  static const char kTemperatureSensorAlert[];
  static const char kHdcDrdyInterrupt[];

  static const char kVccInt0V85Fail[];
  static const char kMgtraVcc0V85Fail[];
  static const char kMgtraVcc0V90Fail[];
  static const char kVee1V2Fail[];
  static const char kVddDdr1V2Fail[];
  static const char kDdrVttFail[];
  static const char kDdrVrefFail[];
  static const char kVii1V5Fail[];
  static const char kVii1V6Fail[];
  static const char kVjj1V6Fail[];
  static const char kVcc1V8Fail[];
  static const char kSfpPowerFail[];
  static const char kVaaMgt1V8Fail[];
  static const char kVii2V1Fail[];
  static const char kVcc2V5Fail[];
  static const char kVii2V8Fail[];
  static const char kVcc3V3Fail[];
  static const char kVcc5V0Fail[];

  static const char kHumidity[];
  static const char kTemperatureAtHumidity[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal1V8Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
