/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "FbisMessageBase.h"

namespace Messages {
class Message;
}  // namespace Messages
namespace Communication::Protocols::FbisNetwork {
class CHMCStatusRead : public FbisMessageBase {
 public:
  static std::optional<std::unique_ptr<Messages::Message>>
  Deserialize(std::unique_ptr<std::vector<char>> input);

  static std::optional<std::unique_ptr<std::vector<char>>> Serialize(
    Messages::Message * message);

 private:
  static const int32_t kCurrentScale;
  static const int32_t kVoltageScale;

  static const char kBoardName[];
  static const char kMezzanineCardPresent[];
  static const char kMezzanineCardEnable[];
  static const char kMezzanineCardReset[];
  static const char kDriverEnable[];
  static const char kDriverReset[];
  static const char kOperatingHours[];
  static const char kMCDriverInputSignalOverride[];
  static const char kMCDriverOutputSignalOverride[];
  static const char kMCDriverStatus1[];
  static const char kMCDriverStatus2[];
  static const char kMainCurrent[];
  static const char kInternalMainVoltage[];
  static const char kInternalStandbyVoltage[];
  static const char kInternal3V3Voltage[];
  static const char kInternal1V8Voltage[];
};
}  // namespace Communication::Protocols::FbisNetwork
