/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/Value.h"
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "MCTemperatureRead.h"

namespace Communication::Protocols::FbisNetwork {

const char MCTemperatureRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char MCTemperatureRead::kUpperPCBEdgeTemperature[] = "UpperPCBEdgeTemperature";
const char MCTemperatureRead::kLowerPCBEdgeTemperature[] = "LowerPCBEdgeTemperature";

std::optional<std::unique_ptr<Messages::Message>>
MCTemperatureRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_MC_TEMPERATURE_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add upper PCB edge temperature item
  message->AddData(
    kUpperPCBEdgeTemperature,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/UpperTemperature/Value"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add lower PCB edge temperature item
  message->AddData(
    kLowerPCBEdgeTemperature,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/LowerTemperature/Value"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> MCTemperatureRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "Upper PCB Edge Temperature" ========
    if (const auto &item = message->GetData(kUpperPCBEdgeTemperature)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
    // ======== Serialize "Lower PCB Edge Temperature" ========
    if (const auto &item = message->GetData(kLowerPCBEdgeTemperature)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::Value<int32_t>>(
        item.value()->GetValue())) {
        auto temperature = static_cast<int32_t>(retVal.value().GetValue());
        FbisMessageBase::Serialize(out.get(), temperature);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
