/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Visitor/VisitorProperty.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Message.h"
#include "UnknownCommand.h"

namespace Communication::Protocols::FbisNetwork {

std::optional<std::unique_ptr<Messages::Message>>
UnknownCommand::Deserialize(const Types::RequestId &requestId) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(requestId.GetValue());
  // Check message length
  message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::COMMAND_NOT_IMPLEMENTED);
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> UnknownCommand::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
