/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "SMODInfoRead.h"

namespace Communication::Protocols::FbisNetwork {

const char SMODInfoRead::kMezzanineCardPresent[] = "MezzanineCardPresent";
const char SMODInfoRead::kManufacturer[] = "Manufacturer";
const char SMODInfoRead::kBoardName[] = "BoardNameString";
const char SMODInfoRead::kRevision[] = "BoardRevision";
const char SMODInfoRead::kSerialNumber[] = "SerialNumber";
const char SMODInfoRead::kUniqueID[] = "UUID";

std::optional<std::unique_ptr<Messages::Message>>
SMODInfoRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SMOD_INFO_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode SMOD
  auto smod = static_cast<uint8_t>((*input)[kFirstParam]);
  auto itemPath = std::string();
  switch (smod) {
    case 1:  // SCU Power Supply slot 1
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
    case 2:  // SCU Power Supply slot 2
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
    case 10:  // Serializer
      itemPath = std::string("Serializer").append("/Serializer/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    case 20:  // MC Slot 1
    case 21:  // MC Slot 2
    case 22:  // MC Slot 3
    case 23:  // MC Slot 4
    case 24:  // MC Slot 5
    case 25:  // MC Slot 6
    case 26:  // MC Slot 7
    case 27:  // MC Slot 8
    case 28:  // MC Slot 9
    case 29:  // MC Slot 10
    case 30:  // MC Slot 11
    case 31:  // MC Slot 12
      // Add mezzanine card present item
      message->AddData(
        kMezzanineCardPresent,
        std::make_unique<Messages::Visitor::VisitorPropertyGet>(
          std::string("MC").append(std::to_string(smod - 19)).append("/CardDetectionItems/MezzanineCardPresent"),
          Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
      itemPath = std::string("MC").append(std::to_string(smod - 19)).append("/Management/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    case 40:  // SCU Fan Unit
      itemPath = std::string("FanUnit").append("/FanUnit/EEProm/");
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::OK);
      break;
    default:
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
      return std::move(message);
  }
  // Add SMOD manufacturer item
  message->AddData(
    kManufacturer,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(itemPath).append(kManufacturer),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add SMOD card name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(itemPath).append(kBoardName),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add SMOD card board revision number
  message->AddData(
    kRevision,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(itemPath).append(kRevision),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add SMOD card serial number
  message->AddData(
    kSerialNumber,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(itemPath).append(kSerialNumber),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add SMOD card unique ID
  message->AddData(
    kUniqueID,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string(itemPath).append(kUniqueID),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SMODInfoRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if ((dataStatus == Messages::Types::DataStatus::Enum::OK) ||
      (dataStatus == Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE)) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (item) {  // true (existing), if mezzanine card read
        if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue()).value()) {
          dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
        }
      }
    }
  }
  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // Serialize SMOD card manufacturer
    if (const auto &item = message->GetData(kManufacturer)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (32 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card name
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card revision
    if (const auto &item = message->GetData(kRevision)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card serial number
    if (const auto &item = message->GetData(kSerialNumber)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (16 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
    // Serialize SMOD card unique id
    if (const auto &item = message->GetData(kUniqueID)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<std::string>(item.value()->GetValue())) {
        std::string strValue = retVal.value();
        for (char &c : strValue) {
          out->push_back(c);
        }
        for (std::string::size_type i = 0; i < (4 - strValue.length()); i++) {
          out->push_back(0);
        }
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
