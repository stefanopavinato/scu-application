/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "SFPStatusRead.h"

namespace Communication::Protocols::FbisNetwork {

const uint32_t SFPStatusRead::kTransceiverIndexMin = 1;
const uint32_t SFPStatusRead::kTransceiverIndexMax = 4;

const char SFPStatusRead::kTxFault[] = "TxFault";
const char SFPStatusRead::kRxLos[] = "RxLos";
const char SFPStatusRead::kRateSelect0[] = "RateSelect0";
const char SFPStatusRead::kRateSelect1[] = "RateSelect1";
const char SFPStatusRead::kTxDisable[] = "TxDisable";
const char SFPStatusRead::kPresent[] = "Present";

// ToDo: Add indicator if other serializer is available

std::optional<std::unique_ptr<Messages::Message>>
SFPStatusRead::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_SFP_STATUS_READ);
  // Check message length
  auto length = (*input).size();
  if (length != 6) {  // outer frame 5 chars, inner frame 1 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode transceiverIndex
  auto transceiverIndex = static_cast<uint8_t>((*input)[kFirstParam]);
  if (transceiverIndex < kTransceiverIndexMin || transceiverIndex > kTransceiverIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Add MC Driver status signals
  message->AddData(
    kTxFault,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/TxFault"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kRxLos,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/RxLos"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  message->AddData(
    kRateSelect0,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule")
        .append(std::to_string(transceiverIndex)).append("/RateSelect0"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
  message->AddData(
    kRateSelect1,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule")
        .append(std::to_string(transceiverIndex)).append("/RateSelect1"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
  message->AddData(
    kTxDisable,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/TxDisable"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT));
  message->AddData(
    kPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer/Serializer/SfpModule").append(std::to_string(transceiverIndex)).append("/Present"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> SFPStatusRead::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  // Serialize SFP status
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    uint8_t sfpStatus = 0;
    if (const auto &item = message->GetData(kTxFault)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto txFault = static_cast<bool>(retVal.value());
        if (txFault) {
          sfpStatus |= 0x01;
        }
      }
    }
    if (const auto &item = message->GetData(kRxLos)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto rxLos = static_cast<bool>(retVal.value());
        if (rxLos) {
          sfpStatus |= 0x02;
        }
      }
    }
    if (const auto &item0 = message->GetData(kRateSelect0)) {
      if (const auto &item1 = message->GetData(kRateSelect1)) {
        if (const auto &retVal0 = ::Types::VariantValueUtils::GetValue<bool>(item0.value()->GetValue())) {
          if (const auto &retVal1 = ::Types::VariantValueUtils::GetValue<bool>(item1.value()->GetValue())) {
            auto rateSelect = static_cast<bool>(retVal0.value()) && static_cast<bool>(retVal1.value());
            if (rateSelect) {
              sfpStatus |= 0x04;
            }
          }
        }
      }
    }
    if (const auto &item = message->GetData(kTxDisable)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto txDisable = static_cast<bool>(retVal.value());
        if (txDisable) {
          sfpStatus |= 0x08;
        }
      }
    }
    if (const auto &item = message->GetData(kPresent)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        auto present = static_cast<bool>(retVal.value());
        if (present) {
          sfpStatus |= 0x10;
        }
      }
    }
    FbisMessageBase::Serialize(out.get(), sfpStatus);
  }
  return std::move(out);
}
}  // namespace Communication::Protocols::FbisNetwork
