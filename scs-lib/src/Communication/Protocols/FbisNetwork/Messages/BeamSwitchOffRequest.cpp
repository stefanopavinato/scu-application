/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Types/ModuleType.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Message.h"
#include "BeamSwitchOffRequest.h"

namespace Communication::Protocols::FbisNetwork {

const char BeamSwitchOffRequest::kBeamSwitchOffRequestStatus[] = "BeamSwitchOffRequestStatus";

std::optional<std::unique_ptr<Messages::Message>>
BeamSwitchOffRequest::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_BEAM_SWITCH_OFF_REQUEST_STATUS);
  // Check message length
  auto length = (*input).size();
  if (length != 5) {  // outer frame 5 chars, inner frame 0 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Add kBeamSwitchOffRequestStatus item
  message->AddData(
    kBeamSwitchOffRequestStatus,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("Serializer").append("/Serializer/BeamSwitchOffStatusCollection"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Return message
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> BeamSwitchOffRequest::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  // ======== Serialize "Data Status" ========
  FbisMessageBase::Serialize(out.get(), static_cast<uint8_t>(dataStatus));

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Serialize "kBeamSwitchOffRequestStatus" ========
    if (const auto &item = message->GetData(kBeamSwitchOffRequestStatus)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<uint32_t>(item.value()->GetValue())) {
        auto beamSwitchOffRequestStatus = static_cast<uint32_t>(retVal.value());
        FbisMessageBase::Serialize(out.get(), beamSwitchOffRequestStatus);
      }
    }
  }
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
