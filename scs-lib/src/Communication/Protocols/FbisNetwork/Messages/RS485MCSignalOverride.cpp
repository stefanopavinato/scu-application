/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Types/VariantValue.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Types/DataStatus.h"
#include "Messages/Visitor/VisitorPropertyGet.h"
#include "Messages/Visitor/VisitorPropertySet.h"
#include "Messages/Message.h"
#include "RS485MCSignalOverride.h"

namespace Communication::Protocols::FbisNetwork {

const char RS485MCSignalOverride::kBoardName[] = "BoardName";
const char RS485MCSignalOverride::kMezzanineCardPresent[] = "MezzanineCardPresent";
const int32_t RS485MCSignalOverride::kSignalIndexPosition = 4;
const int32_t RS485MCSignalOverride::kSignalValuePosition = 5;
const int32_t RS485MCSignalOverride::kSignalIndexMin = 1;
const int32_t RS485MCSignalOverride::kSignalIndexMax = 8;

std::optional<std::unique_ptr<Messages::Message>>
RS485MCSignalOverride::Deserialize(std::unique_ptr<std::vector<char>> input) {
  // Create message
  auto message = std::make_unique<Messages::Message>();
  // Set receiver
  message->SetReceiver(std::make_unique<Messages::Addresses::AddressModule>
                         (::Types::SoftwareModule::Enum::MESSAGE_HANDLER));
  // Set request id
  message->SetRequestId(Types::RequestId::Enum::SCU_RS485MC_SIGNAL_OVERRIDE);
  // Check message length
  auto length = (*input).size();
  if (length != 8) {  // outer frame 5 chars, inner frame 3 char
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT);
    return std::move(message);
  }
  // Decode slot
  auto slot = static_cast<uint8_t>((*input)[kFirstParam]);
  if (slot < kSlotMin || slot > kSlotMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::SLOT_NOT_VALID);
    return std::move(message);
  }
  // Decode index of signal to be overridden
  auto signalIndex = static_cast<uint8_t>((*input)[kSignalIndexPosition]);
  if (signalIndex < kSignalIndexMin || signalIndex > kSignalIndexMax) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
    return std::move(message);
  }
  // Decode value of signal to be overridden and check if valid
  auto signalValueTest = static_cast<uint8_t>((*input)[kSignalValuePosition]);
  if (signalValueTest > 1) {
    message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_VALUE);
    return std::move(message);
  }
  // Add mezzanine card board name
  message->AddData(
    kBoardName,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/Management/EEProm/BoardName"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));
  // Add mezzanine card present item
  message->AddData(
    kMezzanineCardPresent,
    std::make_unique<Messages::Visitor::VisitorPropertyGet>(
      std::string("MC").append(std::to_string(slot)).append("/CardDetectionItems/MezzanineCardPresent"),
      Messages::Visitor::Types::AttributeType::Enum::VALUE_IN));

  auto signalValue = static_cast<bool>((*input)[kSignalValuePosition]);
  std::string signalName;
  switch (signalIndex) {
    case 1: {
      signalName = "Port1_Discrete_Signal0";
      break;
    }
    case 2: {
      signalName = "Port1_Discrete_Signal1";
      break;
    }
    case 3: {
      signalName = "Port1_Discrete_Signal2";
      break;
    }
    case 4: {
      signalName = "Port1_Discrete_Signal3";
      break;
    }
    case 5: {
      signalName = "Port2_Discrete_Signal0";
      break;
    }
    case 6: {
      signalName = "Port2_Discrete_Signal1";
      break;
    }
    case 7: {
      signalName = "Port2_Discrete_Signal2";
      break;
    }
    case 8: {
      signalName = "Port2_Discrete_Signal3";
      break;
    }
    default: {
      message->DataStatus()->SetValue(Messages::Types::DataStatus::Enum::INVALID_SIGNAL_INDEX);
      return std::move(message);
    }
  }
  if (signalValue) {
    message->AddData(
//            "default-value",
      signalName,
      std::make_unique<Messages::Visitor::VisitorPropertySet>(
        std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver")
          .append("/SignalOverride/").append(signalName),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE,
        signalValue));
  } else {
    message->AddData(
//            "default-value",
      signalName,
      std::make_unique<Messages::Visitor::VisitorPropertySet>(
        std::string("MC").append(std::to_string(slot)).append("/Application/MCDriver")
          .append("/SignalOverride/").append(signalName),
        Messages::Visitor::Types::AttributeType::Enum::VALUE_OUT_FORCE_RELEASE,
        signalValue));
  }
  return std::move(message);
}

std::optional<std::unique_ptr<std::vector<char>>> RS485MCSignalOverride::Serialize(
  Messages::Message * message) {
  // Prepare out vector
  auto out = std::make_unique<std::vector<char>>();
  auto dataStatus = message->DataStatus()->GetValue();

  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    // ======== Check MC ========
    if (const auto &item = message->GetData(kMezzanineCardPresent)) {
      if (!::Types::VariantValueUtils::GetValue<bool>(item.value()->GetValue())) {
        dataStatus = Messages::Types::DataStatus::Enum::MC_NOT_PRESENT;
      }
    }
  }
  if (dataStatus == Messages::Types::DataStatus::Enum::OK) {
    if (const auto &item = message->GetData(kBoardName)) {
      if (const auto &retVal = ::Types::VariantValueUtils::GetValue<::Types::ModuleType>(
        item.value()->GetValue())) {
        if (retVal.value().GetEnum() != ::Types::ModuleType::Enum::SCU_RS485MC) {
          dataStatus = Messages::Types::DataStatus::Enum::WRONG_MEZZANINE_CARD;
        }
      } else {
        dataStatus = Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE;
      }
    }
  }

  // ======== Serialize "Data Status" ========
  out->push_back(static_cast<char>(dataStatus));
  return std::move(out);
}

}  // namespace Communication::Protocols::FbisNetwork
