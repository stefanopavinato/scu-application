/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ProtocolFbisNetwork.h"
#include "ProtocolJson.h"
#include "ProtocolArbitration.h"
#include "ProtocolFactory.h"

namespace Communication::Protocols {

std::optional<std::unique_ptr<ProtocolBase>> ProtocolFactory::Create(
  Logger::ILogger * logger,
  const Types::ProtocolType::Enum &protocolType) {
  switch (protocolType) {
    case Types::ProtocolType::Enum::FBIS_NETWORK: {
      return std::make_unique<ProtocolFbisNetwork>(logger);
    }
    case Types::ProtocolType::Enum::JSON: {
      return std::make_unique<ProtocolJson>(logger);
    }
    case Types::ProtocolType::Enum::ARBITRATION: {
      return std::make_unique<ProtocolArbitration>(logger);
    }
    case Types::ProtocolType::Enum::NONE: {
      return std::nullopt;
    }
  }
  return std::nullopt;
}

}  // namespace Communication::Protocols
