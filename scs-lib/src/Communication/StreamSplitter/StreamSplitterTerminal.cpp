/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "StreamSplitterTerminal.h"

namespace Communication::StreamSplitter {

const uint32_t StreamSplitterTerminal::kSizeLength = 4;

std::optional<std::unique_ptr<std::vector<char>>> StreamSplitterTerminal::Unpack(
  std::unique_ptr<std::vector<char>> input) {
  // Append stream segment
  if (!streamSegment_) {
    streamSegment_ = std::move(input);
  } else {
    streamSegment_->insert(
      std::end(*streamSegment_),
      std::begin(*input),
      std::end(*input));
  }
  // Test minimum stream segment length
  if (streamSegment_->size() < kSizeLength) {
    return std::nullopt;
  }
  // Get length
  if (length_ == 0) {
    length_ = (static_cast<uint32_t>((*streamSegment_)[0]) & 0x000000FF);
    length_ += ((static_cast<uint32_t>((*streamSegment_)[1]) & 0x000000FF) << 8);
    length_ += ((static_cast<uint32_t>((*streamSegment_)[2]) & 0x000000FF) << 16);
    length_ += ((static_cast<uint32_t>((*streamSegment_)[3]) & 0x000000FF) << 24);
  }
  // Check stream segment length
  if (streamSegment_->size() < length_ + kSizeLength) {
    return std::nullopt;
  }
  // Remove length information
  streamSegment_->erase(
    streamSegment_->begin(),
    streamSegment_->begin() + kSizeLength);
  // Resize out stream
  streamSegment_->resize(length_);
  // Reset length
  length_ = 0;
  return std::move(streamSegment_);
}

std::optional<std::unique_ptr<std::vector<char>>> StreamSplitterTerminal::Pack(
  std::unique_ptr<std::vector<char>> message) {
  // Create output vector
  auto out = std::make_unique<std::vector<char>>();
  // Set length
  out->push_back(static_cast<char>(message->size() & 0x000000FF));
  out->push_back(static_cast<char>((message->size() & 0x0000FF00) >> 8));
  out->push_back(static_cast<char>((message->size() & 0x00FF0000) >> 16));
  out->push_back(static_cast<char>((message->size() & 0xFF000000) >> 24));
  // Append message
  out->insert(
    std::end(*out),
    std::begin(*message),
    std::end(*message));

  return std::move(out);
}

}  // namespace Communication::StreamSplitter
