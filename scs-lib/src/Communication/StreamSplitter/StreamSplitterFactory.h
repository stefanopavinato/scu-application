/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <optional>
#include "Communication/Types/ProtocolType.h"

namespace Communication::StreamSplitter {
class StreamSplitterBase;

class StreamSplitterFactory {
 public:
  static std::optional<std::unique_ptr<StreamSplitterBase>> Create(
    const Types::ProtocolType::Enum &protocolType);
};

}  // namespace Communication::StreamSplitter
