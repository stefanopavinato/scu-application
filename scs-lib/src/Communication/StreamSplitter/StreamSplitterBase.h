/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <vector>
#include <optional>

namespace Communication::StreamSplitter {
class StreamSplitterBase {
 public:
  virtual ~StreamSplitterBase() = default;

  virtual std::optional<std::unique_ptr<std::vector<char>>> Unpack(
    std::unique_ptr<std::vector<char>> input) = 0;

  virtual std::optional<std::unique_ptr<std::vector<char>>> Pack(
    std::unique_ptr<std::vector<char>> message) = 0;
};
}  // namespace Communication::StreamSplitter
