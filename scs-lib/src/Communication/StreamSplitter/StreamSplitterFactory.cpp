/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "StreamSplitterFbisNetwork.h"
#include "StreamSplitterTerminal.h"
#include "StreamSplitterFactory.h"

namespace Communication::StreamSplitter {

std::optional<std::unique_ptr<StreamSplitterBase>> StreamSplitterFactory::Create(
  const Types::ProtocolType::Enum &protocolType) {
  switch (protocolType) {
    case Types::ProtocolType::Enum::FBIS_NETWORK: {
      return std::make_unique<StreamSplitterFbisNetwork>();
    }
    case Types::ProtocolType::Enum::JSON: {
      return std::make_unique<StreamSplitterTerminal>();
    }
    case Types::ProtocolType::Enum::ARBITRATION: {
      return std::make_unique<StreamSplitterTerminal>();
    }
    case Types::ProtocolType::Enum::NONE: {
      return std::nullopt;
    }
  }
  return std::nullopt;
}

}  // namespace Communication::StreamSplitter
