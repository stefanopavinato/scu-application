/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include <memory>
#include <vector>
#include <chrono>
#include <optional>
#include "Messages/Data/DataVisitorBase.h"
#include "Communication/Types/ProtocolType.h"
#include "Utils/ThreadBase.h"
#include "Distributor/IDistributorSubscriber.h"

namespace Communication {
namespace StreamSplitter {
class StreamSplitterBase;
}  // namespace StreamSplitter
namespace Protocols {
class ProtocolBase;
}  // namespace Protocols
namespace Clients {
class Client :
  public Utils::ThreadBase,
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriber {
 public:
  Client(
    Logger::ILogger *logger,
    const int32_t &fd,
    std::string ip,
    const uint16_t &port,
    const std::chrono::seconds & timeout,
    std::unique_ptr<Protocols::ProtocolBase> protocol,
    std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter);

  ~Client() override = default;

  bool OnMessageReceived(Messages::MessageBase * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  static std::optional<std::unique_ptr<Client>> Create(
    Logger::ILogger * logger,
    const std::string & ip,
    const uint16_t & port,
    const std::chrono::seconds & timeout,
    const Types::ProtocolType & protocolType);

  static std::optional<std::unique_ptr<Client>> Create(
    Logger::ILogger * logger,
    const int32_t & fdListener,
    const std::chrono::seconds & timeout,
    const Types::ProtocolType & protocolType);

 protected:
  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  [[nodiscard]] bool Send(std::unique_ptr<std::vector<char>> stream);

  bool Receive(std::unique_ptr<std::vector<char>> input);

  Logger::ILogger *const logger_;

  const int32_t fd_;

  std::chrono::seconds timeout_;

  std::chrono::steady_clock::time_point expired_;

  std::unique_ptr<Protocols::ProtocolBase> protocol_;

  std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter_;

  static const char kName[];

  static const char kAddress[];
  static const char kProtocol[];
  static const char kIsRunning[];
};
}  // namespace Clients
}  // namespace Communication
