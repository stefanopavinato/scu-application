/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriber.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Messages {
class MessageBase;
}  // namespace Messages
namespace Communication::Clients {
class ClientListener;
class ClientEstablisher;
class ClientHandler  :
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriber {
 public:
  explicit ClientHandler(
    Logger::ILogger * logger);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageData * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] bool AddListener(ClientListener * listener);

  [[nodiscard]] bool AddEstablisher(ClientEstablisher * establisher);

 private:
  Logger::ILogger *const logger_;

  std::list<ClientListener *> listeners_;

  std::list<ClientEstablisher *> establishers_;

  static const char kListener[];
  static const char kEstablisher[];
};
}  // namespace Communication::Clients
