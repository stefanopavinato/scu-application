/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "Communication/Types/ProtocolType.h"
#include "Utils/ThreadBase.h"
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriberManager.h"

namespace Messages::Addresses {
class AddressListener;
}  // namespace Messages::Addresses
namespace Communication::Clients {
class ClientListener :
  public Utils::ThreadBase,
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriberManager {
 public:
  ClientListener(
    Logger::ILogger *logger,
    const Types::ProtocolType::Enum & protocolType,
    const uint16_t &port,
    const std::chrono::seconds &timeout,
    const int32_t & maxClientCount);

  ~ClientListener() override = default;

  bool OnMessageReceived(Messages::MessageBase * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  Logger::ILogger *const logger_;

  const Types::ProtocolType  protocolType_;

  const uint16_t port_;

  std::chrono::seconds timeout_;

  const int32_t maxClientCount_;

  int32_t fd_;

  std::list<std::unique_ptr<Clients::Client>> clients_;

  static const char kName[];

  static const char kAddress[];
  static const char kProtocol[];
  static const char kClients[];
};
}  // namespace Communication::Clients
