/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/MessageBase.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "ClientDescriptor.h"
#include "Client.h"
#include "ClientEstablisher.h"

namespace Communication::Clients {

const char ClientEstablisher::kName[] = "client-establisher";

const char ClientEstablisher::kAddress[] = "address";
const char ClientEstablisher::kProtocol[] = "protocol";
const char ClientEstablisher::kClients[] = "clients";
const char ClientEstablisher::kIsRunning[] = "is_running";

ClientEstablisher::ClientEstablisher(
  Logger::ILogger * logger)
  : Utils::ThreadBase(logger, kName)
  , Distributor::IDistributorSubscriberManager(std::make_unique<Messages::Addresses::AddressModule>(
    ::Types::SoftwareModule::Enum::CLIENT_ESTABLISHER))
  , logger_(logger) {
}

bool ClientEstablisher::OnMessageReceived(Messages::MessageBase * message) {
  return message->Accept(this);
}

bool ClientEstablisher::AddDescriptor(std::unique_ptr<ClientDescriptor> descriptorNew) {
  for (auto &descriptor : descriptors_) {
    if (*descriptor == *descriptorNew) {
      return false;
    }
  }
  descriptors_.push_back(std::move(descriptorNew));
  return true;
}

bool ClientEstablisher::Initialize() {
  return true;
}

bool ClientEstablisher::Run() {
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
//  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  for (auto &descriptor : descriptors_) {
    auto client = clients_.find(descriptor.get());
    if (client == clients_.end()) {
      // Create client
      auto clientNew = Clients::Client::Create(
        logger_,
        descriptor->GetIp(),
        descriptor->GetPort(),
        descriptor->GetTimeout(),
        descriptor->GetProtocol());

      if (!clientNew) {
        logger_->Debug("Failed to create client");
        return true;
      }
      // Add client to client handler
      if (!AddSubscriber(clientNew.value().get())) {
        logger_->Error("Failed to set distributor");
        return true;
      }
      // Start client
      if (!clientNew.value()->Start()) {
        logger_->Error("Failed to start client");
        RemoveSubscriber(clientNew.value().get());
        return false;
      }
      // Store client
      clients_.insert(std::make_pair(descriptor.get(), std::move(clientNew.value())));
      continue;
    }
  }
  // Remove client
  for (auto iterator = clients_.begin();
       iterator != clients_.end();) {
    if (!iterator->second->IsRunning()) {
      // Remove subscriber
      RemoveSubscriber(iterator->second.get());
      // Remove client
      iterator->second->Stop();
      iterator = clients_.erase(iterator);
      continue;
    }
    iterator++;
  }
  return true;
}

bool ClientEstablisher::DeInitialize() {
  return false;
}

bool ClientEstablisher::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  if (GetAddress()->Visit(data)) {
    (*val)[kAddress] = *data->Pop();
  }
  uint32_t i = 0;
  for (auto & descriptor : descriptors_) {
    if (descriptor->Visit(data)) {
      (*val)[kClients][i] = *data->Pop();
      // Set client run state
      auto client = clients_.find(descriptor.get());
      if (client != clients_.end()) {
        (*val)[kClients][i][kIsRunning] = client->second->IsRunning();
      } else {
        (*val)[kClients][i][kIsRunning] = false;
      }
    }
    i++;
  }
  data->Push(std::move(val));
  return true;
}

}  // namespace Communication::Clients
