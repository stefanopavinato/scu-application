/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include <unordered_map>
#include "Utils/ThreadBase.h"
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriberManager.h"

namespace Communication::Clients {
class ClientDescriptor;
class Client;
class ClientEstablisher :
  public Utils::ThreadBase,
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriberManager {
 public:
  explicit ClientEstablisher(
    Logger::ILogger * logger);

  ~ClientEstablisher() override = default;

  bool OnMessageReceived(Messages::MessageBase * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] bool AddDescriptor(std::unique_ptr<ClientDescriptor> descriptorNew);

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  Logger::ILogger * const logger_;

  std::list<std::unique_ptr<ClientDescriptor>> descriptors_;

  std::unordered_map<ClientDescriptor *, std::unique_ptr<Client>> clients_;

  static const char kName[];

  static const char kAddress[];
  static const char kProtocol[];
  static const char kClients[];
  static const char kIsRunning[];
};
}  // namespace Communication::Clients
