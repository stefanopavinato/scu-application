/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <json/json.h>
#include "Logger/ILogger.h"
#include "Messages/MessageBase.h"
#include "Messages/Addresses/AddressClient.h"
#include "Messages/Data/ResponseOverview.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/StreamSplitter/StreamSplitterFactory.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Communication/Protocols/ProtocolFactory.h"
#include "Client.h"

namespace Communication::Clients {

const char Client::kName[] = "Client";

const char Client::kAddress[] = "address";
const char Client::kProtocol[] = "protocol";
const char Client::kIsRunning[] = "is_running";

Client::Client(
  Logger::ILogger *logger,
  const int32_t &fd,
  std::string ip,
  const uint16_t &port,
  const std::chrono::seconds & timeout,
  std::unique_ptr<Protocols::ProtocolBase> protocol,
  std::unique_ptr<StreamSplitter::StreamSplitterBase> streamSplitter)
  : ThreadBase(logger, kName)
  , Distributor::IDistributorSubscriber(std::make_unique<Messages::Addresses::AddressClient>(
    std::move(ip),
    protocol->GetProtocolType()))
  , logger_(logger)
  , fd_(fd)
  , timeout_(timeout)
  , protocol_(std::move(protocol))
  , streamSplitter_(std::move(streamSplitter)) {
}

bool Client::OnMessageReceived(Messages::MessageBase * message) {
  // Serialize message
  auto stream = protocol_->Serialize(message);
  if (!stream) {
    logger_->Error("Failed to serialize message");
    return false;
  }
  // Pack stream
  auto output = streamSplitter_->Pack(std::move(stream.value()));
  if (!output) {
    logger_->Error("Failed to pack stream");
    return false;
  }
  // Send stream
  return Send(std::move(output.value()));
}

std::optional<std::unique_ptr<Client>> Client::Create(
  Logger::ILogger * logger,
  const std::string & ip,
  const uint16_t & port,
  const std::chrono::seconds & timeout,
  const Types::ProtocolType & protocolType) {
  // Create socket
  auto fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd < 0) {
    logger->Error(strerror(errno));
    return std::nullopt;
  }
  // Set socket address
  struct sockaddr_in socket{};
  socket.sin_family = AF_INET;
  socket.sin_port = htons(port);
  if (inet_pton(AF_INET, ip.c_str(), &socket.sin_addr) <= 0) {
    logger->Error(strerror(errno));
    close(fd);
    return std::nullopt;
  }
  // Connect socket
  if (connect(fd, (struct sockaddr *)&socket, sizeof(socket)) < 0) {
    logger->Debug(strerror(errno));
    close(fd);
    return std::nullopt;
  }
  // Create protocol
  auto protocol = Protocols::ProtocolFactory::Create(
    logger,
    protocolType.GetValue());
  if (!protocol) {
    logger->Error("Protocol not found");
    close(fd);
    return std::nullopt;
  }
  // Create stream splitter
  auto streamSplitter = StreamSplitter::StreamSplitterFactory::Create(
    protocolType.GetValue());
  if (!streamSplitter) {
    logger->Error("Stream splitter not found");
    close(fd);
    return std::nullopt;
  }
  // Create client
  auto client = std::make_unique<Communication::Clients::Client>(
    logger,
    fd,
    ip,
    port,
    timeout,
    std::move(protocol.value()),
    std::move(streamSplitter.value()));
  if (!client) {
    logger->Error("Failed to create client");
    close(fd);
    return std::nullopt;
  }
  return std::move(client);
}

std::optional<std::unique_ptr<Client>> Client::Create(
  Logger::ILogger * logger,
  const int32_t & fdListener,
  const std::chrono::seconds & timeout,
  const Types::ProtocolType & protocolType) {
  // Accept connection and get file descriptor
  struct sockaddr_in socket{};
  socklen_t socklen = sizeof(socket);
  auto fd = accept(
    fdListener,
    reinterpret_cast<sockaddr *>(&socket),
    &socklen);
  if (fd < 0) {
    logger->Error(std::strerror(errno));
    return std::nullopt;
  }
  // Get client address
  if (getpeername(
    fd,
    reinterpret_cast<sockaddr *>(&socket),
    &socklen)) {
    logger->Error("Failed to get peer name");
    close(fd);
    return std::nullopt;
  }
  // Convert client address
  char ip[16] = "";
  inet_ntop(
    AF_INET,
    &socket.sin_addr,
    ip,
    sizeof(ip));
  // Create protocol
  auto protocol = Protocols::ProtocolFactory::Create(
    logger,
    protocolType.GetValue());
  if (!protocol) {
    logger->Error("Protocol not found");
    close(fd);
    return std::nullopt;
  }
  // Create stream splitter
  auto streamSplitter = StreamSplitter::StreamSplitterFactory::Create(
    protocolType.GetValue());
  if (!streamSplitter) {
    logger->Error("Stream splitter not found");
    close(fd);
    return std::nullopt;
  }
  // Create client
  auto client = std::make_unique<Clients::Client>(
    logger,
    fd,
    ip,
    socket.sin_port,
    timeout,
    std::move(protocol.value()),
    std::move(streamSplitter.value()));
  if (!client) {
    logger->Error("Failed to create client");
    close(fd);
    return std::nullopt;
  }
  return std::move(client);
}

bool Client::Initialize() {
  // Write log info
  logger_->Info(
    std::string("ip = ").append(GetAddress()->ToString())
      .append(", protocol = ").append(protocol_->GetProtocolType().ToString())
      .append(", fd = ").append(std::to_string(fd_)));
  // Reset Time
  expired_ = timeout_ + std::chrono::steady_clock::now();
  return true;
}

bool Client::Run() {
  // Wait for input data
  struct timeval tv{};
  tv.tv_sec = 0;
  tv.tv_usec = 100000;

  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(fd_, &rfds);

  auto n = select(fd_ + 1, &rfds, nullptr, nullptr, &tv);
  if (n < 0) {
    logger_->Error("Select error");
    return false;
  } else if (n == 0) {
    // Check connection timeout
    if (expired_ < std::chrono::steady_clock::now()) {
      logger_->Error("Client receive timeout");
      return false;
    }
    return true;
  }
  // Reset Time
  expired_ = timeout_ + std::chrono::steady_clock::now();
  // Receive data
  auto inBuffer = std::make_unique<std::vector<char>>();
  inBuffer->resize(1024);
  auto nBytes = recv(
    fd_,
    inBuffer->data(),
    inBuffer->size(),
    0);
  // Disconnect because of remote logout
  if (nBytes == 0) {
    logger_->Info("Client remote logout");
    return false;
  }
  // Disconnect because of error
  if (nBytes < 0) {
    logger_->Error(std::string("Client socket error: ").append(std::strerror(errno)));
    return false;
  }
  // Handle received string
  inBuffer->resize(static_cast<uint32_t>(nBytes));
  // Process received data
  if (!Receive(std::move(inBuffer))) {
    logger_->Error("Client error");
    return false;
  }
  return true;
}

bool Client::DeInitialize() {
  shutdown(fd_, SHUT_RDWR);
  close(fd_);
  // Write log info
  logger_->Info(
    std::string("ip = ").append(GetAddress()->ToString())
      .append(", protocol = ").append(protocol_->GetProtocolType().ToString())
      .append(", fd = ").append(std::to_string(fd_)));
  return false;
}

bool Client::Send(std::unique_ptr<std::vector<char>> stream) {
  if (stream->empty()) {
    logger_->Error("Stream empty");
    return false;
  }
  // Write stream
  auto nBytes = send(fd_, stream->data(), stream->size(), 0);
  if (nBytes != static_cast<ssize_t>(stream->size())) {
    logger_->Error("Failed to write socket");
    return false;
  }
  return true;
}

bool Client::Receive(std::unique_ptr<std::vector<char>> input) {
  // Split input stream
  auto stream = streamSplitter_->Unpack(std::move(input));
  if (!stream) {
    // Wait for further data
    logger_->Debug("Wait for further data");
    return true;
  }
  // ToDo: crc could be checked here (future extension)
  // ToDo: un-compressing of data could be processed here (future extension)
  // Deserialize message
  std::string s(stream.value()->begin(), stream.value()->end());
  auto message = protocol_->Deserialize(std::move(stream.value()));
  if (!message) {
    logger_->Error("Failed to deserialize message");
    return false;
  }
  // Set sender address
  message.value()->SetSender(GetAddress()->Clone());
  // Pass message to client handler
  if (!IDistributorSubscriber::Send(message->get())) {
    logger_->Error("Failed to send message internal");
  }
  return true;
}

bool Client::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  if (GetAddress()->Visit(data)) {
    (*val)[kAddress] = *data->Pop();
  }
  (*val)[kProtocol] = protocol_->GetProtocolType().ToString();
  (*val)[kIsRunning] = IsRunning();
  data->Push(std::move(val));
  return true;
}

}  // namespace Communication::Clients
