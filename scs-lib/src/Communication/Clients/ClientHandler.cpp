/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include <json/json.h>
#include "Logger/ILogger.h"
#include "Messages/MessageData.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/ResponseOverview.h"
#include "Distributor/IDistributorSubscriber.h"
#include "ClientEstablisher.h"
#include "ClientListener.h"
#include "ClientHandler.h"

namespace Communication::Clients {

const char ClientHandler::kListener[] = "listener";
const char ClientHandler::kEstablisher[] = "establisher";

ClientHandler::ClientHandler(
  Logger::ILogger * logger)
  : Distributor::IDistributorSubscriber(
    std::make_unique<Messages::Addresses::AddressModule>(
      ::Types::SoftwareModule::Enum::CLIENT_HANDLER))
  , logger_(logger) {
}

bool ClientHandler::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool ClientHandler::Visit(Messages::MessageData * message) {
  // Update message direction
  message->SwapAddresses();
  // Create response
  auto response = Messages::Data::ResponseOverview::Create(message->GetData());
  if (!response) {
    logger_->Error("Failed to create response");
    return false;
  }
  // Process response
  if (!response.value()->Accept(this)) {
    logger_->Error("Failed to process response");
    return false;
  }
  // Send message
  message->SetData(std::move(response.value()));
  return Send(message);
}

bool ClientHandler::Visit(Messages::Data::ResponseOverview * data) {
  auto jsonValue = std::make_unique<Json::Value>();
  switch (data->GetDetails().GetEnum()) {
    default: {
      if (data->GetLevel() > 0) {
        uint32_t i = 0;
        for (const auto & establisher : establishers_) {
          (*jsonValue)[kEstablisher][i]["test"] = i;
          if (establisher->Visit(data)) {
            (*jsonValue)[kEstablisher][i++] = *data->Pop();
          }
        }
      }
      if (data->GetLevel() > 0) {
        uint32_t i = 0;
        for (const auto & listener : listeners_) {
          (*jsonValue)[kListener][i]["test"] = i;
          if (listener->Visit(data)) {
            (*jsonValue)[kListener][i++] = *data->Pop();
          }
        }
      }
    }
      data->Push(std::move(jsonValue));
  }
  return true;
}

bool ClientHandler::AddListener(ClientListener * listener) {
  listeners_.push_back(listener);
  return true;
}

bool ClientHandler::AddEstablisher(ClientEstablisher * establisher) {
  establishers_.push_back(establisher);
  return true;
}


}  // namespace Communication::Clients
