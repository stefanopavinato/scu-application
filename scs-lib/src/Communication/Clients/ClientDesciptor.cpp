/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <json/json.h>
#include "Messages/Data/ResponseOverview.h"
#include "ClientDescriptor.h"

namespace Communication::Clients {

const char ClientDescriptor::kIp[] = "ip";
const char ClientDescriptor::kPort[] = "port";
const char ClientDescriptor::kProtocol[] = "protocol";

ClientDescriptor::ClientDescriptor(
  std::string ip,
  const uint16_t &port,
  const std::chrono::seconds &timeout,
  const Types::ProtocolType::Enum &protocol)
  : ip_(std::move(ip))
  , port_(port)
  , timeout_(timeout)
  , protocol_(protocol) {
}

bool ClientDescriptor::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  (*val)[kIp] = ip_;
  (*val)[kPort] = port_;
  (*val)[kProtocol] = protocol_.ToString();
  data->Push(std::move(val));
  return true;
}

std::string ClientDescriptor::GetIp() const {
  return ip_;
}

uint16_t ClientDescriptor::GetPort() const {
  return port_;
}

std::chrono::seconds ClientDescriptor::GetTimeout() const {
  return timeout_;
}

Types::ProtocolType ClientDescriptor::GetProtocol() const {
  return protocol_;
}

bool ClientDescriptor::operator==(const ClientDescriptor & other) const {
  return ip_ == other.ip_ && port_ == other.port_ && protocol_ == other.protocol_;
}

}  // namespace Communication::Clients

size_t std::hash< Communication::Clients::ClientDescriptor>::operator()(
  const  Communication::Clients::ClientDescriptor & descriptor) const {
  return std::hash<std::string>()(descriptor.ip_) ^
    std::hash<uint16_t>()(descriptor.port_) ^
    std::hash<Communication::Types::ProtocolType>()(descriptor.protocol_);
}
