/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "Communication/Types/ProtocolType.h"
#include "Messages/Data/DataVisitorBase.h"

namespace Communication::Clients {
class ClientDescriptor;
}  // namespace Communication::Clients

template<>
class std::hash<Communication::Clients::ClientDescriptor> {
 public:
  size_t operator()(const Communication::Clients::ClientDescriptor & descriptor) const;
};

namespace Communication::Clients {
class ClientDescriptor :
  public Messages::Data::DataVisitorBase {
  friend size_t std::hash<ClientDescriptor>::operator()(const ClientDescriptor & descriptor) const;

 public:
  ClientDescriptor(
    std::string  ip,
    const uint16_t & port,
    const std::chrono::seconds &timeout,
    const Types::ProtocolType::Enum & protocol);

  bool Visit(Messages::Data::ResponseOverview * data) override;

  [[nodiscard]] std::string GetIp() const;

  [[nodiscard]] uint16_t GetPort() const;

  [[nodiscard]] std::chrono::seconds GetTimeout() const;

  [[nodiscard]] Types::ProtocolType GetProtocol() const;

  bool operator==(const ClientDescriptor & other) const;

 private:
  const std::string ip_;

  const uint16_t port_;

  const std::chrono::seconds timeout_;

  const Types::ProtocolType protocol_;

  static const char kIp[];
  static const char kPort[];
  static const char kProtocol[];
};
}  // namespace Communication::Clients
