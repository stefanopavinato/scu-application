/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <unistd.h>
#include <arpa/inet.h>
#include <json/json.h>
#include "Logger/ILogger.h"
#include "Messages/Addresses/AddressListener.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/MessageBase.h"
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Client.h"
#include "ClientListener.h"

namespace Communication::Clients {

const char ClientListener::kName[] = "client-listener";

const char ClientListener::kAddress[] = "address";
const char ClientListener::kProtocol[] = "protocol";
const char ClientListener::kClients[] = "clients";

ClientListener::ClientListener(
  Logger::ILogger *logger,
  const Types::ProtocolType::Enum & protocolType,
  const uint16_t &port,
  const std::chrono::seconds &timeout,
  const int32_t & maxClientCount)
  : Utils::ThreadBase(
    logger,
    std::string(kName).append(": ").append(Communication::Types::ProtocolType::ToString(protocolType)))
  , Distributor::IDistributorSubscriberManager(std::make_unique<Messages::Addresses::AddressListener>(port))
  , logger_(logger)
  , protocolType_(protocolType)
  , port_(port)
  , timeout_(timeout)
  , maxClientCount_(maxClientCount)
  , fd_(0) {
}

bool ClientListener::OnMessageReceived(Messages::MessageBase * message)  {
  return message->Accept(this);
}

bool ClientListener::Initialize() {
  // Setup socket listener
  fd_ = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (fd_ < 0) {
    logger_->Error("Socket listener create error");
    return false;
  }

  int reuse = 1;
  auto a = setsockopt(
    fd_,
    SOL_SOCKET, SO_REUSEADDR,
    (const char *) &reuse,
    sizeof(reuse));
  if (a < 0) {
    logger_->Error("Failed to set socket options");
    return false;
  }
  // Bind server to socket
  struct sockaddr_in socketAddress{};
  socketAddress.sin_family = AF_INET;
  socketAddress.sin_port = htons(port_);
  socketAddress.sin_addr.s_addr = htonl(INADDR_ANY);

  auto r = bind(
    fd_,
    (struct sockaddr *) &socketAddress,
    sizeof(socketAddress));
  if (r < 0) {
    logger_->Error("Failed to bind socket");
    return false;
  }
  // Start listening
  r = listen(fd_, maxClientCount_);
  if (r < 0) {
    logger_->Error("Failure during listening");
    return false;
  }
  // Write log info
  logger_->Info(std::string("Protocol ").append(protocolType_.ToString())
                  .append(" activated on port: ").append(std::to_string(port_)));

  return true;
}

bool ClientListener::Run() {
  // Wait for connection event
  struct timeval tv{};
  tv.tv_sec = 0;
  tv.tv_usec = 100000;

  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(fd_, &rfds);

  auto retVal = select(fd_ + 1, &rfds, nullptr, nullptr, &tv);
  if (retVal == -1) {
    logger_->Error("Select error");
    return false;
  } else if (retVal) {
    // Create client
    auto client = Clients::Client::Create(
      logger_,
      fd_,
      timeout_,
      protocolType_);

    if (!client) {
      logger_->Error("Failed to create client");
      return true;
    }
    // Add client to client handler
    if (!AddSubscriber(client.value().get())) {
      logger_->Error("Failed to set distributor");
      return true;
    }
    // Check maximum client count
    if (clients_.size() >= maxClientCount_) {
      logger_->Error("Maximum client count exceeded");
      RemoveSubscriber(client.value().get());
      return true;
    }
    // Start client
    if (!client.value()->Start()) {
      logger_->Error("Failed to start client");
      RemoveSubscriber(client.value().get());
      return false;
    }
    // Store client
    clients_.push_back(std::move(client.value()));
    return true;
  } else {
    // Select timeout
    for (auto client = clients_.begin(); client != clients_.end();) {
      if (!client->get()->IsRunning()) {
        // Remove subscriber
        RemoveSubscriber(client->get());
        // Remove client
        client->get()->Stop();
        client = clients_.erase(client);
        continue;
      }
      client++;
    }
    return true;
  }
}

bool ClientListener::DeInitialize() {
  shutdown(fd_, SHUT_RDWR);
  close(fd_);
  // Write log info
  logger_->Info(
    std::string("protocol = ").append(protocolType_.ToString())
      .append(", port = ").append(std::to_string(port_))
      .append(", fd = ").append(std::to_string(fd_)));
  return false;
}

bool ClientListener::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();
  if (GetAddress()->Visit(data)) {
    (*val)[kAddress] = *data->Pop();
  }
  (*val)[kProtocol] = protocolType_.ToString();
  uint32_t i = 0;
  for (const auto & client : clients_) {
    if (client->Visit(data)) {
      (*val)[kClients][i++] = *data->Pop();
    }
  }
  data->Push(std::move(val));
  return true;
}

}  // namespace Communication::Clients
