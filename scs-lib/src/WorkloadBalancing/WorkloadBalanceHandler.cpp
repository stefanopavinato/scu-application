/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Messages/MessageData.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/DataStatus.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/Data/ResponsePropertyGet.h"
#include "Messages/Data/ResponsePropertySet.h"
#include "ResourceSharing/ILock.h"
#include "ItemHandler.h"
#include "SystemModules/Framework/Components/ItemBase.h"
#include "SystemModules/Framework/Accessors/AccessorGPIO.h"
#include "WorkloadBalanceHandler.h"


namespace WorkloadBalancing {

const char WorkloadBalanceHandler::kName[] = "WorkloadBalanceHandler";
const char WorkloadBalanceHandler::kDescription[] = "";
const char WorkloadBalanceHandler::kComponentType[] = "WorkloadBalanceHandler";

const char WorkloadBalanceHandler::kUpdater[] = "Updater";

WorkloadBalanceHandler::WorkloadBalanceHandler(
  Logger::ILogger *logger)
  : Utils::ThreadBase(
    logger,
    kComponentType)
  , Distributor::IDistributorSubscriber(
    std::make_unique<Messages::Addresses::AddressModule>(::Types::SoftwareModule::Enum::BALANCER))
  , logger_(logger)
  , updateInterval_(std::chrono::milliseconds(100)) {
  logger->Info("Created");
}

bool WorkloadBalanceHandler::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool WorkloadBalanceHandler::Visit(Messages::MessageData * message) {
  // Update message direction
  message->SwapAddresses();
  // Response property get
  {
    if (auto retVal = Messages::Data::ResponsePropertyGet::Create(message->GetData())) {
      if (retVal.value()->Accept(this)) {
        message->SetData(std::move(retVal.value()));
        return Send(message);
      }
    }
  }
  // Response property set
  {
    if (auto retVal = Messages::Data::ResponsePropertySet::Create(message->GetData())) {
      if (retVal.value()->Accept(this)) {
        message->SetData(std::move(retVal.value()));
        return Send(message);
      }
    }
  }
  // Response overview
  {
    if (auto retVal = Messages::Data::ResponseOverview::Create(message->GetData())) {
      if (retVal.value()->Accept(this)) {
        message->SetData(std::move(retVal.value()));
        return Send(message);
      }
    }
  }
  message->SetData(std::make_unique<Messages::Data::DataStatus>(
    Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE,
    "Message data not supported"));
  return Send(message);
}

bool WorkloadBalanceHandler::Visit(Messages::Data::ResponseOverview * data) {
  if (data->GetPath()->empty()) {
    auto jsonValue = std::make_unique<Json::Value>();
    uint32_t i = 0;
    for (const auto &handler : itemHandlers_)
      if (handler.second->Visit(data)) {
        (*jsonValue)[kUpdater][i++] = *data->Pop();
      }
    data->Push(std::move(jsonValue));
    return true;
  } else {
    auto handler = itemHandlers_.find(Types::LockId::FromString(data->GetPath()->front()));
    if (handler != itemHandlers_.end()) {
      return handler->second->Visit(data);
    }
  }
  return false;
}

bool WorkloadBalanceHandler::Visit(Messages::Data::ResponsePropertyGet * data) {
  auto handler = itemHandlers_.find(Types::LockId::FromString(data->GetPath()->front()));
  if (handler == itemHandlers_.end()) {
    return false;
  }
  data->GetPath()->pop();
  return handler->second->Visit(data);
}

bool WorkloadBalanceHandler::Visit(Messages::Data::ResponsePropertySet * data) {
  auto handler = itemHandlers_.find(Types::LockId::FromString(data->GetPath()->front()));
  if (handler == itemHandlers_.end()) {
    return false;
  }
  data->GetPath()->pop();
  return handler->second->Visit(data);
}

bool WorkloadBalanceHandler::Run() {
  auto timeNext = std::chrono::steady_clock::now() + updateInterval_;
  auto timePointNow = std::chrono::steady_clock::now();
  // Check if all item handlers are running
  for (auto &itemHandler : itemHandlers_) {
    if (!itemHandler.second->IsRunning()) {
      return false;
    }
  }
  std::this_thread::sleep_until(timeNext);
  return true;
}

bool WorkloadBalanceHandler::Initialize() {
  for (auto &itemHandler : itemHandlers_) {
    if (!itemHandler.second->Start()) {
      return false;
    }
  }
  return true;
}

bool WorkloadBalanceHandler::DeInitialize() {
  for (auto &itemHandler : itemHandlers_) {
    itemHandler.second->Stop();
  }
  return false;
}

bool WorkloadBalanceHandler::AddUpdateHandler(
  ResourceSharing::ILock * lock,
  const std::chrono::milliseconds &updateInterval) {
  auto itemHandler = std::make_unique<ItemHandler>(
    Utils::ThreadBase::Logger(),
    lock,
    updateInterval);

  itemHandlers_.insert(std::make_pair(lock->GetId().GetValue(), std::move(itemHandler)));
  return true;
}

bool WorkloadBalanceHandler::AddItem(SystemModules::Framework::Components::ItemBase * item) {
  auto itemHandler = itemHandlers_.find(item->GetAffiliation());
  if (itemHandler == itemHandlers_.end()) {
    Utils::ThreadBase::Logger()->Error("Object not found");
    return false;
  }
  return itemHandler->second->AddItem(item);
}

bool WorkloadBalanceHandler::RemoveItem(SystemModules::Framework::Components::ItemBase * item) {
  auto itemHandler = itemHandlers_.find(item->GetAffiliation());
  if (itemHandler == itemHandlers_.end()) {
    Utils::ThreadBase::Logger()->Error("Object not found");
    return false;
  }
  return itemHandler->second->RemoveItem(item);
}

}  // namespace WorkloadBalancing
