/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <list>
#include "Utils/ThreadBase.h"
#include "Utils/Filter.h"
#include "Messages/Data/DataVisitorBase.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Framework::Components {
class ItemBase;
}  // namespace SystemModules::Framework::Components
namespace ResourceSharing {
class ILock;
}  // namespace ResourceSharing
namespace WorkloadBalancing {
class ItemHandler :
  public Utils::ThreadBase,
  public Messages::Data::DataVisitorBase {
 public:
  ItemHandler(
    Logger::ILogger *logger,
    ResourceSharing::ILock *lock,
    const std::chrono::milliseconds &updateInterval);

  bool Visit(Messages::Data::ResponseOverview * data) override;

  bool Visit(Messages::Data::ResponsePropertySet * data) override;

  bool Visit(Messages::Data::ResponsePropertyGet * data) override;

  void Pause();

  void Resume();

  [[nodiscard]] bool AddItem(SystemModules::Framework::Components::ItemBase * item);

  [[nodiscard]] bool RemoveItem(SystemModules::Framework::Components::ItemBase * item);

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  Logger::ILogger *logger_;

  Utils::Filter::Filter<std::chrono::milliseconds> filter_;

  const std::chrono::milliseconds updateInterval_;

  std::list<SystemModules::Framework::Components::ItemBase *> items_;

  ResourceSharing::ILock *lock_;

  std::atomic<bool> pause_;

  mutable std::mutex mutex_;

  static const char kItemCount[];
  static const char kUpdateInterval[];
  static const char kUpdateDuration[];
  static const char kLock[];
  static const char kIsPaused[];
};
}  // namespace WorkloadBalancing
