/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ResourceSharing/ILock.h"
#include "SystemModules/Framework/Components/ItemBase.h"
#include "Messages/Data/ResponseOverview.h"
#include "Messages/Data/ResponsePropertyGet.h"
#include "Messages/Data/ResponsePropertySet.h"
#include "ItemHandler.h"

namespace WorkloadBalancing {

const char ItemHandler::kItemCount[] = "item-count";
const char ItemHandler::kUpdateInterval[] = "interval";
const char ItemHandler::kUpdateDuration[] = "duration";
const char ItemHandler::kLock[] = "lock";
const char ItemHandler::kIsPaused[] = "is_paused";

ItemHandler::ItemHandler(
  Logger::ILogger *logger,
  ResourceSharing::ILock *lock,
  const std::chrono::milliseconds &updateInterval)
  : Utils::ThreadBase(
  logger,
  std::move(Types::LockId(lock->GetId()).ToString()))
  , logger_(logger)
  , filter_(10)
  , updateInterval_(updateInterval)
  , lock_(lock)
  , pause_(false) {
}

bool ItemHandler::Visit(Messages::Data::ResponseOverview * data) {
  auto val = std::make_unique<::Json::Value>();

  (*val)[kItemCount] = items_.size();
  (*val)[kUpdateInterval] = updateInterval_.count();
  (*val)[kUpdateDuration] = filter_.Update().count();
  (*val)[kLock] = lock_->GetId().ToString();
  (*val)[kIsPaused] = (pause_ == true);

  data->Push(std::move(val));
  return true;
}

bool ItemHandler::Visit(Messages::Data::ResponsePropertySet * data) {
  switch (data->GetAttribute().GetValue()) {
    case Messages::Visitor::Types::AttributeType::Enum::PAUSE: {
      Pause();
      return true;
    }
    case Messages::Visitor::Types::AttributeType::Enum::PAUSE_RESUME: {
      Resume();
      return true;
    }
    default: {
      return false;
    }
  }
}

bool ItemHandler::Visit(Messages::Data::ResponsePropertyGet * data) {
  switch (data->GetAttribute().GetValue()) {
    case Messages::Visitor::Types::AttributeType::Enum::PAUSE_STATUS: {
      data->SetValue(pause_);
      return true;
    }
    default: {
      return false;
    }
  }
}

bool ItemHandler::AddItem(SystemModules::Framework::Components::ItemBase * item) {
  std::lock_guard<std::mutex> lock{mutex_};
  items_.push_back(item);
  return true;
}

bool ItemHandler::RemoveItem(SystemModules::Framework::Components::ItemBase * item) {
  std::lock_guard<std::mutex> lock{mutex_};
  for (const auto i : items_) {
    if (i == item) {
      items_.remove(i);
      return true;
    }
  }
  return false;
}

void ItemHandler::Pause() {
  logger_->Info(lock_->GetId().ToString());
  pause_ = true;
}

void ItemHandler::Resume() {
  logger_->Info(lock_->GetId().ToString());
  pause_ = false;
}

bool ItemHandler::Run() {
  // Get time
  auto timeNow = std::chrono::steady_clock::now();
  auto timeNext = timeNow + updateInterval_;
  // Test if pausing
  if (pause_) {
    std::this_thread::sleep_until(timeNext);
    return true;
  }
  // Acquire lock
  lock_->Acquire();

  while (!lock_->Update()) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
  {
    // Lock items
    std::lock_guard<std::mutex> lock{mutex_};
    // Update items
    for (const auto &item : items_) {
      (void) item->Update(timeNow);
    }
  }
  // Delay release of lock (i2c-mux-idle-disconnect delay)
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
  // Release lock
  lock_->Release();

  auto duration = std::chrono::steady_clock::now() - timeNow;
  filter_.Push(std::chrono::duration_cast<std::chrono::milliseconds>(duration));

  // Calc duration
  if (duration > updateInterval_) {
    logger_->Warning(std::string(lock_->GetId().ToString()).append(": update duration (")
        .append(std::to_string(duration.count()/1000000)).append(" ms) > interval time (")
        .append(std::to_string(updateInterval_.count())).append(" ms)"));
  }

  std::this_thread::sleep_until(timeNext);
  return true;
}

bool ItemHandler::Initialize() {
  return true;
}

bool ItemHandler::DeInitialize() {
  return false;
}

}  // namespace WorkloadBalancing
