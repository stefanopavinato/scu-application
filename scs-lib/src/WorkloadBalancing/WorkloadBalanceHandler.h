/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <unordered_map>
#include <memory>
#include "Types/LockId.h"
#include "Utils/ThreadBase.h"
#include "Messages/Data/DataVisitorBase.h"
#include "Distributor/IDistributorSubscriber.h"

namespace SystemModules::Framework::Components {
class ItemBase;
}  // namespace SystemModules::Framework::Components
namespace ResourceSharing {
class ILock;
}  // namespace ResourceSharing
namespace WorkloadBalancing {
class ItemHandler;
class WorkloadBalanceHandler :
  public Utils::ThreadBase,
  public Messages::Data::DataVisitorBase,
  public Distributor::IDistributorSubscriber {
 public:
  explicit WorkloadBalanceHandler(
    Logger::ILogger *logger);

  ~WorkloadBalanceHandler() override = default;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageData * message) override;

  bool Visit(Messages::Data::ResponseOverview * data) override;

  bool Visit(Messages::Data::ResponsePropertyGet * data) override;

  bool Visit(Messages::Data::ResponsePropertySet * data) override;

  [[nodiscard]] bool AddUpdateHandler(
    ResourceSharing::ILock * lock,
    const std::chrono::milliseconds &updateInterval);

  [[nodiscard]] bool AddItem(SystemModules::Framework::Components::ItemBase * item);

  [[nodiscard]] bool RemoveItem(SystemModules::Framework::Components::ItemBase * item);

 protected:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

 private:
  Logger::ILogger *const logger_;

  std::unordered_map<Types::LockId::Enum, std::unique_ptr<ItemHandler>> itemHandlers_;

  std::chrono::milliseconds updateInterval_;

  static const char kName[];
  static const char kDescription[];
  static const char kComponentType[];

  static const char kUpdater[];
};
}  // namespace WorkloadBalancing
