/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Common {
class ConfigurationCommon;
}  // namespace SystemModules::Configuration::Common
namespace SystemModules::Configuration::Slots {
class Slots;
}  // namespace SystemModules::Configuration::Slots
namespace SystemModules::Configuration::Modules {
class Modules;
}  // namespace SystemModules::Configuration::Modules
namespace SystemModules::Configuration {
class Configuration : public ::Configuration::ConfigContainer {
 public:
  Configuration(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] Common::ConfigurationCommon * Common() const;

  [[nodiscard]] Slots::Slots * Slots() const;

  [[nodiscard]] Modules::Modules * Modules() const;

 private:
  Common::ConfigurationCommon *common_;

  Slots::Slots *slots_;

  Modules::Modules *modules_;

  static const char kConfiguration[];
};

}  // namespace SystemModules::Configuration
