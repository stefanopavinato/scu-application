/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigArray.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
class ConfigGpio;
}  // namespace Configuration
namespace SystemModules::Configuration::Slots {
class SlotMcGpio : public ::Configuration::ConfigArray {
 public:
  SlotMcGpio(
    Logger::ILogger *logger,
    const uint32_t &slot);

  [[nodiscard]] ::Configuration::ConfigGpio *Present() const;

  [[nodiscard]] ::Configuration::ConfigGpio *Enable() const;

  [[nodiscard]] ::Configuration::ConfigGpio *Reset() const;

 private:
  ::Configuration::ConfigGpio *present_;

  ::Configuration::ConfigGpio *enable_;

  ::Configuration::ConfigGpio *reset_;

  static const char kName[];

  static const char kPresentKey[];
  static const uint32_t kPresentAddressArray[];
  static const char *kPresentNameArray[];

  static const char kEnableKey[];
  static const uint32_t kEnableAddressArray[];
  static const char *kEnableNameArray[];

  static const char kResetKey[];
  static const uint32_t kResetAddressArray[];
  static const char *kResetNameArray[];
};
}  // namespace SystemModules::Configuration::Slots
