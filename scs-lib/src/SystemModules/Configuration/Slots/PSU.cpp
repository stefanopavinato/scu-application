/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <minini/minIni.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "PSU.h"


namespace SystemModules::Configuration::Slots {

const char PSU::kName[] = "PSU";
const char PSU::kEnable[] = "Enable";

const char PSU::kIniFilePSUnitSection[] = "FBIS-SCU-MEC";
const char *PSU::kIniFilePSUEnableKey[2] = {"PSU_BKP_1_Enable", "PSU_BKP_2_Enable"};

PSU::PSU(
  Logger::ILogger *logger,
  const std::string &iniFilePath,
  const uint32_t & slot)
  : ::Configuration::ConfigContainer(std::string(kName).append(std::to_string(slot))) {
  // Open ini file
  auto iniFile = minIni(iniFilePath);

  auto value = iniFile.getbool(
    kIniFilePSUnitSection,
    kIniFilePSUEnableKey[slot-1]);

  enable_ = this->Add().KeyValue<bool>(
    kEnable,
    value);
}

::Configuration::ConfigKeyValue<bool> *PSU::Enable() const {
  return enable_;
}

}  // namespace SystemModules::Configuration::Slots
