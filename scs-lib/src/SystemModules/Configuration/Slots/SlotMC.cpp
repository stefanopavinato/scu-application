/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Configuration/Modules/ModuleSerializer.h"
#include "SystemModules/Configuration/Modules/ConfigurationSerializerAddress.h"
#include "Configuration_Slots.h"
#include "SlotMC.h"

namespace SystemModules::Configuration::Slots {

const char SlotMC::kName[] = "MC";

SlotMC::SlotMC(
  Logger::ILogger *logger,
  const std::string &iniFilePath,
  Configuration::Modules::ModuleSerializer * serializer)
  : ::Configuration::ConfigArray(
  kName) {
  auto okNokAddress = serializer->Address()->Base()->OKNOK()->Value();
  auto ndsAddress = serializer->Address()->Base()->SMF()->Value();
  for (uint32_t i = 0; i < 12; i++) {
    auto mc = std::make_unique<Slots::Configuration_Slots>(
      logger,
      i + 1,
      iniFilePath,
      serializer->Address()->Base()->MC()->Value(),
      serializer->Address()->Offset()->MC()->Value(),
      &okNokAddress,
      &ndsAddress);
    mc_.insert(std::make_pair(i, mc.get()));
    AddComponent(std::move(mc));
  }
}

Configuration_Slots * SlotMC::Mc(const uint32_t & i) const {
  return mc_.find(i)->second;
}

}  // namespace SystemModules::Configuration::Slots
