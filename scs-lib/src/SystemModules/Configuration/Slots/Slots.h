/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Modules {
class ModuleSerializer;
}  // namespace SystemModules::Configuration::Modules
namespace SystemModules::Configuration::Slots {
class ConfigurationSerializerSlot;
class SlotSCU;
class SlotPSU;
class SlotMC;
class Slots :
  public ::Configuration::ConfigContainer {
 public:
  Slots(
    Logger::ILogger *logger,
    const std::string &iniFilePath,
    Modules::ModuleSerializer * serializer);

  [[nodiscard]] SlotSCU * SCU() const;

  [[nodiscard]] SlotPSU * PSU() const;

  [[nodiscard]] SlotMC * MC() const;

 private:
  SlotSCU * scu_;

  SlotPSU * psu_;

  SlotMC * mc_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Slots
