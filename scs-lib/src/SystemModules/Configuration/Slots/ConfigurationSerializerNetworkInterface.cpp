/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Configuration/ConfigKeyValue.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerNetworkInterface.h"

namespace SystemModules::Configuration::Slots {

const char ConfigurationSerializerNetworkInterface::kMacAddress[] = "MacAddress";
const char ConfigurationSerializerNetworkInterface::kIpAddress[] = "IpAddress";
const char ConfigurationSerializerNetworkInterface::kNetMask[] = "NetMask";

ConfigurationSerializerNetworkInterface::ConfigurationSerializerNetworkInterface(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &macAddress,
  const std::string &ipAddress,
  const std::string &netMask)
  : ::Configuration::ConfigContainer(name) {
  macAddress_ = Add().KeyValue(
    kMacAddress,
    macAddress);
  ipAddress_ = Add().KeyValue(
    kIpAddress,
    ipAddress);
  netMask_ = Add().KeyValue(
    kNetMask,
    netMask);
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerNetworkInterface::MacAddress() const {
  return macAddress_;
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerNetworkInterface::IpAddress() const {
  return ipAddress_;
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationSerializerNetworkInterface::NetMask() const {
  return netMask_;
}


}  // namespace SystemModules::Configuration::Slots
