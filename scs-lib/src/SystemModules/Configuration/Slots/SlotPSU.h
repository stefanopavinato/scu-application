/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigArray.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Slots {
class PSU;
class SlotPSU :
  public ::Configuration::ConfigArray {
 public:
  SlotPSU(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] PSU * Psu(const uint32_t & i) const;

 private:
  std::unordered_map<uint32_t, PSU *> psu_;

  static const char kName[];
};

}  // namespace SystemModules::Configuration::Slots
