/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Slots {
class ConfigurationMezzanineCardApplication : public ::Configuration::ConfigContainer {
 public:
  ConfigurationMezzanineCardApplication(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *DoNotActOnBI() const;

 private:
  ::Configuration::ConfigKeyValue<bool> *doNotActOnBI_;

  static const char kName[];

  static const char kDoNotActOnBIName[];

  static const char kApplicationSection[];
  static const char kDoNotActOnBIKey[];
};
}  // namespace SystemModules::Configuration::Slots
