/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Slots {
class ConfigurationSerializerNetwork;
class ConfigurationSerializerSlot : public ::Configuration::ConfigContainer {
 public:
  ConfigurationSerializerSlot(
    Logger::ILogger *logger,
    const std::string &iniFilePath,
    const uint32_t & i);

  [[nodiscard]] ConfigurationSerializerNetwork *Network() const;

 private:
  ConfigurationSerializerNetwork *network_;

  static const char *kSlotIndex[2];
};
}  // namespace SystemModules::Configuration::Slots
