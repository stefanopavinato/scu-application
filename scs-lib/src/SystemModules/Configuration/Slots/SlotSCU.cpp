/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ConfigurationSerializerSlot.h"
#include "SlotSCU.h"

namespace SystemModules::Configuration::Slots {

const char SlotSCU::kName[] = "SCU";

SlotSCU::SlotSCU(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigArray(
  kName) {
  for (uint32_t i = 1; i < 3; i++) {
    auto scu = std::make_unique<ConfigurationSerializerSlot>(
      logger,
      iniFilePath,
      i);
    scu_.insert(std::make_pair(i, scu.get()));
    AddComponent(std::move(scu));
  }
}

ConfigurationSerializerSlot * SlotSCU::Scu(const uint32_t &i) const {
  return scu_.find(i)->second;
}

}  // namespace SystemModules::Configuration::Slots
