/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigArray.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Modules {
class ModuleSerializer;
}  // namespace SystemModules::Configuration::Modules
namespace SystemModules::Configuration::Slots {
class Configuration_Slots;
class SlotMC :
  public ::Configuration::ConfigArray {
 public:
  SlotMC(
    Logger::ILogger *logger,
    const std::string &iniFilePath,
    Configuration::Modules::ModuleSerializer * serializer);

  [[nodiscard]] Configuration_Slots * Mc(const uint32_t & i) const;

 private:
  std::unordered_map<uint32_t, Configuration_Slots *> mc_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Slots
