/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "PSU.h"
#include "SlotPSU.h"

namespace SystemModules::Configuration::Slots {

const char SlotPSU::kName[] = "PSU";

SlotPSU::SlotPSU(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigArray(
  kName) {
  for (uint32_t i = 1; i < 3; i++) {
    auto psu = std::make_unique<Slots::PSU>(
      logger,
      iniFilePath,
      i);
    psu_.insert(std::make_pair(i, psu.get()));
    AddComponent(std::move(psu));
  }
}

PSU * SlotPSU::Psu(const uint32_t & i) const {
  return psu_.find(i)->second;
}

}  // namespace SystemModules::Configuration::Slots
