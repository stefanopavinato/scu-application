/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <array>
#include "Logger/ILogger.h"
#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigScuRegister.h"
#include "SystemModules/Configuration/ConfigurationMezzanineCardScuArray.h"
#include "SystemModules/Configuration/ConfigurationMezzanineCardScuValues.h"
#include "SystemModules/Configuration/ConfigurationMezzanineCardScuAddress.h"

namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Slots {
class ConfigurationMezzanineCardScu : public ::Configuration::ConfigContainer {
 public:
  ConfigurationMezzanineCardScu(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const uint64_t &mcAddress,
    uint64_t *okNokAddress,
    uint64_t *ndsAddress);

  [[nodiscard]] ConfigurationMezzanineCardScuArray *Arrays() const;

  [[nodiscard]] ConfigurationMezzanineCardScuValues *Values() const;

  [[nodiscard]] ConfigurationMezzanineCardScuAddress *Address() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *SlotNumberAddress() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *ExpectedTypeAddress() const;

 private:
  ConfigurationMezzanineCardScuArray *arrays_;

  ConfigurationMezzanineCardScuValues *values_;

  ConfigurationMezzanineCardScuAddress *address_;

  ::Configuration::ConfigKeyValue<uint64_t> *slotNumberAddress_;

  ::Configuration::ConfigKeyValue<uint64_t> *expectedTypeAddress_;

  static const char kScu[];

  static const char kSourceInternal[];

  static const char kSlotNumberAddressKey[];
  static const uint64_t kSlotNumberAddress;

  static const char kExpectedTypeAddressKey[];
  static const uint64_t kExpectedTypeAddress;
};
}  // namespace SystemModules::Configuration::Slots
