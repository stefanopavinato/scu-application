/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Configuration/ConfigGpio.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "SlotMcGpio.h"

namespace SystemModules::Configuration::Slots {

const char SlotMcGpio::kName[] = "Gpio";

const char SlotMcGpio::kPresentKey[] = "Present";
const uint32_t SlotMcGpio::kPresentAddressArray[] =
  {498, 502, 506, 510, 466, 470, 474, 478, 482, 486, 490, 494};
const char *SlotMcGpio::kPresentNameArray[] =
    {"MC1_PRSN_N", "MC2_PRSN_N", "MC3_PRSN_N", "MC4_PRSN_N", "MC5_PRSN_N", "MC6_PRSN_N",
     "MC7_PRSN_N", "MC8_PRSN_N", "MC9_PRSN_N", "MC10_PRSN_N", "MC11_PRSN_N", "MC12_PRSN_N"};

const char SlotMcGpio::kEnableKey[] = "Enable";
const uint32_t SlotMcGpio::kEnableAddressArray[] =
  {497, 501, 505, 509, 465, 469, 473, 477, 481, 485, 489, 493};
const char *SlotMcGpio::kEnableNameArray[] =
    {"MC1_PWREN", "MC2_PWREN", "MC3_PWREN", "MC4_PWREN", "MC5_PWREN", "MC6_PWREN",
     "MC7_PWREN", "MC8_PWREN", "MC9_PWREN", "MC10_PWREN", "MC11_PWREN", "MC12_PWREN"};

const char SlotMcGpio::kResetKey[] = "Reset";
const uint32_t SlotMcGpio::kResetAddressArray[] =
  {496, 500, 504, 508, 464, 468, 472, 476, 480, 484, 488, 492};
const char *SlotMcGpio::kResetNameArray[] =
    {"MC1_RST_N", "MC2_RST_N", "MC3_RST_N", "MC4_RST_N", "MC5_RST_N", "MC6_RST_N",
     "MC7_RST_N", "MC8_RST_N", "MC9_RST_N", "MC10_RST_N", "MC11_RST_N", "MC12_RST_N"};

SlotMcGpio::SlotMcGpio(
  Logger::ILogger *logger,
  const uint32_t &slot)
  : ::Configuration::ConfigArray(
  kName) {
  // Present address
  present_ = this->Add().Gpio(
    kPresentKey,
    kPresentAddressArray[slot - 1],
    kPresentNameArray[slot - 1],
    true);
  // Enable address
  enable_ = this->Add().Gpio(
    kEnableKey,
    kEnableAddressArray[slot - 1],
    kEnableNameArray[slot - 1],
    false);
  // Reset address
  reset_ = this->Add().Gpio(
    kResetKey,
    kResetAddressArray[slot - 1],
    kResetNameArray[slot - 1],
    true);
}

::Configuration::ConfigGpio *SlotMcGpio::Present() const {
  return present_;
}

::Configuration::ConfigGpio *SlotMcGpio::Enable() const {
  return enable_;
}

::Configuration::ConfigGpio *SlotMcGpio::Reset() const {
  return reset_;
}

}  // namespace SystemModules::Configuration::Slots
