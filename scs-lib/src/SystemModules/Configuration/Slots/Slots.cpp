/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SlotSCU.h"
#include "SlotMC.h"
#include "ConfigurationSerializerSlot.h"
#include "SlotPSU.h"
#include "Slots.h"

namespace SystemModules::Configuration::Slots {

const char Slots::kName[] = "Slots";

Slots::Slots(
  Logger::ILogger *logger,
  const std::string &iniFilePath,
  Modules::ModuleSerializer * serializer)
  : ::Configuration::ConfigContainer(
    kName) {
  // Add SCUs
  auto scu = std::make_unique<SlotSCU>(
    logger,
    iniFilePath);
  scu_ = scu.get();
  AddComponent(std::move(scu));
  // Add PSUs
  auto psu = std::make_unique<SlotPSU>(
    logger,
    iniFilePath);
  psu_ = psu.get();
  AddComponent(std::move(psu));
  // Add MCs
  auto mc = std::make_unique<SlotMC>(
    logger,
    iniFilePath,
    serializer);
  mc_ = mc.get();
  AddComponent(std::move(mc));
}

SlotSCU * Slots::SCU() const {
  return scu_;
}

SlotPSU * Slots::PSU() const {
  return psu_;
}

SlotMC * Slots::MC() const {
  return mc_;
}
}  // namespace SystemModules::Configuration::Slots
