/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ConfigurationSerializerNetwork.h"
#include "ConfigurationSerializerSlot.h"

namespace SystemModules::Configuration::Slots {

const char *ConfigurationSerializerSlot::kSlotIndex[2] = {"A", "B"};

ConfigurationSerializerSlot::ConfigurationSerializerSlot(
  Logger::ILogger *logger,
  const std::string &iniFilePath,
  const uint32_t & i)
  : ConfigContainer(std::string("SCU").append(std::to_string(i))) {
  // Network
  auto network = std::make_unique<ConfigurationSerializerNetwork>(
    logger,
    kSlotIndex[i - 1],
    iniFilePath);
  network_ = network.get();
  AddComponent(std::move(network));
}

ConfigurationSerializerNetwork *ConfigurationSerializerSlot::Network() const {
  return network_;
}

}  // namespace SystemModules::Configuration::Slots
