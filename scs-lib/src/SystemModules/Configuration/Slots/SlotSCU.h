/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigArray.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Slots {
class ConfigurationSerializerSlot;
class SlotSCU :
  public ::Configuration::ConfigArray {
 public:
  SlotSCU(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ConfigurationSerializerSlot * Scu(const uint32_t & i) const;

 private:
  std::unordered_map<uint32_t, ConfigurationSerializerSlot *> scu_;

  static const char kName[];
};

}  // namespace SystemModules::Configuration::Slots
