/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template <class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Slots {
class PSU
  : public ::Configuration::ConfigContainer {
 public:
  PSU(
    Logger::ILogger *logger,
    const std::string &iniFilePath,
    const uint32_t & slot);

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *Enable() const;

 private:
  ::Configuration::ConfigKeyValue<bool> *enable_;

  static const char kName[];

  static const char kIniFilePSUnitSection[];
  static const char *kIniFilePSUEnableKey[];

  static const char kEnable[];
};
}  // namespace SystemModules::Configuration::Slots
