/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"
#include "Configuration/ConfigScuRegisterArray.h"
#include "Configuration/ConfigGpio.h"
#include "ConfigurationMezzanineCardScu.h"
#include "SlotMcGpio.h"
#include "ConfigurationMezzanineCardApplication.h"

namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Slots {
class Configuration_Slots : public ::Configuration::ConfigContainer {
 public:
  Configuration_Slots(
    Logger::ILogger *logger,
    const uint32_t &slot,
    const std::string &iniFilePath,
    const uint64_t &mcAddressBase,
    const uint64_t &mcAddressOffset,
    uint64_t *okNokAddress,
    uint64_t *ndsAddress);

  [[nodiscard]] ConfigurationMezzanineCardScu *Scu() const;

  [[nodiscard]] SlotMcGpio *Gpio() const;

  [[nodiscard]] ConfigurationMezzanineCardApplication *Application() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Name() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint32_t> *Slot() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *PathI2CManagement() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *PathI2CApplication() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *PathI2CApplicationDevice() const;


 private:
  ConfigurationMezzanineCardScu *scu_;

  SlotMcGpio *gpio_;

  ConfigurationMezzanineCardApplication *application_;

  ::Configuration::ConfigKeyValue<std::string> *name_;

  ::Configuration::ConfigKeyValue<uint32_t> *slot_;

  ::Configuration::ConfigKeyValue<std::string> *pathI2CManagement_;

  ::Configuration::ConfigKeyValue<std::string> *pathI2CApplication_;

  ::Configuration::ConfigKeyValue<std::string> *pathI2CApplicationDevice_;


  static const char kMC[];

  static const char kNameKey[];

  static const char kSlotKey[];

  static const char kPathI2CManagementKey[];

  static const char kI2CChannelSlot1[];
  static const char kI2CChannelSlot2[];
  static const char kI2CChannelSlot3[];
  static const char kI2CChannelSlot4[];
  static const char kI2CChannelSlot5[];
  static const char kI2CChannelSlot6[];
  static const char kI2CChannelSlot7[];
  static const char kI2CChannelSlot8[];
  static const char kI2CChannelSlot9[];
  static const char kI2CChannelSlot10[];
  static const char kI2CChannelSlot11[];
  static const char kI2CChannelSlot12[];
  static const char *kI2CChannels[];

  static const char kPathI2CApplicationKey[];

  static const char kPathI2cApplicationDeviceKey[];
  static const char kI2CPathBase[];
};
}  // namespace SystemModules::Configuration::Slots
