//// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Configuration/ConfigKeyValue.h"
#include <SystemModules/Framework/Accessors/AccessorGPIO.h>
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationCommonSerializer.h"

namespace SystemModules::Configuration::Common {

const char ConfigurationCommonSerializer::kName[] = "Serializer";

const char ConfigurationCommonSerializer::kSlotName[] = "Slot";
const uint32_t ConfigurationCommonSerializer::kSlotId = 499;
const char ConfigurationCommonSerializer::kSlotGpioName[] = "SLOT_ID";

ConfigurationCommonSerializer::ConfigurationCommonSerializer(
  Logger::ILogger *logger)
  : ::Configuration::ConfigContainer(
  kName) {
  // Set slot
  auto reg = SystemModules::Framework::Accessors::AccessorGPIO::Read(
    logger,
    kSlotId,
    kSlotGpioName,
    false);
  std::string slot;
  if (!reg) {
    logger->Error("Failed to read configuration");
    throw std::exception();
  }
  if (!reg.value()) {
    slot = "A";
  } else {
    slot = "B";
  }
  slot_ = Add().KeyValue(
    kSlotName,
    slot);
}

::Configuration::ConfigKeyValue<std::string> *ConfigurationCommonSerializer::Slot() const {
  return slot_;
}
}  // namespace SystemModules::Configuration::Common
