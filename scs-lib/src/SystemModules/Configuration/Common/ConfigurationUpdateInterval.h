/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Common {
class ConfigurationUpdateInterval : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationUpdateInterval(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *VerySlow() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Slow() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Medium() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *Fast() const;

 private:
  ::Configuration::ConfigKeyValue<uint64_t> *verySlow_;

  ::Configuration::ConfigKeyValue<uint64_t> *slow_;

  ::Configuration::ConfigKeyValue<uint64_t> *medium_;

  ::Configuration::ConfigKeyValue<uint64_t> *fast_;

  static const char kName[];

  static const char kVerySlowName[];
  static const uint64_t kVerySlowValue;

  static const char kSlowName[];
  static const uint64_t kSlowValue;

  static const char kMediumName[];
  static const uint64_t kMediumValue;

  static const char kFastName[];
  static const uint64_t kFastValue;
};
}  // namespace SystemModules::Configuration::Common
