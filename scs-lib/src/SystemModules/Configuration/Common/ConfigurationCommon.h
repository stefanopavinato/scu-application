/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Common {
class ConfigurationUpdateInterval;
class ConfigurationCommonSerializer;
class ConfigurationSerializerSoftware;
class ConfigurationCommon : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationCommon(
    Logger::ILogger * logger);

  [[nodiscard]] ConfigurationUpdateInterval *UpdateInterval() const;

  [[nodiscard]] ConfigurationCommonSerializer *Serializer() const;

  [[nodiscard]] ConfigurationSerializerSoftware *Software() const;

 private:
  ConfigurationUpdateInterval *updateInterval_;

  ConfigurationCommonSerializer * serializer_;

  ConfigurationSerializerSoftware *software_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Common
