/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Common {
class ConfigurationSerializerSvnInfo : public ::Configuration::ConfigContainer {
 public:
  ConfigurationSerializerSvnInfo(
    Logger::ILogger *logger,
    const std::string &name,
    const uint32_t &version,
    const std::string &branch,
    const uint32_t &timestamp,
    const std::string &date,
    const std::string &time,
    const bool &isDirty);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint32_t> *Version() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Branch() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint32_t> *Timestamp() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Date() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Time() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *IsDirty() const;

 private:
  ::Configuration::ConfigKeyValue<uint32_t> *version_;

  ::Configuration::ConfigKeyValue<std::string> *branch_;

  ::Configuration::ConfigKeyValue<uint32_t> *timestamp_;

  ::Configuration::ConfigKeyValue<std::string> *date_;

  ::Configuration::ConfigKeyValue<std::string> *time_;

  ::Configuration::ConfigKeyValue<bool> *isDirty_;

  static const char kVersionName[];

  static const char kBranchName[];

  static const char kTimestampName[];

  static const char kDateName[];

  static const char kTimeName[];

  static const char kIsDirtyName[];
};
}  // namespace SystemModules::Configuration::Common
