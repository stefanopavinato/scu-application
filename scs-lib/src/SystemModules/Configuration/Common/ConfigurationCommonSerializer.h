/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Common {
class ConfigurationCommonSerializer : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationCommonSerializer(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<std::string> *Slot() const;

 private:
  ::Configuration::ConfigKeyValue<std::string> *slot_;

  static const char kName[];

  static const char kSlotName[];
  static const uint32_t kSlotId;
  static const char kSlotGpioName[];
};
}  // namespace SystemModules::Configuration::Common
