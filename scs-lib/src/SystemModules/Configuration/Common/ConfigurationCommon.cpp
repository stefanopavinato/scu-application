/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ConfigurationUpdateInterval.h"
#include "ConfigurationCommonSerializer.h"
#include "ConfigurationSerializerSoftware.h"
#include "ConfigurationCommon.h"

namespace SystemModules::Configuration::Common {

const char ConfigurationCommon::kName[] = "Common";

ConfigurationCommon::ConfigurationCommon(
  Logger::ILogger * logger)
  : ::Configuration::ConfigContainer(
  kName) {
  // Update interval
  auto updateinterval = std::make_unique<ConfigurationUpdateInterval>(
    logger);
  updateInterval_ = updateinterval.get();
  AddComponent(std::move(updateinterval));
  // Serializer
  auto serializer = std::make_unique<ConfigurationCommonSerializer>(
    logger);
  serializer_ = serializer.get();
  AddComponent(std::move(serializer));
  // Software
  auto software = std::make_unique<ConfigurationSerializerSoftware>(
    logger);
  software_ = software.get();
  AddComponent(std::move(software));
}

ConfigurationUpdateInterval *ConfigurationCommon::UpdateInterval() const {
  return updateInterval_;
}

ConfigurationCommonSerializer *ConfigurationCommon::Serializer() const {
  return serializer_;
}

ConfigurationSerializerSoftware *ConfigurationCommon::Software() const {
  return software_;
}

}  // namespace SystemModules::Configuration::Common
