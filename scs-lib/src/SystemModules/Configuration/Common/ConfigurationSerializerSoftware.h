/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigArray.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Common {
class ConfigurationSerializerSvnInfo;
class ConfigurationSerializerSoftware : public ::Configuration::ConfigArray {
 public:
  explicit ConfigurationSerializerSoftware(
    Logger::ILogger *logger);

  [[nodiscard]] ConfigurationSerializerSvnInfo *Application() const;

  [[nodiscard]] ConfigurationSerializerSvnInfo *Platform() const;

 private:
  ConfigurationSerializerSvnInfo *application_;

  ConfigurationSerializerSvnInfo *platform_;

  static const char kName[];

  static const char kApplicationName[];
  static const char kApplicationPath[];

  static const char kPlatformName[];
  static const char kPlatformPath[];
};
}  // namespace SystemModules::Configuration::Common
