/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Configuration/ConfigKeyValue.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerAddressBase.h"

namespace SystemModules::Configuration::Modules {

const char ConfigurationSerializerAddressBase::kName[] = "Base";

const char ConfigurationSerializerAddressBase::kCFGName[] = "CFG";
const uint64_t ConfigurationSerializerAddressBase::kCFGAddress = 0x0;

const char ConfigurationSerializerAddressBase::kMCName[] = "MC";
const uint64_t ConfigurationSerializerAddressBase::kMCAddress = 0x400;

const char ConfigurationSerializerAddressBase::kSLINKName[] = "SLINK";
const uint64_t ConfigurationSerializerAddressBase::kSLINKAddress = 0x1000;

const char ConfigurationSerializerAddressBase::kOKNOKName[] = "OKNOK";
const uint64_t ConfigurationSerializerAddressBase::kOKNOKAddress = 0x2000;

const char ConfigurationSerializerAddressBase::kSMFName[] = "SMF";
const uint64_t ConfigurationSerializerAddressBase::kSMFAddress = 0x3000;

ConfigurationSerializerAddressBase::ConfigurationSerializerAddressBase(
  Logger::ILogger *logger)
  : ::Configuration::ConfigContainer(
    kName) {
  cfg_ = Add().KeyValue(
    kCFGName,
    kCFGAddress);
  mc_ = Add().KeyValue(
    kMCName,
    kMCAddress);
  slink_ = Add().KeyValue(
    kSLINKName,
    kSLINKAddress);
  oknok_ = Add().KeyValue(
    kOKNOKName,
    kOKNOKAddress);
  smf_ = Add().KeyValue(
    kSMFName,
    kSMFAddress);
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressBase::CFG() const {
  return cfg_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressBase::MC() const {
  return mc_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressBase::SLINK() const {
  return slink_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressBase::OKNOK() const {
  return oknok_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressBase::SMF() const {
  return smf_;
}

}  // namespace SystemModules::Configuration::Modules
