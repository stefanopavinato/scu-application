/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace SystemModules::Configuration::Modules {
class ModuleBUMC
  : public ::Configuration::ConfigContainer {
 public:
  ModuleBUMC();

 private:
  static const char kName[];
};
}  // namespace SystemModules::Configuration::Modules
