/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Modules {
class ModuleFanUnit : public ::Configuration::ConfigContainer {
 public:
  ModuleFanUnit(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint8_t> *FanSpeed() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *FanAEnable() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *FanBEnable() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<bool> *FanMEnable() const;

 private:
  ::Configuration::ConfigKeyValue<uint8_t> *fanSpeed_;

  ::Configuration::ConfigKeyValue<bool> *fanAEnable_;

  ::Configuration::ConfigKeyValue<bool> *fanBEnable_;

  ::Configuration::ConfigKeyValue<bool> *fanMEnable_;

  static const char kSourceIniFile[];

  static const char kIniFileFanUnitSection[];
  static const char kIniFileFanSpeedKey[];
  static const char kIniFileFanAEnableKey[];
  static const char kIniFileFanBEnableKey[];
  static const char kIniFileFanMEnableKey[];

  static const char kFanUnit[];
  static const char kFanSpeedKey[];
  static const char kFanAEnableKey[];
  static const char kFanBEnableKey[];
  static const char kFanMEnableKey[];
};

}  // namespace SystemModules::Configuration::Modules
