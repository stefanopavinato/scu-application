/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace SystemModules::Configuration::Modules {
class ModulePowerSupply
  : public ::Configuration::ConfigContainer {
 public:
  ModulePowerSupply();

 private:
  static const char kName[];
};

}  // namespace SystemModules::Configuration::Modules
