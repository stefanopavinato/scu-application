/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <minini/minIni.h>
#include "Logger/ILogger.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "Configuration/ConfigKeyValue.h"
#include "ModuleFanUnit.h"

namespace SystemModules::Configuration::Modules {

const char ModuleFanUnit::kSourceIniFile[] = "IniFile";

const char ModuleFanUnit::kIniFileFanUnitSection[] = "FBIS-SCU-FU";
const char ModuleFanUnit::kIniFileFanSpeedKey[] = "STARTUP_SPEED_PWM";
const char ModuleFanUnit::kIniFileFanAEnableKey[] = "FAN_A_Enable";
const char ModuleFanUnit::kIniFileFanBEnableKey[] = "FAN_B_Enable";
const char ModuleFanUnit::kIniFileFanMEnableKey[] = "FAN_M_Enable";

const char ModuleFanUnit::kFanUnit[] = "FanUnit";
const char ModuleFanUnit::kFanSpeedKey[] = "FanSpeed";
const char ModuleFanUnit::kFanAEnableKey[] = "FanAEnable";
const char ModuleFanUnit::kFanBEnableKey[] = "FanBEnable";
const char ModuleFanUnit::kFanMEnableKey[] = "FanMEnable";

ModuleFanUnit::ModuleFanUnit(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kFanUnit) {
  // Open ini file
  auto iniFile = minIni(iniFilePath);
  // Fan speed
  {
    auto value = iniFile.geti(
      kIniFileFanUnitSection,
      kIniFileFanSpeedKey);
    if (value == 0) {
      logger->Error("Failed to read value from ini file");
      throw std::exception();
    }
    fanSpeed_ = this->Add().KeyValue(
      kFanSpeedKey,
      static_cast<uint8_t>(value));
  }
  // Fan A enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanAEnableKey);
    fanAEnable_ = this->Add().KeyValue(
      kFanAEnableKey,
      value);
  }
  // Fan B enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanBEnableKey);
    fanBEnable_ = this->Add().KeyValue(
      kFanBEnableKey,
      value);
  }
  // Fan M enable
  {
    auto value = iniFile.getbool(
      kIniFileFanUnitSection,
      kIniFileFanMEnableKey);
    fanMEnable_ = this->Add().KeyValue<bool>(
      kFanMEnableKey,
      value);
  }
}

::Configuration::ConfigKeyValue<uint8_t> *ModuleFanUnit::FanSpeed() const {
  return fanSpeed_;
}

::Configuration::ConfigKeyValue<bool> *ModuleFanUnit::FanAEnable() const {
  return fanAEnable_;
}

::Configuration::ConfigKeyValue<bool> *ModuleFanUnit::FanBEnable() const {
  return fanBEnable_;
}

::Configuration::ConfigKeyValue<bool> *ModuleFanUnit::FanMEnable() const {
  return fanMEnable_;
}

}  // namespace SystemModules::Configuration::Modules
