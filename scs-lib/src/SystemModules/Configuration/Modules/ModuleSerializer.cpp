/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Configuration/Devices/Configuration_HotSwapController.h"
#include "ConfigurationSerializerAddress.h"
#include "ModuleSerializer.h"

namespace SystemModules::Configuration::Modules {

const char ModuleSerializer::kName[] = "Serializer";

ModuleSerializer::ModuleSerializer(
  Logger::ILogger * logger)
  : ::Configuration::ConfigContainer(
  kName) {
  // Hot swap controller
  auto hotSwapController = std::make_unique<Devices::Configuration_HotSwapController>(
    4,
    4,  // Resistor 4 mOhm
    1,
    1,
    490,  // voltage divider invers: 100*((R1+R2)/R2)
    1250);  // The SOURCE pin has a 1/12.5 resistive divider, multiply is 100 to high => 12.5*100
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));

  auto address = std::make_unique<ConfigurationSerializerAddress>(
    logger);
  address_ = address.get();
  AddComponent(std::move(address));
}

Devices::Configuration_HotSwapController *ModuleSerializer::HotSwapController() const {
  return hotSwapController_;
}

ConfigurationSerializerAddress *ModuleSerializer::Address() const {
  return address_;
}

}  // namespace SystemModules::Configuration::Modules
