/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ConfigurationSerializerAddress.h"

namespace SystemModules::Configuration::Modules {

const char ConfigurationSerializerAddress::kName[] = "Address";

ConfigurationSerializerAddress::ConfigurationSerializerAddress(
  Logger::ILogger *logger)
  : ConfigContainer(
    kName) {
  // Address bases
  auto base = std::make_unique<ConfigurationSerializerAddressBase>(logger);
  base_ = base.get();
  AddComponent(std::move(base));
  // Address offsets
  auto offset = std::make_unique<ConfigurationSerializerAddressOffset>(logger);
  offset_ = offset.get();
  AddComponent(std::move(offset));
}

ConfigurationSerializerAddressBase *ConfigurationSerializerAddress::Base() const {
  return base_;
}

ConfigurationSerializerAddressOffset *ConfigurationSerializerAddress::Offset() const {
  return offset_;
}

}  // namespace SystemModules::Configuration::Modules
