/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "ModuleBUMC.h"
#include "ModuleCHMC.h"
#include "ModuleCLMC.h"
#include "ModuleFanUnit.h"
#include "ModuleISMC1.h"
#include "ModuleISMC2.h"
#include "ModuleLVDSMC.h"
#include "ModulePowerSupply.h"
#include "ModuleRS485MC.h"
#include "ModuleSerializer.h"
#include "Modules.h"

namespace SystemModules::Configuration::Modules {

const char Modules::kName[] = "Modules";

Modules::Modules(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kName) {
  // Serializer
  auto serializer = std::make_unique<ModuleSerializer>(
    logger);
  serializer_ = serializer.get();
  AddComponent(std::move(serializer));
  // BUMC
  auto bumc = std::make_unique<ModuleBUMC>();
  bumc_ = bumc.get();
  AddComponent(std::move(bumc));
  // CHMC
  auto chmc = std::make_unique<ModuleCHMC>();
  chmc_ = chmc.get();
  AddComponent(std::move(chmc));
  // CLMC
  auto clmc = std::make_unique<ModuleCLMC>();
  clmc_ = clmc.get();
  AddComponent(std::move(clmc));
  // FanUnit
  auto fanUnit = std::make_unique<ModuleFanUnit>(
    logger,
    iniFilePath);
  fanUnit_ = fanUnit.get();
  AddComponent(std::move(fanUnit));
  // ISMC1
  auto ismc1 = std::make_unique<ModuleISMC1>();
  ismc1_ = ismc1.get();
  AddComponent(std::move(ismc1));
  // ISMC2
  auto ismc2 = std::make_unique<ModuleISMC2>();
  ismc2_ = ismc2.get();
  AddComponent(std::move(ismc2));
  // LVDSMC
  auto lvdsmc = std::make_unique<ModuleLVDSMC>();
  lvdsmc_ = lvdsmc.get();
  AddComponent(std::move(lvdsmc));
  // PowerSupply
  auto powerSupply = std::make_unique<ModulePowerSupply>();
  powerSupply_ = powerSupply.get();
  AddComponent(std::move(powerSupply));
  // RS485MC
  auto rs485mc = std::make_unique<ModuleRS485MC>();
  rs485mc_ = rs485mc.get();
  AddComponent(std::move(rs485mc));
}

ModuleSerializer *Modules::Serializer() const {
  return serializer_;
}


ModuleBUMC *Modules::BUMC() const {
  return bumc_;
}

ModuleCHMC *Modules::CHMC() const {
  return chmc_;
}

ModuleCLMC *Modules::CLMC() const {
  return clmc_;
}

ModuleFanUnit *Modules::FanUnit() const {
  return fanUnit_;
}

ModuleISMC1 *Modules::ISMC1() const {
  return ismc1_;
}

ModuleISMC2 *Modules::ISMC2() const {
  return ismc2_;
}

ModuleLVDSMC *Modules::LVDSMC() const {
  return lvdsmc_;
}

ModulePowerSupply *Modules::PowerSupply() const {
  return powerSupply_;
}

ModuleRS485MC *Modules::RS485MC() const {
  return rs485mc_;
}

}  // namespace SystemModules::Configuration::Modules
