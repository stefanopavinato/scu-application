/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ModuleBUMC.h"

namespace SystemModules::Configuration::Modules {

const char ModuleBUMC::kName[] = "BUMC";

ModuleBUMC::ModuleBUMC()
  : ::Configuration::ConfigContainer(
  kName) {
}

}  // namespace SystemModules::Configuration::Modules
