/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Configuration {
template<class T> class ConfigKeyValue;
}  // namespace Configuration
namespace SystemModules::Configuration::Modules {
class ConfigurationSerializerAddressOffset : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationSerializerAddressOffset(
    Logger::ILogger *logger);

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *MC() const;

  [[nodiscard]] ::Configuration::ConfigKeyValue<uint64_t> *SLINK() const;

 private:
  static const char kName[];

  ::Configuration::ConfigKeyValue<uint64_t> *mc_;

  ::Configuration::ConfigKeyValue<uint64_t> *slink_;

  static const char kMCName[];
  static const uint64_t kMCAddress;

  static const char kSLINKName[];
  static const uint64_t kSLINKAddress;
};
}  // namespace SystemModules::Configuration::Modules
