/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ModulePowerSupply.h"

namespace SystemModules::Configuration::Modules {

const char ModulePowerSupply::kName[] = "PSU";

ModulePowerSupply::ModulePowerSupply()
  : ::Configuration::ConfigContainer(
  kName) {
}

}  // namespace SystemModules::Configuration::Modules
