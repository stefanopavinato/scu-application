/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"
#include "ConfigurationSerializerAddressBase.h"
#include "ConfigurationSerializerAddressOffset.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Modules {
class ConfigurationSerializerAddress : public ::Configuration::ConfigContainer {
 public:
  explicit ConfigurationSerializerAddress(
    Logger::ILogger *logger);

  [[nodiscard]] ConfigurationSerializerAddressBase *Base() const;

  [[nodiscard]] ConfigurationSerializerAddressOffset *Offset() const;

 private:
  ConfigurationSerializerAddressBase *base_;

  ConfigurationSerializerAddressOffset *offset_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Modules
