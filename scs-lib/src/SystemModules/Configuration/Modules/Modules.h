/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Modules {
class ModuleBUMC;
class ModuleCHMC;
class ModuleCLMC;
class ModuleFanUnit;
class ModuleISMC1;
class ModuleISMC2;
class ModuleLVDSMC;
class ModulePowerSupply;
class ModuleRS485MC;
class ModuleSerializer;
class Modules : public ::Configuration::ConfigContainer {
 public:
  Modules(
    Logger::ILogger *logger,
    const std::string &iniFilePath);

  [[nodiscard]] ModuleSerializer *Serializer() const;

  [[nodiscard]] ModuleBUMC *BUMC() const;

  [[nodiscard]] ModuleCHMC *CHMC() const;

  [[nodiscard]] ModuleCLMC *CLMC() const;

  [[nodiscard]] ModuleFanUnit *FanUnit() const;

  [[nodiscard]] ModuleISMC1 *ISMC1() const;

  [[nodiscard]] ModuleISMC2 *ISMC2() const;

  [[nodiscard]] ModuleLVDSMC *LVDSMC() const;

  [[nodiscard]] ModulePowerSupply *PowerSupply() const;

  [[nodiscard]] ModuleRS485MC *RS485MC() const;

 private:
  ModuleSerializer *serializer_;

  ModuleBUMC *bumc_;

  ModuleCHMC *chmc_;

  ModuleCLMC *clmc_;

  ModuleFanUnit *fanUnit_;

  ModuleISMC1 *ismc1_;

  ModuleISMC2 *ismc2_;

  ModuleLVDSMC *lvdsmc_;

  ModulePowerSupply *powerSupply_;

  ModuleRS485MC *rs485mc_;

  static const char kName[];
};

}  // namespace SystemModules::Configuration::Modules
