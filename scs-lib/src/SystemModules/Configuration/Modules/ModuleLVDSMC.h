/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace SystemModules::Configuration::Devices {
class Configuration_HotSwapController;
}  // namespace SystemModules::Configuration::Devices
namespace SystemModules::Configuration::Modules {
class ModuleLVDSMC : public ::Configuration::ConfigContainer {
 public:
  ModuleLVDSMC();

  [[nodiscard]] Devices::Configuration_HotSwapController *HotSwapController() const;

 private:
  Devices::Configuration_HotSwapController *hotSwapController_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Modules
