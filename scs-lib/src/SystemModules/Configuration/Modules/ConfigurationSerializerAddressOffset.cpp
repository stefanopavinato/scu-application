/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "Configuration/ConfigKeyValue.h"
#include "Configuration/Builder/ConfigurationBuilder.h"
#include "ConfigurationSerializerAddressOffset.h"

namespace SystemModules::Configuration::Modules {

const char ConfigurationSerializerAddressOffset::kName[] = "Offset";

const char ConfigurationSerializerAddressOffset::kMCName[] = "MC";
const uint64_t ConfigurationSerializerAddressOffset::kMCAddress = 0x100;

const char ConfigurationSerializerAddressOffset::kSLINKName[] = "SLINK";
const uint64_t ConfigurationSerializerAddressOffset::kSLINKAddress = 0x100;

ConfigurationSerializerAddressOffset::ConfigurationSerializerAddressOffset(
  Logger::ILogger *logger)
  : ::Configuration::ConfigContainer(
  kName) {
  mc_ = Add().KeyValue(
    kMCName,
    kMCAddress);
  slink_ = Add().KeyValue(
    kSLINKName,
    kSLINKAddress);
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressOffset::MC() const {
  return mc_;
}

::Configuration::ConfigKeyValue<uint64_t> *ConfigurationSerializerAddressOffset::SLINK() const {
  return slink_;
}

}  // namespace SystemModules::Configuration::Modules
