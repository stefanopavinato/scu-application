/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigContainer.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules::Configuration::Devices {
class Configuration_HotSwapController;
}  // namespace SystemModules::Configuration::Devices
namespace SystemModules::Configuration::Modules {
class ConfigurationSerializerAddress;
class ModuleSerializer : public ::Configuration::ConfigContainer {
 public:
  explicit ModuleSerializer(
    Logger::ILogger * logger);

  [[nodiscard]] Devices::Configuration_HotSwapController *HotSwapController() const;

  [[nodiscard]] ConfigurationSerializerAddress *Address() const;

 private:
  Devices::Configuration_HotSwapController *hotSwapController_;

  ConfigurationSerializerAddress *address_;

  static const char kName[];
};
}  // namespace SystemModules::Configuration::Modules
