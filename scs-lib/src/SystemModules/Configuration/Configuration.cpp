/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Slots/Slots.h"
#include "SystemModules/Configuration/Modules/Modules.h"
#include "Configuration.h"

namespace SystemModules::Configuration {

const char Configuration::kConfiguration[] = "Scu";

Configuration::Configuration(
  Logger::ILogger *logger,
  const std::string &iniFilePath)
  : ::Configuration::ConfigContainer(
  kConfiguration) {
  (void) logger;
  (void) iniFilePath;
  // Common
  auto common = std::make_unique<Common::ConfigurationCommon>(
    logger);
  common_ = common.get();
  AddComponent(std::move(common));
  // Mezzanine cards
  auto modules = std::make_unique<Modules::Modules>(
    logger,
    iniFilePath);
  modules_ = modules.get();
  AddComponent(std::move(modules));
  // Slots
  auto slots = std::make_unique<Slots::Slots>(
    logger,
    iniFilePath,
    modules_->Serializer());
  slots_ = slots.get();
  AddComponent(std::move(slots));
}

Common::ConfigurationCommon * Configuration::Common() const {
  return common_;
}

Slots::Slots * Configuration::Slots() const {
  return slots_;
}

Modules::Modules *Configuration::Modules() const {
  return modules_;
}

}  // namespace SystemModules::Configuration
