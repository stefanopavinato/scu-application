/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/ScuStatus.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework::Actions {
class ActionSerializerStatus : public Framework::Actions::ActionTemplate<Types::ScuStatus> {
 public:
  explicit ActionSerializerStatus(
    Framework::Components::ModuleHandler const *moduleHandler);

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    Types::ScuStatus *valueOut) override;

 private:
  static const char kDescription[];

  const Framework::Components::ModuleHandler *moduleHandler_;

  Types::ScuStatus status_;

  Types::ScuStatus statusOld_;
};
}  // namespace SystemModules::Framework::Actions
