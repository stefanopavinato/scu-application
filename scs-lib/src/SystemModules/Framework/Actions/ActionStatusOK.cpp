/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ActionStatusOK.h"

namespace SystemModules {
namespace Framework {
namespace Actions {

const char ActionStatusOK::kDescription[] = "Checks if the status of an item is ok";

ActionStatusOK::ActionStatusOK(
  Types::ComponentStatus const *itemStatus)
  : ActionTemplate<bool>(kDescription), itemStatus_(itemStatus) {
}

bool ActionStatusOK::Update(
  const std::chrono::steady_clock::time_point &now,
  bool *valueOut) {
  (void) now;
  if (!valueOut || !itemStatus_) {
    return false;
  }
  *valueOut = itemStatus_->AlertSeverity().GetEnum() < Types::AlertSeverity::Enum::INFORMATIONAL;
  return true;
}

}  // namespace Actions
}  // namespace Framework
}  // namespace SystemModules
