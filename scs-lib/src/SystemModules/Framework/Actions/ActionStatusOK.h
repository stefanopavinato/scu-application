/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <Types/ComponentStatus.h>
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework::Actions {

class ActionStatusOK : public ActionTemplate<bool> {
 public:
  ActionStatusOK(
    Types::ComponentStatus const *itemStatus);

  ~ActionStatusOK() override = default;

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    bool *triggerOutput) override;

 private:
  Types::ComponentStatus const *const itemStatus_;

  static const char kDescription[];
};

}  // namespace SystemModules::Framework::Actions
