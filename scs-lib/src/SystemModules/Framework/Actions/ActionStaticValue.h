/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/ModuleType.h"
#include "ActionTemplate.h"

namespace SystemModules::Framework::Actions {
template<class T>
class ActionStaticValue : public ActionTemplate<T> {
 public:
  explicit ActionStaticValue(
    const T &value);

  ~ActionStaticValue() override = default;

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    T *valueOut) override;

 private:
  const T value_;

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Actions

#include "ActionStaticValue.tpp"
