/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "ActionFanSpeed.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"

namespace SystemModules::Framework::Actions {

const char ActionFanSpeed::kDescription[] = "FAN_UNIT";

const uint8_t ActionFanSpeed::pwm_max_ = 110;

const uint8_t ActionFanSpeed::pwm_min_ = 254;

const uint8_t ActionFanSpeed::pwm_default_ = 200;

ActionFanSpeed::ActionFanSpeed(
  Framework::Components::ModuleHandler *handler,
  Framework::Components::ItemIn<Types::Value<double>> *speed_self,
  Framework::Components::ItemOut<int32_t> *enable_self,
  Framework::Components::ItemIn<Types::Value<double>> *speed_other1,
  Framework::Components::ItemOut<int32_t> *enable_other1,
  Framework::Components::ItemIn<Types::Value<double>> *speed_other2,
  Framework::Components::ItemOut<int32_t> *enable_other2)
  : ActionTemplate(kDescription), handler_(handler), speed_self_(speed_self), enable_self_(enable_self),
    speed_other1_(speed_other1), enable_other1_(enable_other1), speed_other2_(speed_other2),
    enable_other2_(enable_other2) {
}

bool ActionFanSpeed::Update(
  const std::chrono::steady_clock::time_point &now,
  uint8_t *valueOut) {
  (void) now;
  // Check if the own fan is ok
  if (speed_self_->GetAlertSeverity() >= Types::AlertSeverity::Enum::ERROR) {
    *valueOut = pwm_default_;
    return true;
  }
  // Check status of system temperature
  /*
  if (handler_->GroupAlertSeverity(Types::ValueGroup::Enum::TEMPERATURE).IsHigherThan(
      Types::AlertSeverity::Enum::ERROR)) {
    *value = pwm_max_;
    return;
  }
   */
  // Check status of the other fans
  if ((*enable_other1_->GetValue() > 0 &&
       speed_other1_->GetAlertSeverity() >= Types::AlertSeverity::Enum::ERROR) ||
      (*enable_other2_->GetValue() > 0 &&
       speed_other2_->GetAlertSeverity() >= Types::AlertSeverity::Enum::ERROR)) {
    *valueOut = pwm_max_;
    return true;
  }
  // Recover if everything is ok
  *valueOut = pwm_default_;
  return true;
}

}  // namespace SystemModules::Framework::Actions
