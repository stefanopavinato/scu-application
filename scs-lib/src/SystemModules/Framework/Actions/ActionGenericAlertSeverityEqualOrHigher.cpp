/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ActionGenericAlertSeverityEqualOrHigher.h"

namespace SystemModules::Framework::Actions {

const char ActionGenericAlertSeverityEqualOrHigher::kDescription[] = "Action alert severity level.";

ActionGenericAlertSeverityEqualOrHigher::ActionGenericAlertSeverityEqualOrHigher(
  const Types::AlertSeverity::Enum &maximumAlertSeverity)
  : ActionTemplate<bool>(
  kDescription), maximumAlertSeverity_(maximumAlertSeverity), isSetup_(false) {
}

void ActionGenericAlertSeverityEqualOrHigher::AddStatus(
  SystemModules::Framework::Components::Component const *component) {
  // Check item for nullptr
  if (!component) {
    return;
  }
  // Add status
  components_.push_back(component);
}

bool ActionGenericAlertSeverityEqualOrHigher::Update(
  const std::chrono::steady_clock::time_point &now,
  bool *valueOut) {
  (void) now;
  bool severityLevelReached = false;
  // Validate status of added items
  for (const auto &component : components_) {
    if (component->GetAlertSeverity() >= maximumAlertSeverity_.GetEnum()) {
      severityLevelReached = true;
    }
  }
  *valueOut = severityLevelReached;
  return true;
}

}  // namespace SystemModules::Framework::Actions
