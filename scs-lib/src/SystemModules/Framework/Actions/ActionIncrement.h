/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ActionTemplate.h"

namespace SystemModules::Framework::Actions {
template <typename T>
class ActionIncrement : public ActionTemplate<T> {
 public:
  ActionIncrement(
    T const *valueIn,
    const T &step);

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    T *valueOut) override;

 private:
  T const *valueIn_;

  const T step_;

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Actions

#include "ActionIncrement.tpp"
