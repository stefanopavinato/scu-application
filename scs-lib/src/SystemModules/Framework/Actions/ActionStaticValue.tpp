/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Actions {
template<class T> const char ActionStaticValue<T>::kDescription[] = "Action returning a static value";

template<class T>
ActionStaticValue<T>::ActionStaticValue(
  const T &value)
  : ActionTemplate<T>(kDescription), value_(value) {
}

template<class T>
bool ActionStaticValue<T>::Update(
  const std::chrono::steady_clock::time_point &now,
  T *valueOut) {
  (void) now;
  if (valueOut == nullptr) {
    return false;
  }
  *valueOut = value_;
  return true;
}

}  // namespace SystemModules::Framework::Actions
