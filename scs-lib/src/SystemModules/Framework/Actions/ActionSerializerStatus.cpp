/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ActionSerializerStatus.h"

namespace SystemModules::Framework::Actions {

const char ActionSerializerStatus::kDescription[] = "SerializerStatus";

ActionSerializerStatus::ActionSerializerStatus(
  Framework::Components::ModuleHandler const *moduleHandler)
  : ActionTemplate(kDescription), moduleHandler_(moduleHandler) {
}

bool ActionSerializerStatus::Update(
  const std::chrono::steady_clock::time_point &now,
  Types::ScuStatus *valueOut) {
  (void) now;
  (void) valueOut;
  /*
  auto alertSeverity = moduleHandler_->GetGroupAlertSeverity(
      Types::ValueGroup(Types::ValueGroup::Enum::SPEED));

  // Mögliche Fehler
  // Out of memory
  // Boot Vorgang fehlgeschlagen
  switch (status_.GetEnum()) {
    case Types::ScuStatus::Enum::STARTUP: {
      // On fatal error
      // Fehler beim ini file parsen
      //
      if (alertSeverity.GetEnum() == Types::AlertSeverity::Enum::ERROR) {
        break;
      }
      // On startup complete
      if (true) {
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::PBIT: {
      // On fatal error
      if (true) {
        break;
      }
      // On warm reboot
      if (true) {
        break;
      }
      // On pbit completed
      if (true) {
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::OPERATIONAL_NORMAL: {
      // On shutdown
      if (true) {
        break;
      }
      // On fatal error
      if (alertSeverity.GetEnum() >= Types::AlertSeverity::Enum::ERROR) {
        break;
      }
      // On warning detected
      if (alertSeverity.GetEnum() >= Types::AlertSeverity::Enum::WARNING) {
        status_.SetEnum(Types::ScuStatus::Enum::OPERATIONAL_WARNING);
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::OPERATIONAL_WARNING: {
      // On shutdown
      if (true) {
        break;
      }
      // On fatal error
      if (true) {
        break;
      }
      // On warning resolved
      if (alertSeverity.GetEnum() == Types::AlertSeverity::Enum::NONE) {
        status_.SetEnum(Types::ScuStatus::Enum::OPERATIONAL_NORMAL);
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::ERROR: {
      // dar
      // On shutdown
      if (true) {
        break;
      }
      // On error cleared
      // Reset automatisch  aflls monitoring function den status ändert.
      if (true) {
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::FATAL_ERROR: {
      break;
    }
    case Types::ScuStatus::Enum::SHUTDOWN: {
      // On fatal error
      // Beim öffnen des front handle switches -> mit Indikator dass Karte entfernt werden darf..
      if (true) {
        break;
      }
      break;
    }
    case Types::ScuStatus::Enum::NONE: {
      break;
    }
  }
   */
  return true;
}
}  // namespace SystemModules::Framework::Actions
