/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Actions {
template<class T> const char ActionGetValue<T>::kDescription[] = "Action copy value";

template<class T>
ActionGetValue<T>::ActionGetValue(
  T const *valueIn)
  : ActionTemplate<T>(kDescription), valueIn_(valueIn) {
}

template<class T>
bool ActionGetValue<T>::Update(
  const std::chrono::steady_clock::time_point &now,
  T *valueOut) {
  (void) now;
  if (valueIn_ == nullptr || valueOut == nullptr) {
    return false;
  }
  *valueOut = *valueIn_;
  return true;
}

}  // namespace SystemModules::Framework::Actions
