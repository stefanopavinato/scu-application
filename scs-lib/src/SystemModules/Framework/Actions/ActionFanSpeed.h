/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework {
namespace Components {
class ModuleHandler;
}  // namespace Components
namespace Actions {
class ActionFanSpeed : public Framework::Actions::ActionTemplate<uint8_t> {
 public:
  ActionFanSpeed(
    Framework::Components::ModuleHandler *handler,
    Framework::Components::ItemIn<Types::Value<double>> *speed_self,
    Framework::Components::ItemOut<int32_t> *enable_self,
    Framework::Components::ItemIn<Types::Value<double>> *speed_other1,
    Framework::Components::ItemOut<int32_t> *enable_other1,
    Framework::Components::ItemIn<Types::Value<double>> *speed_other2,
    Framework::Components::ItemOut<int32_t> *enable_other2);

  ~ActionFanSpeed() override = default;

  bool Update(
    const std::chrono::steady_clock::time_point &now,
    uint8_t *valueOut) override;

 private:
  Framework::Components::ModuleHandler *handler_;
  Framework::Components::ItemIn<Types::Value<double>> *speed_self_;
  Framework::Components::ItemOut<int32_t> *enable_self_;
  Framework::Components::ItemIn<Types::Value<double>> *speed_other1_;
  Framework::Components::ItemOut<int32_t> *enable_other1_;
  Framework::Components::ItemIn<Types::Value<double>> *speed_other2_;
  Framework::Components::ItemOut<int32_t> *enable_other2_;

  static const char kDescription[];

  static const uint8_t pwm_max_;

  static const uint8_t pwm_min_;

  static const uint8_t pwm_default_;
};

}  // namespace Actions
}  // namespace SystemModules::Framework
