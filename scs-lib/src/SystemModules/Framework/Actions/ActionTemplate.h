/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "ActionBase.h"

namespace SystemModules::Framework::Actions {
template<class T>
class ActionTemplate : public ActionBase {
 public:
  explicit ActionTemplate(
    const std::string &description);

  ~ActionTemplate() override = default;

  [[nodiscard]] virtual bool Update(
    const std::chrono::steady_clock::time_point &now,
    T *valueOut) = 0;
};
}  // namespace SystemModules::Framework::Actions

#include "ActionTemplate.tpp"
