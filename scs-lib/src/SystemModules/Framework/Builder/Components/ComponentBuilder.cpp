/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Components/I2CDevice24C02.h"
#include "ComponentBuilderAdd.h"
#include "I2CEEPRomBuilderAdd.h"
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
ComponentBuilder::ComponentBuilder(Framework::Components::Container *parent)
  : parent_(parent) {
}

ComponentBuilder ComponentBuilder::Create(Framework::Components::Container *parent) {
  return ComponentBuilder(parent);
}

Framework::Components::Container *ComponentBuilder::GetParent() {
  return parent_;
}

ComponentBuilderAdd ComponentBuilder::Container(
  const std::string &name,
  const std::string &description) {
  return ComponentBuilderAdd(
    GetParent(),
    std::make_unique<Framework::Components::Container>(
      GetParent()->Logger(),
      name,
      description));
}

I2CEEPromBuilderAdd ComponentBuilder::EEProm(
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path) {
  return I2CEEPromBuilderAdd(
    GetParent(),
    std::make_unique<Framework::Components::I2CDevice24C02>(
      GetParent()->Logger(),
      name,
      description,
      address,
      path));
}

}  // namespace SystemModules::Framework::Builder::Components
