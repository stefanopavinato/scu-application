/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <memory>
#include "ComponentBuilder.h"

namespace SystemModules::Framework {
namespace Components {
class I2CDevice;
}  // namespace Components
namespace Builder::Components {
class I2CDeviceBuilderAdd : public ComponentBuilder {
 public:
  I2CDeviceBuilderAdd(
    Framework::Components::Container *parent,
    std::unique_ptr<Framework::Components::I2CDevice> container);

  void AndAdd();

  [[nodiscard]] Framework::Components::I2CDevice *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::I2CDevice> container_;
};
}  // namespace Builder::Components
}  // namespace SystemModules::Framework
