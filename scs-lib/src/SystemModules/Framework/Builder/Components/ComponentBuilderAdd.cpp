/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include "ComponentBuilderAdd.h"

namespace SystemModules::Framework::Builder::Components {

ComponentBuilderAdd::ComponentBuilderAdd(
  Framework::Components::Container *parent,
  std::unique_ptr<Framework::Components::Container> container)
  : ComponentBuilder(parent), container_(std::move(container)) {
}

void ComponentBuilderAdd::AndAdd() {
  this->GetParent()->AddComponent(std::move(container_));
}

Framework::Components::Container *ComponentBuilderAdd::AddAndReturnPointer() {
  auto retVal_ptr = container_.get();
  this->GetParent()->AddComponent(std::move(container_));
  return retVal_ptr;
}

}  // namespace SystemModules::Framework::Builder::Components
