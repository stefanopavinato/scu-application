/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#pragma once

#include <memory>
#include "ComponentBuilder.h"

namespace SystemModules::Framework::Builder::Components {
class ComponentBuilderAdd : public ComponentBuilder {
 public:
  explicit ComponentBuilderAdd(
    Framework::Components::Container *parent,
    std::unique_ptr<Framework::Components::Container> container);

  void AndAdd();

  [[nodiscard]] Framework::Components::Container *AddAndReturnPointer();

 private:
  std::unique_ptr<Framework::Components::Container> container_;
};
}  // namespace SystemModules::Framework::Builder::Components
