/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/AlertSeverity.h"
#include "SystemModules/Framework/Components/ItemIn.h"

namespace SystemModules::Framework::Builder::MonitoringFunctions {
template<typename VALUE>
class MFBuilder {
  friend class Framework::Components::ItemIn<VALUE>;

 public:
  explicit MFBuilder(
    Framework::Components::MonitoringFunctionItem<VALUE> *value);

  void IsEqualTo(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsNotEqualTo(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsLowerThan(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsEqualOrLowerThan(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsHigherThan(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsEqualOrHigherThan(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &compareValue);

  void IsInside(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &lowerLimit,
    const VALUE &upperLimit);

  void IsOutside(
    const Types::AlertSeverity::Enum &severity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay,
    const VALUE &lowerLimit,
    const VALUE &upperLimit);

 private:
  Framework::Components::MonitoringFunctionItem<VALUE> *value_;
};
}  // namespace SystemModules::Framework::Builder::MonitoringFunctions

#include "MFBuilder.tpp"
