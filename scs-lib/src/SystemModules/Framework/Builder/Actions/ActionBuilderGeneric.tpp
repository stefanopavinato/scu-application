/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Builder::Actions {

template<typename VALUE>
ActionBuilderGeneric<VALUE>::ActionBuilderGeneric(
  Framework::Components::ActionItem<VALUE> *value)
  : ActionBuilderBase<VALUE>(value) {
}

template<typename VALUE>
Framework::Actions::ActionGenericAlertSeverityEqualOrHigher *ActionBuilderGeneric<VALUE>::AlertSeverityEqualOrHigher(
  const Types::AlertSeverity::Enum &alertSeverity) {
  auto action = std::make_unique<Framework::Actions::ActionGenericAlertSeverityEqualOrHigher>(
    alertSeverity);
  auto actionPtr = action.get();
  ActionBuilderBase<VALUE>::SetAction(std::move(action));
  return actionPtr;
}

template<typename VALUE>
Framework::Actions::ActionGenericBooleanEquation *ActionBuilderGeneric<VALUE>::BooleanEquation(
  const std::string &equation,
  const Types::AlertSeverity::Enum &maximumAlertSeverity) {
  auto action = std::make_unique<Framework::Actions::ActionGenericBooleanEquation>(
    equation,
    maximumAlertSeverity);
  auto actionPtr = action.get();
  ActionBuilderBase<VALUE>::SetAction(std::move(action));
  return actionPtr;
}

template<typename VALUE>
Framework::Actions::ActionCollectData *
ActionBuilderGeneric<VALUE>::CollectData() {
  auto action = std::make_unique<Framework::Actions::ActionCollectData>();
  auto actionPtr = action.get();
  ActionBuilderBase<VALUE>::SetAction(std::move(action));
  return actionPtr;
}

}  // namespace SystemModules::Framework::Builder::Actions
