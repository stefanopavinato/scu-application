/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/ComponentStatus.h"
#include "SystemModules/Framework/Components/ActionItem.h"
#include "ActionBuilderFanUnit.h"
#include "ActionBuilderSerializer.h"
#include "ActionBuilderMiscellaneous.h"
#include "ActionBuilderGeneric.h"

namespace SystemModules::Framework::Builder::Actions {
template<typename T>
class ActionBuilder : public ActionBuilderBase<T> {
 public:
  explicit ActionBuilder(
    Framework::Components::ActionItem<T> *item);

  void StaticValue(
    const T &value);

  void ReadValue(
    T *value);

  void ItemStatus(
    Types::ComponentStatus const *itemStatus);

  [[nodiscard]] ActionBuilderGeneric<T> Generic();

  [[nodiscard]] ActionBuilderFanUnit<T> FanUnit();

  [[nodiscard]] ActionBuilderSerializer<T> Serializer();

  [[nodiscard]] ActionBuilderMiscellaneous<T> Miscellaneous();

 protected:
  static ActionBuilder Create(
    Framework::Components::ActionItem<T> *item);
};
}  // namespace SystemModules::Framework::Builder::Actions

#include "ActionBuilder.tpp"
