/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionTemplate.h"
#include "SystemModules/Framework/Actions/ActionFanSpeed.h"
#include "SystemModules/Framework/Actions/ActionStaticValue.h"

namespace SystemModules::Framework::Builder::Actions {

template<class T>
ActionBuilderFanUnit<T>::ActionBuilderFanUnit(
  Components::ActionItem<T> *value)
  : ActionBuilderBase<T>(value) {
}

template<class T>
void ActionBuilderFanUnit<T>::FanSpeed(
  Framework::Components::ModuleHandler *handler,
  Framework::Components::ItemIn<Types::Value<double>> *speed_self,
  Framework::Components::ItemOut<int32_t> *enable_self,
  Framework::Components::ItemIn<Types::Value<double>> *speed_other1,
  Framework::Components::ItemOut<int32_t> *enable_other1,
  Framework::Components::ItemIn<Types::Value<double>> *speed_other2,
  Framework::Components::ItemOut<int32_t> *enable_other2) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionFanSpeed>(
      handler,
      speed_self,
      enable_self,
      speed_other1,
      enable_other1,
      speed_other2,
      enable_other2));
}

}  // namespace SystemModules::Framework::Builder::Actions
