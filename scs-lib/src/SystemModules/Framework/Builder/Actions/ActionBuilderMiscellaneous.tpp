/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Actions/ActionIncrement.h"

namespace SystemModules::Framework::Builder::Actions {

template<class T>
ActionBuilderMiscellaneous<T>::ActionBuilderMiscellaneous(
  Framework::Components::ActionItem<T> *item)
  : ActionBuilderBase<T>(item) {
}

template<class T>
void ActionBuilderMiscellaneous<T>::Increment(
  T const *valueIn,
  const T &step) {
  ActionBuilderBase<T>::SetAction(
    std::make_unique<Framework::Actions::ActionIncrement<T>>(
      valueIn,
      step));
}

}  // namespace SystemModules::Framework::Builder::Actions
