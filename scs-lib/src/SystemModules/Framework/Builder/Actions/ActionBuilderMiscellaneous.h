/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemInOut.h"
#include "ActionBuilderBase.h"

namespace SystemModules::Framework::Builder::Actions {
template<class T>
class ActionBuilderMiscellaneous : public ActionBuilderBase<T> {
 public:
  explicit ActionBuilderMiscellaneous(
    Framework::Components::ActionItem<T> *item);

  void Increment(
    T const *valueIn,
    const T &step);
};
}  // namespace SystemModules::Framework::Builder::Actions

#include "ActionBuilderMiscellaneous.tpp"
