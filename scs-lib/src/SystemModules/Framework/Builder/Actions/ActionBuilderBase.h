/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ActionItem.h"

namespace SystemModules::Framework::Builder::Actions {
template<class T>
class ActionBuilderBase {
 public:
  explicit ActionBuilderBase(
    Framework::Components::ActionItem<T> *item);

 protected:
  Framework::Components::ActionItem<T> *GetItem();

  void SetAction(
    std::unique_ptr<Framework::Actions::ActionTemplate<T>> action);

 private:
  Framework::Components::ActionItem<T> *item_;
};
}  // namespace SystemModules::Framework::Builder::Actions

#include "ActionBuilderBase.tpp"
