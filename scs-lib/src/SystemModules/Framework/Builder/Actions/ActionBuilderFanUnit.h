/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/VariantValue.h"
#include "ActionBuilderBase.h"

namespace SystemModules::Framework {
namespace Components {
template<typename VALUE>
class ItemOut;

template<typename VALUE>
class ItemIn;

class ModuleHandler;
}  // namespace Components
namespace Builder::Actions {
template<class T>
class ActionBuilderFanUnit : public ActionBuilderBase<T> {
 public:
  explicit ActionBuilderFanUnit(Components::ActionItem<T> *value);

  void FanSpeed(
    Framework::Components::ModuleHandler *handler,
    Framework::Components::ItemIn<Types::Value<double>> *speed_self,
    Framework::Components::ItemOut<int32_t> *enable_self,
    Framework::Components::ItemIn<Types::Value<double>> *speed_other1,
    Framework::Components::ItemOut<int32_t> *enable_other1,
    Framework::Components::ItemIn<Types::Value<double>> *speed_other2,
    Framework::Components::ItemOut<int32_t> *enable_other2);
};
}  // namespace Builder::Actions
}  // namespace SystemModules::Framework

#include "ActionBuilderFanUnit.tpp"
