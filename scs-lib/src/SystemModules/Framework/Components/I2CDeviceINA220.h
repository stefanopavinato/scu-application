/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "I2CDevice.h"

namespace SystemModules::Framework::Components {
class I2CDeviceINA220 : public I2CDevice {
 public:
  I2CDeviceINA220(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint32_t &i2cBus,
    const uint16_t &address,
    const std::string &path,
    const std::string &devicePath);

  ~I2CDeviceINA220() override = default;

  [[nodiscard]] std::string GetDevicePath() const;

 private:
  std::string devicePath_;

  std::string pathINA220_;

  static const char kComponentType[];

  static const char kDriverName[];

  static const char kSubPath[];
};
}  // namespace SystemModules::Framework::Components
