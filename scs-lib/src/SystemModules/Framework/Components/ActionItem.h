/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "Logger/ILogger.h"
#include "SystemModules/Framework/Actions/ActionTemplate.h"

namespace SystemModules::Framework {
namespace Builder::Actions {
template<typename VALUE>
class ActionBuilder;

template<class T>
class ActionBuilderBase;
}  // namespace Builder::Actions
namespace Components {
template<typename VALUE>
class ActionItem {
  friend class Builder::Actions::ActionBuilderBase<VALUE>;

 public:
  explicit ActionItem(
    Logger::ILogger *logger);

  virtual ~ActionItem() = default;

 protected:
  Framework::Actions::ActionTemplate<VALUE> *Action() const;

  bool SetAction(
    std::unique_ptr<Framework::Actions::ActionTemplate<VALUE>> actionItem);

 private:
  Logger::ILogger *logger_;

  std::unique_ptr<Actions::ActionTemplate<VALUE>> action_;
};
}  // namespace Components
}  // namespace SystemModules::Framework

#include "ActionItem.tpp"
