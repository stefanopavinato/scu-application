/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <list>
#include "Logger/ILogger.h"
#include "SystemModules/Framework/MonitoringFunctions/MFTemplate.h"

namespace SystemModules::Framework {
namespace Builder::MonitoringFunctions {
template<typename VALUE>
class MFBuilder;
}  // namespace Builder::MonitoringFunctions
namespace Components {
template<typename VALUE>
class MonitoringFunctionItem {
  friend class Builder::MonitoringFunctions::MFBuilder<VALUE>;

 public:
  explicit MonitoringFunctionItem(
    Logger::ILogger *logger);

  virtual ~MonitoringFunctionItem() = default;

 protected:
  std::list<std::unique_ptr<MonitoringFunctions::MFTemplate<VALUE>>> *MonitoringFunctions();

  [[nodiscard]] std::list<MonitoringFunctions::MFBase *> MonitoringFunctionsBase() const;

  void AddMonitoringFunction(
    std::unique_ptr<Framework::MonitoringFunctions::MFTemplate<VALUE>> monitoringFunction);

 private:
  Logger::ILogger *logger_;

  std::list<std::unique_ptr<MonitoringFunctions::MFTemplate<VALUE>>> monitoringFunctions_;

  std::list<MonitoringFunctions::MFBase *> monitoringFunctions_ptr_;
};
}  // namespace Components
}  // namespace SystemModules::Framework

#include "MonitoringFunctionItem.tpp"
