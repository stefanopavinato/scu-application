/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "I2CDevice.h"

namespace SystemModules::Framework::Components {
class I2CDevicePCA9539 : public I2CDevice {
 public:
  I2CDevicePCA9539(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path);

  ~I2CDevicePCA9539() override = default;

  [[nodiscard]] uint32_t GetBaseAddress() const;

 private:
  uint32_t baseAddress_;

  static const char kComponentType[];

  static const char kDriverName[];

  static const char kSubPath[];

  static const char kSubSubPath[];

  static const char kBaseAddressSubPath[];
};
}  // namespace SystemModules::Framework::Components
