/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/AlertSeverity.h"
#include "Types/ValueGroup.h"
#include "ItemBase.h"

namespace SystemModules::Framework::Components {
template<class T>
class Item : public ItemBase {
 public:
  Item<T>(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &updateInterval,
    const T &initValue);

  ~Item<T>() override = default;

  [[nodiscard]] Types::TestVariantValue GetVariantValue() const;

  [[nodiscard]] T GetValue() const;

  void SetValue(const T &value);

 protected:
  void Initialize() override;

  void DeInitialize() override;

  void ProcessInputs(
    const Types::AlertSeverity &parentAlertSeverity,
    const std::chrono::steady_clock::time_point &timePointNow) override;

  void ProcessOutputs(
    const std::chrono::steady_clock::time_point &timePointNow) override;

 private:
  T value_{};

  static const char kComponentType[];

  static const Types::ValueType::Enum kValueType;
};
}  // namespace SystemModules::Framework::Components

#include "Item.tpp"
