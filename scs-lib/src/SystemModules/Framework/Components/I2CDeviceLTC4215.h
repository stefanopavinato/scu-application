/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "I2CDevice.h"

namespace SystemModules::Framework::Components {
class I2CDeviceLTC4215 : public I2CDevice {
 public:
  I2CDeviceLTC4215(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path);

  ~I2CDeviceLTC4215() override = default;

  [[nodiscard]] std::string GetDevicePath() const;

 private:
  std::string pathLTC4215_;

  static const char kComponentType[];

  static const char kDriverName[];

  static const char kSubPath[];
};
}  // namespace SystemModules::Framework::Components
