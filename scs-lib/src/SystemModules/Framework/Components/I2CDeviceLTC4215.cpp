/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <experimental/filesystem>
#include "I2CDeviceLTC4215.h"

namespace SystemModules::Framework::Components {

const char I2CDeviceLTC4215::kComponentType[] = "I2CDevice_LTC4215";

const char I2CDeviceLTC4215::kDriverName[] = "ltc4215";

const char I2CDeviceLTC4215::kSubPath[] = "hwmon";

I2CDeviceLTC4215::I2CDeviceLTC4215(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Construct device path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(
      GetDeviceLocationPath().append("/").append(kSubPath))) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathLTC4215_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  if (pathLTC4215_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
}

std::string I2CDeviceLTC4215::GetDevicePath() const {
  return pathLTC4215_;
}

}  // namespace SystemModules::Framework::Components
