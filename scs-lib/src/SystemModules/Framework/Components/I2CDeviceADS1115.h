/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "I2CDevice.h"

namespace SystemModules::Framework::Components {
class I2CDeviceADS1115 : public I2CDevice {
 public:
  I2CDeviceADS1115(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path);

  ~I2CDeviceADS1115() override = default;

  [[nodiscard]] std::string GetDevicePath() const;

 private:
  std::string pathADS1115_;

  static const char kComponentType[];

  static const char kDriverName[];

  static const char kSubPath[];
};
}  // namespace SystemModules::Framework::Components
