/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <queue>
#include <string>

namespace SystemModules::Framework::Components {
class ToBeNamed;

class IComponent {
 public:
  virtual ~IComponent() = default;

  virtual ToBeNamed *GetComponent(std::queue<std::string> *path) = 0;
};
}  // namespace SystemModules::Framework::Components
