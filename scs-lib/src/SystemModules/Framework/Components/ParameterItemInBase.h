/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include "Types/VariantValue.h"
#include "ParameterItemBase.h"

namespace SystemModules::Framework::Components {
class ParameterItemInBase : public ParameterItemBase {
 public:
  ParameterItemInBase(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const std::string &componentType);

  ~ParameterItemInBase() override = default;

  [[nodiscard]] bool Accept(Messages::Visitor::VisitorBase *visitor) final;

  [[nodiscard]] virtual std::optional<Types::VariantValue> GetVariantValue() const = 0;

  [[nodiscard]] virtual Accessors::AccessorBase *GetAccessor() const = 0;
};

}  // namespace SystemModules::Framework::Components
