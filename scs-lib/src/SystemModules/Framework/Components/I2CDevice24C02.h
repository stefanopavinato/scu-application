/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <list>
#include <unordered_map>
#include "I2CDevice.h"

namespace SystemModules::Framework::Components {
class I2CDevice24C02 : public I2CDevice {
 public:
  I2CDevice24C02(
    Logger::ILogger *logger,
    const std::string &name,
    const std::string &description,
    const uint16_t &address,
    const std::string &path);

  ~I2CDevice24C02() override = default;

  [[nodiscard]] std::string GetDevicePath() const;

 private:
  std::string path24C02_;

  static const char kComponentType[];

  static const char kDriverName[];

  static const char kSubPath[];
};
}  // namespace SystemModules::Framework::Components
