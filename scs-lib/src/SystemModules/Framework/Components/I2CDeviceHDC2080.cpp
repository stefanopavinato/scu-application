/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <experimental/filesystem>
#include "I2CDeviceHDC2080.h"

namespace SystemModules::Framework::Components {

const char I2CDeviceHDC2080::kComponentType[] = "I2CDevice_HDC2080";

const char I2CDeviceHDC2080::kDriverName[] = "hdc2080";

const char I2CDeviceHDC2080::kSubPath[] = "iio:device";

I2CDeviceHDC2080::I2CDeviceHDC2080(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Construct device path
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(GetDeviceLocationPath())) {
      if (entry.path().filename().string().find(kSubPath) != std::string::npos) {
        pathHDC2080_ = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  if (pathHDC2080_.empty()) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
}

std::string I2CDeviceHDC2080::GetDevicePath() const {
  return pathHDC2080_;
}

}  // namespace SystemModules::Framework::Components
