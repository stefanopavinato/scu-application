/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Logger/ILogger.h"
#include "Types/AlertSeverity.h"
#include "Types/ValueGroup.h"
#include "Messages/Visitor/IVisitor.h"
#include "IComponent.h"

namespace SystemModules::Framework::Components {
class ModuleHandler;

class ToBeNamed : public Messages::Visitor::IVisitor, public IComponent {
 public:
  ToBeNamed(
    Logger::ILogger *logger,
    std::string name,
    std::string description,
    std::string componentType);

  ~ToBeNamed() override = default;

  [[nodiscard]] Logger::ILogger *Logger() const;

  /// \brief function returning the type of the component
  [[nodiscard]] std::string GetComponentType() const;

  /// \brief function returning the name of the component
  [[nodiscard]] std::string GetName() const;

  /// \brief function returning the description of the component
  [[nodiscard]] std::string GetDescription() const;

  [[nodiscard]] virtual std::string GetPath() const = 0;

  /// \brief function to collect alert severity level of a certain group.
  [[nodiscard]] virtual Types::AlertSeverity GetGroupAlertSeverity(
    const Types::ValueGroup::Enum &group) const = 0;

 protected:
  [[nodiscard]] virtual ModuleHandler const *GetModuleHandler() const = 0;

 private:
  Logger::ILogger *const logger_;

  const std::string componentType_;

  const std::string name_;

  const std::string description_;
};
}  // namespace SystemModules::Framework::Components

