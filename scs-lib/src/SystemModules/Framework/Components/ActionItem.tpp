/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Components {

template<typename VALUE>
ActionItem<VALUE>::ActionItem(
  Logger::ILogger *logger)
  : logger_(logger) {
}

template<typename VALUE>
Framework::Actions::ActionTemplate<VALUE> *ActionItem<VALUE>::Action() const {
  return action_.get();
}

template<typename VALUE>
bool ActionItem<VALUE>::SetAction(
  std::unique_ptr<Framework::Actions::ActionTemplate<VALUE>> actionItem) {
  if (!actionItem) {
    logger_->Error("Action is nullptr");
    return false;
  }
  action_ = std::move(actionItem);
  return true;
}

}  // namespace SystemModules::Framework::Components