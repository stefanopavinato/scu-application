/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Components {

template<typename VALUE>
AccessorItem<VALUE>::AccessorItem(
  Logger::ILogger *logger)
  : logger_(logger) {
}

template<typename VALUE>
Logger::ILogger *AccessorItem<VALUE>::GetLogger() {
  return logger_;
}

template<typename VALUE>
Framework::Accessors::AccessorTemplate<VALUE> *AccessorItem<VALUE>::Accessor() const {
  return accessor_.get();
}

template<typename VALUE>
bool AccessorItem<VALUE>::SetAccessor(
  std::unique_ptr<Framework::Accessors::AccessorTemplate<VALUE>> accessor) {
  if (!accessor) {
    logger_->Error("Hardware accessor is nullptr");
    return false;
  }
  accessor_ = std::move(accessor);
  return true;
}

}  // namespace SystemModules::Framework::Components
