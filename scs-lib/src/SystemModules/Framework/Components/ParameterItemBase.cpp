/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ParameterItemBase.h"

namespace SystemModules::Framework::Components {

ParameterItemBase::ParameterItemBase(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const std::string &componentType)
  : Component(
  logger,
  name,
  description,
  componentType) {
}

Types::AlertSeverity ParameterItemBase::GetGroupAlertSeverity(
  const Types::ValueGroup::Enum &group) const {
  (void) group;
  return Types::AlertSeverity(Types::AlertSeverity::Enum::OK);
}

Component *ParameterItemBase::GetComponent(
  std::queue<std::string> *path) {
  // Check if path is empty
  if (path->empty()) {
    return this;
  }
  return nullptr;
}

void ParameterItemBase::ResetMonitoringFunctions() {}

}  // namespace SystemModules::Framework::Components
