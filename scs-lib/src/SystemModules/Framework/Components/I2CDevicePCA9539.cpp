/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <thread>
#include <chrono>
#include <experimental/filesystem>
#include "SystemModules/Framework/Accessors/AccessorFile.h"
#include "I2CDevicePCA9539.h"

namespace SystemModules::Framework::Components {

const char I2CDevicePCA9539::kComponentType[] = "I2CDevice_PCA9539";

const char I2CDevicePCA9539::kDriverName[] = "pca9539";

const char I2CDevicePCA9539::kSubPath[] = "gpio";

const char I2CDevicePCA9539::kSubSubPath[] = "gpiochip";

const char I2CDevicePCA9539::kBaseAddressSubPath[] = "/base";

I2CDevicePCA9539::I2CDevicePCA9539(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  // Construct device path
  std::string devicePath;
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));
  try {
    for (auto &entry : std::experimental::filesystem::directory_iterator(
      GetDeviceLocationPath().append("/").append(kSubPath))) {
      if (entry.path().filename().string().find(kSubSubPath) != std::string::npos) {
        devicePath = entry.path();
      }
    }
  } catch (...) {
    Logger()->Error(std::string("Directory access failure: ")
                                  .append(GetDeviceLocationPath().append("/").append(kSubPath)));
    throw std::invalid_argument(std::string("Directory access failure: ")
                                  .append(GetDeviceLocationPath().append("/").append(kSubPath)));
  }
  if (devicePath.empty()) {
    Logger()->Error(std::string("Path not found: ")
                                  .append(GetDeviceLocationPath().append("/").append(kSubPath)));
    throw std::invalid_argument(std::string("Path not found: ")
                                  .append(GetDeviceLocationPath().append("/").append(kSubPath)));
  }
  // Get base address
  auto retVal = SystemModules::Framework::Accessors::AccessorFile<uint32_t, uint32_t, std::string>::Read(
    logger,
    devicePath.append(kBaseAddressSubPath),
    1,
    1);
  if (!retVal) {
    Logger()->Error(std::string("Path not found: ").append(GetDeviceLocationPath()));
    throw std::invalid_argument(std::string("Path not found: ").append(GetDeviceLocationPath()));
  }
  baseAddress_ = retVal.value();
}

uint32_t I2CDevicePCA9539::GetBaseAddress() const {
  return baseAddress_;
}

}  // namespace SystemModules::Framework::Components
