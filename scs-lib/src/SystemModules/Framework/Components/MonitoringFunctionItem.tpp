/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MonitoringFunctionItem.h"

namespace SystemModules::Framework::Components {

template<typename VALUE>
MonitoringFunctionItem<VALUE>::MonitoringFunctionItem(
  Logger::ILogger *logger)
  : logger_(logger) {
}

template<typename VALUE>
std::list<MonitoringFunctions::MFBase *> MonitoringFunctionItem<VALUE>::MonitoringFunctionsBase() const {
  return monitoringFunctions_ptr_;
}

template<typename VALUE>
std::list<std::unique_ptr<MonitoringFunctions::MFTemplate < VALUE>>> *

MonitoringFunctionItem<VALUE>::MonitoringFunctions() {
  return &monitoringFunctions_;
}

template<typename VALUE>
void MonitoringFunctionItem<VALUE>::AddMonitoringFunction(
  std::unique_ptr<MonitoringFunctions::MFTemplate<VALUE>> monitoringFunction) {
  monitoringFunctions_ptr_.push_back(monitoringFunction.get());
  monitoringFunctions_.push_back(std::move(monitoringFunction));
}

}  // namespace SystemModules::Framework::Components
