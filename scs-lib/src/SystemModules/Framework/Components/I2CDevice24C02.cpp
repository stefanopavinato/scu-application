/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <fstream>
#include <experimental/filesystem>
#include <thread>
#include "Messages/Visitor/VisitorBase.h"
#include "SystemModules/Framework/Accessors/AccessorBase.h"
#include "SystemModules/Framework/Actions/ActionBase.h"
#include "I2CDevice24C02.h"

namespace SystemModules::Framework::Components {

const char I2CDevice24C02::kComponentType[] = "I2CDevice_24C02";

const char I2CDevice24C02::kDriverName[] = "24c02";

const char I2CDevice24C02::kSubPath[] = "eeprom";

I2CDevice24C02::I2CDevice24C02(
  Logger::ILogger *logger,
  const std::string &name,
  const std::string &description,
  const uint16_t &address,
  const std::string &path)
  : I2CDevice(
  logger,
  name,
  description,
  kComponentType,
  kDriverName,
  address,
  path) {
  std::this_thread::sleep_for(std::chrono::milliseconds(100));
  // Construct eeprom path
  path24C02_ = GetDeviceLocationPath().append("/").append(kSubPath);
}

std::string I2CDevice24C02::GetDevicePath() const {
  return path24C02_;
}

}  // namespace SystemModules::Framework::Components
