/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <memory>
#include <unordered_map>
#include "Utils/ThreadBase.h"
#include "Types/ValueGroup.h"
#include "Types/AlertSeverity.h"
#include "Distributor/IDistributorSubscriber.h"
#include "ToBeNamed.h"

namespace ResourceSharing {
class ILock;
}  // namespace ResourceSharing
namespace SystemModules::Framework::Components {
class ModuleBase;
class ModuleHandler :
  public Utils::ThreadBase,
  public Distributor::IDistributorSubscriber,
  public ToBeNamed {
 public:
  ModuleHandler(
      Logger::ILogger *logger,
      const std::string &name,
      ResourceSharing::ILock *lock,
      const std::chrono::microseconds &updateInterval);

  ~ModuleHandler() override = default;

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::Message *message) override;

  bool Visit(Messages::MessageData *message) override;

  [[nodiscard]] bool Accept(Messages::Visitor::VisitorBase *visitor) override;

  ToBeNamed *GetComponent(std::queue<std::string> *path) override;

  [[nodiscard]] std::string GetPath() const override;

  [[nodiscard]] Types::AlertSeverity GetGroupAlertSeverity(
      const Types::ValueGroup::Enum &group) const override;

  void ResetAllModules();

  void StopAllModules();

  void ReleaseStopAllModules();

  bool AddModule(std::unique_ptr<Framework::Components::ModuleBase> module);

  bool AddGroupAlertSeverity(const Types::ValueGroup::Enum &group);

  std::unordered_map<std::string, std::unique_ptr<Framework::Components::ModuleBase>> *GetModules();

  std::unordered_map<Types::ValueGroup::Enum, Types::AlertSeverity> *GetGroupAlertSeverities();

 protected:
  [[nodiscard]] ModuleHandler const *GetModuleHandler() const override;

 private:
  [[nodiscard]] bool Run() override;

  [[nodiscard]] bool Initialize() override;

  [[nodiscard]] bool DeInitialize() override;

  void UpdateGroupAlertSeverities();

  std::chrono::microseconds updateInterval_;

  std::unordered_map<std::string, std::unique_ptr<Framework::Components::ModuleBase>> modules_;

  std::unordered_map<Types::ValueGroup::Enum, Types::AlertSeverity> groupAlertSeverities_;

  ResourceSharing::ILock * lock_;

  static const char kDescription[];

  static const char kComponentType[];
};
}  // namespace SystemModules::Framework::Components
