/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::MonitoringFunctions {
template<class T>
MFTemplate<T>::MFTemplate(
  const std::string &componentType,
  const Types::AlertSeverity::Enum &alertSeverity,
  const Types::MonitoringFunctionId::Enum &id,
  const std::string &description,
  const Types::ValueGroup::Enum &group,
  const std::chrono::milliseconds &onDelay,
  const std::chrono::milliseconds &offDelay)
  : MFBase(
  componentType,
  alertSeverity,
  id,
  description,
  group,
  onDelay,
  offDelay), externalAlertSeverityOld_(
  Types::AlertSeverity(
    Types::AlertSeverity::Enum::OK)), criterionActive_(false), criterionActiveOld_(false), delayOut_(false),
    resetOut_(false), valueOut_(false), reset_(false) {
}

template<class T>
void MFTemplate<T>::Update(
  const T &value,
  const Types::AlertSeverity &externalAlertSeverity,
  const std::chrono::steady_clock::time_point &timePointNow) {
  // Save the old compare value
  criterionActiveOld_ = criterionActive_;
  // Get the new compare value
  criterionActive_ = CompareValue(value);
  // Handle delays only if no external error occurred
  if (externalAlertSeverity.GetEnum() < Types::AlertSeverity::Enum::ERROR) {
    // Falling edge of external error detected -> reset timer of inactive event
    if (externalAlertSeverityOld_.GetEnum() >= Types::AlertSeverity::Enum::ERROR) {
      if (delayOut_) {
        SetOffEvent(
          timePointNow + GetOffDelay());
      } else {
        SetOnEvent(
          timePointNow + GetOnDelay());
      }
    }
    // Rising edge on compare value detected -> set on timer
    if (criterionActive_ && !criterionActiveOld_) {
      SetOnEvent(
        timePointNow + GetOnDelay());
    }
    // Falling edge on compare value detected -> set off timer
    if (!criterionActive_ && criterionActiveOld_) {
      SetOffEvent(
        timePointNow + GetOffDelay());
    }
    // On timer expired -> set delay out to true
    if (criterionActive_ && timePointNow >= GetOnEvent()) {
      delayOut_ = true;
    }
    // Off timer expired -> set delay out to false
    if (!criterionActive_ && timePointNow >= GetOffEvent()) {
      delayOut_ = false;
    }
    // Set reset out
    if (Status().AlertSeverity().GetEnum() >= Types::AlertSeverity::Enum::ERROR) {
      if (reset_) {
        reset_ = false;
        resetOut_ = false;
      }
      if (delayOut_) {
        resetOut_ = true;
      }
    } else {
      resetOut_ = delayOut_;
    }
    // Set value out and event timer
    if (valueOut_ != resetOut_) {
      if (resetOut_) {
        SetOnEventTime(std::chrono::system_clock::now());
      } else {
        SetOffEventTime(std::chrono::system_clock::now());
      }
      valueOut_ = resetOut_;
    }
  }
}

template<class T>
bool MFTemplate<T>::IsActive() const {
  return valueOut_;
}

template<class T>
void MFTemplate<T>::RequestReset() {
  if (Status().AlertSeverity().GetEnum() >= Types::AlertSeverity::Enum::ERROR) {
    reset_ = true;
  }
}

}  // namespace SystemModules::Framework::MonitoringFunctions
