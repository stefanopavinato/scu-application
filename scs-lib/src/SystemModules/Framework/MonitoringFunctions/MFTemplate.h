/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MFBase.h"

namespace SystemModules::Framework::MonitoringFunctions {
template<class T>
class MFTemplate : public MFBase {
 public:
  MFTemplate(
    const std::string &componentType,
    const Types::AlertSeverity::Enum &alertSeverity,
    const Types::MonitoringFunctionId::Enum &id,
    const std::string &description,
    const Types::ValueGroup::Enum &group,
    const std::chrono::milliseconds &onDelay,
    const std::chrono::milliseconds &offDelay);

  ~MFTemplate() override = default;

  void Update(
    const T &value,
    const Types::AlertSeverity &externalAlertSeverity,
    const std::chrono::steady_clock::time_point &timePointNow);

  [[nodiscard]] bool IsActive() const override;

  void RequestReset() override;

 protected:
  virtual bool CompareValue(const T &value) = 0;

 private:
  Types::AlertSeverity externalAlertSeverityOld_;

  bool criterionActive_;

  bool criterionActiveOld_;

  bool delayOut_;

  bool resetOut_;

  bool valueOut_;

  bool reset_;
};

}  // namespace SystemModules::Framework::MonitoringFunctions

#include "MFTemplate.tpp"
