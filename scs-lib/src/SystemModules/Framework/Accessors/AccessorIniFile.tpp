/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

#include <minini/minIni.h>

#include <utility>
#include "Utils/TypeConverter.h"

namespace SystemModules::Framework::Accessors {

template<class T>
const char AccessorIniFile<T>::kName[] = "AccessorIniFile";

template<class T>
const char AccessorIniFile<T>::kDescription[] = "";

template<class T>
AccessorIniFile<T>::AccessorIniFile(
  Logger::ILogger *logger,
  std::string path,
  std::string section,
  std::string key)
  : AccessorTemplate<T>(
  logger,
  kName,
  kDescription)
  , path_(std::move(path))
  , section_(std::move(section))
  , key_(std::move(key)) {
}

template<class T>
bool AccessorIniFile<T>::Initialize() {
  return true;
}

template<class T>
bool AccessorIniFile<T>::DeInitialize() {
  return true;
}

template<class T>
bool AccessorIniFile<T>::Read(T *value) const {
  (void) value;
  char readValue[50];
  ini_gets(
    section_.c_str(),
    key_.c_str(),
    "",
    readValue,
    sizeof(readValue),
    path_.c_str());

  auto a = std::string(readValue);

  return false;
}

template<class T>
bool AccessorIniFile<T>::Write(const T &value) {
  (void) value;
  char a[50];
  ini_puts(
    section_.c_str(),
    key_.c_str(),
    a,
    path_.c_str());
  return false;
}

}  // namespace SystemModules::Framework::Accessors
