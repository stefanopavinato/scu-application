/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace SystemModules::Framework::Accessors {

template<class T>
AccessorTemplate<T>::AccessorTemplate(
  Logger::ILogger *logger,
  const std::string &typeName,
  const std::string &description)
  : AccessorBase(logger, typeName, description) {
}

}  // namespace SystemModules::Framework::Accessors
