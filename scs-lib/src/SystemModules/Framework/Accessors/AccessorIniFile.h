/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorTemplate.h"

namespace SystemModules::Framework::Accessors {
template<class T>
class AccessorIniFile : public AccessorTemplate<T> {
 public:
  AccessorIniFile(
    Logger::ILogger *logger,
    std::string path,
    std::string section,
    std::string key);

  ~AccessorIniFile() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(T *value) const override;

  bool Write(const T &value) override;

 private:
  const std::string path_;

  const std::string section_;

  const std::string key_;

  static const char kName[];
  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Accessors

#include "AccessorIniFile.tpp"
