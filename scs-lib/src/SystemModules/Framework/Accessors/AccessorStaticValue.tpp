/************************************************************
* @Description  :
* @Author       : ZHAW-IAMP-SKS, Philippe Hindermann
* @Copyright    :
************************************************************/

namespace SystemModules::Framework::Accessors {

template<class T>
const char AccessorStaticValue<T>::kName[] = "StaticValue";

template<class T>
const char AccessorStaticValue<T>::kDescription[] = "Internal static value accessor";

template<class T>
AccessorStaticValue<T>::AccessorStaticValue(
  Logger::ILogger *logger,
  const T &value)
  : AccessorTemplate<T>(logger, kName, kDescription), value_(value) {
}

template<class T>
bool AccessorStaticValue<T>::Initialize() {
  this->SetInitialized(true);
  return true;
}

template<class T>
bool AccessorStaticValue<T>::DeInitialize() {
  this->SetInitialized(false);
  return true;
}

template<class T>
bool AccessorStaticValue<T>::Read(T *value) const {
  *value = value_;
  return true;
}

template<class T>
bool AccessorStaticValue<T>::Write(const T &value) {
  (void) value;
  return true;
}

}  // namespace SystemModules::Framework::Accessors
