/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <vector>
#include "Types/ValueType.h"
#include "AccessorTemplate.h"

namespace SystemModules::Framework::Accessors {
template<typename VALUE, typename VALUE_IN_OUT = VALUE>
class AccessorEEProm : public AccessorTemplate<VALUE> {
 public:
  explicit AccessorEEProm(
    Logger::ILogger *logger,
    const std::string &path,
    const uint32_t &address,
    const uint32_t &length,
    const VALUE_IN_OUT &scaleMultiply,
    const VALUE_IN_OUT &scaleDivide);

  ~AccessorEEProm() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(VALUE *value) const override;

  bool Write(const VALUE &value) override;

 private:
  [[nodiscard]] bool ConvertAndScaleReadValue(
    const std::vector<char> &vectorRead,
    VALUE_IN_OUT *value) const;

  [[nodiscard]] bool ScaleAndConvertWriteValue(
    const VALUE_IN_OUT &valueWrite,
    std::vector<char> *value) const;

  const std::string path_;

  const uint32_t address_;

  const uint32_t size_;

  const VALUE_IN_OUT scaleMultiply_;

  const VALUE_IN_OUT scaleDivide_;

  static const char kName[];

  static const char kDescription[];

  static const uint32_t kSize;
};

template
class AccessorEEProm<Types::Value<int32_t>, int32_t>;

template
class AccessorEEProm<Types::Value<int16_t>, int16_t>;

template
class AccessorEEProm<Types::Value<int8_t>, int8_t>;

template
class AccessorEEProm<Types::Value<uint32_t>, uint32_t>;

template
class AccessorEEProm<Types::Value<uint16_t>, uint16_t>;

template
class AccessorEEProm<Types::Value<uint8_t>, uint8_t>;

template
class AccessorEEProm<Types::Value<double>, double>;

template
class AccessorEEProm<Types::Value<float>, float>;

template
class AccessorEEProm<int32_t>;

template
class AccessorEEProm<int16_t>;

template
class AccessorEEProm<int8_t>;

template
class AccessorEEProm<uint32_t>;

template
class AccessorEEProm<uint16_t>;

template
class AccessorEEProm<uint8_t>;

template
class AccessorEEProm<double>;

template
class AccessorEEProm<float>;

template
class AccessorEEProm<bool>;

template
class AccessorEEProm<std::string>;

template
class AccessorEEProm<Types::ModuleType, std::string>;

template
class AccessorEEProm<std::chrono::minutes, uint32_t>;
}  // namespace SystemModules::Framework::Accessors
