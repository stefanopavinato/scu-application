/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <fstream>
#include <sstream>
#include <gpiod.h>
#include "Logger/ILogger.h"
#include "AccessorGPIO.h"

#ifndef CONSUMER
#define CONSUMER "scu-application"
#endif

namespace SystemModules::Framework::Accessors {

const char AccessorGPIO::kName[] = "General purpose output";

const char AccessorGPIO::kDescription[] = "General purpose output";

const char AccessorGPIO::kPathExport[] = "/sys/class/gpio/export";
const char AccessorGPIO::kPathUnExport[] = "/sys/class/gpio/unexport";
const char AccessorGPIO::kPathGpio[] = "/sys/class/gpio/gpio";
const char AccessorGPIO::kDirection[] = "/direction";
const char AccessorGPIO::kDirectionIn[] = "in";
const char AccessorGPIO::kDirectionOut[] = "out";
const char AccessorGPIO::kValue[] = "/value";

AccessorGPIO::AccessorGPIO(
  Logger::ILogger *logger,
  const uint32_t &gpio,
  const char *name,
  Types::Direction::Enum direction,
  const bool &isInverted)
  : AccessorTemplate<bool>(
  logger,
  kName,
  kDescription)
  , logger_(logger)
  , gpionumber_(gpio)
  , gpioname_(name)
  , direction_(direction)
  , isInverted_(isInverted)
  , pathDirection_(std::string(kPathGpio).append(std::to_string(gpio)).append(kDirection))
  , pathValue_(std::string(kPathGpio).append(std::to_string(gpio)).append(kValue)) {
  use_gpioname_ = false;
}

bool AccessorGPIO::Initialize() {
  // Check if gpio is already initialized
  if (IsInitialized()) {
    std::stringstream stream;
    stream << "GPIO is already initialized (Id: " << gpionumber_ << ")";
    Logger()->Info(stream.str());
    return true;
  }
  bool retVal;
  line = gpiod_line_find((gpioname_));
  if (line) {
    // use gpio name
    // get chipname and line offset
    chipname_ = gpiod_chip_name(gpiod_line_get_chip(line));
    line_offset_ = gpiod_line_offset(line);
    consumer_ = CONSUMER;
    use_gpioname_ = true;
    logger_->Info(std::string((gpioname_)).append(" ").append(std::to_string(gpionumber_)).append(" ")
                      .append(chipname_).append(" ").append(std::to_string(line_offset_))
                      .append(" ").append(consumer_));
    // Set gpio direction
    switch (direction_.GetEnum()) {
        case Types::Direction::Enum::IN: {
            int val = gpiod_line_request_input(line, consumer_);
            retVal = (val >= 0);
          break;
        }
        case Types::Direction::Enum::OUT: {
            int val = gpiod_line_request_output(line, consumer_, 0);
            retVal = (val >= 0);
          break;
        }
        case Types::Direction::Enum::INOUT: {
          // Initialize as input (default)
            int val = gpiod_line_request_input(line, consumer_);
            retVal = (val >= 0);
          break;
        }
        case Types::Direction::Enum::NONE: {
          Logger()->Error("Direction not set");
          retVal = false;
          break;
        }
      }
    } else {
    // use gpio number
    // Export gpio
    logger_->Info(std::string((gpioname_)).append(" ").append(std::to_string(gpionumber_)));
    auto fileExport = std::ofstream(kPathExport);
    if (!fileExport.is_open()) {
      std::stringstream stream;
      stream << "Failed to open export path (" << kPathExport << ")";
      Logger()->Error(stream.str());
      return false;
    }
    fileExport << gpionumber_;
    fileExport.close();
    // Set gpio direction
    switch (direction_.GetEnum()) {
      case Types::Direction::Enum::IN: {
        retVal = SetDirection(true);
        break;
      }
      case Types::Direction::Enum::OUT: {
        retVal = SetValue(false);
        retVal &= SetDirection(false);
        break;
      }
      case Types::Direction::Enum::INOUT: {
        // Initialize as input (default)
        retVal = SetDirection(true);
        break;
      }
      case Types::Direction::Enum::NONE: {
        Logger()->Error("Direction not set");
        retVal = false;
        break;
      }
    }
  }
  SetInitialized(retVal);
  return true;
}

bool AccessorGPIO::DeInitialize() {
  // Check if gpio is already initialized
  if (!IsInitialized()) {
    std::stringstream stream;
    stream << "GPIO is already de-initialized (Id: " << gpionumber_ << ")";
    Logger()->Info(stream.str());
    return true;
  }
  SetInitialized(false);
  if (use_gpioname_) {
    gpiod_line_close_chip(line);
  } else {
    // Un-export gpio
    auto fileUnExport = std::ofstream(kPathUnExport);
    if (!fileUnExport.is_open()) {
      std::stringstream stream;
      stream << "Failed to open un-export path (" << kPathUnExport << ")";
      Logger()->Error(stream.str());
      return false;
    }
    fileUnExport << gpionumber_;
    fileUnExport.close();
  }
  return true;
}

bool AccessorGPIO::Read(bool *value) const {
  // Check direction
  if (direction_.GetEnum() == Types::Direction::Enum::NONE) {
    Logger()->Error("Direction not set");
    return false;
  }
  // Read gpio
  bool tempVal;
  if (use_gpioname_) {
    errno = 0;
    int val = gpiod_line_get_value(line);
    int err = errno;
    if (val < 0) {
      logger_->Info(std::string((gpioname_)).append(" error ").append(std::to_string(err)));
      perror("Read line input failed\n");
      return false;
    }
    tempVal = (val == 1);
  } else {
    if (!GetValue(&tempVal)) {
      return false;
    }
  }
  // Invert value if needed (XOR)
  *value = tempVal ^ isInverted_;
  return true;
}

bool AccessorGPIO::Write(const bool &value) {
  if (use_gpioname_) {
    switch (direction_.GetEnum()) {
      case Types::Direction::Enum::IN: {
        Logger()->Error("Direction not valid");
        return false;
      }
      case Types::Direction::Enum::OUT: {
        // Write gpio
        int val = gpiod_line_set_value(line, value ^ isInverted_);
        if (val < 0) {
          perror("Write line input failed\n");
        }
        return val >= 0;
      }
      case Types::Direction::Enum::INOUT: {
        if (value ^ isInverted_) {
          // Set as input
          int dir = gpiod_line_direction(line);
          if (dir != GPIOD_LINE_DIRECTION_INPUT) {
            gpiod_line_release(line);
            int val = gpiod_line_request_input(line, consumer_);
            return (val >= 0);
          }
          return true;
        } else {
          // Set as output
          int dir = gpiod_line_direction(line);
          if (dir != GPIOD_LINE_DIRECTION_OUTPUT) {
            gpiod_line_release(line);
            int val = gpiod_line_request_output(line, consumer_, 0);
            if (val < 0) {
              return false;
            }
          }
          // Set value to false
          int val = gpiod_line_set_value(line, 0);
          if (val < 0) {
            perror("Write line input failed\n");
          }
          return val >= 0;
        }
      }
      case Types::Direction::Enum::NONE: {
        Logger()->Error("Direction not set");
        return false;
      }
    }
  } else {
    switch (direction_.GetEnum()) {
      case Types::Direction::Enum::IN: {
        Logger()->Error("Direction not valid");
        return false;
      }
      case Types::Direction::Enum::OUT: {
        // Write gpio
        return SetValue(value ^ isInverted_);
      }
      case Types::Direction::Enum::INOUT: {
        if (value ^ isInverted_) {
          // Set as input
          return SetDirection(true);
        } else {
          // Set as output
          if (!SetDirection(false)) {
            return false;
          }
          // Set value to false
          return SetValue(false);
        }
      }
      case Types::Direction::Enum::NONE: {
        Logger()->Error("Direction not set");
        return false;
      }
    }
  }
  return false;
}

bool AccessorGPIO::SetDirection(const bool &isInput) {
  auto fileDirection = std::ofstream(pathDirection_);
  if (!fileDirection.is_open()) {
    std::stringstream stream;
    stream << "Failed to open direction path (" << pathDirection_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  if (isInput) {
    fileDirection << kDirectionIn;
  } else {
    fileDirection << kDirectionOut;
  }
  fileDirection.close();
  return true;
}

bool AccessorGPIO::SetValue(const bool &value) {
  auto fileWrite = std::ofstream(pathValue_);
  if (!fileWrite.is_open()) {
    std::stringstream stream;
    stream << "Failed to open value path (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  fileWrite << std::noboolalpha << value;
  fileWrite.close();
  return true;
}

bool AccessorGPIO::GetValue(bool *value) const {
  // Read gpio
  auto fileRead = std::ifstream(pathValue_);
  if (!fileRead.is_open()) {
    std::stringstream stream;
    stream << "Failed to open value path (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    return false;
  }
  if (fileRead.peek() == std::ifstream::traits_type::eof()) {
    std::stringstream stream;
    stream << "Failed to read value  (" << pathValue_ << ")";
    Logger()->Error(stream.str());
    fileRead.close();
    return false;
  }
  fileRead >> std::noboolalpha >> *value;
  fileRead.close();
  return true;
}

std::optional<bool> AccessorGPIO::Read(
  Logger::ILogger *logger,
  const uint32_t &gpio,
  const char *name,
  const bool &isInverted) {
  bool retVal;
  auto accessor = AccessorGPIO(
    logger,
    gpio,
    name,
    Types::Direction::Enum::IN,
    isInverted);
  if (!accessor.Initialize()) {
    logger->Error("Failed to initialize gpio");
    return std::nullopt;
  }
  if (!accessor.Read(&retVal)) {
    logger->Error("Failed to read gpio");
    return std::nullopt;
  }
  if (!accessor.DeInitialize()) {
    logger->Error("Failed to de-initialize gpio");
    return std::nullopt;
  }
  return retVal;
}


}  // namespace SystemModules::Framework::Accessors
