/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "AccessorBase.h"

namespace SystemModules::Framework::Accessors {
template<class T>
class AccessorTemplate : public AccessorBase {
 public:
  AccessorTemplate(
    Logger::ILogger *logger,
    const std::string &typeName,
    const std::string &description);

  ~AccessorTemplate() override = default;

  virtual bool Read(T *value) const = 0;

  virtual bool Write(const T &value) = 0;
};
}  // namespace SystemModules::Framework::Accessors

#include "AccessorTemplate.tpp"
