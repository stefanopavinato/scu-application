/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include "AccessorTemplate.h"

namespace SystemModules::Framework::Accessors {
template<class T>
class AccessorStaticValue : public AccessorTemplate<T> {
 public:
  AccessorStaticValue(
    Logger::ILogger *logger,
    const T &value);

  ~AccessorStaticValue() override = default;

  bool Initialize() override;

  bool DeInitialize() override;

  bool Read(T *value) const override;

  bool Write(const T &value) override;

 private:
  const T value_;

  static const char kName[];

  static const char kDescription[];
};
}  // namespace SystemModules::Framework::Accessors

#include "AccessorStaticValue.tpp"
