/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "MCStateDeInitialize.h"
#include "MCStateInserted1.h"
#include "MCStateEmpty.h"

namespace SystemModules::Framework::States {

MCStateEmpty::MCStateEmpty(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::EMPTY, module) {
}

void MCStateEmpty::OnEntry() {
}

void MCStateEmpty::OnExit() {
}

void MCStateEmpty::OnUpdate() {
  // DeInitialize if shutdown
  if (Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateDeInitialize>(Module()));
    return;
  }
  // CardInserted1 if mezzanine card present
  if (Module()->CardDetectionItems()->MezzanineCardPresent()->IsValid() &&
    *Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue()) {
    SetState(std::make_unique<MCStateInserted1>(Module()));
    return;
  }
}
}  // namespace SystemModules::Framework::States
