/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "MCStateInitialize2.h"
#include "MCStateInitialize1.h"

namespace SystemModules::Framework::States {

MCStateInitialize1::MCStateInitialize1(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INITIALIZE1, module) {
}

void MCStateInitialize1::OnEntry() {
}

void MCStateInitialize1::OnExit() {
  if (!Module()->InstallCardDetectionItems()) {
    Module()->Logger()->Error("Failed to install card detection items");
  }
}

void MCStateInitialize1::OnUpdate() {
  // Empty if always
  SetState(std::make_unique<MCStateInitialize2>(Module()));
}
}  // namespace SystemModules::Framework::States
