/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved3.h"
#include "MCStateInserted4.h"
#include "MCStateInserted3.h"

namespace SystemModules::Framework::States {

const std::chrono::milliseconds MCStateInserted3::delay_ = std::chrono::milliseconds(2000);

MCStateInserted3::MCStateInserted3(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::INSERTED3, module) {
}

void MCStateInserted3::OnEntry() {
  // Set RSTn to tri-state
  *Module()->MezzanineCardReset() = false;
  // Set timer
  timerExpired_ = std::chrono::steady_clock::now() + delay_;
}

void MCStateInserted3::OnExit() {
}

void MCStateInserted3::OnUpdate() {
  // CardRemoved3 if card removed or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() >= Types::AlertSeverity::Enum::WARNING ||
      Module()->GetStop() ||
      Module()->GetReset() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved3>(Module()));
    return;
  }
  // CardInserted4 otherwise if PWREN_n == 1 and RST_n == 1
  if (std::chrono::steady_clock::now() < timerExpired_) {
    SetNote(kNoteTimer);
    return;
  }
  if (!(*Module()->CardDetectionItems()->MezzanineCardEnable()->GetValueIn())) {
    SetNote(kNoteMezzanineCardEnable);
    return;
  }
  if (*Module()->CardDetectionItems()->MezzanineCardReset()->GetValueIn()) {
    SetNote(KNoteMezzanineCardReset);
    return;
  }

  SetState(std::make_unique<MCStateInserted4>(Module()));

  // ToDo: Add status to log file
  // ToDo: Ev. locking mechanismus erweitern so dass er auch bei installation verwendet wird.
}
}  // namespace SystemModules::Framework::States
