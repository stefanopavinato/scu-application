/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateRemoved2 : public MCStateBase {
 public:
  explicit MCStateRemoved2(Components::ModuleMezzanineCard *module);

  ~MCStateRemoved2() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;

 private:
  std::chrono::steady_clock::time_point timerExpired_;

  static const std::chrono::milliseconds delay_;
};
}  // namespace SystemModules::Framework::States
