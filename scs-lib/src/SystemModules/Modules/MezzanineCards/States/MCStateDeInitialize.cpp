/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateDeInitialize.h"

namespace SystemModules::Framework::States {

MCStateDeInitialize::MCStateDeInitialize(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::DE_INITIALIZE, module) {
}

void MCStateDeInitialize::OnEntry() {
  // Uninstall card detection components
  (void) Module()->UninstallCardDetectionItems();
}

void MCStateDeInitialize::OnExit() {
}

void MCStateDeInitialize::OnUpdate() {
}
}  // namespace SystemModules::Framework::States
