/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <unordered_map>
#include <queue>
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateBase.h"

namespace SystemModules::Framework::States {

const char MCStateBase::kNoteForcedStop[] = "Mezzanine card manually stopped";
const char MCStateBase::kNoteAlertActive[] = "Mezzanine card alert active";
const char MCStateBase::kNoteUnexpectedCard[] = "Unexpected mezzanine card detected";
const char MCStateBase::kNoteTimer[] = "Wait for the timer to expire";
const char MCStateBase::kNoteMezzanineCardEnable[] = "Wait for mezzanine card enable feedback";
const char MCStateBase::KNoteMezzanineCardReset[] = "Wait for mezzanine card reset feedback";

MCStateBase::MCStateBase(
  const Types::ModuleState::Enum &id,
  Components::ModuleMezzanineCard *module)
  : id_(Types::ModuleState(id))
  , module_(module)
  , note_({}) {
}

Types::ModuleState MCStateBase::GetId() const {
  return id_;
}

Components::ModuleMezzanineCard *MCStateBase::Module() const {
  return module_;
}

void MCStateBase::SetState(std::unique_ptr<MCStateBase> state) {
  module_->SetState(std::move(state));
}

std::string MCStateBase::GetNote() const {
  return note_;
}

void MCStateBase::SetNote(const std::string &note) {
  note_ = note;
}

}  // namespace SystemModules::Framework::States
