/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateRemoved4.h"
#include "MCStateRunning.h"

namespace SystemModules::Framework::States {

MCStateRunning::MCStateRunning(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::RUNNING, module) {
}

void MCStateRunning::OnEntry() {
  // Clear driver reset in firmware
  *Module()->DriverReset() = false;
}

void MCStateRunning::OnExit() {
  // Set driver reset in firmware
  *Module()->DriverReset() = true;
}

void MCStateRunning::OnUpdate() {
  // Checks if module has a supply problem
  /*
  if (Module()->GroupAlertSeverity(
      Types::ValueGroup::Enum::SUPPLY).GetEnum() >= Types::AlertSeverity::Enum::ERROR) {
    SetState(std::make_unique<MCStateRemoved4>(Module()));
  }
  */
  // CardRemoved4 if card removed or reset or shutdown
  if (!*Module()->CardDetectionItems()->MezzanineCardPresent()->GetValue() ||
      Module()->GetGroupAlertSeverity(Types::ValueGroup::Enum::ANY).GetEnum() >= Types::AlertSeverity::Enum::WARNING ||
      Module()->GetStop() ||
      Module()->GetReset() ||
      Module()->GetShutDown()) {
    SetState(std::make_unique<MCStateRemoved4>(Module()));
    return;
  }
}

}  // namespace SystemModules::Framework::States
