/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateInitialize1 : public MCStateBase {
 public:
  explicit MCStateInitialize1(Components::ModuleMezzanineCard *module);

  ~MCStateInitialize1() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
