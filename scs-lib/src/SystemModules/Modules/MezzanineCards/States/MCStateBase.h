/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <string>
#include "Types/ModuleState.h"

namespace SystemModules::Framework {
namespace Components {
class ModuleMezzanineCard;
}  // namespace Components
namespace States {
class MCStateBase {
 public:
  MCStateBase(
    const Types::ModuleState::Enum &id,
    Components::ModuleMezzanineCard *module);

  virtual ~MCStateBase() = default;

  virtual void OnEntry() = 0;

  virtual void OnExit() = 0;

  virtual void OnUpdate() = 0;

  [[nodiscard]] Types::ModuleState GetId() const;

  void SetState(
    std::unique_ptr<MCStateBase> stateNew);

  [[nodiscard]] Components::ModuleMezzanineCard *Module() const;

  [[nodiscard]] std::string GetNote() const;

 protected:
  void SetNote(const std::string &note);

  static const char kNoteUnexpectedCard[];
  static const char kNoteForcedStop[];
  static const char kNoteAlertActive[];
  static const char kNoteTimer[];
  static const char kNoteMezzanineCardEnable[];
  static const char KNoteMezzanineCardReset[];

 private:
  Types::ModuleState id_;

  Components::ModuleMezzanineCard *module_;

  std::string note_;
};
}  // namespace States
}  // namespace SystemModules::Framework
