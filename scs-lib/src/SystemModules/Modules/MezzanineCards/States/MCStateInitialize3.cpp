/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "SystemModules/Modules/MezzanineCards/MezzanineCardDetection/CardDetectionItems.h"
#include "MCStateEmpty.h"
#include "MCStateInitialize3.h"

namespace SystemModules::Framework::States {

MCStateInitialize3::MCStateInitialize3(Components::ModuleMezzanineCard *module)
: MCStateBase(Types::ModuleState::Enum::INITIALIZE3, module) {
}

void MCStateInitialize3::OnEntry() {
}

void MCStateInitialize3::OnExit() {
}

void MCStateInitialize3::OnUpdate() {
  // Empty if always
  SetState(std::make_unique<MCStateEmpty>(Module()));
}
}  // namespace SystemModules::Framework::States
