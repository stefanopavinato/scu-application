/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "MCStateBase.h"

namespace SystemModules::Framework::States {
class MCStateEmpty : public MCStateBase {
 public:
  explicit MCStateEmpty(Components::ModuleMezzanineCard *module);

  ~MCStateEmpty() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
