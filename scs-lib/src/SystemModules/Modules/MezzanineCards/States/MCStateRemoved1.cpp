/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h"
#include "MCStateEmpty.h"
#include "MCStateRemoved1.h"

namespace SystemModules::Framework::States {

MCStateRemoved1::MCStateRemoved1(Components::ModuleMezzanineCard *module)
  : MCStateBase(Types::ModuleState::Enum::REMOVED1, module) {
}

void MCStateRemoved1::OnEntry() {
  // Uninstall management
  (void) Module()->UninstallManagement();
}

void MCStateRemoved1::OnExit() {
}

void MCStateRemoved1::OnUpdate() {
  // Empty if always
  SetState(std::make_unique<MCStateEmpty>(Module()));
}
}  // namespace SystemModules::Framework::States
