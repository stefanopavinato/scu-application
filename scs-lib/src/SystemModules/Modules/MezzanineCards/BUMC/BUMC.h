/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Modules/ModuleBUMC.h"
#include "SystemModules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::BUMC {
class BUMC_MCDriver;

class BUMC : public Framework::Components::Container {
 public:
  BUMC(
    Logger::ILogger *logger,
    const Configuration::Modules::ModuleBUMC &configBUMC,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Slots::Configuration_Slots &config);

  [[nodiscard]] BUMC_MCDriver *MCDriver() const;

 private:
  BUMC_MCDriver *mcDriver_;

  static const char kName[];
  static const char kDescription[];
};
}  // namespace SystemModules::Modules::BUMC
