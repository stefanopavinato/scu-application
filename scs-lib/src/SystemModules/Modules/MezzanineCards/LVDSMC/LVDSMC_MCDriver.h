/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <cstdint>
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::LVDSMC {
class LVDSMC_MCDriverStatus;

class LVDSMC_MCDriverOverride;

class LVDSMC_MCDriverPort;

class LVDSMC_MCDriver : public Framework::Components::Container {
 public:
  LVDSMC_MCDriver(
    Logger::ILogger *logger,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Slots::Configuration_Slots &config);

  [[nodiscard]] LVDSMC_MCDriverPort *Port1() const;

  [[nodiscard]] LVDSMC_MCDriverPort *Port2() const;

  [[nodiscard]] LVDSMC_MCDriverPort *Port3() const;

 private:
  LVDSMC_MCDriverPort *port1_;

  LVDSMC_MCDriverPort *port2_;

  LVDSMC_MCDriverPort *port3_;
};

}  // namespace SystemModules::Modules::LVDSMC
