/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Modules/ModuleLVDSMC.h"
#include "SystemModules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules {
namespace Common {
class VoltageMonitor_3V3_1V8;
class HotSwapController;
}  // namespace Common
namespace LVDSMC {
class LVDSMC_MCDriver;

class LVDSMC : public Framework::Components::Container {
 public:
  LVDSMC(
    Logger::ILogger *logger,
    const Configuration::Modules::ModuleLVDSMC &configLVDSMC,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Slots::Configuration_Slots &config);

  [[nodiscard]] LVDSMC_MCDriver *MCDriver() const;

  [[nodiscard]] Common::VoltageMonitor_3V3_1V8 *VoltageMonitor() const;

  [[nodiscard]] Modules::Common::HotSwapController *HotSwapController() const;

 private:
  LVDSMC_MCDriver *mcDriver_;

  Common::VoltageMonitor_3V3_1V8 *voltageMonitor_;

  Modules::Common::HotSwapController *hotSwapController_;
};
}  // namespace LVDSMC
}  // namespace SystemModules::Modules
