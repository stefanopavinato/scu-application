/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Slots/Configuration_Slots.h"

namespace SystemModules::Modules::ISMC2 {
class ISMC2_MCDriverStatus;

class ISMC2_MCDriverOverride;

class ISMC2_MCDriver : public Framework::Components::Container {
 public:
  ISMC2_MCDriver(
    Logger::ILogger *logger,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Slots::Configuration_Slots &config);

  [[nodiscard]] ISMC2_MCDriverStatus *Status() const;

  [[nodiscard]] ISMC2_MCDriverOverride *Override() const;

 private:
  ISMC2_MCDriverStatus *status_;

  ISMC2_MCDriverOverride *override_;
};

}  // namespace SystemModules::Modules::ISMC2
