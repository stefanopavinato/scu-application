/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Value.h"
#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/I2CDeviceLM75.h"

namespace SystemModules::Modules::RS485MC {
class RS485MCAnalogIn1 : public Framework::Components::I2CDeviceLM75 {
 public:
  RS485MCAnalogIn1(
    Logger::ILogger *logger,
    const std::string &path,
    const std::string &devicePath,
    const uint32_t &slot);

  [[nodiscard]] Framework::Components::ItemIn<Types::Value<int32_t>> *TemperatureEthernetSwitch() const;

 private:
  Framework::Components::ItemIn<Types::Value<int32_t>> *temperatureEthernetSwitch_;

  static const char kName[];
  static const char kDescription[];
  static const uint16_t kAddress;

  static const char kTemperatureEthernetSwitchName[];
  static const char kTemperatureEthernetSwitchDescription[];
  static const char kTemperatureEthernetSwitchSubPath[];

  static const int32_t kTemperatureEthernetSwitchUpperLimit;
  static const int32_t kTemperatureEthernetSwitchLowerLimit;
};

}  // namespace SystemModules::Modules::RS485MC
