/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateDeInit.h"
#include "IMStateRun.h"

namespace SystemModules::Framework::States {

IMStateRun::IMStateRun(Components::ModuleInternal *module)
  : IMStateBase(Types::ModuleState::Enum::RUNNING, module) {
}

void IMStateRun::OnEntry() {
}

void IMStateRun::OnExit() {
}

void IMStateRun::OnUpdate() {
  // Check run state
  if (Module()->GetShutDown()) {
    SetState(std::make_unique<IMStateDeInit>(Module()));
  }
}

}  // namespace SystemModules::Framework::States
