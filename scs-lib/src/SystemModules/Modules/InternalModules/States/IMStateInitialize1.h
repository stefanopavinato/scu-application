/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "IMStateBase.h"

namespace SystemModules::Framework::States {
class IMStateInitialize1 : public IMStateBase {
 public:
  explicit IMStateInitialize1(Components::ModuleInternal *module);

  ~IMStateInitialize1() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
