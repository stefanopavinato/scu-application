/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateDeInit.h"

namespace SystemModules::Framework::States {

IMStateDeInit::IMStateDeInit(Components::ModuleInternal *module)
  : IMStateBase(Types::ModuleState::Enum::DE_INITIALIZE, module) {
}

void IMStateDeInit::OnEntry() {
  // Deinstall application
  Module()->UninstallApplication();
}

void IMStateDeInit::OnExit() {
}

void IMStateDeInit::OnUpdate() {
}

}  // namespace SystemModules::Framework::States
