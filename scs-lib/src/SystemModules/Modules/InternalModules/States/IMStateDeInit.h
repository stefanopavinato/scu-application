/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "IMStateBase.h"

namespace SystemModules::Framework::States {
class IMStateDeInit : public IMStateBase {
 public:
  explicit IMStateDeInit(Components::ModuleInternal *module);

  ~IMStateDeInit() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
