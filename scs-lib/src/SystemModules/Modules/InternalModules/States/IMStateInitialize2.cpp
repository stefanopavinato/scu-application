/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateInitialize3.h"
#include "IMStateInitialize2.h"

namespace SystemModules::Framework::States {

IMStateInitialize2::IMStateInitialize2(Components::ModuleInternal *module)
  : IMStateBase(Types::ModuleState::Enum::INITIALIZE2, module) {
}

void IMStateInitialize2::OnEntry() {
}

void IMStateInitialize2::OnExit() {
}

void IMStateInitialize2::OnUpdate() {
  // Switch to state run
  SetState(std::make_unique<IMStateInitialize3>(Module()));
}

}  // namespace SystemModules::Framework::States
