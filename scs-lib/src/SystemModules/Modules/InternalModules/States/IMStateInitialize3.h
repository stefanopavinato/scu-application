/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "IMStateBase.h"

namespace SystemModules::Framework::States {
class IMStateInitialize3 : public IMStateBase {
 public:
  explicit IMStateInitialize3(Components::ModuleInternal *module);

  ~IMStateInitialize3() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
