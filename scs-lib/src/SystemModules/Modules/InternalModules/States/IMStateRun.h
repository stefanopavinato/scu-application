/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "IMStateBase.h"

namespace SystemModules::Framework::States {
class IMStateRun : public IMStateBase {
 public:
  explicit IMStateRun(Components::ModuleInternal *module);

  ~IMStateRun() override = default;

  void OnEntry() final;

  void OnExit() final;

  void OnUpdate() final;
};
}  // namespace SystemModules::Framework::States
