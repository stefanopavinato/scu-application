/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "IMStateInitialize2.h"
#include "IMStateInitialize1.h"

namespace SystemModules::Framework::States {

IMStateInitialize1::IMStateInitialize1(Components::ModuleInternal *module)
  : IMStateBase(Types::ModuleState::Enum::INITIALIZE1, module) {
}

void IMStateInitialize1::OnEntry() {
}

void IMStateInitialize1::OnExit() {
}

void IMStateInitialize1::OnUpdate() {
  *Module()->PowerEnablePullUpDrive() = false;
  // Install application
  if (!Module()->InstallApplication()) {
    return;
  }
  // Switch to state run
  SetState(std::make_unique<IMStateInitialize2>(Module()));
}

}  // namespace SystemModules::Framework::States
