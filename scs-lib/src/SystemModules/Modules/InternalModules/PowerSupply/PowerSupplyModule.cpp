/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Configuration/ConfigKeyValue.h"
#include "SystemModules/Configuration/Common/ConfigurationUpdateInterval.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "Logger/ILogger.h"
#include "PowerSupplyAnalogIn1.h"
#include "PowerSupplyAnalogIn2.h"
#include "PowerSupplyModule.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupplyModule::kPowerSupplyModuleName[] = "POWER_SUPPLY_MODULE";
const char PowerSupplyModule::kPowerSupplyModuleDescription[] = "Power Supply unit of the SCU";

const char PowerSupplyModule::kI2CPathBase[] = "/sys/bus/i2c/devices/0-0071/channel-4/";
const uint16_t PowerSupplyModule::kI2CAddressAnalog1[] = {0x40, 0x44};
const uint16_t PowerSupplyModule::kI2CAddressAnalog2[] = {0x41, 0x45};

const char PowerSupplyModule::kPRSTName[] = "Present";
const char PowerSupplyModule::kPRSTDescription[] = "Present";
const uint32_t PowerSupplyModule::kPRSTAddressOffset = 0;
const char *PowerSupplyModule::kPRSTGpioName[] = {"PS1_PRST", "PS2_PRST"};

const char PowerSupplyModule::kPWR_FAILName[] = "Power_Fail";
const char PowerSupplyModule::kPWR_FAILDescription[] = "Power fail";
const uint32_t PowerSupplyModule::kPWR_FAILAddressOffset = 1;
const char *PowerSupplyModule::kPWR_FAILGpioName[] = {"PS1_PWR_FAIL", "PS2_PWR_FAIL"};

const char PowerSupplyModule::kINH_PS_ONName[] = "Inhibit";
const char PowerSupplyModule::kINH_PS_ONDescription[] = "Inhibit";
const uint32_t PowerSupplyModule::kINH_PS_ONAddressOffset = 2;
const char *PowerSupplyModule::kINH_PS_ONGpioName[] = {"PS1_INH_PS_ON", "PS2_INH_PS_ON"};

const char PowerSupplyModule::kTemp_WarningName[] = "Temp_Warning";
const char PowerSupplyModule::kTemp_WarningDescription[] = "Temp warning";
const uint32_t PowerSupplyModule::kTemp_WarningAddressOffset = 3;
const char *PowerSupplyModule::kTemp_WarningGpioName[] = {"PS1_Temp_Warning", "PS2_Temp_Warning"};

const char PowerSupplyModule::kS200_3Name[] = "S200_3";
const char PowerSupplyModule::kS200_3Description[] = "reserved";
const uint32_t PowerSupplyModule::kS200_3AddressOffset = 4;
const char *PowerSupplyModule::kS200_3GpioName[] = {"PS1_S200_3", "PS2_S200_3"};

const char PowerSupplyModule::kS200_4Name[] = "S200_4";
const char PowerSupplyModule::kS200_4Description[] = "reserved";
const uint32_t PowerSupplyModule::kS200_4AddressOffset = 5;
const char *PowerSupplyModule::kS200_4GpioName[] = {"PS1_S200_4", "PS2_S200_4"};

PowerSupplyModule::PowerSupplyModule(
  Logger::ILogger *logger,
  const Configuration::Common::ConfigurationCommon &configCommon,
  const Configuration::Modules::ModulePowerSupply &configPSU,
  std::string const &name,
  uint32_t const &baseAddress,
  uint8_t const &psunumber)
  : SystemModules::Framework::Components::Container(
  logger,
  name,
  kPowerSupplyModuleDescription) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  // Setup items
  present_ = Create().ItemIn<bool>(
      kPRSTName,
      kPRSTDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED).AddAndReturnPointer();
  present_->CreateAccessor().DigitalInput(
      baseAddress + kPRSTAddressOffset,
      kPRSTGpioName[psunumber-1],
      true);

  // Setup items
  power_fail_ = Create().ItemIn<bool>(
      kPWR_FAILName,
      kPWR_FAILDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED).AddAndReturnPointer();
  power_fail_->CreateAccessor().DigitalInput(
      baseAddress + kPWR_FAILAddressOffset,
      kPWR_FAILGpioName[psunumber-1],
      true);

  // Setup items
  inhibit_ = Create().ItemOut<bool>(
      kINH_PS_ONName,
      kINH_PS_ONDescription,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED,
      Types::WriteStrategy::Enum::ON_UPDATE,
      true,
      false).AddAndReturnPointer();
  inhibit_->CreateAccessor().DigitalOutput(
      baseAddress + kINH_PS_ONAddressOffset,
      kINH_PS_ONGpioName[psunumber-1],
      false);
  inhibit_->CreateAction().StaticValue(
      false);

  // Setup items
  temp_warning_ = Create().ItemIn<bool>(
      kTemp_WarningName,
      kTemp_WarningDescription,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED).AddAndReturnPointer();
  temp_warning_->CreateAccessor().DigitalInput(
      baseAddress + kTemp_WarningAddressOffset,
      kTemp_WarningGpioName[psunumber-1],
      true);

  // Setup items
  s200_3_ = Create().ItemIn<bool>(
      kS200_3Name,
      kS200_3Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED).AddAndReturnPointer();
  s200_3_->CreateAccessor().DigitalInput(
      baseAddress + kS200_3AddressOffset,
      kS200_3GpioName[psunumber-1],
      false);

  // Setup items
  s200_4_ = Create().ItemIn<bool>(
      kS200_4Name,
      kS200_4Description,
      Types::ValueGroup::Enum::NONE,
      std::chrono::milliseconds(updateInterval),
      Types::LockId::Enum::SHARED).AddAndReturnPointer();
  s200_4_->CreateAccessor().DigitalInput(
      baseAddress + kS200_4AddressOffset,
      kS200_4GpioName[psunumber-1],
      false);

  // Create INA220 device measuring MAIN Supply
  auto analogIn1 = std::make_unique<PowerSupplyAnalogIn1>(
    logger,
    configCommon,
    kI2CPathBase,
    kI2CPathBase,  // TBD
    kI2CAddressAnalog1[psunumber-1]);
  analogIn1_ = analogIn1.get();
  AddComponent(std::move(analogIn1));

  // Create INA220 device measuring STANDBY Supply
  auto analogIn2 = std::make_unique<PowerSupplyAnalogIn2>(
    logger,
    configCommon,
    kI2CPathBase,
    kI2CPathBase,  // TBD
    kI2CAddressAnalog2[psunumber-1]);
  analogIn2_ = analogIn2.get();
  AddComponent(std::move(analogIn2));
  // ToDo Create GPIO device
}

Framework::Components::ItemIn<bool> *PowerSupplyModule::Present() const {
  return present_;
}

Framework::Components::ItemIn<bool> *PowerSupplyModule::PowerFail() const {
  return power_fail_;
}

Framework::Components::ItemOut<bool> *PowerSupplyModule::Inhibit() const {
  return inhibit_;
}

Framework::Components::ItemIn<bool> *PowerSupplyModule::TempWarning() const {
  return temp_warning_;
}

Framework::Components::ItemIn<bool> *PowerSupplyModule::S200_3() const {
  return s200_3_;
}

Framework::Components::ItemIn<bool> *PowerSupplyModule::S200_4() const {
  return s200_4_;
}

PowerSupplyAnalogIn1 *PowerSupplyModule::AnalogIn1() const {
  return analogIn1_;
}

PowerSupplyAnalogIn2 *PowerSupplyModule::AnalogIn2() const {
  return analogIn2_;
}
}  // namespace SystemModules::Modules::PowerSupply
