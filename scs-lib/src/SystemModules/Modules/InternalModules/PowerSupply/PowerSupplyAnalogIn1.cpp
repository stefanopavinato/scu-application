/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/ConfigKeyValue.h"
#include "SystemModules/Configuration/Common/ConfigurationUpdateInterval.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "PowerSupplyAnalogIn1.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupplyAnalogIn1::kName[] = "MainSupply";
const char PowerSupplyAnalogIn1::kDescription[] =
  "Current and voltage values from INA220-chip on the power supply backplane measuring the MAIN Supply";

const char PowerSupplyAnalogIn1::kMainSupplyCurrentName[] = "MainSupplyCurrent";
const char PowerSupplyAnalogIn1::kMainSupplyCurrentDescription[] = "MAIN Supply Current";
// const char PowerSupplyAnalogIn1::kMainSupplyCurrentSubPath[] = "/in0_input";
const char PowerSupplyAnalogIn1::kMainSupplyCurrentSubPath[] = "/curr1_input";

const char PowerSupplyAnalogIn1::kMainSupplyVoltageName[] = "MainSupplyVoltage";
const char PowerSupplyAnalogIn1::kMainSupplyVoltageDescription[] = "MAIN Supply Voltage";
const char PowerSupplyAnalogIn1::kMainSupplyVoltageSubPath[] = "/in1_input";

PowerSupplyAnalogIn1::PowerSupplyAnalogIn1(
  Logger::ILogger *logger,
  const Configuration::Common::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint16_t &address)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  26,
  address,
  path,
  devicePath) {
  // Get update interval
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();

  mainSupplyCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kMainSupplyCurrentName,
    kMainSupplyCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  mainSupplyCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kMainSupplyCurrentSubPath),
    826,  // an experimentally found calibration factor
    100);
  mainSupplyVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kMainSupplyVoltageName,
    kMainSupplyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  mainSupplyVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kMainSupplyVoltageSubPath),
    1,
    1);
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn1::MainSupplyCurrent() const {
  return mainSupplyCurrent_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn1::MainSupplyVoltage() const {
  return mainSupplyVoltage_;
}

}  // namespace SystemModules::Modules::PowerSupply
