/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <memory>
#include "Logger/ILogger.h"
#include "Configuration/ConfigKeyValue.h"
#include "SystemModules/Configuration/Slots/SlotPSU.h"
#include "SystemModules/Configuration/Slots/PSU.h"
#include "PowerSupplyModule.h"
#include "PowerSupply.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupply::kPowerSupplyName[] = "PSUnit";
const char PowerSupply::kPowerSupplyDescription[] = "Power Supply unit of the SCU";

PowerSupply::PowerSupply(
  Logger::ILogger *logger,
  const Configuration::Common::ConfigurationCommon &configCommon,
  const Configuration::Modules::ModulePowerSupply &configPSU,
  const Configuration::Slots::Slots & configSlots)
  : SystemModules::Framework::Components::Container(
  logger,
  kPowerSupplyName,
  kPowerSupplyDescription) {
  // Create powersupply module 1
  if (configSlots.PSU()->Psu(1)->Enable()->Value()) {
    auto psumodule1 = std::make_unique<PowerSupplyModule>(
        logger,
        configCommon,
        configPSU,
        "PSUModule1",
        400,
        1);
    psumodule1_ = psumodule1.get();
    AddComponent(std::move(psumodule1));
  }

  // Create powersupply module 1
  if (configSlots.PSU()->Psu(2)->Enable()->Value()) {
    auto psumodule2 = std::make_unique<PowerSupplyModule>(
        logger,
        configCommon,
        configPSU,
        "PSUModule2",
        400,
        2);
    psumodule1_ = psumodule2.get();
    AddComponent(std::move(psumodule2));
  }
}

PowerSupplyModule *PowerSupply::PSUModule1() const {
  return psumodule1_;
}

PowerSupplyModule *PowerSupply::PSUModule2() const {
  return psumodule2_;
}

}  // namespace SystemModules::Modules::PowerSupply
