/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Modules/ModulePowerSupply.h"
#include "SystemModules/Configuration/Slots/Slots.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Modules::PowerSupply {
class PowerSupplyModule;

// class PowerSupply
class PowerSupply : public Framework::Components::Container {
 public:
  explicit PowerSupply(
    Logger::ILogger *logger,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Modules::ModulePowerSupply &config,
    const Configuration::Slots::Slots & configSlots);

  [[nodiscard]] PowerSupplyModule *PSUModule1() const;

  [[nodiscard]] PowerSupplyModule *PSUModule2() const;

 private:
  PowerSupplyModule *psumodule1_;
  PowerSupplyModule *psumodule2_;

  static const char kPowerSupplyName[];
  static const char kPowerSupplyDescription[];
};
}  // namespace SystemModules::Modules::PowerSupply
