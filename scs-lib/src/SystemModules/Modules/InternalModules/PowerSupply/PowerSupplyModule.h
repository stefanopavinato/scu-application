/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/ItemIn.h"
#include "SystemModules/Framework/Components/ItemOut.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"
#include "SystemModules/Configuration/Modules/ModulePowerSupply.h"
#include "SystemModules/Framework/Components/Container.h"

namespace SystemModules::Modules::PowerSupply {
class PowerSupplyAnalogIn1;

class PowerSupplyAnalogIn2;

// class PowerSupplyModule
class PowerSupplyModule : public Framework::Components::Container {
 public:
  explicit PowerSupplyModule(
    Logger::ILogger *logger,
    const Configuration::Common::ConfigurationCommon &configCommon,
    const Configuration::Modules::ModulePowerSupply &config,
    std::string const &name,
    uint32_t const &baseAddress,
    uint8_t const &psunumber);

  [[nodiscard]] Framework::Components::ItemIn<bool> *Present() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *PowerFail() const;

  [[nodiscard]] Framework::Components::ItemOut<bool> *Inhibit() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *TempWarning() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *S200_3() const;

  [[nodiscard]] Framework::Components::ItemIn<bool> *S200_4() const;

  [[nodiscard]] PowerSupplyAnalogIn1 *AnalogIn1() const;

  [[nodiscard]] PowerSupplyAnalogIn2 *AnalogIn2() const;

 private:
  Framework::Components::ItemIn<bool> *present_;

  Framework::Components::ItemIn<bool> *power_fail_;

  Framework::Components::ItemOut<bool> *inhibit_;

  Framework::Components::ItemIn<bool> *temp_warning_;

  Framework::Components::ItemIn<bool> *s200_3_;

  Framework::Components::ItemIn<bool> *s200_4_;

  PowerSupplyAnalogIn1 *analogIn1_;

  PowerSupplyAnalogIn2 *analogIn2_;

  static const char kPowerSupplyModuleName[];
  static const char kPowerSupplyModuleDescription[];

  static const char kI2CPathBase[];
  static const uint16_t kI2CAddressAnalog1[];
  static const uint16_t kI2CAddressAnalog2[];

  static const char kPRSTName[];
  static const char kPRSTDescription[];
  static const uint32_t kPRSTAddressOffset;
  static const char *kPRSTGpioName[];

  static const char kPWR_FAILName[];
  static const char kPWR_FAILDescription[];
  static const uint32_t kPWR_FAILAddressOffset;
  static const char *kPWR_FAILGpioName[];

  static const char kINH_PS_ONName[];
  static const char kINH_PS_ONDescription[];
  static const uint32_t kINH_PS_ONAddressOffset;
  static const char *kINH_PS_ONGpioName[];

  static const char kTemp_WarningName[];
  static const char kTemp_WarningDescription[];
  static const uint32_t kTemp_WarningAddressOffset;
  static const char *kTemp_WarningGpioName[];

  static const char kS200_3Name[];
  static const char kS200_3Description[];
  static const uint32_t kS200_3AddressOffset;
  static const char *kS200_3GpioName[];

  static const char kS200_4Name[];
  static const char kS200_4Description[];
  static const uint32_t kS200_4AddressOffset;
  static const char *kS200_4GpioName[];
};
}  // namespace SystemModules::Modules::PowerSupply
