/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/ConfigKeyValue.h"
#include "SystemModules/Configuration/Common/ConfigurationUpdateInterval.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "PowerSupplyAnalogIn2.h"

namespace SystemModules::Modules::PowerSupply {

const char PowerSupplyAnalogIn2::kName[] = "StandbySupply";
const char PowerSupplyAnalogIn2::kDescription[] =
  "Current and voltage values from INA220-chip on the power supply backplane measuring the STANDBY Supply";

const char PowerSupplyAnalogIn2::kStandbySupplyCurrentName[] = "StandbySupplyCurrent";
const char PowerSupplyAnalogIn2::kStandbySupplyCurrentDescription[] = "STANDBY Supply Current";
const char PowerSupplyAnalogIn2::kStandbySupplyCurrentSubPath[] = "/curr1_input";

const char PowerSupplyAnalogIn2::kStandbySupplyVoltageName[] = "StandbySupplyVoltage";
const char PowerSupplyAnalogIn2::kStandbySupplyVoltageDescription[] = "STANDBY Supply Voltage";
const char PowerSupplyAnalogIn2::kStandbySupplyVoltageSubPath[] = "/in1_input";

PowerSupplyAnalogIn2::PowerSupplyAnalogIn2(
  Logger::ILogger *logger,
  const Configuration::Common::ConfigurationCommon &configCommon,
  const std::string &path,
  const std::string &devicePath,
  const uint16_t &address)
  : Framework::Components::I2CDeviceINA220(
  logger,
  kName,
  kDescription,
  26,
  address,
  path,
  devicePath) {
  // Get update intervaval
  auto updateInterval = configCommon.UpdateInterval()->Slow()->Value();

  standbySupplyCurrent_ = Create().ItemIn<Types::Value<int32_t>>(
    kStandbySupplyCurrentName,
    kStandbySupplyCurrentDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::SHARED,
    Types::Unit::Enum::AMPERE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  standbySupplyCurrent_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kStandbySupplyCurrentSubPath),
    // shunt resistor is 1500uOhm but driver would round it to multiples of 1000uOhm multiplied by
    8,  // an experimentally found calibration factor
    100);  // therefore it is set to 1000uOhm, but this needs a correction
  standbySupplyVoltage_ = Create().ItemIn<Types::Value<int32_t>>(
    kStandbySupplyVoltageName,
    kStandbySupplyVoltageDescription,
    Types::ValueGroup::Enum::SUPPLY,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::SHARED,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI).AddAndReturnPointer();
  standbySupplyVoltage_->CreateAccessor().File<int32_t, std::string>(
    GetDevicePath().append(kStandbySupplyVoltageSubPath),
    1,
    1);
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn2::StandbySupplyCurrent() const {
  return standbySupplyCurrent_;
}

Framework::Components::ItemIn<Types::Value<int32_t>> *PowerSupplyAnalogIn2::StandbySupplyVoltage() const {
  return standbySupplyVoltage_;
}

}  // namespace SystemModules::Modules::PowerSupply
