/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include "Logger/ILogger.h"
#include "SystemModules/Configuration/Configuration.h"
#include "SystemModules/Modules/InternalModules/ModuleInternal.h"
#include "Types/ModuleType.h"

namespace SystemModules {
namespace Framework::Components {
class ModuleHandler;

class Container;
}  // namespace Framework::Components
namespace Modules {
class InternalModuleFactory {
 public:
  static std::optional<std::unique_ptr<Framework::Components::Container>> Create(
    Logger::ILogger *logger,
    SystemModules::Framework::Components::ModuleInternal *module,
    const SystemModules::Configuration::Configuration &configuration,
    const Types::ModuleType::Enum &type);
};
}  // namespace Modules
}  // namespace SystemModules
