/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "SystemModules/Configuration/Configuration.h"
#include "SystemModules/Configuration/Modules/Modules.h"
#include "SystemModules/Modules/InternalModules/Serializer/Serializer.h"
#include "SystemModules/Modules/InternalModules/PowerSupply/PowerSupply.h"
#include "SystemModules/Modules/InternalModules/FanUnit/FanUnit.h"
#include "InternalModuleFactory.h"

namespace SystemModules::Modules {
std::optional<std::unique_ptr<Framework::Components::Container>> InternalModuleFactory::Create(
  Logger::ILogger *logger,
  SystemModules::Framework::Components::ModuleInternal * module,
  const Configuration::Configuration &configuration,
  const Types::ModuleType::Enum &type) {
  switch (type) {
    case Types::ModuleType::Enum::SCU_SER: {
      return std::make_unique<SystemModules::Modules::Serializer::Serializer>(
        logger,
        module->PowerEnablePullUpDrive(),
        *configuration.Modules()->Serializer(),
        *configuration.Common());
    }
    case Types::ModuleType::Enum::SCU_PWSUP: {
      return std::make_unique<SystemModules::Modules::PowerSupply::PowerSupply>(
        logger,
        *configuration.Common(),
        *configuration.Modules()->PowerSupply(),
        *configuration.Slots());
    }
    case Types::ModuleType::Enum::SCU_FU: {
      return std::make_unique<SystemModules::Modules::FanUnit::FanUnit>(
        logger,
        *configuration.Common(),
        *configuration.Modules()->FanUnit());
    }
    default: {
      return std::nullopt;
    }
  }
  return std::nullopt;
}
}  // namespace SystemModules::Modules
