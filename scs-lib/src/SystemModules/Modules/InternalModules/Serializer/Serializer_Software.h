/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Configuration/Common/ConfigurationCommon.h"

namespace SystemModules::Modules {
class Serializer_SvnInfo;
class Serializer_Software : public Framework::Components::Container {
 public:
  Serializer_Software(
    Logger::ILogger *logger,
    const Configuration::Common::ConfigurationCommon &config);

  [[nodiscard]] Serializer_SvnInfo *Platform() const;

  [[nodiscard]] Serializer_SvnInfo *Application() const;

 private:
  Serializer_SvnInfo *platform_;

  Serializer_SvnInfo *application_;

  static const char kName[];
  static const char kDescription[];

  static const char kPlatformName[];
  static const char kPlatformDescription[];

  static const char kApplicationName[];
  static const char kApplicationDescription[];
};
}  // namespace SystemModules::Modules
