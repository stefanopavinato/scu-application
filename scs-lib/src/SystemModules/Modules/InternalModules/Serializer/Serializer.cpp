/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Configuration/ConfigKeyValue.h"
#include "SystemModules/Configuration/Common/ConfigurationUpdateInterval.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilder.h"
#include "SystemModules/Framework/Builder/Actions/ActionBuilderSerializer.h"
#include "SystemModules/Framework/Builder/Accessors/ReadAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/WriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Accessors/ReadWriteAccessorBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ItemInOutBuilder.h"
#include "SystemModules/Framework/Builder/Components/ParameterItemInBuilder.h"
#include "SystemModules/Framework/Builder/Components/ComponentBuilder.h"
#include "Serializer.h"

namespace SystemModules::Modules::Serializer {

const char Serializer::kName[] = "Serializer";
const char Serializer::kDescription[] = "SCU Serializer";

const char Serializer::kI2CPathBase[] = "/sys/bus/i2c/devices/";

const uint32_t Serializer::kBoardGPIOBaseAddress = 400;

const char Serializer::kUpperTemperatureName[] = "UpperTemperature";
const char Serializer::kUpperTemperatureDescription[] = "Temperature at the upper pcb edge.";
const uint16_t Serializer::kUpperTemperatureAddress = 0x48;

const char Serializer::kSfpModule1Name[] = "SfpModule1";
const uint32_t Serializer::kSfpModule1BaseAddress = 448;

const char Serializer::kSfpModule2Name[] = "SfpModule2";
const uint32_t Serializer::kSfpModule2BaseAddress = 456;

const char Serializer::kSfpModule3Name[] = "SfpModule3";
const uint32_t Serializer::kSfpModule3BaseAddress = 432;

const char Serializer::kSLinkInterface1Name[] = "SLinkInterface1";

const char Serializer::kSLinkInterface2Name[] = "SLinkInterface2";

const char Serializer::kSLinkInterface3Name[] = "SLinkInterface3";

const char Serializer::kSlotName[] = "Slot";
const char Serializer::kSlotDescription[] = "";
const uint32_t Serializer::kSlotId = 499;
const char Serializer::kSlotGpioName[] = "SLOT_ID";

const char Serializer::kPgmClkStatusLockDetectName[] = "PgmClkStatusLockDetect";
const char Serializer::kPgmClkStatusLockDetectDescription[] = "";
const uint32_t Serializer::kPgmClkStatusLockDetectOffset = 16;
const char Serializer::kPgmClkStatusLockDetectGpioName[] = "PGMCLK_STATUS_LD";

const char Serializer::kPgmClkStatusHoldoverName[] = "PgmClkStatusHoldover";
const char Serializer::kPgmClkStatusHoldoverDescription[] = "";
const uint32_t Serializer::kPgmClkStatusHoldoverOffset = 17;
const char Serializer::kPgmClkStatusHoldoverGpioName[] = "PGMCLK_STATUS_HOLDOVER";

const char Serializer::kPgmClkStatusClkIn0Name[] = "PgmClkStatusClkIn0";
const char Serializer::kPgmClkStatusClkIn0Description[] = "";
const uint32_t Serializer::kPgmClkStatusClkIn0Offset = 18;
const char Serializer::kPgmClkStatusClkIn0GpioName[] = "PGMCLK_STATUS_CLKIN0";

const char Serializer::kPgmClkStatusClkIn1Name[] = "PgmClkStatusClkIn1";
const char Serializer::kPgmClkStatusClkIn1Description[] = "";
const uint32_t Serializer::kPgmClkStatusClkIn1Offset = 19;
const char Serializer::kPgmClkStatusClkIn1GpioName[] = "PGMCLK_STATUS_CLKIN1";

const char Serializer::kPgmClkSyncName[] = "PgmClkSync";
const char Serializer::kPgmClkSyncDescription[] = "";
const uint32_t Serializer::kPgmClkSyncOffset = 20;
const char Serializer::kPgmClkSyncGpioName[] = "PGMCLK_SYNC";

const char Serializer::kVccInt0V85FailName[] = "VCCINT_0V85_FAIL";
const char Serializer::kVccInt0V85FailDescription[] = "VCCINT 0.85V fail";
const uint32_t Serializer::kVccInt0V85FailOffset = 21;
const char Serializer::kVccInt0V85FailGpioName[] = "VCCINT_0V85_FAIL";

const char Serializer::kMgtraVcc0V85FailName[] = "MGTRAVCC_0V85_FAIL";
const char Serializer::kMgtraVcc0V85FailDescription[] = "MGTRAVCC 0.85V fail";
const uint32_t Serializer::kMgtraVcc0V85FailOffset = 22;
const char Serializer::kMgtraVcc0V85FailGpioName[] = "MGTRAVCC_0V85_FAIL";

const char Serializer::kMgtraVcc0V90FailName[] = "MGTRAVCC_0V90_FAIL";
const char Serializer::kMgtraVcc0V90FailDescription[] = "MGTRAVCC 0.9V fail";
const uint32_t Serializer::kMgtraVcc0V90FailOffset = 23;
const char Serializer::kMgtraVcc0V90FailGpioName[] = "MGTRAVCC_0V90_FAIL";

const char Serializer::kMainPowerSupplyPowerGoodName[] = "MainPowerSupplyPowerGood";
const char Serializer::kMainPowerSupplyPowerGoodDescription[] = "Main Power Supply, Power Good";
const uint32_t Serializer::kMainPowerSupplyPowerGoodOffset = 24;
const char Serializer::kMainPowerSupplyPowerGoodGpioName[] = "MAIN_PWR_LI_PG";

const char Serializer::kMainPowerSupplyNotAlertName[] = "MainPowerSupplyNotAlert";
const char Serializer::kMainPowerSupplyNotAlertDescription[] = "Main Power Supply, NotAlert";
const uint32_t Serializer::kMainPowerSupplyNotAlertOffset = 25;
const char Serializer::kMainPowerSupplyNotAlertGpioName[] = "MAIN_PWR_LI_ALERT_N";

const char Serializer::kTemperatureSensorAlertName[] = "TemperatureSensorAlert";
const char Serializer::kTemperatureSensorAlertDescription[] = "Temperature Sensor Alert";
const uint32_t Serializer::kTemperatureSensorAlertOffset = 26;
const char Serializer::kTemperatureSensorAlertGpioName[] = "TEMP_ALERT";

const char Serializer::kHdcDrdyInterruptName[] = "HdcDrdyInterrupt";
const char Serializer::kHdcDrdyInterruptDescription[] = "Humidity and Temperature Sensor Interrupt";
const uint32_t Serializer::kHdcDrdyInterruptOffset = 27;
const char Serializer::kHdcDrdyInterruptGpioName[] = "HDC_DRDY_INT";

const char Serializer::kVccInternal0V85NotAlertName[] = "VccInternal0V85NotAlert";
const char Serializer::kVccInternal0V85NotAlertDescription[] = "Internal 0.85V Supply, Not Alert";
const uint32_t Serializer::kVccInternal0V85NotAlertOffset = 28;
const char Serializer::kVccInternal0V85NotAlertGpioName[] = "VCCINT_0V85_ALERT_N";

const char Serializer::kVaa1V2failName[] = "VAA_1V2_FAIL";
const char Serializer::kVaa1V2failDescription[] = "VAA 1.2V fail";
const uint32_t Serializer::kVaa1V2failOffset = 29;
const char Serializer::kVaa1V2failGpioName[] = "VAA_1V2_FAIL";

const char Serializer::kVcc1V2failName[] = "VCC_1V2_FAIL";
const char Serializer::kVcc1V2failDescription[] = "VCC 1.2V fail";
const uint32_t Serializer::kVcc1V2failOffset = 30;
const char Serializer::kVcc1V2failGpioName[] = "VCC_1V2_FAIL";

const char Serializer::kVdd1V2failName[] = "VDD_1V2_FAIL";
const char Serializer::kVdd1V2failDescription[] = "VDD 1.2V fail";
const uint32_t Serializer::kVdd1V2failOffset = 31;
const char Serializer::kVdd1V2failGpioName[] = "VDD_1V2_FAIL";

const char Serializer::kVee1V2FailName[] = "VEE_1V2_FAIL";
const char Serializer::kVee1V2FailDescription[] = "VDD 1.2V fail";
const uint32_t Serializer::kVee1V2FailOffset = 31;
const char Serializer::kVee1V2FailGpioName[] = "VEE_1V2_FAIL";

const char Serializer::kVddDdr1V2FailName[] = "VDD_DDR_1V2_FAIL";
const char Serializer::kVddDdr1V2FailDescription[] = "VDD DDR 1.2V fail";
const uint32_t Serializer::kVddDdr1V2FailOffset = 31;
const char Serializer::kVddDdr1V2FailGpioName[] = "VDD_DDR_1V2_FAIL";

const char Serializer::kDdrVttFailName[] = "DDR_VTT_FAIL";
const char Serializer::kDdrVttFailDescription[] = "DDR VTT fail";
const uint32_t Serializer::kDdrVttFailOffset = 31;
const char Serializer::kDdrVttFailGpioName[] = "DDR_VTT_FAIL";

const char Serializer::kDdrVrefFailName[] = "DDR_VREF_FAIL";
const char Serializer::kDdrVrefFailDescription[] = "DDR Vref fail";
const uint32_t Serializer::kDdrVrefFailOffset = 31;
const char Serializer::kDdrVrefFailGpioName[] = "DDR_VREF_FAIL";

const char Serializer::kVii1V5FailName[] = "VII_1V5_FAIL";
const char Serializer::kVii1V5FailDescription[] = "VII 1.5V fail";
const uint32_t Serializer::kVii1V5Failffset = 31;
const char Serializer::kVii1V5FailGpioName[] = "VII_1V5_FAIL";

const char Serializer::kVii1V6FailName[] = "VII_1V6_FAIL";
const char Serializer::kVii1V6FailDescription[] = "VII 1.6V fail";
const uint32_t Serializer::kVii1V6FailOffset = 31;
const char Serializer::kVii1V6FailGpioName[] = "VII_1V6_FAIL";

const char Serializer::kVjj1V6FailName[] = "VJJ_1V6_FAIL";
const char Serializer::kVjj1V6FailDescription[] = "VJJ 1.6V fail";
const uint32_t Serializer::kVjj1V6FailOffset = 31;
const char Serializer::kVjj1V6FailGpioName[] = "VJJ_1V6_FAIL";

const char Serializer::kVcc1V8FailName[] = "VCC_1V8_FAIL";
const char Serializer::kVcc1V8FailDescription[] = "VCC 1.8V fail";
const uint32_t Serializer::kVcc1V8FailOffset = 31;
const char Serializer::kVcc1V8FailGpioName[] = "VCC_1V8_FAIL";

const char Serializer::kSfpPowerFailName[] = "SFP_POWER_FAIL";
const char Serializer::kSfpPowerFailDescription[] = "SFP power fail";
const uint32_t Serializer::kSfpPowerFailOffset = 31;
const char Serializer::kSfpPowerFailGpioName[] = "SFP_POWER_FAIL";

const char Serializer::kVaaMgt1V8FailName[] = "VAA_MGT_1V8_FAIL";
const char Serializer::kVaaMgt1V8FailDescription[] = "VAA MGT 1.8V fail";
const uint32_t Serializer::kVaaMgt1V8FailOffset = 31;
const char Serializer::kVaaMgt1V8FailGpioName[] = "VAA_MGT_1V8_FAIL";

const char Serializer::kVii2V1FailName[] = "VII_2V1_FAIL";
const char Serializer::kVii2V1FailDescription[] = "VII 2.1V fail";
const uint32_t Serializer::kVii2V1FailOffset = 31;
const char Serializer::kVii2V1FailGpioName[] = "VII_2V1_FAIL";

const char Serializer::kVcc2V5FailName[] = "VCC_2V5_FAIL";
const char Serializer::kVcc2V5FailDescription[] = "VCC 2.5V fail";
const uint32_t Serializer::kVcc2V5FailOffset = 31;
const char Serializer::kVcc2V5FailGpioName[] = "VCC_2V5_FAIL";

const char Serializer::kVii2V8FailName[] = "VII_2V8_FAIL";
const char Serializer::kVii2V8FailDescription[] = "VII 2.8V fail";
const uint32_t Serializer::kVii2V8FailOffset = 31;
const char Serializer::kVii2V8FailGpioName[] = "VII_2V8_FAIL";

const char Serializer::kVcc3V3FailName[] = "VCC_3V3_FAIL";
const char Serializer::kVcc3V3FailDescription[] = "VCC 3.3V fail";
const uint32_t Serializer::kVcc3V3FailOffset = 31;
const char Serializer::kVcc3V3FailGpioName[] = "VCC_3V3_FAIL";

const char Serializer::kVcc5V0FailName[] = "VCC_5V0_FAIL";
const char Serializer::kVcc5V0FailDescription[] = "VCC 5.0V fail";
const uint32_t Serializer::kVcc5V0FailOffset = 31;
const char Serializer::kVcc5V0FailGpioName[] = "VCC_5V0_FAIL";

const char Serializer::kPLBuildTimestampName[] = "PLBuildTimestamp";
const char Serializer::kPLBuildTimestampDescription[] = "Build timestamp of firmware (PL-part).";
const uint32_t Serializer::kPLBuildTimestampPosition = 4;

const char Serializer::kPLBuildVersionName[] = "PLBuildVersion";
const char Serializer::kPLBuildVersionDescription[] = "Build version of firmware (PL-part).";
const uint32_t Serializer::kPLBuildVersionPosition = 8;

const char Serializer::kPLLocalIDName[] = "PLLocalID";
const char Serializer::kPLLocalIDDescription[] = "Local ID of firmware (PL-part).";
const uint32_t Serializer::kPLLocalIDPosition = 12;

const char Serializer::kNumberOfOplInterfacesName[] = "NumberOfOplInterfaces";
const char Serializer::kNumberOfOplInterfacesDescription[] =
  "Number of OPL Interfaces instantiated.";
const uint32_t Serializer::kNumberOfOplInterfacesAddress = 44;

const char Serializer::kNumberOfSLinkInterfacesName[] = "NumberOfSLinkInterfaces";
const char Serializer::kNumberOfSLinkInterfacesDescription[] =
  "Number of SLink Interfaces instantiated.";
const uint32_t Serializer::kNumberOfSLinkInterfacesAddress = 56;

const char Serializer::kStatusName[] = "SCUStatus";
const char Serializer::kStatusDescription[] = "SCU Status";
const uint32_t Serializer::kStatusAddressPosition = 16;

const char Serializer::kBeamSwitchOffStatusCollectionName[] = "BeamSwitchOffStatusCollection";
const char Serializer::kBeamSwitchOffStatusCollectionDescription[] =
  "Collection of Beam Switch-Off status items.";
const uint32_t Serializer::kBeamSwitchOffStatusCollectionAddressPosition = 28;

const char Serializer::kSWBIRequestName[] = "SWBIRequest";
const char Serializer::kSWBIRequestDescription[] =
  "Software BI Request. 0x9==Do not request BI, all others==request BI";
const uint32_t Serializer::kSWBIRequestPosition = 32;

const char Serializer::kSWRBIRequestName[] = "SWRBIRequest";
const char Serializer::kSWRBIRequestDescription[] =
  "Regular Software BI Request. 0x9==Do not request RBI, all others==request RBI";
const uint32_t Serializer::kSWRBIRequestPosition = 36;

const char Serializer::kSWEBIRequestName[] = "SWEBIRequest";
const char Serializer::kSWEBIRequestDescription[] =
  "Emergency Software BI Request. 0x9==Do not request EBI, all others==request EBI";
const uint32_t Serializer::kSWEBIRequestPosition = 40;

Serializer::Serializer(
  Logger::ILogger *logger,
  bool * powerEnablePullUpDrive,
  const Configuration::Modules::ModuleSerializer &configSerializer,
  const Configuration::Common::ConfigurationCommon &configCommon)
  : Framework::Components::Container(
  logger,
  kName,
  kDescription) {
  // Register base addresses
  auto baseAddress_CFG = configSerializer.Address()->Base()->CFG()->Value();

  // Get update interval
  auto updateInterval = configCommon.UpdateInterval()->Medium()->Value();

  // Create software
  auto software_obj = std::make_unique<Serializer_Software>(
    logger,
    configCommon);
  software_ = software_obj.get();
  AddComponent(std::move(software_obj));

  // Create card info
  auto cardInfo_obj = std::make_unique<Serializer_CardInfo>(
    logger,
    std::string(kI2CPathBase).append("1-0070/channel-4/"));
  cardInfo_ = cardInfo_obj.get();
  AddComponent(std::move(cardInfo_obj));

  auto gpio = std::make_unique<Serializer_Gpio>(
    logger,
    configCommon,
    powerEnablePullUpDrive);
  gpio_ = gpio.get();
  AddComponent(std::move(gpio));

  auto sfpModule1 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule1Name,
    kSfpModule1BaseAddress,
    1);
  sfpModule1_ = sfpModule1.get();
  AddComponent(std::move(sfpModule1));

  auto sfpModule2 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule2Name,
    kSfpModule2BaseAddress,
    2);
  sfpModule2_ = sfpModule2.get();
  AddComponent(std::move(sfpModule2));

  auto sfpModule3 = std::make_unique<Serializer_SfpModule>(
    logger,
    configCommon,
    kSfpModule3Name,
    kSfpModule3BaseAddress,
    3);
  sfpModule3_ = sfpModule3.get();
  AddComponent(std::move(sfpModule3));

  auto sLinkInterface1 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface1Name,
    configCommon,
    configSerializer.Address()->Base()->CFG()->Value(),
    configSerializer.Address()->Base()->SLINK()->Value());
  sLinkInterface1_ = sLinkInterface1.get();
  AddComponent(std::move(sLinkInterface1));

  auto sLinkInterface2 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface2Name,
    configCommon,
    configSerializer.Address()->Base()->CFG()->Value(),
    configSerializer.Address()->Base()->SLINK()->Value() +
    configSerializer.Address()->Offset()->SLINK()->Value());
  sLinkInterface2_ = sLinkInterface2.get();
  AddComponent(std::move(sLinkInterface2));

  auto sLinkInterface3 = std::make_unique<Serializer_SLinkInterface>(
    logger,
    kSLinkInterface3Name,
    configCommon,
    configSerializer.Address()->Base()->CFG()->Value(),
    configSerializer.Address()->Base()->SLINK()->Value() +
      configSerializer.Address()->Offset()->SLINK()->Value() * 2);
  sLinkInterface3_ = sLinkInterface3.get();
  AddComponent(std::move(sLinkInterface3));

  auto oplInterface = std::make_unique<Serializer_OplInterface>(
    logger,
    configCommon,
    configSerializer);
  oplInterface_ = oplInterface.get();
  AddComponent(std::move(oplInterface));
/*
  auto iniFile = std::make_unique<Serializer_IniFile>(
      logger);
  iniFile_ = iniFile.get();
  AddComponent(std::move(iniFile));
*/
  slot_ = Create().ParameterIn<bool>(
    kSlotName,
    kSlotDescription,
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  slot_->CreateAccessor().DigitalInput(
    kSlotId,
    kSlotGpioName,
    false);
  // Pgm Clock Status
  pgmClkStatusLockDetect_ = Create().ItemIn<bool>(
    kPgmClkStatusLockDetectName,
    kPgmClkStatusLockDetectDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusLockDetect_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusLockDetectOffset,
    kPgmClkStatusLockDetectGpioName,
    false);
  pgmClkStatusHoldover_ = Create().ItemIn<bool>(
    kPgmClkStatusHoldoverName,
    kPgmClkStatusHoldoverDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusHoldover_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusHoldoverOffset,
    kPgmClkStatusHoldoverGpioName,
    false);
  pgmClkStatusClkIn0_ = Create().ItemIn<bool>(
    kPgmClkStatusClkIn0Name,
    kPgmClkStatusClkIn0Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusClkIn0_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusClkIn0Offset,
    kPgmClkStatusClkIn0GpioName,
    false);
  pgmClkStatusClkIn1_ = Create().ItemIn<bool>(
    kPgmClkStatusClkIn1Name,
    kPgmClkStatusClkIn1Description,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkStatusClkIn1_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkStatusClkIn1Offset,
    kPgmClkStatusClkIn1GpioName,
    false);
  pgmClkSync_ = Create().ItemIn<bool>(
    kPgmClkSyncName,
    kPgmClkSyncDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  pgmClkSync_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kPgmClkSyncOffset,
    kPgmClkSyncGpioName,
    false);
  vccInt0V85Fail_ = Create().ItemIn<bool>(
    kVccInt0V85FailName,
    kVccInt0V85FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vccInt0V85Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVccInt0V85FailOffset,
    kVccInt0V85FailGpioName,
    false);
  mgtraVcc0V85Fail_ = Create().ItemIn<bool>(
    kMgtraVcc0V85FailName,
    kMgtraVcc0V85FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  mgtraVcc0V85Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMgtraVcc0V85FailOffset,
    kMgtraVcc0V85FailGpioName,
    false);
  mgtraVcc0V90Fail_ = Create().ItemIn<bool>(
    kMgtraVcc0V90FailName,
    kMgtraVcc0V90FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  mgtraVcc0V90Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMgtraVcc0V90FailOffset,
    kMgtraVcc0V90FailGpioName,
    false);
  // Power status
  mainPowerSupplyPowerGood_ = Create().ItemIn<bool>(
    kMainPowerSupplyPowerGoodName,
    kMainPowerSupplyPowerGoodDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  mainPowerSupplyPowerGood_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMainPowerSupplyPowerGoodOffset,
    kMainPowerSupplyPowerGoodGpioName,
    false);
  mainPowerSupplyNotAlert_ = Create().ItemIn<bool>(
    kMainPowerSupplyNotAlertName,
    kMainPowerSupplyNotAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  mainPowerSupplyNotAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kMainPowerSupplyNotAlertOffset,
    kMainPowerSupplyNotAlertGpioName,
    false);
  vaa1V2fail_ = Create().ItemIn<bool>(
    kVaa1V2failName,
    kVaa1V2failDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vaa1V2fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVaa1V2failOffset,
    kVaa1V2failGpioName,
    false);
  vcc1V2fail_ = Create().ItemIn<bool>(
    kVcc1V2failName,
    kVcc1V2failDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vcc1V2fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVcc1V2failOffset,
    kVcc1V2failGpioName,
    false);
  vdd1V2fail_ = Create().ItemIn<bool>(
    kVdd1V2failName,
    kVdd1V2failDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vdd1V2fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVdd1V2failOffset,
    kVdd1V2failGpioName,
    false);
  vccInternal0V85NotAlert_ = Create().ItemIn<bool>(
    kVccInternal0V85NotAlertName,
    kVccInternal0V85NotAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vccInternal0V85NotAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVccInternal0V85NotAlertOffset,
    kVccInternal0V85NotAlertGpioName,
    false);
  // Temperature Alerts
  temperatureSensorAlert_ = Create().ItemIn<bool>(
    kTemperatureSensorAlertName,
    kTemperatureSensorAlertDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  temperatureSensorAlert_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kTemperatureSensorAlertOffset,
    kTemperatureSensorAlertGpioName,
    true);  // the polarity of the Alert output is by default "low active". See datasheet of chip
  hdcDrdyInterrupt_ = Create().ItemIn<bool>(
    kHdcDrdyInterruptName,
    kHdcDrdyInterruptDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  hdcDrdyInterrupt_->CreateAccessor().DigitalInput(
      kBoardGPIOBaseAddress + kHdcDrdyInterruptOffset,
      kHdcDrdyInterruptGpioName,
    true);  // the polarity of the Interrupt output is by default "low active". See datasheet of chip
  // Vee 1V2 fail
  vee1V2Fail_ = Create().ItemIn<bool>(
    kVee1V2FailName,
    kVee1V2FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vee1V2Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVee1V2FailOffset,
    kVee1V2FailGpioName,
    false);
  // Vdd ddr 1V2 fail
  vddDdr1V2Fail_ = Create().ItemIn<bool>(
    kVddDdr1V2FailName,
    kVddDdr1V2FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vddDdr1V2Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVddDdr1V2FailOffset,
    kVddDdr1V2FailGpioName,
    false);
  // Ddr vtt fail
  ddrVttFail_ = Create().ItemIn<bool>(
    kDdrVttFailName,
    kDdrVttFailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  ddrVttFail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kDdrVttFailOffset,
    kDdrVttFailGpioName,
    false);
  // Ddr vref fail
  ddrVrefFail_ = Create().ItemIn<bool>(
    kDdrVrefFailName,
    kDdrVrefFailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  ddrVrefFail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kDdrVrefFailOffset,
    kDdrVrefFailGpioName,
    false);
  // Vii 1V5 fail
  vii1V5Fail_ = Create().ItemIn<bool>(
    kVii1V5FailName,
    kVii1V5FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vii1V5Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVii1V5Failffset,
    kVii1V5FailGpioName,
    false);
  // Vii 1V6 fail
  vii1V6Fail_ = Create().ItemIn<bool>(
    kVii1V6FailName,
    kVii1V6FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vii1V6Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVii1V6FailOffset,
    kVii1V6FailGpioName,
    false);
  // Vjj 1V6 fail
  vjj1V6Fail_ = Create().ItemIn<bool>(
    kVjj1V6FailName,
    kVjj1V6FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vjj1V6Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVjj1V6FailOffset,
    kVjj1V6FailGpioName,
    false);
  // Vcc 1V8 fail
  vcc1V8Fail_ = Create().ItemIn<bool>(
    kVcc1V8FailName,
    kVcc1V8FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vcc1V8Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVcc1V8FailOffset,
    kVcc1V8FailGpioName,
    false);
  // Sfp power fail
  sfpPowerFail_ = Create().ItemIn<bool>(
    kSfpPowerFailName,
    kSfpPowerFailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  sfpPowerFail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kSfpPowerFailOffset,
    kSfpPowerFailGpioName,
    false);
  // Vaa mgt 1V8 fail
  vaaMgt1V8Fail_ = Create().ItemIn<bool>(
    kVaaMgt1V8FailName,
    kVaaMgt1V8FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vaaMgt1V8Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVaaMgt1V8FailOffset,
    kVaaMgt1V8FailGpioName,
    false);
  // Vii 2V1 fail
  vii2V1Fail_ = Create().ItemIn<bool>(
    kVii2V1FailName,
    kVii2V1FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vii2V1Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVii2V1FailOffset,
    kVii2V1FailGpioName,
    false);
  // Vcc 2V5 fail
  vcc2V5Fail_ = Create().ItemIn<bool>(
    kVcc2V5FailName,
    kVcc2V5FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vcc2V5Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVcc2V5FailOffset,
    kVcc2V5FailGpioName,
    false);
  // Vii 2V8 fail
  vii2V8Fail_ = Create().ItemIn<bool>(
    kVii2V8FailName,
    kVii2V8FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vii2V8Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVii2V8FailOffset,
    kVii2V8FailGpioName,
    false);
  // Vcc 3V3 fail
  vcc3V3Fail_ = Create().ItemIn<bool>(
    kVcc3V3FailName,
    kVcc3V3FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vcc3V3Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVcc3V3FailOffset,
    kVcc3V3FailGpioName,
    false);
  // Vcc 5V0 fail
  vcc5V0Fail_ = Create().ItemIn<bool>(
    kVcc5V0FailName,
    kVcc5V0FailDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  vcc5V0Fail_->CreateAccessor().DigitalInput(
    kBoardGPIOBaseAddress + kVcc5V0FailOffset,
    kVcc5V0FailGpioName,
    false);

  // Setup Firmware Build Timestamp input
  plBuildTimestamp_ = Create().ParameterIn<uint32_t>(  // TBD: Welcher Datentyp? Der Wert ist in sekunden UNIX epoch
    kPLBuildTimestampName,
    kPLBuildTimestampDescription,
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();  // TBD: muss eigentlich nur einmal gelesen werden
  plBuildTimestamp_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLBuildTimestampPosition,
    0,
    32);

  // Setup PL local ID input
  plLocalID_ = Create().ParameterIn<uint32_t>(
      kPLLocalIDName,
      kPLLocalIDDescription,
      Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  plLocalID_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLLocalIDPosition,
      0,
      32);

  // Setup Firmware GIT revision short hash input
  plBuildVersion_ = Create().ParameterIn<uint32_t>(
      kPLBuildVersionName,
      kPLBuildVersionDescription,
      Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  plBuildVersion_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kPLBuildVersionPosition,
      0,
      32);

  // Number of "SLink Interfaces"
  numberOfSLinkInterfaces_ = Create().ParameterIn<uint32_t>(
    kNumberOfSLinkInterfacesName,
    kNumberOfSLinkInterfacesDescription,
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  numberOfSLinkInterfaces_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kNumberOfSLinkInterfacesAddress,
    0,
    32);

  // Number of "OPL Interface"
  numberOfOplInterfaces_ = Create().ParameterIn<uint32_t>(
    kNumberOfOplInterfacesName,
    kNumberOfOplInterfacesDescription,
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  numberOfOplInterfaces_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kNumberOfOplInterfacesAddress,
    0,
    32);

  // Setup SCU State inout
  // ToDo: Die Software sollte den status periodisch setzen und zwar gemäss
  // ihrem Zustand. Dazu ist eine State-machine zu implementieren
  // und mit Monitoring Functions sowie Front-Handle zu "verknüpfen"
  status_ = Create().ItemInOut<Types::ScuStatus>(
    kStatusName,
    kStatusDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    false).AddAndReturnPointer();
  // ToDo: Wird in der Regel von der SW geschrieben, kann aber von der
  // Firmware überschrieben werden wenn die SW zu lange nicht schreibt!
  status_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kStatusAddressPosition,
    0,
    8);  // ??
  status_->CreateAction().Serializer().Status(
    GetModuleHandler());

  // Setup Beam Switch-Off Status collection
  beamSwitchOffStatusCollection_ = Create().ItemIn<uint32_t>(
    kBeamSwitchOffStatusCollectionName,
    kBeamSwitchOffStatusCollectionDescription,
    Types::ValueGroup::Enum::NONE,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE).AddAndReturnPointer();
  beamSwitchOffStatusCollection_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kBeamSwitchOffStatusCollectionAddressPosition,
    0,
    32);

  // Setup SW Beam Inhibit Request output (default value = 9)
  swBIRequest_ = Create().ItemOut<uint8_t>(
    kSWBIRequestName,
    kSWBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWBIRequestPosition,
    0,
    8);
  swBIRequest_->CreateAction().StaticValue(
    9);

  // Setup SW Regular Beam Inhibit Request output (default value = 9)
  swRBIRequest_ = Create().ItemOut<uint8_t>(
    kSWRBIRequestName,
    kSWRBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swRBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWRBIRequestPosition,
    0,
    8);
  swRBIRequest_->CreateAction().StaticValue(
    9);

  // Setup SW Emergency Beam Inhibit Request output (default value = 9)
  swEBIRequest_ = Create().ItemOut<uint8_t>(
    kSWEBIRequestName,
    kSWEBIRequestDescription,
    std::chrono::milliseconds(updateInterval),
    Types::LockId::Enum::EXCLUSIVE,
    Types::WriteStrategy::Enum::ON_UPDATE,
    true,
    0).AddAndReturnPointer();
  swEBIRequest_->CreateAccessor().ScuRegister(
      baseAddress_CFG + kSWEBIRequestPosition,
    0,
    8);
  swEBIRequest_->CreateAction().StaticValue(
    9);

  // Create hot swap controller
  auto hotSwapController = std::make_unique<Modules::Common::HotSwapController>(
    logger,
    *configSerializer.HotSwapController(),
    configCommon,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    0x4a);
  hotSwapController_ = hotSwapController.get();
  AddComponent(std::move(hotSwapController));

  // Create humidity sensor
  auto humiditySensor = std::make_unique<Modules::Common::HumiditySensor>(
    logger,
    configCommon,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    0x41);
  humiditySensor_ = humiditySensor.get();
  AddComponent(std::move(humiditySensor));

  // Create temperature
  auto upperTemperature = std::make_unique<Modules::Temperature>(
    logger,
    configCommon,
    kUpperTemperatureName,
    kUpperTemperatureDescription,
    kUpperTemperatureAddress,
    std::string(kI2CPathBase).append("1-0070/channel-4/"),
    Types::MonitoringFunctionId::Enum::SCU_MON_W_FUNCTION_temp_upper_PCB_Edge,
    Types::MonitoringFunctionId::Enum::SCU_MON_E_FUNCTION_temp_upper_PCB_Edge);
  upperTemperature_ = upperTemperature.get();
  AddComponent(std::move(upperTemperature));
}

Serializer_Software *Serializer::Software() const {
  return software_;
}

Serializer_CardInfo *Serializer::CardInfo() const {
  return cardInfo_;
}

Serializer_Gpio *Serializer::Gpio() const {
  return gpio_;
}

Serializer_SfpModule *Serializer::SfpModule1() const {
  return sfpModule1_;
}

Serializer_SfpModule *Serializer::SfpModule2() const {
  return sfpModule2_;
}

Serializer_SfpModule *Serializer::SfpModule3() const {
  return sfpModule3_;
}

Serializer_OplInterface *Serializer::OplInterface() const {
  return oplInterface_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface1() const {
  return sLinkInterface1_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface2() const {
  return sLinkInterface2_;
}

Serializer_SLinkInterface *Serializer::SLinkInterface3() const {
  return sLinkInterface3_;
}

Framework::Components::ParameterItemIn<bool> *Serializer::Slot() {
  return slot_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusLockDetect() const {
  return pgmClkStatusLockDetect_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusHoldover() const {
  return pgmClkStatusHoldover_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusClkIn0() const {
  return pgmClkStatusClkIn0_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkStatusClkIn1() const {
  return pgmClkStatusClkIn1_;
}

Framework::Components::ItemIn<bool> *Serializer::PgmClkSync() const {
  return pgmClkSync_;
}

Framework::Components::ItemIn<bool> *Serializer::MainPowerSupplyPowerGood() const {
  return mainPowerSupplyPowerGood_;
}

Framework::Components::ItemIn<bool> *Serializer::MainPowerSupplyNotAlert() const {
  return mainPowerSupplyNotAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::VccInt0V85Fail() const {
  return vccInt0V85Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::MgtraVcc0V85Fail() const {
  return mgtraVcc0V85Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::MgtraVcc0V90Fail() const {
  return mgtraVcc0V90Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vaa1V2fail() const {
  return vaa1V2fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vcc1V2fail() const {
  return vcc1V2fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vdd1V2fail() const {
  return vdd1V2fail_;
}

Framework::Components::ItemIn<bool> *Serializer::VccInternal0V85NotAlert() const {
  return vccInternal0V85NotAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::TemperatureSensorAlert() const {
  return temperatureSensorAlert_;
}

Framework::Components::ItemIn<bool> *Serializer::HdcDrdyInterrupt() const {
  return hdcDrdyInterrupt_;
}

Framework::Components::ItemIn<bool> *Serializer::Vee1V2Fail() const {
  return vee1V2Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::VddDdr1V2Fail() const {
  return vddDdr1V2Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::DdrVttFail() const {
  return ddrVttFail_;
}

Framework::Components::ItemIn<bool> *Serializer::DdrVrefFail() const {
  return ddrVrefFail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vii1V5Fail() const {
  return vii1V5Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vii1V6Fail() const {
  return vii1V6Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vjj1V6Fail() const {
  return vjj1V6Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vcc1V8Fail() const {
  return vcc1V8Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::SfpPowerFail() const {
  return sfpPowerFail_;
}

Framework::Components::ItemIn<bool> *Serializer::VaaMgt1V8Fail() const {
  return vaaMgt1V8Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vii2V1Fail() const {
  return vii2V1Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vcc2V5Fail() const {
  return vcc2V5Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vii2V8Fail() const {
  return vii2V8Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vcc3V3Fail() const {
  return vcc3V3Fail_;
}

Framework::Components::ItemIn<bool> *Serializer::Vcc5V0Fail() const {
  return vcc5V0Fail_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlBuildTimestamp() const {
  return plBuildTimestamp_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlBuildVersion() const {
  return plBuildVersion_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::PlLocalID() const {
  return plLocalID_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::NumberOfOplInterfaces() const {
  return numberOfOplInterfaces_;
}

Framework::Components::ParameterItemIn<uint32_t> *Serializer::NumberOfSLinkInterfaces() const {
  return numberOfSLinkInterfaces_;
}

Framework::Components::ItemInOut<Types::ScuStatus> *Serializer::Status() const {
  return status_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwBIRequest() const {
  return swBIRequest_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwRBIRequest() const {
  return swRBIRequest_;
}

Framework::Components::ItemOut<uint8_t> *Serializer::SwEBIRequest() const {
  return swEBIRequest_;
}

Modules::Common::HotSwapController *Serializer::HotSwapController() const {
  return hotSwapController_;
}

Modules::Common::HumiditySensor *Serializer::HumiditySensor() const {
  return humiditySensor_;
}

Modules::Temperature *Serializer::UpperTemperature() const {
  return upperTemperature_;
}

}  // namespace SystemModules::Modules::Serializer
