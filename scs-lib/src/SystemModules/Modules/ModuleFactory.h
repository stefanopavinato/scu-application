/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <optional>
#include <memory>
#include "Types/ModuleType.h"
#include "SystemModules/Configuration/Configuration.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace SystemModules {
namespace Framework::Components {
class Container;
}  // namespace Framework::Components
namespace Modules {
class ModuleFactory {
 public:
  static std::optional<std::unique_ptr<Framework::Components::Container>> Create(
    Logger::ILogger *logger,
    const Configuration::Configuration &configuration,
    const uint32_t & slot,
    const Types::ModuleType::Enum &type);
};
}  // namespace Modules
}  // namespace SystemModules
