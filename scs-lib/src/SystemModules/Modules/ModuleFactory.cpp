/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Logger/ILogger.h"
#include "SystemModules/Configuration/Modules/Modules.h"
#include "SystemModules/Configuration/Slots/Slots.h"
#include "SystemModules/Configuration/Slots/SlotMC.h"
#include "SystemModules/Framework/Components/Container.h"
#include "SystemModules/Framework/Components/ModuleHandler.h"
#include "SystemModules/Modules/MezzanineCards/BUMC/BUMC.h"
#include "SystemModules/Modules/MezzanineCards/CLMC/CLMC.h"
#include "SystemModules/Modules/MezzanineCards/CHMC/CHMC.h"
#include "SystemModules/Modules/MezzanineCards/RS485MC/RS485MC.h"
#include "SystemModules/Modules/MezzanineCards/LVDSMC/LVDSMC.h"
#include "SystemModules/Modules/MezzanineCards/ISMC1/ISMC1.h"
#include "SystemModules/Modules/MezzanineCards/ISMC2/ISMC2.h"
#include "ModuleFactory.h"

namespace SystemModules::Modules {
std::optional<std::unique_ptr<Framework::Components::Container>> ModuleFactory::Create(
  Logger::ILogger *logger,
  const Configuration::Configuration &configuration,
  const uint32_t & slot,
  const Types::ModuleType::Enum &type) {
  switch (type) {
    case Types::ModuleType::Enum::SCU_DYMC: {
      return std::nullopt;
    }
    case Types::ModuleType::Enum::SCU_BUMC: {
      return std::make_unique<SystemModules::Modules::BUMC::BUMC>(
        logger,
        *configuration.Modules()->BUMC(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_CLMC: {
      return std::make_unique<SystemModules::Modules::CLMC::CLMC>(
        logger,
        *configuration.Modules()->CLMC(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_RS485MC: {
      return std::make_unique<SystemModules::Modules::RS485MC::RS485MC>(
        logger,
        *configuration.Modules()->RS485MC(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_LVDSMC: {
      return std::make_unique<SystemModules::Modules::LVDSMC::LVDSMC>(
        logger,
        *configuration.Modules()->LVDSMC(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_ISMC1: {
      return std::make_unique<SystemModules::Modules::ISMC1::ISMC1>(
        logger,
        *configuration.Modules()->ISMC1(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_ISMC2: {
      return std::make_unique<SystemModules::Modules::ISMC2::ISMC2>(
        logger,
        *configuration.Modules()->ISMC2(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    case Types::ModuleType::Enum::SCU_CHMC: {
      return std::make_unique<SystemModules::Modules::CHMC::CHMC>(
        logger,
        *configuration.Modules()->CHMC(),
        *configuration.Common(),
        *configuration.Slots()->MC()->Mc(slot));
    }
    default: {
      return std::nullopt;
    }
  }
}
}  // namespace SystemModules::Modules
