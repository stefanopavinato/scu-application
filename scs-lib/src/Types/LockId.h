/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class LockId;
}  // namespace Types

template<>
class std::hash<Types::LockId> {
 public:
  size_t operator()(const Types::LockId & id) const;
};

namespace Types {
class LockId {
  friend size_t std::hash<LockId>::operator()(const LockId & id) const;

 public:
  enum class Enum : uint32_t {
    EXCLUSIVE,
    SHARED,
    MODULE_HANDLER,
    NONE
  };

  LockId();

  explicit LockId(const Enum &id);

  [[nodiscard]] Enum GetValue() const;

  [[nodiscard]] static Enum ToEnum(const uint32_t &value);

  [[nodiscard]] std::string ToString() const;

  bool operator==(const LockId &other) const;

  bool operator!=(const LockId &other) const;

  static Enum FromString(const std::string &str);

 private:
  Enum id_;

  static const char kExclusive[];
  static const char kShared[];
  static const char kModuleHandler[];
  static const char kNone[];
};
}  // namespace Types
