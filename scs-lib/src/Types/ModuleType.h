/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ModuleType {
 public:
  enum class Enum {
    SCU_PWSUP = 0x01,
    SCU_SER = 0x02,
    SCU_FU = 0x03,
    SCU_DYMC = 0x10,
    SCU_BUMC = 0x20,
    SCU_CLMC = 0x30,
    SCU_RS485MC = 0x40,
    SCU_LVDSMC = 0x50,
    SCU_ISMC1 = 0x61,
    SCU_ISMC2 = 0x62,
    SCU_CHMC = 0x70,
    NONE
  };

  ModuleType();

  ModuleType(const ModuleType &other);

  explicit ModuleType(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  ModuleType &operator=(const ModuleType &other);

  // ModuleType& operator= (const std::string& other);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  bool operator==(const ModuleType &other) const;

  bool operator!=(const ModuleType &other) const;

  bool operator>(const ModuleType &other) const;

  bool operator<(const ModuleType &other) const;

  bool operator>=(const ModuleType &other) const;

  bool operator<=(const ModuleType &other) const;

  friend std::ostream &operator<<(std::ostream &out, const ModuleType &moduleType) {
    return out << moduleType.ToString();
  }

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kSCU_PWSUPName[];
  static const char kSCU_SERName[];
  static const char kSCU_FUName[];
  static const char kSCU_CLMCName[];
  static const char kSCU_RS485MCName[];
  static const char kSCU_LVDSMCName[];
  static const char kSCU_ISMC1Name[];
  static const char kSCU_ISMC2Name[];
  static const char kSCU_CHMCName[];
  static const char kSCU_DYMCName[];
  static const char kSCU_BUMCName[];
  static const char kNoneName[];
};

}  // namespace Types
