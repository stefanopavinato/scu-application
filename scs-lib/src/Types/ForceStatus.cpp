/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************


#include "ForceStatus.h"

namespace Types {

const char ForceStatus::kNone[] = "NONE";
const char ForceStatus::kPermanent[] = "PERMANENT";
const char ForceStatus::kOnce[] = "ONCE";

ForceStatus::ForceStatus(const Enum &value)
  : value_(value) {
}

ForceStatus::Enum ForceStatus::GetValue() const {
  return value_;
}

void ForceStatus::SetValue(const Enum &value) {
  value_ = value;
}

std::string ForceStatus::ToString() const {
  switch (value_) {
    case Enum::PERMANENT: {
      return kPermanent;
    }
    case Enum::ONCE: {
      return kOnce;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
}

}  // namespace Types
