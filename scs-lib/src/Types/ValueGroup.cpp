/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ValueGroup.h"

namespace Types {

const char ValueGroup::kTypeName[] = "ValueGroup";

const char ValueGroup::kTemperature[] = "TEMPERATURE";
const char ValueGroup::kSupply[] = "SUPPLY";
const char ValueGroup::kSpeed[] = "SPEED";
const char ValueGroup::kAny[] = "ANY";
const char ValueGroup::kNone[] = "NONE";

ValueGroup::ValueGroup()
  : value_(Enum::NONE) {
}

ValueGroup::ValueGroup(const Enum &value)
  : value_(value) {
}

std::string ValueGroup::GetTypeName() {
  return kTypeName;
}

ValueGroup::Enum ValueGroup::GetValue() const {
  return value_;
}

void ValueGroup::SetValue(const ValueGroup::Enum &value) {
  value_ = value;
}

bool ValueGroup::operator==(const ValueGroup &other) const {
  return value_ == other.GetValue();
}

bool ValueGroup::operator!=(const ValueGroup &other) const {
  return value_ != other.GetValue();
}

std::string ValueGroup::ToString() const {
  switch (value_) {
    case Enum::TEMPERATURE: {
      return kTemperature;
    }
    case Enum::SUPPLY: {
      return kSupply;
    }
    case Enum::SPEED: {
      return kSpeed;
    }
    case Enum::ANY: {
      return kAny;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void ValueGroup::FromString(const std::string &str) {
  if (str == kTemperature) {
    value_ = Enum::TEMPERATURE;
  } else if (str == kSupply) {
    value_ = Enum::SUPPLY;
  } else if (str == kSpeed) {
    value_ = Enum::SPEED;
  } else if (str == kAny) {
    value_ = Enum::ANY;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
