/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Types/Unit.h"
#include "Types/UnitPrefix.h"

namespace Types {
template<class T>
class Value {
 public:
  Value() = default;

  explicit Value(
    const T &value);

  Value(
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix);

  Value(
    const T &value,
    const Types::Unit::Enum &unit,
    const Types::UnitPrefix::Enum &prefix);

  Value(const Value<T> &other);

  [[nodiscard]] T GetValue() const;

  [[nodiscard]] T GetValue(const Types::UnitPrefix::Enum &prefix);

  [[nodiscard]] T GetValue(const Types::UnitPrefix &prefix);

  T *ASDF();

  bool SetValue(const T &value);

  bool SetValue(const T &value, const Types::UnitPrefix &prefix);

  [[nodiscard]] Types::Unit GetUnit() const;

  [[nodiscard]] Types::UnitPrefix GetPrefix() const;

  [[nodiscard]] std::string ToString() const;

  Value<T> &operator=(const Value &other);

  bool operator==(const Value &other) const;

  bool operator!=(const Value &other) const;

  bool operator>(const Value &other) const;

  bool operator<(const Value &other) const;

  bool operator>=(const Value &other) const;

  bool operator<=(const Value &other) const;

  Value operator*(const Value &other) const;

  Value operator/(const Value &other) const;

  Value operator+(const Value &other) const;

  friend std::ostream &operator<<(std::ostream &out, const Value<T> &value) {
    return out << value.GetValue() << " " << value.GetPrefix() << value.GetUnit();
  }

 private:
  T value_;

  Types::Unit unit_;

  Types::UnitPrefix prefix_;
};
}  // namespace Types

#include "Value.tpp"
