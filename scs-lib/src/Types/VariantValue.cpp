/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <iostream>
#include <sstream>
#include <cstdint>
#include "Messages/Visitor/VisitorBase.h"
#include "VariantValue.h"

namespace Types {

Types::ValueType VariantValueUtils::GetType(const VariantValue & value) {
  return Types::ValueType(static_cast<Types::ValueType::Enum>(value.index()));
}

std::optional<VariantValue> VariantValueUtils::FromString(
  const Types::ValueType &type,
  const std::string &str) {
  try {
    switch (type.GetValue()) {
      case Types::ValueType::Enum::VALUE_INT32: {
        auto val = Value<int32_t>();
        val.SetValue(std::stoi(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_INT16: {
        auto val = Value<int16_t>();
        val.SetValue(std::stoi(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_INT8: {
        auto val = Value<int8_t>();
        val.SetValue(std::stoi(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_UINT32: {
        auto val = Value<uint32_t>();
        val.SetValue(std::stoul(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_UINT16: {
        auto val = Value<uint16_t>();
        val.SetValue(std::stoul(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_UINT8: {
        auto val = Value<uint8_t>();
        val.SetValue(std::stoul(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_DOUBLE: {
        auto val = Value<double>();
        val.SetValue(std::stod(str));
        return val;
      }
      case Types::ValueType::Enum::VALUE_FLOAT: {
        auto val = Value<float>();
        val.SetValue(std::stof(str));
        return val;
      }
      case Types::ValueType::Enum::INT32: {
        return static_cast<int32_t>(std::stoi(str));
      }
      case Types::ValueType::Enum::INT16: {
        return static_cast<int16_t>(std::stoi(str));
      }
      case Types::ValueType::Enum::INT8: {
        return static_cast<int8_t>(std::stoi(str));
      }
      case Types::ValueType::Enum::UINT32: {
        return static_cast<uint32_t>(std::stoul(str));
      }
      case Types::ValueType::Enum::UINT16: {
        return static_cast<uint16_t>(std::stoul(str));
      }
      case Types::ValueType::Enum::UINT8: {
        return static_cast<uint8_t>(std::stoul(str));
      }
      case Types::ValueType::Enum::DOUBLE: {
        return std::stod(str);
      }
      case Types::ValueType::Enum::FLOAT: {
        return std::stof(str);
      }
      case Types::ValueType::Enum::BOOL: {
        if (str == "0" || str == "false" || str == "FALSE") {
          return false;
        } else if (str == "1" || str == "true" || str == "TRUE") {
          return true;
        }
        return std::nullopt;
      }
      case Types::ValueType::Enum::STRING: {
        return str;
      }
      case Types::ValueType::Enum::ALERT_SEVERITY: {
        return std::nullopt;
      }
      case Types::ValueType::Enum::MODULE_TYPE: {
        return std::nullopt;
      }
      case Types::ValueType::Enum::MODULE_STATE: {
        return std::nullopt;
      }
      case Types::ValueType::Enum::CHRONO_MINUTES: {
        return std::nullopt;
      }
      case Types::ValueType::Enum::CHRONO_MILLISECONDS: {
        return std::nullopt;
      }
      case Types::ValueType::Enum::NONE: {
        return std::nullopt;
      }
    }
  } catch (...) {
    return std::nullopt;
  }
  return std::nullopt;
}

template<class... Ts>
struct overloaded : Ts ... {
  using Ts::operator()...;
};
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

std::string VariantValueUtils::ToString(const VariantValue & value) {
  std::string out;
  std::visit(overloaded{
    [&out](const Types::Value<uint32_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<uint16_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<uint8_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int32_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int16_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<int8_t> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<double> &arg) { out.append(arg.ToString()); },
    [&out](const Types::Value<float> &arg) { out.append(arg.ToString()); },
    [&out](const uint32_t &arg) { out.append(std::to_string(arg)); },
    [&out](const uint16_t &arg) { out.append(std::to_string(arg)); },
    [&out](const uint8_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int32_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int16_t &arg) { out.append(std::to_string(arg)); },
    [&out](const int8_t &arg) { out.append(std::to_string(arg)); },
    [&out](const double &arg) { out.append(std::to_string(arg)); },
    [&out](const float &arg) { out.append(std::to_string(arg)); },
    [&out](bool arg) { out.append(std::to_string(arg)); },
    [&out](const std::string &arg) { out.append(arg); },
    [&out](const Types::ModuleType &arg) { out.append(arg.ToString()); },
    [&out](const Types::ModuleState &arg) { out.append(arg.ToString()); },
    [&out](Types::AlertSeverity arg) { out.append(arg.ToString()); },
    [&out](std::chrono::minutes arg) { out.append(std::to_string(arg.count())); },
    [&out](std::chrono::milliseconds arg) { out.append(std::to_string(arg.count())); },
    [&out](Types::ScuStatus arg) { out.append(arg.ToString()); },
    [&out](Types::OplStatus arg) { out.append(arg.ToString()); },
    [&out](const std::vector<uint32_t> &arg) {
      for (const auto &value : arg) {
        out.append(std::to_string(value)).append(", ");
      }
      if (!out.empty()) {
        out.resize(out.size() - 2);
      }
    }
  }, value);
  return out;
}

}  // namespace Types
