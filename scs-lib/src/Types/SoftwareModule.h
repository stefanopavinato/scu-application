/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class SoftwareModule;
}  // namespace Types

template<>
class std::hash<Types::SoftwareModule> {
 public:
  size_t operator()(const Types::SoftwareModule & module) const;
};

namespace Types {
class SoftwareModule {
  friend size_t std::hash<SoftwareModule>::operator()(const SoftwareModule & module) const;

 public:
  enum class Enum {
    MESSAGE_HANDLER = 0,
    DIRECT_ACCESS = 1,
    LOCK_HANDLER = 2,
    CLIENT_HANDLER = 3,
    CLIENT_ESTABLISHER = 4,
    BALANCER = 5,
    NONE
  };

  SoftwareModule();

  SoftwareModule(const SoftwareModule &other);

  explicit SoftwareModule(const Enum &e);

  virtual ~SoftwareModule() = default;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum& e);

  SoftwareModule &operator=(const SoftwareModule &other);

  bool operator==(const SoftwareModule& other) const;

  static const char kMessageHandler[];
  static const char kDirectAccess[];
  static const char kLockHandler[];
  static const char kClientHandler[];
  static const char kClientEstablisher[];
  static const char kBalancer[];
  static const char kNone[];

 private:
  Enum enum_;
};
}  // namespace Types
