/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************


#pragma once

#include <string>
#include <type_traits>
#include <chrono>
#include "Value.h"
#include "ModuleType.h"
#include "ModuleState.h"
#include "AlertSeverity.h"

namespace Types {
class ValueType {
 public:
  enum class Enum {
    VALUE_INT32 = 0,
    VALUE_INT16 = 1,
    VALUE_INT8 = 2,
    VALUE_UINT32 = 3,
    VALUE_UINT16 = 4,
    VALUE_UINT8 = 5,
    VALUE_DOUBLE = 6,
    VALUE_FLOAT = 7,
    INT32 = 8,
    INT16 = 9,
    INT8 = 10,
    UINT32 = 11,
    UINT16 = 12,
    UINT8 = 13,
    DOUBLE = 14,
    FLOAT = 15,
    BOOL = 16,
    STRING = 17,
    ALERT_SEVERITY = 18,
    MODULE_TYPE = 19,
    CHRONO_MINUTES = 20,
    CHRONO_MILLISECONDS = 21,
    MODULE_STATE = 22,
    NONE
  };

  ValueType();

  explicit ValueType(const Enum &value);

  explicit ValueType(const std::string &name);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetValue() const;

  void SetValue(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  bool operator==(const ValueType &other) const;

  template<typename VALUE>
  static Enum GetEnum();

  static const char kValueInt32[];
  static const char kValueInt16[];
  static const char kValueInt8[];
  static const char kValueUInt32[];
  static const char kValueUInt16[];
  static const char kValueUInt8[];
  static const char kValueDouble[];
  static const char kValueFloat[];
  static const char kInt32[];
  static const char kInt16[];
  static const char kInt8[];
  static const char kUInt32[];
  static const char kUInt16[];
  static const char kUInt8[];
  static const char kDouble[];
  static const char kFloat[];
  static const char kBool[];
  static const char kString[];
  static const char kAlertSeverity[];
  static const char kModuleType[];
  static const char kModuleState[];
  static const char kChronoMinutes[];
  static const char kChronoMilliseconds[];

 private:
  Enum value_;

  static const char kTypeName[];

  static const char kNone[];
};

template<typename VALUE>
ValueType::Enum ValueType::GetEnum() {
  if (std::is_same<VALUE, Types::Value<int32_t>>::value) { return ValueType::Enum::VALUE_INT32; }
  if (std::is_same<VALUE, Types::Value<int16_t>>::value) { return ValueType::Enum::VALUE_INT16; }
  if (std::is_same<VALUE, Types::Value<int8_t>>::value) { return ValueType::Enum::VALUE_INT8; }
  if (std::is_same<VALUE, Types::Value<uint32_t>>::value) { return ValueType::Enum::VALUE_UINT32; }
  if (std::is_same<VALUE, Types::Value<uint16_t>>::value) { return ValueType::Enum::VALUE_UINT16; }
  if (std::is_same<VALUE, Types::Value<uint8_t>>::value) { return ValueType::Enum::VALUE_UINT8; }
  if (std::is_same<VALUE, Types::Value<double>>::value) { return ValueType::Enum::VALUE_DOUBLE; }
  if (std::is_same<VALUE, Types::Value<float>>::value) { return ValueType::Enum::VALUE_FLOAT; }
  if (std::is_same<VALUE, int32_t>::value) { return ValueType::Enum::INT32; }
  if (std::is_same<VALUE, int16_t>::value) { return ValueType::Enum::INT16; }
  if (std::is_same<VALUE, int8_t>::value) { return ValueType::Enum::INT8; }
  if (std::is_same<VALUE, uint32_t>::value) { return ValueType::Enum::UINT32; }
  if (std::is_same<VALUE, uint16_t>::value) { return ValueType::Enum::UINT16; }
  if (std::is_same<VALUE, uint8_t>::value) { return ValueType::Enum::UINT8; }
  if (std::is_same<VALUE, double>::value) { return ValueType::Enum::DOUBLE; }
  if (std::is_same<VALUE, float>::value) { return ValueType::Enum::FLOAT; }
  if (std::is_same<VALUE, bool>::value) { return ValueType::Enum::BOOL; }
  if (std::is_same<VALUE, std::string>::value) { return ValueType::Enum::STRING; }
  if (std::is_same<VALUE, Types::AlertSeverity>::value) { return ValueType::Enum::ALERT_SEVERITY; }
  if (std::is_same<VALUE, Types::ModuleType>::value) { return ValueType::Enum::MODULE_TYPE; }
  if (std::is_same<VALUE, Types::ModuleState>::value) { return ValueType::Enum::MODULE_STATE; }
  if (std::is_same<VALUE, std::chrono::minutes>::value) { return ValueType::Enum::CHRONO_MINUTES; }
  if (std::is_same<VALUE, std::chrono::milliseconds>::value) { return ValueType::Enum::CHRONO_MILLISECONDS; }
  return ValueType::Enum::NONE;
}
}  // namespace Types
