/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

namespace Types {
class ValueConverter {
 public:
  template<typename T, typename U>
  static U Convert(const T &value);
};

template<typename T, typename U>
U ValueConverter::Convert(const T &value) {
  (void) value;
  auto retVal = U();
//  retVal = value;
  return retVal;
}

}  // namespace Types
