/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <sstream>
#include "SoftwareModule.h"

namespace Types {

const char SoftwareModule::kMessageHandler[] = "modules";
const char SoftwareModule::kDirectAccess[] = "DirectAccess";
const char SoftwareModule::kLockHandler[] = "arbitration";
const char SoftwareModule::kClientHandler[] = "client-handler";
const char SoftwareModule::kClientEstablisher[] = "client-establisher";
const char SoftwareModule::kBalancer[] = "balancer";
const char SoftwareModule::kNone[] = "None";

SoftwareModule::SoftwareModule()
  : enum_(Enum::NONE) {
}

SoftwareModule::SoftwareModule(const SoftwareModule &other) {
  enum_ = other.enum_;
}

SoftwareModule::SoftwareModule(const Types::SoftwareModule::Enum &e)
  : enum_(e) {
}

std::string SoftwareModule::ToString() const {
  switch (enum_) {
    case Enum::DIRECT_ACCESS: {
      return  kDirectAccess;
    }
    case Enum::MESSAGE_HANDLER: {
      return  kMessageHandler;
    }
    case Enum::LOCK_HANDLER: {
      return  kLockHandler;
    }
    case Enum::CLIENT_HANDLER: {
      return  kClientHandler;
    }
    case Enum::CLIENT_ESTABLISHER: {
      return  kClientEstablisher;
    }
    case Enum::BALANCER: {
      return  kBalancer;
    }
    case Enum::NONE: {
      return  kNone;
    }
  }
  return kNone;
}

void SoftwareModule::FromString(const std::string &str) {
  if (str == kDirectAccess) {
    enum_ = Enum::DIRECT_ACCESS;
  } else if (str == kMessageHandler) {
    enum_ = Enum::MESSAGE_HANDLER;
  } else if (str == kLockHandler) {
    enum_ = Enum::LOCK_HANDLER;
  } else if (str == kClientHandler) {
    enum_ = Enum::CLIENT_HANDLER;
  } else if (str == kClientEstablisher) {
    enum_ = Enum::CLIENT_ESTABLISHER;
  } else if (str == kBalancer) {
    enum_ = Enum::BALANCER;
  } else {
    enum_ = Enum::NONE;
  }
}

SoftwareModule::Enum SoftwareModule::GetEnum() const {
  return enum_;
}

void SoftwareModule::SetEnum(const Enum& e) {
  enum_ = e;
}

SoftwareModule &SoftwareModule::operator=(const SoftwareModule &other) {
  if (this != &other) {
    this->enum_ = other.enum_;
  }
  return *this;
}

bool SoftwareModule::operator==(const SoftwareModule& other) const {
  return enum_ == other.enum_;
}

}  // namespace Types

size_t std::hash<Types::SoftwareModule>::operator()(const Types::SoftwareModule & module) const {
  return std::hash<Types::SoftwareModule::Enum>()(module.enum_);
}
