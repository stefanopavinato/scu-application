/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "WriteStrategy.h"

namespace Types {

const char WriteStrategy::kOnUpdate[] = "OnUpdate";
const char WriteStrategy::kOnChange[] = "OnChange";
const char WriteStrategy::kNone[] = "None";

WriteStrategy::WriteStrategy()
  : value_(Enum::NONE) {
}

WriteStrategy::WriteStrategy(const Enum &value)
  : value_(value) {
}

void WriteStrategy::SetEnum(const Enum &value) {
  value_ = value;
}

WriteStrategy::Enum WriteStrategy::GetEnum() const {
  return value_;
}

std::string WriteStrategy::ToString() const {
  switch (value_) {
    case Enum::ON_UPDATE: {
      return kOnUpdate;
    }
    case Enum::ON_CHANGE: {
      return kOnChange;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

bool WriteStrategy::operator==(const WriteStrategy &other) const {
  return value_ == other.value_;
}

bool WriteStrategy::operator!=(const WriteStrategy &other) const {
  return value_ != other.value_;
}

}  // namespace Types
