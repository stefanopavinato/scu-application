/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class Unit {
 public:
  enum class Enum {
    VOLTAGE,
    AMPERE,
    DEGREE_CELSIUS,
    PERCENT,
    ROTATIONAL_SPEED,
    NONE
  };

  Unit();

  explicit Unit(const Enum &value);

  Unit(const Unit &other);


  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  Unit &operator=(const Unit &other);

  bool operator==(const Unit &other) const;

  friend std::ostream &operator<<(std::ostream &out, const Unit &unit) {
    return out << unit.ToString();
  }

 private:
  Enum enum_;

  static const char kTypeName[];

  static const char kVolt[];
  static const char kAmpere[];
  static const char kDegreeCelsius[];
  static const char kPercent[];
  static const char kRotationalSpeed[];
  static const char kNone[];
};

}  // namespace Types
