/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Types {
/*
template <class T>
Value<T>::Value()
  : item_({})
  , unit_(Types::Unit::Enum::NONE)
  , prefix_({}) {
}
*/

template<class T>
Value<T>::Value(
  const T &value)
  : value_(value) {
}

template<class T>
Value<T>::Value(
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix)
  : unit_(unit), prefix_(prefix) {
}

template<class T>
Value<T>::Value(
  const T &value,
  const Types::Unit::Enum &unit,
  const Types::UnitPrefix::Enum &prefix)
  : value_(value), unit_(unit), prefix_(prefix) {
}

template<class T>
Value<T>::Value(
  const Value <T> &other)
  : value_(other.GetValue()), unit_(other.GetUnit()), prefix_(other.GetPrefix()) {
}

template<class T>
T Value<T>::GetValue() const {
  return value_;
}

template<class T>
T Value<T>::GetValue(const Types::UnitPrefix::Enum &prefix) {
  return GetValue(Types::UnitPrefix(prefix));
}

template<class T>
T Value<T>::GetValue(const Types::UnitPrefix &prefix) {
  T tempValue = value_;
  auto prefixDiff = prefix_ - prefix;
  if (prefixDiff > 0) {
    for (int32_t i = 0; i < std::abs(prefixDiff); ++i) {
      tempValue = static_cast<T>(tempValue * 10);
    }
  } else if (prefixDiff < 0) {
    for (int32_t i = 0; i < std::abs(prefixDiff); ++i) {
      tempValue = static_cast<T>(tempValue / 10);
    }
  }
  return tempValue;
}

template<class T>
T *Value<T>::ASDF() {
  return &value_;
}

template<class T>
bool Value<T>::SetValue(const T &value) {
  value_ = value;
  return true;
}

template<class T>
bool Value<T>::SetValue(const T &value, const Types::UnitPrefix &prefix) {
  (void) value;
  (void) prefix;
  return false;
}

template<class T>
Types::Unit Value<T>::GetUnit() const {
  return unit_;
}

template<class T>
Types::UnitPrefix Value<T>::GetPrefix() const {
  return prefix_;
}

template<class T>
std::string Value<T>::ToString() const {
  return std::to_string(value_).append(" ").append(prefix_.ToString()).append(unit_.ToString());
}

template<class T>
Value <T> &Value<T>::operator=(const Value <T> &other) {
  if (this != &other) {
    value_ = other.GetValue();
    prefix_ = other.GetPrefix();
    unit_ = other.GetUnit();
  }
  return *this;
}

template<class T>
bool Value<T>::operator==(const Value <T> &other) const {
  return (value_ == other.GetValue() && prefix_ == other.GetPrefix());
}

template<class T>
bool Value<T>::operator!=(const Value <T> &other) const {
  return (value_ != other.GetValue() && prefix_ == other.GetPrefix());
}

template<class T>
bool Value<T>::operator>(const Value <T> &other) const {
  return (value_ > other.GetValue());  // && prefix_ == other.GetPrefix());
}

template<class T>
bool Value<T>::operator<(const Value <T> &other) const {
  return (value_ < other.GetValue());  // && prefix_ == other.GetPrefix());
}

template<class T>
bool Value<T>::operator>=(const Value <T> &other) const {
  return (value_ >= other.GetValue());  // && prefix_ == other.GetPrefix());
}

template<class T>
bool Value<T>::operator<=(const Value <T> &other) const {
  return (value_ <= other.GetValue());  // && prefix_ == other.GetPrefix());
}

template<class T>
Value<T> Value<T>::operator*(const Value &other) const {
  Value<T> temp;
  temp.value_ = value_ * other.value_;
  return temp;
}

template<class T>
Value<T> Value<T>::operator/(const Value &other) const {
  Value<T> temp;
  temp.value_ = value_ / other.value_;
  return temp;
}

template<class T>
Value<T> Value<T>::operator+(const Value &other) const {
  Value<T> temp;
  temp.value_ = value_ + other.value_;
  return temp;
}

}  // namespace Types
