/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class ComponentStatusId {
 public:
  enum class Enum {
    OK,
    ExternalFailure,
    ActionNotSet,
    ActionFailure,
    AccessorNotSet,
    AccessorNotInitialized,
    ItemReadFailure,
    ItemWriteFailure,
    ReadBackCompareFailure,

    NoObject,
    InitializationFailure,
    Initialized,
    InitializeConcreteFailure,
    DeInitializeConcreteFailure,

    DeInitializationFailure,
    DeInitialized,
    ParentNotSet,
    MaximumChildStatus,
    PathNotFound,
    Reset,
    Unknown,
    NONE
  };

  ComponentStatusId();

  explicit ComponentStatusId(const Enum &value);

  [[nodiscard]] Enum GetEnum() const;

  void SetValue(const ComponentStatusId::Enum &value);

  bool operator==(const ComponentStatusId &other) const;

  bool operator!=(const ComponentStatusId &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum enum_;

  static const char kOK[];
  static const char kExternalFailure[];
  static const char kActionNotSet[];
  static const char kActionFailure[];
  static const char kAccessorNotSet[];
  static const char kItemReadFailure[];
  static const char kItemWriteFailure[];
  static const char kReadBackCompareFailure[];
  static const char kNoObject[];
  static const char kInitializationFailure[];
  static const char kDeInitializationFailure[];
  static const char kAccessorNotInitialized[];
  static const char kInitialized[];
  static const char kDeInitialized[];
  static const char kReset[];
  static const char kUnknown[];
  static const char kParentNotSet[];
  static const char kPathNotFound[];
  static const char kMaximumChildStatus[];

  static const char kInitializeConcreteFailure[];
  static const char kDeInitializeConcreteFailure[];
  static const char kNone[];
};


}  // namespace Types
