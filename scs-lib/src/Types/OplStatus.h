/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class OplStatus {
 public:
  enum class Enum {
    OK = 1,
    BI = 2,
    RBI = 3,
    EBI = 4,
    NONE
  };

  OplStatus();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  bool operator==(const OplStatus &other) const;

  bool operator!=(const OplStatus &other) const;

 private:
  Enum value_;

  static const char kOK[];
  static const char kBI[];
  static const char kRBI[];
  static const char kEBI[];
  static const char kNone[];
};
}  // namespace Types
