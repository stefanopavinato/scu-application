/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LockId.h"

namespace Types {

const char LockId::kExclusive[] = "Exclusive";
const char LockId::kShared[] = "Shared";
const char LockId::kModuleHandler[] = "ModuleHandler";
const char LockId::kNone[] = "None";

LockId::LockId()
  : id_(Enum::NONE) {
}

LockId::LockId(const Enum &id)
  : id_(id) {
}


LockId::Enum LockId::GetValue() const {
  return id_;
}

LockId::Enum LockId::ToEnum(const uint32_t &value) {
  return static_cast<LockId::Enum>(value);
}

std::string LockId::ToString() const {
  switch (id_) {
    case Enum::SHARED: {
      return kShared;
    }
    case Enum::EXCLUSIVE: {
      return kExclusive;
    }
    case Enum::MODULE_HANDLER: {
      return kModuleHandler;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

LockId::Enum LockId::FromString(const std::string &str) {
  if (str == kShared) {
    return Enum::SHARED;
  } else if (str == kExclusive) {
    return Enum::EXCLUSIVE;
  } else if (str == kModuleHandler) {
    return Enum::MODULE_HANDLER;
  } else {
    return Enum::NONE;
  }
}

bool LockId::operator==(const LockId &other) const {
  return id_ == other.id_;
}

bool LockId::operator!=(const LockId &other) const {
  return id_ != other.id_;
}

}  // namespace Types

size_t std::hash<Types::LockId>::operator()(const Types::LockId & id) const {
  return std::hash<Types::LockId::Enum>()(id.id_);
}
