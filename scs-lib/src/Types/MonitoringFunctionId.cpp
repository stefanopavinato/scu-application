/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "MonitoringFunctionId.h"

namespace Types {

const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_fan_speed_A[] =
  "SCU_MON_W_FUNCTION_fan_speed_A";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_fan_speed_B[] =
  "SCU_MON_W_FUNCTION_fan_speed_B";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_fan_speed_M[] =
  "SCU_MON_W_FUNCTION_fan_speed_M";

const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_internal_main_voltage[] =
  "SCU_MON_E_FUNCTION_internal_main_voltage";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_internal_main_voltage[] =
  "SCU_MON_W_FUNCTION_internal_main_voltage";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_main_current[] =
  "SCU_MON_E_FUNCTION_main_current";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_main_current[] =
  "SCU_MON_W_FUNCTION_main_current";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_internal_standby_voltage[] =
  "SCU_MON_E_FUNCTION_internal_standby_voltage";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_internal_standby_voltage[] =
  "SCU_MON_W_FUNCTION_internal_standby_voltage";

const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_temp_upper_PCB_Edge[] =
  "SCU_MON_E_FUNCTION_temp_upper_PCB_Edge";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_temp_upper_PCB_Edge[] =
  "SCU_MON_W_FUNCTION_temp_upper_PCB_Edge";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_temp_lower_PCB_Edge[] =
  "SCU_MON_E_FUNCTION_temp_lower_PCB_Edge";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_temp_lower_PCB_Edge[] =
  "SCU_MON_W_FUNCTION_temp_lower_PCB_Edge";

const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_mc_voltage_3_3[] =
  "SCU_MON_E_FUNCTION_mc_voltage_3_3";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_mc_voltage_3_3[] =
  "SCU_MON_W_FUNCTION_mc_voltage_3_3";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_mc_voltage_2_5[] =
  "SCU_MON_E_FUNCTION_mc_voltage_2_5";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_mc_voltage_2_5[] =
  "SCU_MON_W_FUNCTION_mc_voltage_2_5";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_mc_voltage_1_8[] =
  "SCU_MON_E_FUNCTION_mc_voltage_1_8";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_mc_voltage_1_8[] =
  "SCU_MON_W_FUNCTION_mc_voltage_1_8";
const char MonitoringFunctionId::kSCU_MON_E_FUNCTION_mc_voltage_1_2[] =
  "SCU_MON_E_FUNCTION_mc_voltage_1_2";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_mc_voltage_1_2[] =
  "SCU_MON_W_FUNCTION_mc_voltage_1_2";

const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_current_BP[] =
  "SCU_MON_W_FUNCTION_clmc_current_BP";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_current_RDY[] =
  "SCU_MON_W_FUNCTION_clmc_current_RDY";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_voltage_BP_RDY[] =
  "SCU_MON_W_FUNCTION_clmc_voltage_BP_RDY";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_current_redBP[] =
  "SCU_MON_W_FUNCTION_clmc_current_redBP";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_current_redRDY[] =
  "SCU_MON_W_FUNCTION_clmc_current_redRDY";
const char MonitoringFunctionId::kSCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY[] =
  "SCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY";

const char MonitoringFunctionId::kOK[] = "OK";

const char MonitoringFunctionId::kNone[] = "None";

MonitoringFunctionId::MonitoringFunctionId()
  : enum_(Enum::NONE) {
}

MonitoringFunctionId::MonitoringFunctionId(const Enum &value)
  : enum_(value) {
}

MonitoringFunctionId::Enum MonitoringFunctionId::GetEnum() const {
  return enum_;
}

void MonitoringFunctionId::SetValue(const Types::MonitoringFunctionId::Enum &value) {
  enum_ = value;
}

bool MonitoringFunctionId::operator==(const MonitoringFunctionId &other) const {
  return enum_ == other.GetEnum();
}

bool MonitoringFunctionId::operator!=(const MonitoringFunctionId &other) const {
  return enum_ != other.GetEnum();
}


std::string MonitoringFunctionId::ToString() const {
  switch (enum_) {
    case Enum::SCU_MON_W_FUNCTION_fan_speed_A: {
      return kSCU_MON_W_FUNCTION_fan_speed_A;
    }
    case Enum::SCU_MON_W_FUNCTION_fan_speed_B: {
      return kSCU_MON_W_FUNCTION_fan_speed_B;
    }
    case Enum::SCU_MON_W_FUNCTION_fan_speed_M: {
      return kSCU_MON_W_FUNCTION_fan_speed_M;
    }
    case Enum::SCU_MON_E_FUNCTION_internal_main_voltage: {
      return kSCU_MON_E_FUNCTION_internal_main_voltage;
    }
    case Enum::SCU_MON_W_FUNCTION_internal_main_voltage: {
      return kSCU_MON_W_FUNCTION_internal_main_voltage;
    }
    case Enum::SCU_MON_E_FUNCTION_main_current: {
      return kSCU_MON_E_FUNCTION_main_current;
    }
    case Enum::SCU_MON_W_FUNCTION_main_current: {
      return kSCU_MON_W_FUNCTION_main_current;
    }
    case Enum::SCU_MON_E_FUNCTION_internal_standby_voltage: {
      return kSCU_MON_E_FUNCTION_internal_standby_voltage;
    }
    case Enum::SCU_MON_W_FUNCTION_internal_standby_voltage: {
      return kSCU_MON_W_FUNCTION_internal_standby_voltage;
    }
    case Enum::SCU_MON_E_FUNCTION_temp_upper_PCB_Edge: {
      return kSCU_MON_E_FUNCTION_temp_upper_PCB_Edge;
    }
    case Enum::SCU_MON_W_FUNCTION_temp_upper_PCB_Edge: {
      return kSCU_MON_W_FUNCTION_temp_upper_PCB_Edge;
    }
    case Enum::SCU_MON_E_FUNCTION_temp_lower_PCB_Edge: {
      return kSCU_MON_E_FUNCTION_temp_lower_PCB_Edge;
    }
    case Enum::SCU_MON_W_FUNCTION_temp_lower_PCB_Edge: {
      return kSCU_MON_W_FUNCTION_temp_lower_PCB_Edge;
    }
    case Enum::SCU_MON_E_FUNCTION_mc_voltage_3_3: {
      return kSCU_MON_E_FUNCTION_mc_voltage_3_3;
    }
    case Enum::SCU_MON_W_FUNCTION_mc_voltage_3_3: {
      return kSCU_MON_W_FUNCTION_mc_voltage_3_3;
    }
    case Enum::SCU_MON_E_FUNCTION_mc_voltage_2_5: {
      return kSCU_MON_E_FUNCTION_mc_voltage_2_5;
    }
    case Enum::SCU_MON_W_FUNCTION_mc_voltage_2_5: {
      return kSCU_MON_W_FUNCTION_mc_voltage_2_5;
    }
    case Enum::SCU_MON_E_FUNCTION_mc_voltage_1_8: {
      return kSCU_MON_E_FUNCTION_mc_voltage_1_8;
    }
    case Enum::SCU_MON_W_FUNCTION_mc_voltage_1_8: {
      return kSCU_MON_W_FUNCTION_mc_voltage_1_8;
    }
    case Enum::SCU_MON_E_FUNCTION_mc_voltage_1_2: {
      return kSCU_MON_E_FUNCTION_mc_voltage_1_2;
    }
    case Enum::SCU_MON_W_FUNCTION_mc_voltage_1_2: {
      return kSCU_MON_W_FUNCTION_mc_voltage_1_2;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_current_BP: {
      return kSCU_MON_W_FUNCTION_clmc_current_BP;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_current_RDY: {
      return kSCU_MON_W_FUNCTION_clmc_current_RDY;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_voltage_BP_RDY: {
      return kSCU_MON_W_FUNCTION_clmc_voltage_BP_RDY;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_current_redBP: {
      return kSCU_MON_W_FUNCTION_clmc_current_redBP;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_current_redRDY: {
      return kSCU_MON_W_FUNCTION_clmc_current_redRDY;
    }
    case Enum::SCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY: {
      return kSCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY;
    }
    case Enum::OK: {
      return kOK;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void MonitoringFunctionId::FromString(const std::string &str) {
  if (str == kSCU_MON_W_FUNCTION_fan_speed_A) {
    enum_ = Enum::SCU_MON_W_FUNCTION_fan_speed_A;
  } else if (str == kSCU_MON_W_FUNCTION_fan_speed_B) {
    enum_ = Enum::SCU_MON_W_FUNCTION_fan_speed_B;
  } else if (str == kSCU_MON_W_FUNCTION_fan_speed_M) {
    enum_ = Enum::SCU_MON_W_FUNCTION_fan_speed_M;
  } else if (str == kSCU_MON_E_FUNCTION_internal_main_voltage) {
    enum_ = Enum::SCU_MON_E_FUNCTION_internal_main_voltage;
  } else if (str == kSCU_MON_W_FUNCTION_internal_main_voltage) {
    enum_ = Enum::SCU_MON_W_FUNCTION_internal_main_voltage;
  } else if (str == kSCU_MON_E_FUNCTION_main_current) {
    enum_ = Enum::SCU_MON_E_FUNCTION_main_current;
  } else if (str == kSCU_MON_W_FUNCTION_main_current) {
    enum_ = Enum::SCU_MON_W_FUNCTION_main_current;
  } else if (str == kSCU_MON_E_FUNCTION_internal_standby_voltage) {
    enum_ = Enum::SCU_MON_E_FUNCTION_internal_standby_voltage;
  } else if (str == kSCU_MON_W_FUNCTION_internal_standby_voltage) {
    enum_ = Enum::SCU_MON_W_FUNCTION_internal_standby_voltage;
  } else if (str == kSCU_MON_E_FUNCTION_temp_upper_PCB_Edge) {
    enum_ = Enum::SCU_MON_E_FUNCTION_temp_upper_PCB_Edge;
  } else if (str == kSCU_MON_W_FUNCTION_temp_upper_PCB_Edge) {
    enum_ = Enum::SCU_MON_W_FUNCTION_temp_upper_PCB_Edge;
  } else if (str == kSCU_MON_E_FUNCTION_temp_lower_PCB_Edge) {
    enum_ = Enum::SCU_MON_E_FUNCTION_temp_lower_PCB_Edge;
  } else if (str == kSCU_MON_W_FUNCTION_temp_lower_PCB_Edge) {
    enum_ = Enum::SCU_MON_W_FUNCTION_temp_lower_PCB_Edge;
  } else if (str == kSCU_MON_E_FUNCTION_mc_voltage_3_3) {
    enum_ = Enum::SCU_MON_E_FUNCTION_mc_voltage_3_3;
  } else if (str == kSCU_MON_W_FUNCTION_mc_voltage_3_3) {
    enum_ = Enum::SCU_MON_W_FUNCTION_mc_voltage_3_3;
  } else if (str == kSCU_MON_E_FUNCTION_mc_voltage_2_5) {
    enum_ = Enum::SCU_MON_E_FUNCTION_mc_voltage_2_5;
  } else if (str == kSCU_MON_W_FUNCTION_mc_voltage_2_5) {
    enum_ = Enum::SCU_MON_W_FUNCTION_mc_voltage_2_5;
  } else if (str == kSCU_MON_E_FUNCTION_mc_voltage_1_8) {
    enum_ = Enum::SCU_MON_E_FUNCTION_mc_voltage_1_8;
  } else if (str == kSCU_MON_W_FUNCTION_mc_voltage_1_8) {
    enum_ = Enum::SCU_MON_W_FUNCTION_mc_voltage_1_8;
  } else if (str == kSCU_MON_E_FUNCTION_mc_voltage_1_2) {
    enum_ = Enum::SCU_MON_E_FUNCTION_mc_voltage_1_2;
  } else if (str == kSCU_MON_W_FUNCTION_mc_voltage_1_2) {
    enum_ = Enum::SCU_MON_W_FUNCTION_mc_voltage_1_2;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_current_BP) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_current_BP;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_current_RDY) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_current_RDY;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_voltage_BP_RDY) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_voltage_BP_RDY;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_current_redBP) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_current_redBP;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_current_redRDY) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_current_redRDY;
  } else if (str == kSCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY) {
    enum_ = Enum::SCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY;
  } else if (str == kOK) {
    enum_ = Enum::OK;
  } else {
    enum_ = Enum::NONE;
  }
}

}  // namespace Types
