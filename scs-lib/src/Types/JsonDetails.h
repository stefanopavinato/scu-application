/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class JsonDetails {
 public:
  enum class Enum : uint8_t {
    VERBOSE = 0,
    COMPACT = 1,
    MINIMAL = 2,
    CONFIG = 3
  };

  JsonDetails();

  JsonDetails(const JsonDetails &other);

  explicit JsonDetails(const Enum &value);

  explicit JsonDetails(const uint32_t &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  void FromValue(const uint32_t & val);

  static const char kVerboseName[];
  static const char kCompactName[];
  static const char kMinimalName[];
  static const char kConfigName[];

 private:
  Enum value_;

  static const char kTypeName[];
};
}  // namespace Types
