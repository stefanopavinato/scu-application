/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ValueType.h"

namespace Types {

const char ValueType::kTypeName[] = "GetValueType";

const char ValueType::kValueInt32[] = "VALUE_INT32";
const char ValueType::kValueInt16[] = "VALUE_INT16";
const char ValueType::kValueInt8[] = "VALUE_INT8";
const char ValueType::kValueUInt32[] = "VALUE_UINT32";
const char ValueType::kValueUInt16[] = "UVALUE_INT16";
const char ValueType::kValueUInt8[] = "VALUE_UINT8";
const char ValueType::kValueDouble[] = "VALUE_DOUBLE";
const char ValueType::kValueFloat[] = "VALUE_FLOAT";
const char ValueType::kInt32[] = "INT32";
const char ValueType::kInt16[] = "INT16";
const char ValueType::kInt8[] = "INT8";
const char ValueType::kUInt32[] = "UINT32";
const char ValueType::kUInt16[] = "UINT16";
const char ValueType::kUInt8[] = "UINT8";
const char ValueType::kDouble[] = "DOUBLE";
const char ValueType::kFloat[] = "FLOAT";
const char ValueType::kBool[] = "BOOL";
const char ValueType::kString[] = "STRING";
const char ValueType::kAlertSeverity[] = "ALERT_SEVERITY";
const char ValueType::kModuleType[] = "MODULE_TYPE";
const char ValueType::kModuleState[] = "MODULE_STATE";
const char ValueType::kChronoMinutes[] = "CHRONO_MINUTES";
const char ValueType::kChronoMilliseconds[] = "CHRONO_MILLISECONDS";
const char ValueType::kNone[] = "NONE";

ValueType::ValueType()
  : value_(Enum::NONE) {
}

ValueType::ValueType(const Enum &value)
  : value_(value) {
}

ValueType::ValueType(const std::string &name) {
  FromString(name);
}

std::string ValueType::GetTypeName() {
  return kTypeName;
}

ValueType::Enum ValueType::GetValue() const {
  return value_;
}

void ValueType::SetValue(const ValueType::Enum &value) {
  value_ = value;
}

bool ValueType::operator==(const ValueType &other) const {
  return value_ == other.GetValue();
}

std::string ValueType::ToString() const {
  switch (value_) {
    case Enum::VALUE_INT32: {
      return kValueInt32;
    }
    case Enum::VALUE_INT16: {
      return kValueInt16;
    }
    case Enum::VALUE_INT8: {
      return kValueInt8;
    }
    case Enum::VALUE_UINT32: {
      return kValueUInt32;
    }
    case Enum::VALUE_UINT16: {
      return kValueUInt16;
    }
    case Enum::VALUE_UINT8: {
      return kValueUInt8;
    }
    case Enum::VALUE_DOUBLE: {
      return kValueDouble;
    }
    case Enum::VALUE_FLOAT: {
      return kValueFloat;
    }
    case Enum::INT32: {
      return kInt32;
    }
    case Enum::INT16: {
      return kInt16;
    }
    case Enum::INT8: {
      return kInt8;
    }
    case Enum::UINT32: {
      return kUInt32;
    }
    case Enum::UINT16: {
      return kUInt16;
    }
    case Enum::UINT8: {
      return kUInt8;
    }
    case Enum::DOUBLE: {
      return kDouble;
    }
    case Enum::FLOAT: {
      return kFloat;
    }
    case Enum::BOOL: {
      return kBool;
    }
    case Enum::STRING: {
      return kString;
    }
    case Enum::ALERT_SEVERITY: {
      return kAlertSeverity;
    }
    case Enum::MODULE_TYPE: {
      return kModuleType;
    }
    case Enum::CHRONO_MINUTES: {
      return kChronoMinutes;
    }
    case Enum::CHRONO_MILLISECONDS: {
      return kChronoMilliseconds;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void ValueType::FromString(const std::string &str) {
  if (str == kValueInt32) {
    value_ = Enum::VALUE_INT32;
  } else if (str == kValueInt16) {
    value_ = Enum::VALUE_INT16;
  } else if (str == kValueInt8) {
    value_ = Enum::VALUE_INT8;
  } else if (str == kValueUInt32) {
    value_ = Enum::VALUE_UINT32;
  } else if (str == kValueUInt16) {
    value_ = Enum::VALUE_UINT16;
  } else if (str == kValueUInt8) {
    value_ = Enum::VALUE_UINT8;
  } else if (str == kValueDouble) {
    value_ = Enum::VALUE_DOUBLE;
  } else if (str == kValueFloat) {
    value_ = Enum::VALUE_FLOAT;
  } else if (str == kInt32) {
    value_ = Enum::INT32;
  } else if (str == kInt16) {
    value_ = Enum::INT16;
  } else if (str == kInt8) {
    value_ = Enum::INT8;
  } else if (str == kUInt32) {
    value_ = Enum::UINT32;
  } else if (str == kUInt16) {
    value_ = Enum::UINT16;
  } else if (str == kUInt8) {
    value_ = Enum::UINT8;
  } else if (str == kDouble) {
    value_ = Enum::DOUBLE;
  } else if (str == kFloat) {
    value_ = Enum::FLOAT;
  } else if (str == kBool) {
    value_ = Enum::BOOL;
  } else if (str == kString) {
    value_ = Enum::STRING;
  } else if (str == kAlertSeverity) {
    value_ = Enum::ALERT_SEVERITY;
  } else if (str == kModuleType) {
    value_ = Enum::MODULE_TYPE;
  } else if (str == kChronoMinutes) {
    value_ = Enum::CHRONO_MINUTES;
  } else if (str == kChronoMilliseconds) {
    value_ = Enum::CHRONO_MILLISECONDS;
  } else {
    value_ = Enum::NONE;
  }
}

}  // namespace Types
