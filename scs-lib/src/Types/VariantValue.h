/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>
#include <variant>
#include <string>
#include <cstdint>
#include <vector>
#include <unordered_map>
#include <optional>
#include "Types/Value.h"
#include "Types/ValueType.h"
#include "Types/ModuleType.h"
#include "Types/ModuleState.h"
#include "Types/AlertSeverity.h"
#include "Types/ScuStatus.h"
#include "Types/OplStatus.h"

namespace Types {
using VariantValue = std::variant<
  Types::Value<int32_t>,
  Types::Value<int16_t>,
  Types::Value<int8_t>,
  Types::Value<uint32_t>,
  Types::Value<uint16_t>,
  Types::Value<uint8_t>,
  Types::Value<double>,
  Types::Value<float>,
  int32_t,
  int16_t,
  int8_t,
  uint32_t,
  uint16_t,
  uint8_t,
  double,
  float,
  bool,
  std::string,
  Types::AlertSeverity,
  Types::ModuleType,
  Types::ModuleState,
  std::chrono::minutes,
  std::chrono::milliseconds,
  Types::ScuStatus,
  Types::OplStatus,
  std::vector<uint32_t>>;

class VariantValueUtils {
 public:
  [[nodiscard]] static Types::ValueType GetType(const VariantValue & value);

  [[nodiscard]] static std::optional<VariantValue> FromString(
    const Types::ValueType &type,
    const std::string &str);

  [[nodiscard]] static std::string ToString(const VariantValue & value);

  template<typename T>
  [[nodiscard]] static std::optional<T> GetValue(const VariantValue & value);
};

template<typename T>
std::optional<T> VariantValueUtils::GetValue(const VariantValue & value) {
  if (!std::holds_alternative<T>(value)) {
    return std::nullopt;
  }
  return std::get<T>(value);
}

}  // namespace Types
