/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ComponentStatusId.h"

namespace Types {

const char ComponentStatusId::kOK[] = "OK";
const char ComponentStatusId::kExternalFailure[] = "ExternalFailure";
const char ComponentStatusId::kActionNotSet[] = "ActionNotSet";
const char ComponentStatusId::kActionFailure[] = "ActionFailure";
const char ComponentStatusId::kAccessorNotSet[] = "AccessorNotSet";
const char ComponentStatusId::kItemReadFailure[] = "ItemReadFailure";
const char ComponentStatusId::kItemWriteFailure[] = "ItemWriteFailure";
const char ComponentStatusId::kReadBackCompareFailure[] = "ReadBackCompareFailure";
const char ComponentStatusId::kNoObject[] = "NoObject";
const char ComponentStatusId::kInitializationFailure[] = "InitializationFailure";
const char ComponentStatusId::kDeInitializationFailure[] = "DeInitializationFailure";

const char ComponentStatusId::kAccessorNotInitialized[] = "AccessorNotInitialized";
const char ComponentStatusId::kInitialized[] = "Initialized";
const char ComponentStatusId::kDeInitialized[] = "DeInitialized";
const char ComponentStatusId::kReset[] = "Reset";
const char ComponentStatusId::kUnknown[] = "Unknown";
const char ComponentStatusId::kParentNotSet[] = "ParentNotSet";
const char ComponentStatusId::kPathNotFound[] = "PathNotFound";

const char ComponentStatusId::kMaximumChildStatus[] = "MaximumChildStatus";


const char ComponentStatusId::kInitializeConcreteFailure[] = "InitializeConcreteFailure";
const char ComponentStatusId::kDeInitializeConcreteFailure[] = "DeInitializeConcreteFailure";

const char ComponentStatusId::kNone[] = "None";

ComponentStatusId::ComponentStatusId()
  : enum_(Enum::NONE) {
}

ComponentStatusId::ComponentStatusId(const Enum &value)
  : enum_(value) {
}

ComponentStatusId::Enum ComponentStatusId::GetEnum() const {
  return enum_;
}

void ComponentStatusId::SetValue(const Types::ComponentStatusId::Enum &value) {
  enum_ = value;
}

bool ComponentStatusId::operator==(const ComponentStatusId &other) const {
  return enum_ == other.GetEnum();
}

bool ComponentStatusId::operator!=(const ComponentStatusId &other) const {
  return enum_ != other.GetEnum();
}


std::string ComponentStatusId::ToString() const {
  switch (enum_) {
    case Enum::OK: {
      return kOK;
    }
    case Enum::ExternalFailure: {
      return kExternalFailure;
    }
    case Enum::ActionNotSet: {
      return kActionNotSet;
    }
    case Enum::ActionFailure: {
      return kActionFailure;
    }
    case Enum::AccessorNotSet: {
      return kAccessorNotSet;
    }
    case Enum::ItemReadFailure: {
      return kItemReadFailure;
    }
    case Enum::ItemWriteFailure: {
      return kItemWriteFailure;
    }
    case Enum::ReadBackCompareFailure: {
      return kReadBackCompareFailure;
    }
    case Enum::NoObject: {
      return kNoObject;
    }
    case Enum::InitializationFailure: {
      return kInitializationFailure;
    }
    case Enum::DeInitializationFailure: {
      return kDeInitializationFailure;
    }
    case Enum::MaximumChildStatus: {
      return kMaximumChildStatus;
    }
    case Enum::AccessorNotInitialized: {
      return kAccessorNotInitialized;
    }
    case Enum::Initialized: {
      return kInitialized;
    }
    case Enum::DeInitialized: {
      return kDeInitialized;
    }
    case Enum::Reset: {
      return kReset;
    }
    case Enum::Unknown: {
      return kUnknown;
    }
    case Enum::ParentNotSet: {
      return kParentNotSet;
    }
    case Enum::InitializeConcreteFailure: {
      return kInitializeConcreteFailure;
    }
    case Enum::DeInitializeConcreteFailure: {
      return kDeInitializeConcreteFailure;
    }
    case Enum::PathNotFound: {
      return kPathNotFound;
    }
    case Enum::NONE: {
      return kNone;
    }
  }
  return kNone;
}

void ComponentStatusId::FromString(const std::string &str) {
  if (str == kOK) {
    enum_ = Enum::OK;
  } else if (str == kExternalFailure) {
    enum_ = Enum::ExternalFailure;
  } else if (str == kActionNotSet) {
    enum_ = Enum::AccessorNotSet;
  } else if (str == kActionFailure) {
    enum_ = Enum::ActionFailure;
  } else if (str == kAccessorNotSet) {
    enum_ = Enum::AccessorNotSet;
  } else if (str == kItemReadFailure) {
    enum_ = Enum::ItemReadFailure;
  } else if (str == kItemWriteFailure) {
    enum_ = Enum::ItemWriteFailure;
  } else if (str == kReadBackCompareFailure) {
    enum_ = Enum::ReadBackCompareFailure;
  } else if (str == kNoObject) {
    enum_ = Enum::NoObject;
  } else if (str == kInitializationFailure) {
    enum_ = Enum::InitializationFailure;
  } else if (str == kDeInitializationFailure) {
    enum_ = Enum::DeInitializationFailure;
  } else if (str == kAccessorNotInitialized) {
    enum_ = Enum::AccessorNotInitialized;
  } else if (str == kInitialized) {
    enum_ = Enum::Initialized;
  } else if (str == kDeInitialized) {
    enum_ = Enum::DeInitialized;
  } else if (str == kReset) {
    enum_ = Enum::Reset;
  } else if (str == kUnknown) {
    enum_ = Enum::Unknown;
  } else if (str == kParentNotSet) {
    enum_ = Enum::ParentNotSet;
  } else if (str == kInitializeConcreteFailure) {
    enum_ = Enum::InitializeConcreteFailure;
  } else if (str == kDeInitializeConcreteFailure) {
    enum_ = Enum::DeInitializeConcreteFailure;
  } else if (str == kPathNotFound) {
    enum_ = Enum::PathNotFound;
  } else if (str == kMaximumChildStatus) {
    enum_ = Enum::MaximumChildStatus;
  } else {
    enum_ = Enum::NONE;
  }
}

}  // namespace Types
