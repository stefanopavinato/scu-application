/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class MonitoringFunctionId {
 public:
  enum class Enum {
    SCU_MON_W_FUNCTION_fan_speed_A,
    SCU_MON_W_FUNCTION_fan_speed_B,
    SCU_MON_W_FUNCTION_fan_speed_M,

    SCU_MON_E_FUNCTION_internal_main_voltage,
    SCU_MON_W_FUNCTION_internal_main_voltage,
    SCU_MON_E_FUNCTION_main_current,
    SCU_MON_W_FUNCTION_main_current,
    SCU_MON_E_FUNCTION_internal_standby_voltage,
    SCU_MON_W_FUNCTION_internal_standby_voltage,

    SCU_MON_E_FUNCTION_temp_upper_PCB_Edge,
    SCU_MON_W_FUNCTION_temp_upper_PCB_Edge,
    SCU_MON_E_FUNCTION_temp_lower_PCB_Edge,
    SCU_MON_W_FUNCTION_temp_lower_PCB_Edge,

    SCU_MON_E_FUNCTION_mc_voltage_3_3,
    SCU_MON_W_FUNCTION_mc_voltage_3_3,
    SCU_MON_E_FUNCTION_mc_voltage_2_5,
    SCU_MON_W_FUNCTION_mc_voltage_2_5,
    SCU_MON_E_FUNCTION_mc_voltage_1_8,
    SCU_MON_W_FUNCTION_mc_voltage_1_8,
    SCU_MON_E_FUNCTION_mc_voltage_1_2,
    SCU_MON_W_FUNCTION_mc_voltage_1_2,
    SCU_MON_W_FUNCTION_clmc_current_BP,
    SCU_MON_W_FUNCTION_clmc_current_RDY,
    SCU_MON_W_FUNCTION_clmc_voltage_BP_RDY,
    SCU_MON_W_FUNCTION_clmc_current_redBP,
    SCU_MON_W_FUNCTION_clmc_current_redRDY,
    SCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY,

    OK,

    NONE
  };

  MonitoringFunctionId();

  explicit MonitoringFunctionId(const Enum &value);

  [[nodiscard]] Enum GetEnum() const;

  void SetValue(const Types::MonitoringFunctionId::Enum &value);

  bool operator==(const MonitoringFunctionId &other) const;

  bool operator!=(const MonitoringFunctionId &other) const;

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

 private:
  Enum enum_;

  static const char kSCU_MON_W_FUNCTION_fan_speed_A[];
  static const char kSCU_MON_W_FUNCTION_fan_speed_B[];
  static const char kSCU_MON_W_FUNCTION_fan_speed_M[];

  static const char kSCU_MON_E_FUNCTION_internal_main_voltage[];
  static const char kSCU_MON_W_FUNCTION_internal_main_voltage[];
  static const char kSCU_MON_E_FUNCTION_main_current[];
  static const char kSCU_MON_W_FUNCTION_main_current[];
  static const char kSCU_MON_E_FUNCTION_internal_standby_voltage[];
  static const char kSCU_MON_W_FUNCTION_internal_standby_voltage[];

  static const char kSCU_MON_E_FUNCTION_temp_upper_PCB_Edge[];
  static const char kSCU_MON_W_FUNCTION_temp_upper_PCB_Edge[];
  static const char kSCU_MON_E_FUNCTION_temp_lower_PCB_Edge[];
  static const char kSCU_MON_W_FUNCTION_temp_lower_PCB_Edge[];

  static const char kSCU_MON_E_FUNCTION_mc_voltage_3_3[];
  static const char kSCU_MON_W_FUNCTION_mc_voltage_3_3[];
  static const char kSCU_MON_E_FUNCTION_mc_voltage_2_5[];
  static const char kSCU_MON_W_FUNCTION_mc_voltage_2_5[];
  static const char kSCU_MON_E_FUNCTION_mc_voltage_1_8[];
  static const char kSCU_MON_W_FUNCTION_mc_voltage_1_8[];
  static const char kSCU_MON_E_FUNCTION_mc_voltage_1_2[];
  static const char kSCU_MON_W_FUNCTION_mc_voltage_1_2[];

  static const char kSCU_MON_W_FUNCTION_clmc_current_BP[];
  static const char kSCU_MON_W_FUNCTION_clmc_current_RDY[];
  static const char kSCU_MON_W_FUNCTION_clmc_voltage_BP_RDY[];
  static const char kSCU_MON_W_FUNCTION_clmc_current_redBP[];
  static const char kSCU_MON_W_FUNCTION_clmc_current_redRDY[];
  static const char kSCU_MON_W_FUNCTION_clmc_voltage_redBP_redRDY[];

  static const char kOK[];

  static const char kNone[];
};

}  // namespace Types
