/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>

namespace Types {
class UnitPrefix {
 public:
  enum class Enum {
    MEGA = 6,
    KILO = 3,
    NONE = 0,
    DECI = -1,
    MILLI = -3,
    MICRO = -6,
    NANO = -9,
    PICO = -12
  };

  UnitPrefix();

  explicit UnitPrefix(const Enum &value);

  [[nodiscard]] static std::string GetTypeName();

  [[nodiscard]] Enum GetEnum() const;

  void SetEnum(const Enum &value);

  [[nodiscard]] std::string ToString() const;

  void FromString(const std::string &str);

  [[nodiscard]] uint32_t GetBase10Exponent(const UnitPrefix &other) const;

  [[nodiscard]] uint32_t GetBase10Exponent(const Enum &value) const;

  bool operator==(const UnitPrefix &other) const;

  int32_t operator-(const UnitPrefix &other) const;

  friend std::ostream &operator<<(std::ostream &out, const UnitPrefix &unitPrefix) {
    return out << unitPrefix.ToString();
  }

 private:
  Enum enum_;

  static const char kTypeName[];

  static const char kMega[];
  static const char kKilo[];
  static const char kNone[];
  static const char kDeci[];
  static const char kMilli[];
  static const char kMicro[];
  static const char kNano[];
  static const char kPico[];
};

}  // namespace Types
