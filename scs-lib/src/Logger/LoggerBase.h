/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ILogger.h"

namespace Logger {
class LoggerBase : public ILogger {
 public:
  LoggerBase();

  explicit LoggerBase(const Types::AlertSeverity::Enum &logLevel);

  ~LoggerBase() override = default;

  void SetLogLevel(const Types::AlertSeverity::Enum &logLevel);

  [[nodiscard]] Types::AlertSeverity LogLevel() const;

 private:
  Types::AlertSeverity logLevel_;
};
}  // namespace Logger
