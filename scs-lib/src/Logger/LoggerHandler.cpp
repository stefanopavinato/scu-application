/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LoggerBase.h"
#include "LoggerHandler.h"

namespace Logger {

LoggerHandler::LoggerHandler()
  : ILogger() {
}

bool LoggerHandler::AddLogger(std::unique_ptr<LoggerBase> logger) {
  loggerList_.push_back(std::move(logger));
  return true;
}

void LoggerHandler::LogFunc(
  const std::string &path,
  const std::string &function,
  const int &line,
  const Types::AlertSeverity::Enum &alertSeverity,
  const std::string &message) const {
  for (auto &logger : loggerList_) {
    // Check log level
    if (logger->LogLevel().GetEnum() > alertSeverity) {
      continue;
    }
    // Log message
    logger->LogFunc(path, function, line, alertSeverity, message);
  }
}

}  // namespace Logger
