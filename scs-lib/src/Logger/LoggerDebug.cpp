/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iomanip>
#include <chrono>
#include <ctime>
#include <iostream>
#include "Utils/TimeFormatter.h"
#include "LoggerDebug.h"

namespace Logger {

LoggerDebug::LoggerDebug(const Types::AlertSeverity::Enum &logLevel)
  : LoggerBase((logLevel)) {
}

void LoggerDebug::LogFunc(const std::string &path,
                          const std::string &function,
                          const int &line,
                          const Types::AlertSeverity::Enum &alertSeverity,
                          const std::string &message) const {
  // Get date and time
  auto now = std::chrono::system_clock::now();
  auto timeString = Utils::TimeFormatter::ToString(now);
  // Get file name
  size_t end = path.rfind('/') + 1;
  std::string fileName = path.substr(end);
  // Print message
  std::cout << timeString <<
            " [" << Types::AlertSeverity(alertSeverity).ToString() << "] " <<
            fileName << ":" << line << " " <<
            function << "(): " <<
            message << std::endl;
}
}  // namespace Logger
