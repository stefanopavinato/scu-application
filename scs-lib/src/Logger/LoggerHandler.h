/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <list>
#include "ILogger.h"

namespace Logger {
class LoggerBase;

class LoggerHandler : public ILogger {
 public:
  LoggerHandler();

  ~LoggerHandler() override = default;

  bool AddLogger(std::unique_ptr<LoggerBase> logger);

  void LogFunc(
    const std::string &path,
    const std::string &function,
    const int &line,
    const Types::AlertSeverity::Enum &alertSeverity,
    const std::string &message) const override;

 private:
  std::list<std::unique_ptr<LoggerBase>> loggerList_;
};
}  // namespace Logger
