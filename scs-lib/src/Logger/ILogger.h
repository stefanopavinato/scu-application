/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <string>
#include "Types/AlertSeverity.h"

namespace Logger {
class ILogger {
 public:
  ILogger() = default;

  virtual ~ILogger() = default;

  virtual void LogFunc(
    const std::string &path,
    const std::string &function,
    const int &line,
    const Types::AlertSeverity::Enum &alertSeverity,
    const std::string &message) const = 0;
};

#define Log(...) LogFunc(__FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

#define Debug(...) Log(::Types::AlertSeverity::Enum::DEBUG, __VA_ARGS__)
#define Info(...) Log(::Types::AlertSeverity::Enum::INFORMATIONAL, __VA_ARGS__)
#define Warning(...) Log(::Types::AlertSeverity::Enum::WARNING, __VA_ARGS__)
#define Error(...) Log(::Types::AlertSeverity::Enum::ERROR, __VA_ARGS__)
}  // namespace Logger
