/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "LoggerBase.h"

namespace Logger {

LoggerBase::LoggerBase()
  : ILogger() {
}

LoggerBase::LoggerBase(const Types::AlertSeverity::Enum &logLevel)
  : logLevel_(logLevel) {
}

void LoggerBase::SetLogLevel(const Types::AlertSeverity::Enum &logLevel) {
  logLevel_.SetEnum(logLevel);
}

Types::AlertSeverity LoggerBase::LogLevel() const {
  return logLevel_;
}

}  // namespace Logger
