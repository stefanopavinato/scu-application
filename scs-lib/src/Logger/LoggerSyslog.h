/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "LoggerBase.h"

namespace Logger {
class LoggerSyslog : public LoggerBase {
 public:
  LoggerSyslog(
    std::string applicationName,
    const Types::AlertSeverity::Enum &logLevel);

  ~LoggerSyslog() override = default;


  void LogFunc(const std::string &path,
               const std::string &function,
               const int &line,
               const Types::AlertSeverity::Enum &alertSeverity,
               const std::string &message) const override;

 private:
  const std::string applicationName_;
};
}  // namespace Logger
