/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "LoggerBase.h"

namespace Logger {
class LoggerDummy : public LoggerBase {
 public:
  LoggerDummy();

  ~LoggerDummy() override = default;

  void LogFunc(const std::string &path,
               const std::string &function,
               const int &line,
               const Types::AlertSeverity::Enum &alertSeverity,
               const std::string &message) const override;
};
}  // namespace Logger
