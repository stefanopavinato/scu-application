/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <algorithm>
#include "Logger/ILogger.h"
#include "Messages/MessageBase.h"
#include "IDistributorSubscriber.h"
#include "Distributor.h"

namespace Distributor {

Distributor::Distributor(Logger::ILogger * logger)
  : logger_(logger) {
  logger_->Info("Created");
}

bool Distributor::OnMessageReceived(Messages::MessageBase * message) {
  for (auto &subscriber : subscribers_) {
    if (*subscriber->GetAddress() == *message->GetReceiver()) {
      return subscriber->OnMessageReceived(message);
    }
  }
  return false;
}

bool Distributor::AddSubscriber(IDistributorSubscriber * subscriberNew) {
  if (!subscriberNew) {
    logger_->Error("Subscriber is nullptr");
    return false;
  }
  if (!subscriberNew->SetDistributor(this)) {
    logger_->Error(std::string("Failed to set distributor: ")
    .append(subscriberNew->GetAddress()->ToString()));
    return false;
  }
  if (!subscriberNew->GetAddress()) {
    logger_->Error(std::string("Address not set distributor: ")
    .append(subscriberNew->GetAddress()->ToString()));
    return false;
  }
  for (auto &subscriber : subscribers_) {
    if (*subscriberNew->GetAddress() == *subscriber->GetAddress()) {
      logger_->Error(std::string("Subscriber already added: ")
      .append(subscriberNew->GetAddress()->ToString()));
      return false;
    }
  }
  subscribers_.push_back(subscriberNew);
  return true;
}

void Distributor::RemoveSubscriber(IDistributorSubscriber * subscriber) {
  if (!subscriber) {
    logger_->Error("Subscriber is nullptr");
    return;
  }
  // Remove from list
  auto removed = std::remove(subscribers_.begin(), subscribers_.end(), subscriber);
  // Truncate list
  subscribers_.erase(removed, subscribers_.end());
}

}  // namespace Distributor
