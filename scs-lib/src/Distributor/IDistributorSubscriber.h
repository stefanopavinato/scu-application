/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include "Messages/MessageVisitorBase.h"

namespace Messages::Addresses {
class AddressBase;
}  // namespace Messages::Addresses
namespace Distributor {
class Distributor;
class IDistributor;
class IDistributorSubscriberManager;
class IDistributorSubscriber :
  public Messages::MessageVisitorBase {
  friend class Distributor;
  friend class IDistributorSubscriberManager;
 public:
  explicit IDistributorSubscriber(
    std::unique_ptr<Messages::Addresses::AddressBase> address);

  ~IDistributorSubscriber() override = default;

  [[nodiscard]] bool Send(Messages::MessageBase * message) const;

  Messages::Addresses::AddressBase * GetAddress();

 private:
  [[nodiscard]] bool SetDistributor(IDistributor * distributor);

  IDistributor * distributor_;

  std::unique_ptr<Messages::Addresses::AddressBase> address_;
};
}  // namespace Distributor
