/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/Addresses/AddressBase.h"
#include "IDistributorSubscriber.h"

namespace Distributor {
class IDistributor;
class IDistributorSubscriberManager :
  public IDistributorSubscriber {
 public:
  explicit IDistributorSubscriberManager(
    std::unique_ptr<Messages::Addresses::AddressBase> address);

  ~IDistributorSubscriberManager() override = default;

  [[nodiscard]] bool AddSubscriber(
    IDistributorSubscriber * subscriber);

  void RemoveSubscriber(
    IDistributorSubscriber * subscriber);
};
}  // namespace Distributor
