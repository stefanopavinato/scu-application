/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/Addresses/AddressBase.h"
#include "IDistributor.h"
#include "IDistributorSubscriber.h"

namespace Distributor {

IDistributorSubscriber::IDistributorSubscriber(std::unique_ptr<Messages::Addresses::AddressBase> address)
  : distributor_(nullptr)
  , address_(std::move(address)) {
}

bool IDistributorSubscriber::Send(Messages::MessageBase * message) const {
  return distributor_->OnMessageReceived(message);
}

Messages::Addresses::AddressBase * IDistributorSubscriber::GetAddress() {
  return address_.get();
}

bool IDistributorSubscriber::SetDistributor(IDistributor * distributor) {
  if (distributor == nullptr) {
    return false;
  }
  distributor_ = distributor;
  return true;
}

}  // namespace Distributor
