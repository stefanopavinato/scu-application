/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Messages/MessageVisitorBase.h"

namespace Messages::Addresses {
class AddressBase;
}  // namespace Messages::Addresses
namespace Distributor {
class IDistributorSubscriber;
class IDistributor :
  public Messages::MessageVisitorBase {
 public:
  ~IDistributor() override = default;

  [[nodiscard]] virtual bool AddSubscriber(
    IDistributorSubscriber * subscriber) = 0;

  virtual void RemoveSubscriber(
    IDistributorSubscriber * subscriber) = 0;
};
}  // namespace Distributor
