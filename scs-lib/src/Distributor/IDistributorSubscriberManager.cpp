/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "IDistributor.h"
#include "IDistributorSubscriberManager.h"

namespace Distributor {

IDistributorSubscriberManager::IDistributorSubscriberManager(
  std::unique_ptr<Messages::Addresses::AddressBase> address)
  : IDistributorSubscriber(std::move(address)) {
}

bool IDistributorSubscriberManager::AddSubscriber(
  IDistributorSubscriber * subscriber) {
  return distributor_->AddSubscriber(subscriber);
}

void IDistributorSubscriberManager::RemoveSubscriber(
  IDistributorSubscriber * subscriber) {
  distributor_->RemoveSubscriber(subscriber);
}

}  // namespace Distributor
