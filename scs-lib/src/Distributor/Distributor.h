/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <list>
#include "IDistributor.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Distributor {
class Distributor :
  public IDistributor {
 public:
  explicit Distributor(Logger::ILogger * logger);

  ~Distributor() override = default;

  [[nodiscard]] bool OnMessageReceived(
    Messages::MessageBase * message) override;

  [[nodiscard]] bool AddSubscriber(
    IDistributorSubscriber * subscriber) override;

  void RemoveSubscriber(
    IDistributorSubscriber * subscriber) override;

 private:
  Logger::ILogger * logger_;

  std::list<IDistributorSubscriber *> subscribers_;
};
}  // namespace Distributor
