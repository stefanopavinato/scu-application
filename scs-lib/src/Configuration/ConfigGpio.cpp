/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigGpio.h"

namespace Configuration {

const char ConfigGpio::kAddress[] = "Address";
const char ConfigGpio::kName[] = "Name";
const char ConfigGpio::kIsInverted[] = "IsInverted";

ConfigGpio::ConfigGpio(
  const std::string &key,
  const uint32_t &address,
  const char *name,
  const bool &isInverted)
  : ConfigComponent(
  key), address_(address), name_(name), isInverted_(isInverted) {
}

uint32_t ConfigGpio::Address() const {
  return address_;
}

char * ConfigGpio::Name() const {
  return const_cast<char *>(name_);
}

bool ConfigGpio::IsInverted() const {
  return isInverted_;
}

Json::Value ConfigGpio::ToJson() const {
  auto json = Json::Value();
  json[kKey] = Key();
  json[kAddress] = address_;
  json[kName] = name_;
  json[kIsInverted] = isInverted_;
  return json;
}

}  // namespace Configuration
