/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ConfigComponent.h"

namespace Configuration {
template <typename T>
class ConfigKeyValue : public ConfigComponent {
 public:
  ConfigKeyValue(
    const std::string &key,
    T value);

  [[nodiscard]] T Value() const;

  [[nodiscard]] Json::Value ToJson() const override;

 private:
  const T value_;

  static const char kAddress[];
};
}  // namespace Configuration

#include "ConfigKeyValue.tpp"
