/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <unordered_map>
#include <string>
#include "ConfigCollection.h"

namespace Configuration {
namespace Builder {
class ConfigurationBuilder;

class ConfigurationBuilderBase;
}  // namespace Builder
class ConfigArray : public ConfigCollection {
  friend class Builder::ConfigurationBuilderBase;

 public:
  explicit ConfigArray(
    const std::string &key);

  ~ConfigArray() override = default;

  [[nodiscard]] Json::Value ToJson() const override;
};
}  // namespace Configuration
