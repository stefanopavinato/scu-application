/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <unordered_map>
#include <string>
#include "ConfigComponent.h"

namespace Configuration {
namespace Builder {
class ConfigurationBuilder;

class ConfigurationBuilderBase;
}  // namespace Builder
class ConfigCollection : public ConfigComponent {
  friend class Builder::ConfigurationBuilderBase;

 public:
  explicit ConfigCollection(
    const std::string &key);

  ~ConfigCollection() override = default;

  ConfigComponent const *GetComponent(
    const std::string &key) const;

  [[nodiscard]] Builder::ConfigurationBuilder Add();

  void AddComponent(std::unique_ptr<ConfigComponent> component);

 protected:
  std::unordered_map<std::string, std::unique_ptr<ConfigComponent>> components_;
};
}  // namespace Configuration
