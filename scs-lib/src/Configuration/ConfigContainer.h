/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <memory>
#include <unordered_map>
#include <string>
#include "ConfigCollection.h"

namespace Configuration {
namespace Builder {
class ConfigurationBuilder;

class ConfigurationBuilderBase;
}  // namespace Builder
class ConfigContainer : public ConfigCollection {
  friend class Builder::ConfigurationBuilderBase;

 public:
  explicit ConfigContainer(
    const std::string &key);

  ~ConfigContainer() override = default;

  [[nodiscard]] Json::Value ToJson() const override;

 protected:
  static const char kContainer[];
};
}  // namespace Configuration
