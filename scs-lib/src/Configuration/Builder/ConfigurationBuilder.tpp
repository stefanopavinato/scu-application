/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

namespace Configuration::Builder {

template <typename T>
ConfigKeyValue<T> * ConfigurationBuilder::KeyValue(
  const std::string &key,
  const T &value) {
  auto scuAddress = std::make_unique<ConfigKeyValue<T>>(
    key,
    value);
  auto scuAddressPtr = scuAddress.get();
  AddComponent(std::move(scuAddress));
  return scuAddressPtr;
}

}  // namespace Configuration::Builder
