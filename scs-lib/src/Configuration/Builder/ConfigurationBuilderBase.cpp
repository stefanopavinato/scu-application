/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigurationBuilderBase.h"

namespace Configuration::Builder {

ConfigurationBuilderBase::ConfigurationBuilderBase(
  ConfigCollection *collection)
  : collection_(collection) {
}

ConfigCollection *ConfigurationBuilderBase::GetCollection() {
  return collection_;
}

void ConfigurationBuilderBase::AddComponent(
  std::unique_ptr<ConfigComponent> component) {
  collection_->AddComponent(std::move(component));
}

}  // namespace Configuration::Builder
