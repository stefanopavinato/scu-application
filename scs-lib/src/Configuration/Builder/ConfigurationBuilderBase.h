/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Configuration/ConfigComponent.h"
#include "Configuration/ConfigContainer.h"

namespace Configuration {
namespace Builder {
class ConfigurationBuilderBase {
 public:
  explicit ConfigurationBuilderBase(
    ConfigCollection *collection);

 protected:
  ConfigCollection *GetCollection();

  void AddComponent(
    std::unique_ptr<ConfigComponent> component);

 private:
  ConfigCollection *collection_;
};
}  // namespace Builder
}  // namespace Configuration
