/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Builder/ConfigurationBuilder.h"
#include "ConfigArray.h"

namespace Configuration {

ConfigArray::ConfigArray(
  const std::string &key)
  : ConfigCollection(key) {
}

Json::Value ConfigArray::ToJson() const {
  auto json = Json::Value();
  int i = 0;
  for (const auto &component : components_) {
    auto index = component.second->ToJson();
    index["Name"] = component.second->Key();
    json[i++] = index;
  }
  return json;
}
}  // namespace Configuration
