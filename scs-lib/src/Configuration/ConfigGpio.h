/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "ConfigComponent.h"

namespace Configuration {
class ConfigGpio : public ConfigComponent {
 public:
  ConfigGpio(
    const std::string &key,
    const uint32_t &address,
    const char *name,
    const bool &isInverted);

  [[nodiscard]] uint32_t Address() const;

  [[nodiscard]] char *Name() const;

  [[nodiscard]] bool IsInverted() const;

  [[nodiscard]] Json::Value ToJson() const override;

 private:
  const uint32_t address_;

  const char *name_;

  const bool isInverted_;

  static const char kAddress[];
  static const char kName[];
  static const char kIsInverted[];
};
}  // namespace Configuration
