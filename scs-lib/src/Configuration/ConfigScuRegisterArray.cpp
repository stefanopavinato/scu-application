/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "ConfigScuRegisterArray.h"


namespace Configuration {

const char ConfigScuRegisterArray::kAddress[] = "Address";
const char ConfigScuRegisterArray::kSize[] = "Size";

ConfigScuRegisterArray::ConfigScuRegisterArray(
  const std::string &key,
  const uint64_t &address,
  const size_t &size)
  : ConfigComponent(
  key), address_(address), size_(size) {
}

uint64_t ConfigScuRegisterArray::Address() const {
  return address_;
}

size_t ConfigScuRegisterArray::Size() const {
  return size_;
}

Json::Value ConfigScuRegisterArray::ToJson() const {
  auto json = Json::Value();
  json[kKey] = Key();
  json[kAddress] = address_;
  json[kSize] = size_;
  return json;
}

}  // namespace Configuration
