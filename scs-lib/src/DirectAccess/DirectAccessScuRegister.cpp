/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include "Messages/MessageData.h"
#include "Messages/Addresses/AddressModule.h"
#include "Messages/Data/DataStatus.h"
#include "Messages/Data/ResponseFirmwareRegisterRead.h"
#include "Messages/Data/ResponseFirmwareRegisterWrite.h"
#include "SystemModules/Framework/Accessors/AccessorScuRegister.h"
#include "DirectAccessScuRegister.h"

namespace DirectAccess {

DirectAccessScuRegister::DirectAccessScuRegister(
  Logger::ILogger *logger)
  : Distributor::IDistributorSubscriber(
  std::make_unique<Messages::Addresses::AddressModule>(::Types::SoftwareModule::Enum::DIRECT_ACCESS))
  , logger_(logger) {
}

bool DirectAccessScuRegister::OnMessageReceived(Messages::MessageBase *message) {
  return message->Accept(this);
}

bool DirectAccessScuRegister::Visit(Messages::MessageData * message) {
  // Set message destination
  message->SwapAddresses();
  // Read firmware register
  if (auto response = Messages::Data::ResponseFirmwareRegisterRead::Create(message->GetData())) {
    // Read value
    auto value = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>::Read(
      logger_,
      response.value()->GetAddress(),
      0,
      32);
    if (!value) {
      // Handle internal error
      message->SetData(std::make_unique<Messages::Data::DataStatus>(
        Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE,
        "Failed to read scu register"));
    } else {
      // Set value
      response.value()->SetValue(value.value());
      message->SetData(std::move(response.value()));
    }
    return Send(message);
  }
  // Write firmware register
  if (auto response = Messages::Data::ResponseFirmwareRegisterWrite::Create(message->GetData())) {
    // Write value
    auto success = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>::Write(
      logger_,
      response.value()->GetAddress(),
      0,
      32,
      response.value()->GetValue());
    if (!success) {
      // Handle internal error
      message->SetData(std::make_unique<Messages::Data::DataStatus>(
        Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE,
        "Failed to write scu register"));
    } else {
      // Set message status
      message->SetData(std::move(response.value()));
    }
    return Send(message);
  }
  message->SetData(std::make_unique<Messages::Data::DataStatus>(
    Messages::Types::DataStatus::Enum::INFORMATION_NOT_AVAILABLE,
    "Direction not supported"));
  return Send(message);
}

}  // namespace DirectAccess
