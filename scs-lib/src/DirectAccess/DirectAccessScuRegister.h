/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include "Distributor/IDistributorSubscriber.h"

namespace Logger {
class ILogger;
}  // namespace Logger
namespace Messages {
class MessageBase;
}  // namespace Messages
namespace DirectAccess {
class DirectAccessScuRegister :
  public Distributor::IDistributorSubscriber {
 public:
  explicit DirectAccessScuRegister(
    Logger::ILogger *logger);

  bool OnMessageReceived(Messages::MessageBase *message) override;

  bool Visit(Messages::MessageData * message) override;

 private:
  Logger::ILogger * logger_;
};
}  // namespace DirectAccess
