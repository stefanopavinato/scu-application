
set(SCS_BINARY "scs-lib")

# collect sources
file(GLOB_RECURSE SRC_BINARY "src/*.c*")
file(GLOB_RECURSE HDR_BINARY "src/*.h*")

# prepare application target
add_library(${SCS_BINARY} ${SRC_BINARY} src/SystemModules/Configuration/Slots/PSU.cpp src/SystemModules/Configuration/Slots/PSU.h)

# include directories
target_include_directories(${SCS_BINARY}
        PUBLIC
        ${CMAKE_CURRENT_BINARY_DIR}
        src
        )

# link to dependencies
target_link_libraries(${SCS_BINARY}
        PRIVATE
        minini
        jsoncpp
        stdc++fs
        pthread
        gpiod
        )

# set compiler flags
target_compile_options(${SCS_BINARY} PRIVATE ${SCS_CFLAGS})

# test steps
setup_cpplint(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})
# setup_cppcheck(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})
# setup_cppclean(${SCS_BINARY} ${SRC_BINARY} ${HDR_BINARY})

# documentation
#setup_doxygen(${SCS_BINARY} "src" ${SRC_BINARY} ${HDR_BINARY})

# let cmake create the version symlinks for the library
set_target_properties(${SCS_BINARY} PROPERTIES
        VERSION ${SCS_VERSION}
        SOVERSION ${SCS_VERSION_MAJOR}
        )

# create and install the service
install(TARGETS ${SCS_BINARY}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        )
