/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <benchmark/benchmark.h>
#include <SystemModules/Framework/Accessors/AccessorScuRegister.h>

static void BM_ScuRegister_Read(benchmark::State& state) {
  auto accessor = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>(
    nullptr,
    1000,
    0,
    32);

  accessor.Initialize();
  uint32_t value;

  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_ScuRegister_Read);

static void BM_ScuRegister_Write(benchmark::State& state) {
  auto accessor = SystemModules::Framework::Accessors::AccessorScuRegister<uint32_t>(
    nullptr,
    1000,
    0,
    32);

  accessor.Initialize();

  for (auto _ : state) {
    if (!accessor.Write(0)) {
      std::cout << "Failed to write value" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_ScuRegister_Write);
