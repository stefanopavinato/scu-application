/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceLM75.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

#define ADDRESS 0x49
#define PATH "/sys/bus/i2c/devices/0-0070/channel-2"

static void BM_Device_LM75_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto device = SystemModules::Framework::Components::I2CDeviceLM75(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<uint8_t, uint8_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/temp1_input"),
    1,
    1);
  accessor.Initialize();

  uint8_t value;
  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_Device_LM75_Read);
