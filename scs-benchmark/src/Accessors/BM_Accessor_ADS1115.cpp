/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceADS1115.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

#define ADDRESS 0x4A
#define PATH "/sys/bus/i2c/devices/0-0070/channel-1"

static void BM_Device_ADS1115_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto device = SystemModules::Framework::Components::I2CDeviceADS1115(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/in_voltage1_raw"),
    1,
    1);
  accessor.Initialize();

  int32_t value;
  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_Device_ADS1115_Read);
