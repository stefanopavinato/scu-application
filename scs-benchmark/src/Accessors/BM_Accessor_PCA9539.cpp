/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <iostream>
#include <gpiod.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Accessors/AccessorGPIO.h>

static void BM_Accessor_PCA9539_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto accessor = SystemModules::Framework::Accessors::AccessorGPIO(
    &logger,
    500,
    Types::Direction::Enum::IN,
    true);

  accessor.Initialize();
  bool value;

  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_PCA9539_Read);

static void BM_Accessor_PCA9539_Write(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto accessor = SystemModules::Framework::Accessors::AccessorGPIO(
    &logger,
    500,
    Types::Direction::Enum::OUT,
    true);

  accessor.Initialize();

  for (auto _ : state) {
    if (!accessor.Write(true)) {
      std::cout << "Failed to write value" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_PCA9539_Write);

#ifndef	CONSUMER
#define	CONSUMER	"Consumer"
#endif

#ifndef CHIP
// #define CHIP "gpiochip7"
#define CHIP "gpiochip0"
#endif

#ifndef LINE
// #define LINE 0
#define LINE 2
#endif

static void BM_LibGpioD_PCA9539_Read(benchmark::State& state) {
  auto chipname = CHIP;
  unsigned int line_num = LINE;	// GPIO Pin #24
  int val;
  struct gpiod_chip *chip;
  struct gpiod_line *line;
  int i, ret;

  chip = gpiod_chip_open_by_name(chipname);
  if (!chip) {
    perror("Open chip failed\n");
    return;
  }

  line = gpiod_chip_get_line(chip, line_num);
  if (!line) {
    gpiod_chip_close(chip);
    perror("Get line failed\n");
    return;
  }

  ret = gpiod_line_request_input(line, CONSUMER);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line);
    gpiod_chip_close(chip);
    return;
  }

  for (auto _ : state) {
    val = gpiod_line_get_value(line);
    if (val < 0) {
      perror("Read line input failed\n");
    }
  }
  gpiod_line_release(line);
  gpiod_chip_close(chip);
}

// Register the function as benchmark
// BENCHMARK(BM_LibGpioD_PCA9539_Read);

static void BM_LibGpioD_PCA9539_Write(benchmark::State& state) {
  auto chipname = CHIP;
  unsigned int line_num = LINE;	// GPIO Pin #24
  int val;
  struct gpiod_chip *chip;
  struct gpiod_line *line;
  int i, ret;

  chip = gpiod_chip_open_by_name(chipname);
  if (!chip) {
    perror("Open chip failed\n");
    return;
  }

  line = gpiod_chip_get_line(chip, line_num);
  if (!line) {
    gpiod_chip_close(chip);
    perror("Get line failed\n");
    return;
  }

  ret = gpiod_line_request_output(line, CONSUMER, 0);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line);
    gpiod_chip_close(chip);
    return;
  }

  for (auto _ : state) {
    val = gpiod_line_set_value(line, 1);
    if (val < 0) {
      perror("Read line input failed\n");
    }
  }
  gpiod_line_release(line);
  gpiod_chip_close(chip);
}

// Register the function as benchmark
// BENCHMARK(BM_LibGpioD_PCA9539_Write);
