
#include <iostream>
#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

static const char path[] = "/home/root/bm_accessor_file";

static void CreateFile(const std::string &path) {
  std::fstream fs;
  fs.open(path, std::ios::out);
  fs.write("100", 3);
  fs.close();
}

static void BM_Accessor_File_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  CreateFile(path);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    path,
    1,
    1,
    0);

  accessor.Initialize();
  int32_t value;

  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read file" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_File_Read);

static void BM_Accessor_File_Write(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  CreateFile(path);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    path,
    1,
    1,
    0);

  accessor.Initialize();

  for (auto _ : state) {
    if (!accessor.Write(10)) {
      std::cout << "Failed to write file" << std::endl;
      return;
    }
  }
  accessor.DeInitialize();
}
// Register the function as benchmark
// BENCHMARK(BM_Accessor_File_Write);
