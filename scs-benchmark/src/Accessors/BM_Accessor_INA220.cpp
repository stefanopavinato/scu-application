/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceINA220.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>

#define I2C_BUS 7
#define ADDRESS 0x40
#define PATH "/sys/bus/i2c/devices/i2c-7"
#define DEVICE_PATH "/sys/bus/i2c/devices/"

static void BM_Device_INA220_Read(benchmark::State& state) {
  auto logger = Logger::LoggerDummy();

  auto device = SystemModules::Framework::Components::I2CDeviceINA220(
    &logger,
    "name",
    "description",
    I2C_BUS,
    ADDRESS,
    PATH,
    DEVICE_PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/in0_input"),
    1,
    1);
  accessor.Initialize();

  int32_t value;
  for (auto _ : state) {
    if (!accessor.Read(&value)) {
      std::cout << "Failed to read value" << std::endl;
      return;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
}
// Register the function as benchmark
BENCHMARK(BM_Device_INA220_Read);
