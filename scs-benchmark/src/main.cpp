/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <benchmark/benchmark.h>
// Results:
//
// Time: The average wall time per iteration.
//
// CPU: The average CPU time per iteration.
// By default this clock is used when determining
// the amount of iterations to run. When you sleep
// your process it is no longer accumulating CPU time.

// Run the benchmark tests
BENCHMARK_MAIN();
