/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <csignal>
#include <memory>
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <arpa/inet.h>
#include <Logger/LoggerDebug.h>
#include <Logger/LoggerSyslog.h>
#include <Logger/LoggerHandler.h>
#include "Communication/StreamSplitter/StreamSplitterBase.h"
#include "Communication/Protocols/ProtocolBase.h"
#include "Communication/Clients/Client.h"
#include <Communication/Clients/ClientListener.h>
#include <Communication/Clients/ClientDescriptor.h>
#include <Communication/Clients/ClientEstablisher.h>
#include <Communication/Clients/ClientHandler.h>
#include <ResourceSharing/Lock.h>
#include <ResourceSharing/LockHandler.h>
#include <WorkloadBalancing/ItemHandler.h>
#include <WorkloadBalancing/WorkloadBalanceHandler.h>
#include <SystemModules/Framework/Accessors/AccessorGPIO.h>
#include <SystemModules/Framework/Accessors/AccessorScuRegister.h>
#include <SystemModules/Framework/Components/ModuleHandler.h>
#include <SystemModules/Configuration/Common/ConfigurationCommonSerializer.h>
#include <SystemModules/Configuration/Slots/Slots.h>
#include <SystemModules/Configuration/Slots/SlotSCU.h>
#include <SystemModules/Configuration/Slots/ConfigurationSerializerNetwork.h>
#include <SystemModules/Configuration/Slots/ConfigurationSerializerNetworkInterface.h>
#include <SystemModules/Configuration/Slots/ConfigurationSerializerSlot.h>
#include <SystemModules/Modules/MezzanineCards/States/MCStateBase.h>
#include <SystemModules/Modules/MezzanineCards/ModuleMezzanineCard.h>
#include <SystemModules/Modules/InternalModules/States/IMStateBase.h>
#include <SystemModules/Modules/InternalModules/ModuleInternal.h>
#include <Distributor/Distributor.h>
#include <DirectAccess/DirectAccessScuRegister.h>
#include <Utils/ThreadHandler.h>
#include "version.h"
#include "Utils/System.h"
#include <Utils/Filter.h>


#define UPDATE_INTERVAL_us 20000
#define UPDATE_INTERVAL_EXCLUSIVE_ms 50
#define UPDATE_INTERVAL_SHARED_ms 1000
#define TIMEOUT_s 5
#define MEMCHECK false

#define DEFAULT_INI_FILE_PATH "/etc/default/scs.d/ini-file"
#define CONFIGURATION_FILE_PATH "/home/root/configuration.json"

static const struct option long_options[] = {
  {"help",    no_argument,       nullptr, 'h'},
  {"path",    required_argument, nullptr, 'p'},
  {"version", no_argument,       nullptr, 1}
};

static volatile sig_atomic_t signalStatus = 0;

void signalHandler(int32_t signal) {
  std::cout << signal << std::endl;
  signalStatus = signal;
}

int main(int argc, char *argv[]) {
  std::string iniFilePath = DEFAULT_INI_FILE_PATH;
  // Parse arguments
  int index;
  int result;
  while ((result = getopt_long(argc, argv, "hp:", long_options, &index)) != -1) {
    if (result == -1) {
      break;
    }
    switch (result) {
      // List version
      case 1: {
        std::cout << "Revision:   " << REVISION << std::endl;
        std::cout << "Branch:     " << BRANCH << std::endl;
        std::cout << "Build date: " << BUILD_DATE << std::endl;
        std::cout << "Build time: " << BUILD_TIME << std::endl;
#ifdef VERSION_IS_DIRTY
        std::cout << "Version is dirty!" << std::endl;
#endif
        return EXIT_SUCCESS;
      }
        // Path
      case 'p': {
        iniFilePath = std::string(optarg);
        break;
      }
        // Help
      case 'h':
      default: {
        std::cout << "Usage: scs-service [options]..." << std::endl;
        std::cout << "The scs-service monitors the inputs and the outputs of the device." << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << "     --version    displays the installed version" << std::endl;
        std::cout << std::endl;
      }
        return EXIT_SUCCESS;
    }
  }


//   std::signal(SIGINT, signalHandler);
//   std::signal(SIGTERM, signalHandler);

//   while (signalStatus == 0) {
//     std::cout << signalStatus << std::endl;
//     std::this_thread::sleep_for(std::chrono::seconds(1));
//   }

  // Create and setup logger
  auto loggerHandler = Logger::LoggerHandler();
  // Add debug logger
  if (!loggerHandler.AddLogger(std::make_unique<Logger::LoggerDebug>(
    Types::AlertSeverity::Enum::INFORMATIONAL))) {
    loggerHandler.Error("Failed to add debug logger to logger handler");
    return EXIT_FAILURE;
  }
  // Add syslog logger
  if (!loggerHandler.AddLogger(std::make_unique<Logger::LoggerSyslog>(
    "scs",
    Types::AlertSeverity::Enum::INFORMATIONAL))) {
    loggerHandler.Error("Failed to add syslog logger to logger handler");
    return EXIT_FAILURE;
  }
  // Reset scu registers
  SystemModules::Framework::Accessors::AccessorScuRegister<bool>::ResetHW(
    &loggerHandler);
  // Parse configuration
  loggerHandler.Info("Parse configuration");
  auto configuration = SystemModules::Configuration::Configuration(
    &loggerHandler,
    iniFilePath);
  // Save configuration
  auto outFile = std::ofstream();
  outFile.open(CONFIGURATION_FILE_PATH);
  outFile << configuration.ToJson().toStyledString();
  outFile.close();
  // Get serializer specific configuration
  std::string ip;
  std::string ipSite;
  if (configuration.Common()->Serializer()->Slot()->Value() == "A") {
    ip = configuration.Slots()->SCU()->Scu(1)->Network()->Interface0()->IpAddress()->Value();
    ipSite = configuration.Slots()->SCU()->Scu(2)->Network()->Interface0()->IpAddress()->Value();
  } else {
    ip = configuration.Slots()->SCU()->Scu(2)->Network()->Interface0()->IpAddress()->Value();
    ipSite = configuration.Slots()->SCU()->Scu(1)->Network()->Interface0()->IpAddress()->Value();
  }
  // Setup ini file parser
  loggerHandler.Info("Setup ini file parser");
  auto systemSetup = Utils::System::System(
    &loggerHandler,
    iniFilePath);

#ifdef SETUP_NETWORK
  // Setup network
  loggerHandler.Info("Setup network");
  if (!systemSetup.SetupNetwork()) {
    loggerHandler.Error("Failed to setup network");
    return EXIT_FAILURE;
  }
#endif

  // Create Distributor
  auto distributor = Distributor::Distributor(&loggerHandler);

  // Create client handler
  auto clientHandler = Communication::Clients::ClientHandler(&loggerHandler);
  if (!distributor.AddSubscriber(&clientHandler)) { return EXIT_FAILURE; }

  // Create FBIS connection listener
  loggerHandler.Info("Add client listeners");
  uint16_t fbisPort;
  if (!systemSetup.GetFbisPort(&fbisPort)) {
    loggerHandler.Error("Failed to parse fbis port from config file");
    return EXIT_FAILURE;
  }
  auto fbisListener = std::make_unique<Communication::Clients::ClientListener>(
    &loggerHandler,
    Communication::Types::ProtocolType::Enum::FBIS_NETWORK,
    fbisPort,
    std::chrono::seconds(TIMEOUT_s),
    5);
  if (!distributor.AddSubscriber(fbisListener.get())) { return EXIT_FAILURE; }
  if (!clientHandler.AddListener(fbisListener.get())) {
    loggerHandler.Error("Failed to add fbis connection listener to client handler");
    return EXIT_FAILURE;
  }

  // Create JSON connection listener
  uint16_t jsonPort;
  if (!systemSetup.GetJsonPort(&jsonPort)) {
    loggerHandler.Error("Failed to parse json port from config file");
    return EXIT_FAILURE;
  }
  auto jsonListener = std::make_unique<Communication::Clients::ClientListener>(
    &loggerHandler,
    Communication::Types::ProtocolType::Enum::JSON,
    jsonPort,
    std::chrono::seconds(TIMEOUT_s),
    5);
  if (!distributor.AddSubscriber(jsonListener.get())) { return EXIT_FAILURE; }
  if (!clientHandler.AddListener(jsonListener.get())) {
    loggerHandler.Error("Failed to add generic terminal connection listener to client handler");
    return EXIT_FAILURE;
  }

  // Create arbitration connection listener
  uint16_t arbitrationPort;
  if (!systemSetup.GetArbitrationPort(&arbitrationPort)) {
    loggerHandler.Error("Failed to parse fbis port from config file");
    return EXIT_FAILURE;
  }
  auto arbitrationListener = std::make_unique<Communication::Clients::ClientListener>(
    &loggerHandler,
    Communication::Types::ProtocolType::Enum::ARBITRATION,
    arbitrationPort,
    std::chrono::seconds(TIMEOUT_s),
    100);
  if (!distributor.AddSubscriber(arbitrationListener.get())) { return EXIT_FAILURE; }
  if (!clientHandler.AddListener(arbitrationListener.get())) {
    loggerHandler.Error("Failed to add arbitration connection listener to client handler");
    return EXIT_FAILURE;
  }

  // Create arbitration connection establisher
  auto clientEstablisher = std::make_unique<Communication::Clients::ClientEstablisher>(
    &loggerHandler);
  if (inet_addr(ip.c_str()) < inet_addr(ipSite.c_str())) {
    if (!clientEstablisher->AddDescriptor(std::make_unique<Communication::Clients::ClientDescriptor>(
      ipSite,
      arbitrationPort,
      std::chrono::seconds(TIMEOUT_s),
      Communication::Types::ProtocolType::Enum::ARBITRATION))) {
      loggerHandler.Error("Failed to add client descriptor");
      return EXIT_FAILURE;
    }
  }
  if (!distributor.AddSubscriber(clientEstablisher.get())) { return EXIT_FAILURE; }
  if (!clientHandler.AddEstablisher(clientEstablisher.get())) {
    loggerHandler.Error("Failed to add client establisher to client handler");
    return EXIT_FAILURE;
  }

  // Create direct access
  auto directAccessScuRegister = DirectAccess::DirectAccessScuRegister(
    &loggerHandler);
  if (!distributor.AddSubscriber(&directAccessScuRegister)) { return EXIT_FAILURE; }

  // Create workload balance handler
  auto workloadBalanceHandler = std::make_unique<WorkloadBalancing::WorkloadBalanceHandler>(
    &loggerHandler);
  if (!distributor.AddSubscriber(workloadBalanceHandler.get())) { return EXIT_FAILURE; }

  // Create lock handler
  auto lockHandler = ResourceSharing::LockHandler<uint32_t, uint32_t>(
    &loggerHandler,
    inet_addr(ip.c_str()));
  if (!distributor.AddSubscriber(&lockHandler)) { return EXIT_FAILURE; }

  // Create locks
  auto lockExclusive = std::make_unique<ResourceSharing::Lock<uint32_t, uint32_t>>(
    &loggerHandler,
    Types::LockId::Enum::EXCLUSIVE);
  if (!workloadBalanceHandler->AddUpdateHandler(
    lockExclusive.get(),
    std::chrono::milliseconds(UPDATE_INTERVAL_EXCLUSIVE_ms))) {
    return EXIT_FAILURE;
  }
  if (!lockHandler.AddLock(std::move(lockExclusive))) { return EXIT_FAILURE; }

  auto lockShared = std::make_unique<ResourceSharing::Lock<uint32_t, uint32_t>>(
    &loggerHandler,
    Types::LockId::Enum::SHARED);
  lockShared->AddSite(ipSite);
  if (!workloadBalanceHandler->AddUpdateHandler(
    lockShared.get(),
    std::chrono::milliseconds(UPDATE_INTERVAL_SHARED_ms))) {
    return EXIT_FAILURE;
  }
  if (!lockHandler.AddLock(std::move(lockShared))) { return EXIT_FAILURE; }

  auto lockModuleHandler = std::make_unique<ResourceSharing::Lock<uint32_t, uint32_t>>(
    &loggerHandler,
    Types::LockId::Enum::MODULE_HANDLER);
  lockModuleHandler->AddSite(ipSite);

  // Create module handler
  auto moduleHandler = std::make_unique<SystemModules::Framework::Components::ModuleHandler>(
    &loggerHandler,
    "Scu",
    lockModuleHandler.get(),
    std::chrono::microseconds(UPDATE_INTERVAL_us));
  if (!lockHandler.AddLock(std::move(lockModuleHandler))) { return EXIT_FAILURE; }
  if (!distributor.AddSubscriber(moduleHandler.get())) { return EXIT_FAILURE; }

  // Add group alert severities
  moduleHandler->AddGroupAlertSeverity(Types::ValueGroup::Enum::SUPPLY);
  moduleHandler->AddGroupAlertSeverity(Types::ValueGroup::Enum::SPEED);
  moduleHandler->AddGroupAlertSeverity(Types::ValueGroup::Enum::TEMPERATURE);
  moduleHandler->AddGroupAlertSeverity(Types::ValueGroup::Enum::NONE);
  // Create serializer
  auto serializer = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
    &loggerHandler,
    &configuration,
    "Serializer",
    workloadBalanceHandler.get(),
    Types::ModuleType::Enum::SCU_SER);
  if (!moduleHandler->AddModule(std::move(serializer))) {
    loggerHandler.Error("Failed to add serializer to module handler");
    return EXIT_FAILURE;
  }
  // Create and add power units
  auto powerSupply = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
      &loggerHandler,
      &configuration,
      "PowerSupplyUnits",
      workloadBalanceHandler.get(),
      Types::ModuleType::Enum::SCU_PWSUP);
  if (!moduleHandler->AddModule(std::move(powerSupply))) {
    loggerHandler.Error("Failed to add power supply unit to module handler");
    return EXIT_FAILURE;
  }
// Create and add fan unit
  auto fanUnit = std::make_unique<SystemModules::Framework::Components::ModuleInternal>(
    &loggerHandler,
    &configuration,
    "FanUnit",
    workloadBalanceHandler.get(),
    Types::ModuleType::Enum::SCU_FU);
  if (!moduleHandler->AddModule(std::move(fanUnit))) {
    loggerHandler.Error("Failed to add fan unit to module handler");
    return EXIT_FAILURE;
  }
  // Create and add mezzanine cards
  loggerHandler.Info("Create and add mezzanine cards");
  for (uint32_t i = 0; i < 12; ++i) {
    auto mezzanineCard = std::make_unique<SystemModules::Framework::Components::ModuleMezzanineCard>(
      &loggerHandler,
      &configuration,
      i,
      workloadBalanceHandler.get());
    if (!moduleHandler->AddModule(std::move(mezzanineCard))) {
      loggerHandler.Error("Failed to add mezzanine card to module handler");
      return EXIT_FAILURE;
    }
  }

  // Initialize front handle switch
  auto frontHandleOpenItem = SystemModules::Framework::Accessors::AccessorGPIO(
    &loggerHandler,
    232,
    "FP_SW_OPEN",
    Types::Direction::Enum::IN,
    false);
  frontHandleOpenItem.Initialize();
  bool frontHandleOpen = false;
  // Initialize front handle led
  auto frontHandleLed = SystemModules::Framework::Accessors::AccessorGPIO(
      &loggerHandler,
      555,
      "BLUE_LED",
      Types::Direction::Enum::OUT,
      false);
  frontHandleLed.Initialize();
  // ToDo: Remove all gpio numbers
  // Create Thread handler
  auto threadHandler = Utils::ThreadHandler(&loggerHandler);
  if (!threadHandler.AddThread(std::move(fbisListener))) { return EXIT_FAILURE; }
  if (!threadHandler.AddThread(std::move(jsonListener))) { return EXIT_FAILURE; }
  if (!threadHandler.AddThread(std::move(arbitrationListener))) { return EXIT_FAILURE; }
  if (!threadHandler.AddThread(std::move(clientEstablisher))) { return EXIT_FAILURE; }
  // Make sure, that the locks are synchronized before starting i2c access
  std::this_thread::sleep_for(std::chrono::seconds(5));
  if (!threadHandler.AddThread(std::move(workloadBalanceHandler))) { return EXIT_FAILURE; }
  if (!threadHandler.AddThread(std::move(moduleHandler))) { return EXIT_FAILURE; }

  // ToDo: make sure, all components are removed when deleted (see clientHandler, distributor)
  if (MEMCHECK) {
    std::this_thread::sleep_for(std::chrono::seconds(20));
  } else {
    auto run = true;
    while (run) {
      if (frontHandleOpenItem.Read(&frontHandleOpen) && frontHandleOpen) {
        loggerHandler.Warning("Shutting down: Front handle open");
        frontHandleLed.Write(true);
        run = false;
      }
      if (!threadHandler.AllThreadsRunning()) {
        loggerHandler.Warning("Shutting down: Thread failure");
        run = false;
      }
      std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
  }
  // Stop all threads
  if (!threadHandler.Stop()) { return EXIT_FAILURE; }
  loggerHandler.Info("scu-application terminated");
  return EXIT_SUCCESS;
}
