/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include "Messages/Data/ResponseFirmwareRegisterRead.h"
#include "Messages/Data/ResponseFirmwareRegisterWrite.h"
#include "Messages/MessageData.h"
#include "Communication/Protocols/FbisNetwork/FbisMessageFactory.h"

namespace Communication::Protocols::FbisNetwork {

TEST(FbisMessageFactory, Serialize_Message_ResponseFirmwareRegisterRead) {
  uint32_t address = 0xDEAD;
  uint32_t value = 0xBEAF;
  // Prepare message
  auto message = Messages::MessageData();
  auto data = std::make_unique<Messages::Data::ResponseFirmwareRegisterRead>(address);
  data->SetValue(value);
  message.SetData(std::move(data));
  // Serialize message
  auto factory = FbisMessageFactory();
  ASSERT_EQ(true, factory.OnMessageReceived(&message));
  auto vector = factory.GetData();
  // Check vector size (validation of vector content is checked in message)
  EXPECT_EQ(9, vector.get()->size());
}

TEST(FbisMessageFactory, Serialize_Message_ResponseFirmwareRegisterWrite) {
  uint32_t address = 0xDEAD;
  uint32_t value = 0xBEAF;
  // Prepare message
  auto message = Messages::MessageData();
  auto data = std::make_unique<Messages::Data::ResponseFirmwareRegisterWrite>(address, value);
  message.SetData(std::move(data));
  // Serialize message
  auto factory = FbisMessageFactory();
  ASSERT_EQ(true, factory.OnMessageReceived(&message));
  auto vector = factory.GetData();
  // Check vector size (validation of vector content is checked in message)
  EXPECT_EQ(5, vector->size());
}
}  // namespace Communication::Protocols::FbisNetwork
