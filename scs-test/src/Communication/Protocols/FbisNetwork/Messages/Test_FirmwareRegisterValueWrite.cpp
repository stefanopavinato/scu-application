/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include "Messages/Message.h"
#include "Messages/MessageData.h"
#include "Messages/MessageVisitor.h"
#include "Messages/Data/DataVisitor.h"
#include "Messages/Data/RequestFirmwareRegisterWrite.h"
#include "Messages/Data/ResponseFirmwareRegisterWrite.h"
#include "Communication/Protocols/FbisNetwork/Messages/FirmwareRegisterValueWrite.h"

namespace Communication::Protocols::FbisNetwork {

TEST(FirmwareRegisterValueWrite, Deserialize_OK) {
  auto vector = std::make_unique<std::vector<char>>();
  // Prepare Id
  uint8_t id = 1;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), id));
  // Prepare data length
  uint16_t dataLength = 8;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), dataLength));
  // Prepare address
  uint32_t address = 0x0000DEAD;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), address));
  // Prepare value
  uint32_t value = 0x0000BEEF;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), value));
  // Prepare crc
  uint16_t crc = 0x0000;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), crc));
  // Deserialize
  auto message = FirmwareRegisterValueWrite::Deserialize(std::move(vector));
  // check for nullptr
  ASSERT_NE(std::nullopt, message);
  // check for message type
  auto visitor = Messages::MessageVisitor<Messages::MessageData>();
  ASSERT_EQ(true, message.value()->Accept(&visitor));
  // validate message data
  auto data = Messages::Data::DataVisitor<Messages::Data::RequestFirmwareRegisterWrite>();
  ASSERT_EQ(true, visitor.GetMessage()->GetData()->Accept(&data));
  EXPECT_EQ(address, data.GetData()->GetAddress());
  EXPECT_EQ(value, data.GetData()->GetValue());
}

TEST(FirmwareRegisterValueWrite, Deserialize_INVALID_REQUEST_FORMAT) {
  auto vector = std::make_unique<std::vector<char>>();
  // Prepare Id
  uint8_t id = 1;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), id));
  // Prepare data length
  uint16_t dataLength = 8;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), dataLength));
  // Prepare address
  uint32_t address = 0x0000DEAD;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), address));
  // Prepare value
  uint32_t value = 0x0000BEEF;
  ASSERT_EQ(true, FbisMessageBase::Serialize(vector.get(), value));
  // Deserialize
  auto message = FirmwareRegisterValueWrite::Deserialize(std::move(vector));
  // check for nullptr
  ASSERT_NE(std::nullopt, message);
  // check for message type
  auto visitor = Messages::MessageVisitor<Messages::Message>();
  ASSERT_EQ(true, message.value()->Accept(&visitor));
  // validate message data
  EXPECT_EQ(Types::RequestId::Enum::FIRMWARE_REGISTER_VALUE_WRITE, visitor.GetMessage()->GetRequestId().GetValue());
  EXPECT_EQ(Messages::Types::DataStatus::Enum::INVALID_REQUEST_FORMAT, visitor.GetMessage()->DataStatus()->GetValue());
}

TEST(FirmwareRegisterValueWrite, Serialize_OK) {
  uint32_t address = 0x0000DEAD;
  uint32_t value = 0x0000BEEF;
  auto message = Messages::Data::ResponseFirmwareRegisterWrite(address, value);
  // Serialize
  auto vector = FirmwareRegisterValueWrite::Serialize(&message);
  // validate message data
  // Check vector length
  EXPECT_EQ(5, vector.get()->size());
  // Parse request id
  uint8_t id;
  ASSERT_EQ(true, FbisMessageBase::DeSerializer(*vector.get(), 0, &id));
  EXPECT_EQ(1, id);
  // Parse message status
  uint8_t messageStatus;
  ASSERT_EQ(true, FbisMessageBase::DeSerializer(*vector.get(), 1, &messageStatus));
  EXPECT_EQ(Messages::Types::MessageStatus::Enum::OK, static_cast<Messages::Types::MessageStatus::Enum>(messageStatus));
  // Parse length
  uint16_t length;
  ASSERT_EQ(true, FbisMessageBase::DeSerializer(*vector.get(), 2, &length));
  EXPECT_EQ(1, length);
  // Parse data status
  uint8_t dataStatus;
  ASSERT_EQ(true, FbisMessageBase::DeSerializer(*vector.get(), 4, &dataStatus));
  EXPECT_EQ(Messages::Types::DataStatus::Enum::OK, static_cast<Messages::Types::DataStatus::Enum>(dataStatus));
}

}  // namespace Communication::Protocols::FbisNetwork
