/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include "Messages/MessageBase.h"
#include "Communication/Protocols/FbisNetwork/Messages/FbisMessageBase.h"

namespace Communication::Protocols::FbisNetwork {

TEST(Test_FbisMessageBase, Serialize_uint8_t) {
  auto data = std::vector<char>();
  uint8_t value = 0xAC;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(1, data.size());
  EXPECT_EQ(0xAC, data[0]);
}

TEST(Test_FbisMessageBase, Serialize_uint16_t) {
  auto data = std::vector<char>();
  uint16_t value = 0xBEEF;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(2, data.size());
  EXPECT_EQ(0xBE, data[0]);
  EXPECT_EQ(0xEF, data[1]);
}

TEST(Test_FbisMessageBase, Serialize_uint32_t) {
  auto data = std::vector<char>();
  uint32_t value = 0xDEADBEEF;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(4, data.size());
  EXPECT_EQ(0xDE, data[0]);
  EXPECT_EQ(0xAD, data[1]);
  EXPECT_EQ(0xBE, data[2]);
  EXPECT_EQ(0xEF, data[3]);
}

TEST(Test_FbisMessageBase, Serialize_int8_t) {
  auto data = std::vector<char>();
  int8_t value = 0xAC;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(1, data.size());
  EXPECT_EQ(0xAC, data[0]);
}

TEST(Test_FbisMessageBase, Serialize_int16_t) {
  auto data = std::vector<char>();
  int16_t value = 0xBEEF;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(2, data.size());
  EXPECT_EQ(0xBE, data[0]);
  EXPECT_EQ(0xEF, data[1]);
}

TEST(Test_FbisMessageBase, Serialize_int32_t) {
  auto data = std::vector<char>();
  int32_t value = 0xDEADBEEF;
  FbisMessageBase::Serialize(&data, value);
  EXPECT_EQ(4, data.size());
  EXPECT_EQ(0xDE, data[0]);
  EXPECT_EQ(0xAD, data[1]);
  EXPECT_EQ(0xBE, data[2]);
  EXPECT_EQ(0xEF, data[3]);
}

}  // namespace Communication::Protocols::FbisNetwork
