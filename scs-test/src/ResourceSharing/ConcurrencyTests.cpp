/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <map>

namespace ResourceSharing {

TEST(ConcurrencyTest, asdf) {
  // Create list
  auto a = std::map<int, int>();
  a.insert(std::make_pair(3, 3));
  a.insert(std::make_pair(2, 2));
  a.insert(std::make_pair(1, 1));
  a.insert(std::make_pair(4, 4));
  a.insert(std::make_pair(5, 5));
  a.insert(std::make_pair(6, 6));

  for (const auto& [key, value] : a) {
    std::cout << key << " = " << value << "; ";
  }
  std::cout << std::endl;
}

}  // namespace ResourceSharing
