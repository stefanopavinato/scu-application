/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <SystemModules/Framework/MonitoringFunctions/MFIsValueHigher.h>

/*
class MonitoringFunctionTest:
    public SystemModules::Framework::MonitoringFunctions::MFIsValueHigher<Types::Value<uint32_t>>,
    public testing::Test {
 protected:
  MonitoringFunctionTest()
      : SystemModules::Framework::MonitoringFunctions::MFIsValueHigher<Types::Value<uint32_t>>(
      Types::AlertSeverity::Enum::WARNING,
      0,
      Types::ValueGroup::Enum::SPEED,
      std::chrono::milliseconds(0),
      std::chrono::milliseconds(0),
      Types::Value<uint32_t>(
          100,
          Types::Unit::Enum::NONE,
          Types::UnitPrefix::Enum::NONE)) {
  }
};

TEST_F(MonitoringFunctionTest, CompareValue) {
  auto compareValue = Types::Value<uint32_t>(
      100,
      Types::Unit::Enum::NONE,
      Types::UnitPrefix::Enum::NONE);
  compareValue.SetValue(101);
  EXPECT_EQ(true, CompareValue(compareValue));
  compareValue.SetValue(100);
  EXPECT_EQ(false, CompareValue(compareValue));
  compareValue.SetValue(99);
  EXPECT_EQ(false, CompareValue(compareValue));
}

TEST_F(MonitoringFunctionTest, OnDelay) {
}

TEST_F(MonitoringFunctionTest, OffDelay) {
}
*/

