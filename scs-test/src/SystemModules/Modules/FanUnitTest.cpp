/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include <Logger/ILogger.h>
#include <SystemModules/Framework/MonitoringFunctions/MFTemplate.h>
#include <SystemModules/Framework/Components/ItemIn.h>
#include <SystemModules/Framework/Components/ItemOut.h>
#include <SystemModules/Framework/Components/Container.h>
#include <SystemModules/Framework/Components/I2CDevice24C02.h>
#include <SystemModules/Framework/Accessors/AccessorEEProm.h>
#include <SystemModules/Modules/InternalModules/States/IMStateBase.h>
#include <SystemModules/Framework/Components/ModuleHandler.h>
#include <SystemModules/Framework/Components/ModuleBase.h>
#include <SystemModules/Framework/Actions/ActionFanSpeed.h>

#include <SystemModules/Modules/InternalModules/FanUnit/FanUnit_Fan.h>
#include <SystemModules/Modules/InternalModules/FanUnit/FanUnit.h>

class MockLogger : public Logger::ILogger {
 public:
  MOCK_CONST_METHOD5(LogFunc, void(
    const std::string&,
    const std::string&,
    const int&,
    const Types::AlertSeverity::Enum&,
    const std::string&));
};
/*
class MockFanUnit:
    public SystemModules::Modules::FanUnit::FanUnit {
 public:
  MockFanUnit(
      Logger::ILogger * logger)
      : SystemModules::Modules::FanUnit::FanUnit(logger) {
  }

  bool ProcessInputs(
      const Types::AlertSeverity& parentAlertSeverity,
      const std::chrono::steady_clock::time_point& timePointNow) override {
    return FanUnit::ProcessInputs(parentAlertSeverity, timePointNow);
  }

  bool ProcessOutputs(
      const std::chrono::steady_clock::time_point& timePointNow) override {
    return FanUnit::ProcessOutputs(timePointNow);
  }
};
*/

/*
class FanUnitTest :
    public testing::Test {
 protected:
  FanUnitTest() {
    auto logger = MockLogger();
    fanUnit = std::make_unique<MockFanUnit>(&logger);
  }
  std::unique_ptr<MockFanUnit> fanUnit;
};

TEST_F(FanUnitTest, DISABLED_ThreeFansEnabledAndOK) {
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::NONE);
  auto speedON = Types::Value<double>();
  speedON.SetValue(3000);
  std::chrono::steady_clock::time_point timePoint;
  // Setup fans
  ASSERT_EQ(true, fanUnit->FanA()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanM()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanM()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanB()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanB()->Speed()->ForceValue(speedON));
  // Process monitoring function
  ASSERT_EQ(true, fanUnit->ProcessInputs(parentAlertSeverity, timePoint));
  ASSERT_EQ(true, fanUnit->ProcessOutputs(timePoint));
  // Read alert speed severity
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanA()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanB()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanM()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  // Read action results
  EXPECT_EQ(200, *fanUnit->FanA()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanM()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanB()->PWM()->GetValue());
}

TEST_F(FanUnitTest, DISABLED_TwoFansEnabledAndOK) {
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::NONE);
  auto speedON = Types::Value<double>();
  speedON.SetValue(3000);
  auto speedOFF = Types::Value<double>();
  speedOFF.SetValue(3000);
  std::chrono::steady_clock::time_point timePoint;
  // Setup fans
  ASSERT_EQ(true, fanUnit->FanA()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanM()->Enable()->ForceValue(0));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  ASSERT_EQ(true, fanUnit->FanB()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  // Process monitoring function
  ASSERT_EQ(true, fanUnit->ProcessInputs(parentAlertSeverity, timePoint));
  ASSERT_EQ(true, fanUnit->ProcessOutputs(timePoint));
  // Read alert speed severity
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanA()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanM()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanB()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  // Read action results
  EXPECT_EQ(200, *fanUnit->FanA()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanM()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanB()->PWM()->GetValue());
}

TEST_F(FanUnitTest, DISABLED_OneFanEnabledAndOK) {
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::NONE);
  auto speedON = Types::Value<double>();
  speedON.SetValue(3000);
  auto speedOFF = Types::Value<double>();
  speedOFF.SetValue(3000);
  std::chrono::steady_clock::time_point timePoint;
  // Setup fans
  ASSERT_EQ(true, fanUnit->FanA()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanM()->Enable()->ForceValue(0));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  ASSERT_EQ(true, fanUnit->FanB()->Enable()->ForceValue(0));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  // Process monitoring function
  ASSERT_EQ(true, fanUnit->ProcessInputs(parentAlertSeverity, timePoint));
  ASSERT_EQ(true, fanUnit->ProcessOutputs(timePoint));
  // Read alert speed severity
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanA()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanM()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanB()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  // Read action results
  EXPECT_EQ(200, *fanUnit->FanA()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanM()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanB()->PWM()->GetValue());
}

TEST_F(FanUnitTest, DISABLED_OneFanNOK) {
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::NONE);
  auto speedON = Types::Value<double>();
  speedON.SetValue(3000);
  auto speedOFF = Types::Value<double>();
  speedOFF.SetValue(3000);
  std::chrono::steady_clock::time_point timePoint;
  // Setup fans
  ASSERT_EQ(true, fanUnit->FanA()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanM()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanB()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  // Process monitoring function
  ASSERT_EQ(true, fanUnit->ProcessInputs(parentAlertSeverity, timePoint));
  ASSERT_EQ(true, fanUnit->ProcessOutputs(timePoint));
  // Read alert speed severity
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanA()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanM()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanB()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  // Read action results
  EXPECT_EQ(110, *fanUnit->FanA()->PWM()->GetValue());
  EXPECT_EQ(110, *fanUnit->FanM()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanB()->PWM()->GetValue());
}

TEST_F(FanUnitTest, DISABLED_TwoFansNOK) {
  auto parentAlertSeverity = Types::AlertSeverity(Types::AlertSeverity::Enum::NONE);
  auto speedON = Types::Value<double>();
  speedON.SetValue(3000);
  auto speedOFF = Types::Value<double>();
  speedOFF.SetValue(3000);
  std::chrono::steady_clock::time_point timePoint;
  // Setup fans
  ASSERT_EQ(true, fanUnit->FanA()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedON));
  ASSERT_EQ(true, fanUnit->FanM()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  ASSERT_EQ(true, fanUnit->FanB()->Enable()->ForceValue(1));
  ASSERT_EQ(true, fanUnit->FanA()->Speed()->ForceValue(speedOFF));
  // Process monitoring function
  ASSERT_EQ(true, fanUnit->ProcessInputs(parentAlertSeverity, timePoint));
  ASSERT_EQ(true, fanUnit->ProcessOutputs(timePoint));
  // Read alert speed severity
  EXPECT_EQ(Types::AlertSeverity::Enum::NONE, fanUnit->FanA()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanM()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  EXPECT_EQ(Types::AlertSeverity::Enum::ERROR, fanUnit->FanB()->Speed()->ComponentStatus()->AlertSeverity().GetEnum());
  // Read action results
  EXPECT_EQ(110, *fanUnit->FanA()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanM()->PWM()->GetValue());
  EXPECT_EQ(200, *fanUnit->FanB()->PWM()->GetValue());
}
*/
