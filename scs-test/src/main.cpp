/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>

int main(int argc, char *argv[]) {
  (void) argc;
  (void) argv;
  ::testing::InitGoogleTest(&argc, argv);

  // run tests
  return RUN_ALL_TESTS();
}

