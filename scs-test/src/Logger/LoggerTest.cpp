/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Logger/LoggerHandler.h>

class DISABLED_LoggerTest :
  public testing::Test {
 protected:
  DISABLED_LoggerTest() {
  }
};

TEST_F(DISABLED_LoggerTest, asdf) {
  // Test
  /*
  loggerHandler.Debug("debug");
  loggerHandler.Info("info");
  loggerHandler.Warning("warning");
  loggerHandler.Error("error");
  */
}
