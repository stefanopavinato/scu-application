/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Types/Value.h>
#include <Types/ValueConverter.h>

class ValueConverterTest :
  public testing::Test {
 protected:
};


TEST_F(ValueConverterTest, DISABLED_Conversion) {
  // auto a = Types::ValueConverter::Convert<double, int>(13.3);
  // EXPECT_EQ(13, a);
  auto b = Types::ValueConverter::Convert<int, Types::Value<int>>(13);
  EXPECT_EQ(13, b.GetValue());
  // String to enum (device-type)
}
