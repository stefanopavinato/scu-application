/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Types/Value.h>
#include <Types/ModuleType.h>

class ModuleTypeTest :
  public testing::Test {
 protected:
};


TEST_F(ModuleTypeTest, StringToModuleType) {
  Types::ModuleType a;
  a.FromString("FBIS-SCU-PWSUP");
  EXPECT_EQ(a.ToString(), "FBIS-SCU-PWSUP");
}

TEST_F(ModuleTypeTest, ModuleTypeToString) {
  std::string a;
  auto b = Types::ModuleType(Types::ModuleType::Enum::SCU_FU);
  a = b.ToString();
}
