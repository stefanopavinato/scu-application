/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <gtest/gtest.h>
#include <Types/Value.h>

class ValueTest :
  public testing::Test {
 protected:
};

TEST_F(ValueTest, Set) {
  auto a = Types::Value<int32_t>(
    100,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::MILLI);
  auto b = a;
  EXPECT_EQ(a.GetUnit(), b.GetUnit());
  EXPECT_EQ(a.GetPrefix(), b.GetPrefix());
  EXPECT_EQ(a.GetValue(), b.GetValue());
  EXPECT_EQ(a, b);
}

TEST_F(ValueTest, GetValue) {
  auto a = Types::Value<int32_t>(
    1000,
    Types::Unit::Enum::VOLTAGE,
    Types::UnitPrefix::Enum::NONE);
  EXPECT_EQ(1000000000, a.GetValue(Types::UnitPrefix::Enum::MICRO));
  EXPECT_EQ(1000000, a.GetValue(Types::UnitPrefix::Enum::MILLI));
  EXPECT_EQ(1000, a.GetValue(Types::UnitPrefix::Enum::NONE));
  EXPECT_EQ(1, a.GetValue(Types::UnitPrefix::Enum::KILO));
  EXPECT_EQ(0, a.GetValue(Types::UnitPrefix::Enum::MEGA));
}

TEST_F(ValueTest, Conversion) {
}

TEST_F(ValueTest, Copy) {
}

TEST_F(ValueTest, SetValue) {
}
