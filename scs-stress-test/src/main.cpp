/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include "Accessors/STRESS_Accessor_ADS1115.h"
#include "Accessors/STRESS_Accessor_AMC6821.h"
#include "Accessors/STRESS_Accessor_LM75.h"
#include "Accessors/STRESS_Accessor_24C02.h"

int main(int argc, char *argv[]) {
  auto duration = std::chrono::minutes(2);
//  STRESS_Accessor_ADS1115::Read(duration);
//  STRESS_Accessor_AMC6821::Read(duration);
//  STRESS_Accessor_LM75::Read(duration);
  STRESS_Accessor_24C02::Read(duration);
//  STRESS_Accessor_24C02::Write(duration);
//  STRESS_Accessor_24C02::ReadFile(duration);
//  STRESS_Accessor_24C02::WriteP(duration);
}
