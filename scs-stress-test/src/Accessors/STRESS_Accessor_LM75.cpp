/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceLM75.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>
#include "STRESS_Accessor_LM75.h"

#define ADDRESS 0x49
#define PATH "/sys/bus/i2c/devices/0-0070/channel-2"

void STRESS_Accessor_LM75::Read(const std::chrono::seconds &seconds) {
  std::cout << "Stress LM75: Start" << std::endl;
  auto logger = Logger::LoggerDummy();
  auto device = SystemModules::Framework::Components::I2CDeviceLM75(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/temp1_input"),
    1,
    1);
  accessor.Initialize();

  int32_t value;
  int32_t iterations = 0;
  int32_t failures = 0;
  auto stop = std::chrono::steady_clock::now() + seconds;
  while (std::chrono::steady_clock::now() < stop) {
    iterations++;
    if (!accessor.Read(&value)) {
      failures++;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
  std::cout << "Stress LM75: Stop" << std::endl;
  std::cout << "Failures: " << failures << std::endl;
  std::cout << "Iterations: " << iterations << std::endl;
}
