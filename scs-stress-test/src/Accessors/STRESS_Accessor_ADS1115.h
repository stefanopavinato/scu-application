/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>

class STRESS_Accessor_ADS1115 {
 public:
  static void Read(const std::chrono::seconds &seconds);
};
