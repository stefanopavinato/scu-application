/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#pragma once

#include <chrono>

class STRESS_Accessor_24C02 {
 public:
  static void Read(const std::chrono::seconds &seconds);

  static void Write(const std::chrono::seconds &seconds);

  static void ReadFile(const std::chrono::seconds &seconds);

  static void WriteP(const std::chrono::seconds &seconds);
};
