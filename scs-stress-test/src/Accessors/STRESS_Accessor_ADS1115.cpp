/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <Logger/LoggerDummy.h>
#include <SystemModules/Framework/Components/I2CDeviceADS1115.h>
#include <SystemModules/Framework/Accessors/AccessorFile.h>
#include "STRESS_Accessor_ADS1115.h"

#define ADDRESS 0x4B
#define PATH "/sys/bus/i2c/devices/0-0070/channel-1"

void STRESS_Accessor_ADS1115::Read(const std::chrono::seconds &seconds) {
  std::cout << "Stress ADS1115: Start" << std::endl;
  auto logger = Logger::LoggerDummy();
  auto device = SystemModules::Framework::Components::I2CDeviceADS1115(
    &logger,
    "name",
    "description",
    ADDRESS,
    PATH);

  auto accessor = SystemModules::Framework::Accessors::AccessorFile<int32_t, int32_t, std::string>(
    &logger,
    std::string(device.GetDevicePath()).append("/in_voltage1_raw"),
    1,
    1);
  accessor.Initialize();

  int32_t value;
  int32_t iterations = 0;
  int32_t failures = 0;
  auto stop = std::chrono::steady_clock::now() + seconds;
  while (std::chrono::steady_clock::now() < stop) {
    iterations++;
    if (!accessor.Read(&value)) {
      failures++;
    }
  }

  (void) accessor.DeInitialize();
  (void) device.DeInitialize();
  std::cout << "Stress ADS1115: Stop" << std::endl;
  std::cout << "Failures: " << failures << std::endl;
  std::cout << "Iterations: " << iterations << std::endl;
}
