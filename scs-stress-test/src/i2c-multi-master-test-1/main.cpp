/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <chrono>
#include <gpiod.h>
#include <Utils/TimeFormatter.h>

#ifndef CONSUMER
#define CONSUMER "I2C-MULTI-MASTER-TEST-1"
#endif

#ifndef CHIP
#define CHIP "gpiochip8"
// #define CHIP "gpiochip0"
#endif

#ifndef LINE
#define LINE 6
// #define LINE 2
#endif

int main3(int argc, char *argv[]) {
  auto chipname = CHIP;
  unsigned int line_num = LINE;  // GPIO Pin #24
  int val;
  struct gpiod_chip *chip;
  struct gpiod_line *line;
  int i, ret;

  chip = gpiod_chip_open_by_name(chipname);
  if (!chip) {
    perror("Open chip failed\n");
    return EXIT_FAILURE;
  }

  line = gpiod_chip_get_line(chip, line_num);
  if (!line) {
    gpiod_chip_close(chip);
    perror("Get line failed\n");
    return EXIT_FAILURE;
  }

  ret = gpiod_line_request_output(line, CONSUMER, 0);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line);
    gpiod_chip_close(chip);
    return EXIT_FAILURE;
  }

  for (;;) {
    val = gpiod_line_set_value(line, 0);
    if (val < 0) {
      perror("Read line input failed\n");
    }
    // Get date and time
    auto now = std::chrono::system_clock::now();
    auto timeString = Utils::TimeFormatter::ToString(now);
    std::cout << timeString << ": " << val << std::endl;
  }
  gpiod_line_release(line);
  gpiod_chip_close(chip);

  std::cout << "hello world" << std::endl;
  while (true) {
  }
}
