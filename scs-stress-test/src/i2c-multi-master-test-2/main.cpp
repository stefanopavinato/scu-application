/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <iostream>
#include <chrono>
#include <gpiod.h>
#include <Utils/TimeFormatter.h>

#ifndef CONSUMER
#define CONSUMER "I2C-MULTI-MASTER-TEST-2"
#endif

#ifndef CHIP1
#define CHIP1 "gpiochip8"
#endif

#ifndef LINE1
#define LINE1 6
#endif

#ifndef CHIP2
#define CHIP2 "gpiochip2"
#endif

#ifndef LINE2
#define LINE2 3
#endif

#ifndef CHIP3
#define CHIP3 "gpiochip1"
#endif

#ifndef LINE3
#define LINE3 3
#endif


int main1(int argc, char *argv[]) {
  int val;
  struct gpiod_chip *chip1, *chip2, *chip3;
  struct gpiod_line *line1, *line2, *line3;
  int i, ret;
  // Setup gpio 1
  chip1 = gpiod_chip_open_by_name(CHIP1);
  if (!chip1) {
    perror("Open chip failed\n");
    return EXIT_FAILURE;
  }

  line1 = gpiod_chip_get_line(chip1, LINE1);
  if (!line1) {
    gpiod_chip_close(chip1);
    perror("Get line failed\n");
    return EXIT_FAILURE;
  }

  ret = gpiod_line_request_output(line1, CONSUMER, 0);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line1);
    gpiod_chip_close(chip1);
    return EXIT_FAILURE;
  }

  // Setup gpio 2
  chip2 = gpiod_chip_open_by_name(CHIP2);
  if (!chip2) {
    perror("Open chip failed\n");
    return EXIT_FAILURE;
  }

  line2 = gpiod_chip_get_line(chip2, LINE2);
  if (!line2) {
    gpiod_chip_close(chip2);
    perror("Get line failed\n");
    return EXIT_FAILURE;
  }

  ret = gpiod_line_request_output(line2, CONSUMER, 0);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line2);
    gpiod_chip_close(chip2);
    return EXIT_FAILURE;
  }

  // Setup gpio 3
  chip3 = gpiod_chip_open_by_name(CHIP3);
  if (!chip3) {
    perror("Open chip failed\n");
    return EXIT_FAILURE;
  }

  line3 = gpiod_chip_get_line(chip3, LINE3);
  if (!line3) {
    gpiod_chip_close(chip3);
    perror("Get line failed\n");
    return EXIT_FAILURE;
  }

  ret = gpiod_line_request_output(line3, CONSUMER, 0);
  if (ret < 0) {
    perror("Request line as input failed\n");
    gpiod_line_release(line3);
    gpiod_chip_close(chip3);
    return EXIT_FAILURE;
  }

  bool val2 = false;
  for (;;) {
    val2 = !val2;
    val = gpiod_line_set_value(line1, val2);
    if (val < 0) {
      perror("Read line input failed\n");
    }
    val = gpiod_line_set_value(line2, val2);
    if (val < 0) {
      perror("Read line input failed\n");
    }
    val = gpiod_line_set_value(line3, val2);
    if (val < 0) {
      perror("Read line input failed\n");
    }
    // Get date and time
    auto now = std::chrono::system_clock::now();
    auto timeString = Utils::TimeFormatter::ToString(now);
    std::cout << timeString << ": " << val << std::endl;
  }
  gpiod_line_release(line1);
  gpiod_chip_close(chip1);
  gpiod_line_release(line2);
  gpiod_chip_close(chip2);
}
