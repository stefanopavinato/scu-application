/// ************************************************************
/// \brief
/// \author ZHAW-IAMP-SKS, Philippe Hindermann
/// \copyright
/// ************************************************************

#include <cstring>
#include <iostream>
#include <arpa/inet.h>
#include <getopt.h>
#include <sstream>

#include <Logger/LoggerDummy.h>
#include <Types/SoftwareModule.h>
#include <Types/Direction.h>
#include <Messages/MessageData.h>
#include <Messages/Addresses/AddressModule.h>
#include <Messages/Data/RequestOverview.h>
#include <Messages/Data/RequestPropertySet.h>
#include <Messages/Data/RequestFirmwareRegisterRead.h>
#include <Messages/Data/RequestFirmwareRegisterWrite.h>
#include <Communication/Protocols/Json/Types/JsonMessageItemType.h>
#include <Communication/Protocols/ProtocolJson.h>
#include <Communication/StreamSplitter/StreamSplitterTerminal.h>
#include "version.h"

#define IP "127.0.0.1"
#define PORT 20002

static const struct option long_options[] = {
  {"help",      no_argument,       nullptr, 'h'},
  {"request",   required_argument, nullptr, 'r'},
  {"module",    required_argument, nullptr, 'm'},
  {"path",      required_argument, nullptr, 'p'},
  {"attribute", required_argument, nullptr, 'a'},
  {"type",      required_argument, nullptr, 't'},
  {"value",     required_argument, nullptr, 'v'},
  {"level",     required_argument, nullptr, 'l'},
  {"detail",    required_argument, nullptr, 'd'},
  {"address",   required_argument, nullptr, 0},
  {"list",      required_argument, nullptr, 1},
  {"version",   no_argument,       nullptr, 2}
};

int main(int argc, char *argv[]) {
  auto request = Communication::Protocols::Json::Types::JsonMessageItemType(
    Communication::Protocols::Json::Types::JsonMessageItemType::Enum::GET_REQUEST);
  auto module = Types::SoftwareModule(Types::SoftwareModule::Enum::MESSAGE_HANDLER);
  std::string path = "/";

  int32_t level = 0;
  auto detail = ::Types::JsonDetails(::Types::JsonDetails::Enum::COMPACT);

  auto attribute = Messages::Visitor::Types::AttributeType();
  auto type = Types::ValueType();
  std::string valueString;

  uint32_t address = 0;

  // Parse arguments
  int index;
  int result;
  while ((result = getopt_long(argc, argv, "hr:m:p:a:t:v:l:d:", long_options, &index)) != -1) {
    if (result == -1) {
      break;
    }
    switch (result) {
      // Request
      case 'r': {
        request.FromString(std::string(optarg));
        if (request.GetValue() == Communication::Protocols::Json::Types::JsonMessageItemType::Enum::NONE) {
          std::cerr << "Error: visitor type not valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // Module
      case 'm': {
        try {
          auto moduleVal = std::stoi(optarg);
          module.SetEnum(static_cast<Types::SoftwareModule::Enum>(moduleVal));
        } catch (const std::exception &e) {
          module.FromString(std::string(optarg));
          if (module.GetEnum() == Types::SoftwareModule::Enum::NONE) {
            std::cerr << "Error: module type not valid." << std::endl;
            return EXIT_FAILURE;
          }
        }
        break;
      }
      // Path
      case 'p': {
        path = std::string(optarg);
        break;
      }
      // Attribute
      case 'a': {
        attribute.FromString(std::string(optarg));
        if (attribute.GetValue() == Messages::Visitor::Types::AttributeType::Enum::NONE) {
          std::cerr << "Error: attribute not valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // Value type
      case 't': {
        type.FromString(std::string(optarg));
        if (type.GetValue() == Types::ValueType::Enum::NONE) {
          std::cerr << "Error: value type not valid." << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // Value
      case 'v': {
        valueString = std::string(optarg);
        break;
      }
      // Level
      case 'l': {
        try {
          level = std::stoi(optarg);
        } catch (const std::exception &e) {
          std::cerr << "Error: depth value not valid" << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // Detail
      case 'd': {
        try {
          detail.FromValue(std::stoi(optarg));
        } catch (const std::exception &e) {
          std::cerr << "Error: depth value not valid" << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // Address
      case 0: {
        try {
          std::stringstream ss;
          ss << std::hex << optarg;
          ss >> address;
        } catch (const std::exception &e) {
          std::cerr << "Error: address value not valid" << std::endl;
          return EXIT_FAILURE;
        }
        break;
      }
      // List parameters
      case 1: {
        if (std::string(optarg) == "request") {
          std::cout << "Requests:" << std::endl;
          std::cout << "  - " << Communication::Protocols::Json::Types::JsonMessageItemType::kSetRequest;
          std::cout << " (usage: scs-terminal <-r Set><-p path><-a attribute>[-t type][-v value])" << std::endl;
          std::cout << "  - " << Communication::Protocols::Json::Types::JsonMessageItemType::kGetRequest;
          std::cout << " (usage: scs-terminal <-r Get><-p path>[-l level][-d detail])" << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "attributes") {
          std::cout << "Attributes:" << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kComponentType << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kName << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kDescription << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kAlertSeverity << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kClass << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kUpdateInterval << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleReset << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleStop << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kModuleStopRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueIn << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInRead << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForceOnce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInIsForced << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueInForceRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOut << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutWrite << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutIsForced << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForceOnce << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kValueOutForceRelease << std::endl;
          std::cout << "  - " << Messages::Visitor::Types::AttributeType::kStatusReset << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "type") {
          std::cout << "Types:" << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueUInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueDouble << std::endl;
          std::cout << "  - " << ::Types::ValueType::kValueFloat << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt32 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt16 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kUInt8 << std::endl;
          std::cout << "  - " << ::Types::ValueType::kDouble << std::endl;
          std::cout << "  - " << ::Types::ValueType::kFloat << std::endl;
          std::cout << "  - " << ::Types::ValueType::kBool << std::endl;
          std::cout << "  - " << ::Types::ValueType::kString << std::endl;
          std::cout << "  - " << ::Types::ValueType::kAlertSeverity << std::endl;
          std::cout << "  - " << ::Types::ValueType::kModuleType << std::endl;
          std::cout << "  - " << ::Types::ValueType::kChronoMinutes << std::endl;
          std::cout << "  - " << ::Types::ValueType::kChronoMilliseconds << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "module") {
          std::cout << "Module:" << std::endl;
          std::cout << " 0 = " << ::Types::SoftwareModule::kMessageHandler << std::endl;
          std::cout << " 1 = " << ::Types::SoftwareModule::kDirectAccess << std::endl;
          std::cout << " 2 = " << ::Types::SoftwareModule::kClientHandler << std::endl;
          std::cout << " 3 = " << ::Types::SoftwareModule::kLockHandler << std::endl;
        }
        if (std::string(optarg) == "detail") {
          std::cout << "Detail:" << std::endl;
          std::cout << "  0 = " << ::Types::JsonDetails::kVerboseName << std::endl;
          std::cout << "  1 = " << ::Types::JsonDetails::kCompactName << std::endl;
          std::cout << "  2 = " << ::Types::JsonDetails::kMinimalName << std::endl;
          std::cout << "  3 = " << ::Types::JsonDetails::kConfigName << std::endl;
          std::cout << std::endl;
        }
        if (std::string(optarg) == "level") {
          std::cout << "Level:" << std::endl;
          std::cout << "  - Number of collected component levels." << std::endl;
        }
        return EXIT_SUCCESS;
      }
        // List version
      case 2: {
        std::cout << "Revision:   " << REVISION << std::endl;
        std::cout << "Branch:     " << BRANCH << std::endl;
        std::cout << "Build date: " << BUILD_DATE << std::endl;
        std::cout << "Build time: " << BUILD_TIME << std::endl;
#ifdef VERSION_IS_DIRTY
        std::cout << "Version is dirty!" << std::endl;
#endif
        return EXIT_SUCCESS;
      }
        // Help
      case 'h':
      default: {
        std::cout << "Usage: scs-terminal [options]..." << std::endl;
        std::cout << "Access scs-service on local machine." << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << " -r, --request <request>                               set request type" << std::endl;
        std::cout << " -m, --module <module>                                 set module address" << std::endl;
        std::cout << " -p, --path <path>                                     set component path" << std::endl;
        std::cout << " -l, --level <level>                                   set maximum tree level" << std::endl;
        std::cout << " -d, --detail <detail>                                 set level of detail" << std::endl;
        std::cout << " -a, --attribute <attribute>                           set attribute of the component" << std::endl;  // NOLINT
        std::cout << " -t, --type <type>                                     set type of value" << std::endl;
        std::cout << " -v, --value <value>                                   set value" << std::endl;
        std::cout << " -h, --help                                            display this help and exit" << std::endl;
        std::cout << "     --list <request|attribute|types|level|module>     display available parameter" << std::endl;
        std::cout << "     --version                                         displays the installed version" << std::endl;  // NOLINT
        std::cout << std::endl;
      }
        return EXIT_SUCCESS;
    }
  }
  // Setup message
  auto message = Messages::MessageData();
  message.SetSender(std::make_unique<Messages::Addresses::AddressModule>(module.GetEnum()));
  switch (module.GetEnum()) {
    case Types::SoftwareModule::Enum::DIRECT_ACCESS: {
      switch (request.GetValue()) {
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::SET_REQUEST: {
          int32_t value = 0;
          try {
            value = std::stoi(valueString);
          } catch (const std::exception &e) {
            std::cerr << "Error: value not valid" << std::endl;
            return EXIT_FAILURE;
          }
          message.SetData(std::make_unique<Messages::Data::RequestFirmwareRegisterWrite>(
            address,
            value));
          break;
        }
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::GET_REQUEST: {
          message.SetData(std::make_unique<Messages::Data::RequestFirmwareRegisterRead>(
            address));
          break;
        }
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::NONE: {
          std::cerr << "Error: request not set." << std::endl;
          return EXIT_FAILURE;
        }
      }
      break;
    }
    default: {
      switch (request.GetValue()) {
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::SET_REQUEST: {
          auto value = ::Types::VariantValueUtils::FromString(type, valueString);
          if (!value) {
            std::cerr << "Error: failed to convert value." << std::endl;
            return EXIT_FAILURE;
          }
          message.SetData(std::make_unique<Messages::Data::RequestPropertySet>(
            path,
            attribute,
            value.value()));
          break;
        }
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::GET_REQUEST: {
          message.SetData(std::make_unique<Messages::Data::RequestOverview>(
            path,
            level,
            detail));
          break;
        }
        case Communication::Protocols::Json::Types::JsonMessageItemType::Enum::NONE: {
          std::cerr << "Error: request not set." << std::endl;
          return EXIT_FAILURE;
        }
      }
    }
  }

  // Create logger
  auto logger = Logger::LoggerDummy();
  // Create protocol instance
  auto protocol = Communication::Protocols::ProtocolJson(&logger);
  // Serialize message
  auto stream = protocol.Serialize(&message);
  // Create stream splitter
  auto streamSplitter = Communication::StreamSplitter::StreamSplitterTerminal();
  // Pack data
  auto outBuffer = streamSplitter.Pack(std::move(stream.value()));
  // Create socket
  auto sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Fill in server ip address
  struct sockaddr_in server{};
  server.sin_family = AF_INET;
  server.sin_port = htons(PORT);
  // Convert IP-address to binary form
  if (inet_pton(AF_INET, IP, &server.sin_addr) <= 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Connect
  if (connect(sock, (struct sockaddr *) &server, sizeof(server)) < 0) {
    std::cerr << "Error: " << std::strerror(errno) << std::endl;
    return EXIT_FAILURE;
  }
  // Send data
  {
    auto nBytes = send(
      sock,
      outBuffer.value()->data(),
      outBuffer.value()->size(),
      0);
    if (nBytes < 0) {
      std::cerr << "Error: " << std::strerror(errno) << std::endl;
      return EXIT_FAILURE;
    }
  }
  // Receive data
  bool inBufferComplete = false;
  while (!inBufferComplete) {
    auto inBuffer = std::make_unique<std::vector<char>>();
    inBuffer->resize(1024);
    {
      auto nBytes = recv(
        sock,
        inBuffer->data(),
        inBuffer->size(),
        0);
      if (nBytes < 0) {
        std::cerr << "Error: " << std::strerror(errno) << std::endl;
        return EXIT_FAILURE;
      }
    }
    // Unpack data
    auto inData = streamSplitter.Unpack(std::move(inBuffer));
    if (inData) {
      inBufferComplete = true;
      // Print result
      std::cout << inData.value()->data() << std::endl;
      return EXIT_SUCCESS;
    }
  }

  return EXIT_FAILURE;
}
